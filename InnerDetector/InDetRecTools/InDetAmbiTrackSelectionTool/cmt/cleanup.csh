# echo "cleanup InDetAmbiTrackSelectionTool InDetAmbiTrackSelectionTool-00-01-37 in /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.7.5/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtInDetAmbiTrackSelectionTooltempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtInDetAmbiTrackSelectionTooltempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=InDetAmbiTrackSelectionTool -version=InDetAmbiTrackSelectionTool-00-01-37 -path=/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools  $* >${cmtInDetAmbiTrackSelectionTooltempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -csh -pack=InDetAmbiTrackSelectionTool -version=InDetAmbiTrackSelectionTool-00-01-37 -path=/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools  $* >${cmtInDetAmbiTrackSelectionTooltempfile}"
  set cmtcleanupstatus=2
  /bin/rm -f ${cmtInDetAmbiTrackSelectionTooltempfile}
  unset cmtInDetAmbiTrackSelectionTooltempfile
  exit $cmtcleanupstatus
endif
set cmtcleanupstatus=0
source ${cmtInDetAmbiTrackSelectionTooltempfile}
if ( $status != 0 ) then
  set cmtcleanupstatus=2
endif
/bin/rm -f ${cmtInDetAmbiTrackSelectionTooltempfile}
unset cmtInDetAmbiTrackSelectionTooltempfile
exit $cmtcleanupstatus

