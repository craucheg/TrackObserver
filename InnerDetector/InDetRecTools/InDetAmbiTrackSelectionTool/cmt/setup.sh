# echo "setup InDetAmbiTrackSelectionTool InDetAmbiTrackSelectionTool-00-01-37 in /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.7.5/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtInDetAmbiTrackSelectionTooltempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtInDetAmbiTrackSelectionTooltempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=InDetAmbiTrackSelectionTool -version=InDetAmbiTrackSelectionTool-00-01-37 -path=/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools  -no_cleanup $* >${cmtInDetAmbiTrackSelectionTooltempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=InDetAmbiTrackSelectionTool -version=InDetAmbiTrackSelectionTool-00-01-37 -path=/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools  -no_cleanup $* >${cmtInDetAmbiTrackSelectionTooltempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmtInDetAmbiTrackSelectionTooltempfile}
  unset cmtInDetAmbiTrackSelectionTooltempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmtInDetAmbiTrackSelectionTooltempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmtInDetAmbiTrackSelectionTooltempfile}
unset cmtInDetAmbiTrackSelectionTooltempfile
return $cmtsetupstatus

