#-- start of make_header -----------------

#====================================
#  Library InDetAmbiTrackSelectionTool
#
#   Generated Thu Apr 14 11:56:12 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetAmbiTrackSelectionTool_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetAmbiTrackSelectionTool_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetAmbiTrackSelectionTool

InDetAmbiTrackSelectionTool_tag = $(tag)

#cmt_local_tagfile_InDetAmbiTrackSelectionTool = $(InDetAmbiTrackSelectionTool_tag)_InDetAmbiTrackSelectionTool.make
cmt_local_tagfile_InDetAmbiTrackSelectionTool = $(bin)$(InDetAmbiTrackSelectionTool_tag)_InDetAmbiTrackSelectionTool.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetAmbiTrackSelectionTool_tag = $(tag)

#cmt_local_tagfile_InDetAmbiTrackSelectionTool = $(InDetAmbiTrackSelectionTool_tag).make
cmt_local_tagfile_InDetAmbiTrackSelectionTool = $(bin)$(InDetAmbiTrackSelectionTool_tag).make

endif

include $(cmt_local_tagfile_InDetAmbiTrackSelectionTool)
#-include $(cmt_local_tagfile_InDetAmbiTrackSelectionTool)

ifdef cmt_InDetAmbiTrackSelectionTool_has_target_tag

cmt_final_setup_InDetAmbiTrackSelectionTool = $(bin)setup_InDetAmbiTrackSelectionTool.make
cmt_dependencies_in_InDetAmbiTrackSelectionTool = $(bin)dependencies_InDetAmbiTrackSelectionTool.in
#cmt_final_setup_InDetAmbiTrackSelectionTool = $(bin)InDetAmbiTrackSelectionTool_InDetAmbiTrackSelectionToolsetup.make
cmt_local_InDetAmbiTrackSelectionTool_makefile = $(bin)InDetAmbiTrackSelectionTool.make

else

cmt_final_setup_InDetAmbiTrackSelectionTool = $(bin)setup.make
cmt_dependencies_in_InDetAmbiTrackSelectionTool = $(bin)dependencies.in
#cmt_final_setup_InDetAmbiTrackSelectionTool = $(bin)InDetAmbiTrackSelectionToolsetup.make
cmt_local_InDetAmbiTrackSelectionTool_makefile = $(bin)InDetAmbiTrackSelectionTool.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetAmbiTrackSelectionToolsetup.make

#InDetAmbiTrackSelectionTool :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetAmbiTrackSelectionTool'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetAmbiTrackSelectionTool/
#InDetAmbiTrackSelectionTool::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

InDetAmbiTrackSelectionToollibname   = $(bin)$(library_prefix)InDetAmbiTrackSelectionTool$(library_suffix)
InDetAmbiTrackSelectionToollib       = $(InDetAmbiTrackSelectionToollibname).a
InDetAmbiTrackSelectionToolstamp     = $(bin)InDetAmbiTrackSelectionTool.stamp
InDetAmbiTrackSelectionToolshstamp   = $(bin)InDetAmbiTrackSelectionTool.shstamp

InDetAmbiTrackSelectionTool :: dirs  InDetAmbiTrackSelectionToolLIB
	$(echo) "InDetAmbiTrackSelectionTool ok"

#-- end of libary_header ----------------
#-- start of library_no_static ------

#InDetAmbiTrackSelectionToolLIB :: $(InDetAmbiTrackSelectionToollib) $(InDetAmbiTrackSelectionToolshstamp)
InDetAmbiTrackSelectionToolLIB :: $(InDetAmbiTrackSelectionToolshstamp)
	$(echo) "InDetAmbiTrackSelectionTool : library ok"

$(InDetAmbiTrackSelectionToollib) :: $(bin)InDetAmbiTrackSelectionTool.o $(bin)InDetDenseEnvAmbiTrackSelectionTool.o $(bin)InDetAmbiTrackSelectionTool_entries.o $(bin)InDetAmbiTrackSelectionTool_load.o
	$(lib_echo) "static library $@"
	$(lib_silent) cd $(bin); \
	  $(ar) $(InDetAmbiTrackSelectionToollib) $?
	$(lib_silent) $(ranlib) $(InDetAmbiTrackSelectionToollib)
	$(lib_silent) cat /dev/null >$(InDetAmbiTrackSelectionToolstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

#
# We add one level of dependency upon the true shared library 
# (rather than simply upon the stamp file)
# this is for cases where the shared library has not been built
# while the stamp was created (error??) 
#

$(InDetAmbiTrackSelectionToollibname).$(shlibsuffix) :: $(bin)InDetAmbiTrackSelectionTool.o $(bin)InDetDenseEnvAmbiTrackSelectionTool.o $(bin)InDetAmbiTrackSelectionTool_entries.o $(bin)InDetAmbiTrackSelectionTool_load.o $(use_requirements) $(InDetAmbiTrackSelectionToolstamps)
	$(lib_echo) "shared library $@"
	$(lib_silent) $(shlibbuilder) $(shlibflags) -o $@ $(bin)InDetAmbiTrackSelectionTool.o $(bin)InDetDenseEnvAmbiTrackSelectionTool.o $(bin)InDetAmbiTrackSelectionTool_entries.o $(bin)InDetAmbiTrackSelectionTool_load.o $(InDetAmbiTrackSelectionTool_shlibflags)
	$(lib_silent) cat /dev/null >$(InDetAmbiTrackSelectionToolstamp) && \
	  cat /dev/null >$(InDetAmbiTrackSelectionToolshstamp)

$(InDetAmbiTrackSelectionToolshstamp) :: $(InDetAmbiTrackSelectionToollibname).$(shlibsuffix)
	$(lib_silent) if test -f $(InDetAmbiTrackSelectionToollibname).$(shlibsuffix) ; then \
	  cat /dev/null >$(InDetAmbiTrackSelectionToolstamp) && \
	  cat /dev/null >$(InDetAmbiTrackSelectionToolshstamp) ; fi

InDetAmbiTrackSelectionToolclean ::
	$(cleanup_echo) objects InDetAmbiTrackSelectionTool
	$(cleanup_silent) /bin/rm -f $(bin)InDetAmbiTrackSelectionTool.o $(bin)InDetDenseEnvAmbiTrackSelectionTool.o $(bin)InDetAmbiTrackSelectionTool_entries.o $(bin)InDetAmbiTrackSelectionTool_load.o
	$(cleanup_silent) /bin/rm -f $(patsubst %.o,%.d,$(bin)InDetAmbiTrackSelectionTool.o $(bin)InDetDenseEnvAmbiTrackSelectionTool.o $(bin)InDetAmbiTrackSelectionTool_entries.o $(bin)InDetAmbiTrackSelectionTool_load.o) $(patsubst %.o,%.dep,$(bin)InDetAmbiTrackSelectionTool.o $(bin)InDetDenseEnvAmbiTrackSelectionTool.o $(bin)InDetAmbiTrackSelectionTool_entries.o $(bin)InDetAmbiTrackSelectionTool_load.o) $(patsubst %.o,%.d.stamp,$(bin)InDetAmbiTrackSelectionTool.o $(bin)InDetDenseEnvAmbiTrackSelectionTool.o $(bin)InDetAmbiTrackSelectionTool_entries.o $(bin)InDetAmbiTrackSelectionTool_load.o)
	$(cleanup_silent) cd $(bin); /bin/rm -rf InDetAmbiTrackSelectionTool_deps InDetAmbiTrackSelectionTool_dependencies.make

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/lib
InDetAmbiTrackSelectionToolinstallname = $(library_prefix)InDetAmbiTrackSelectionTool$(library_suffix).$(shlibsuffix)

InDetAmbiTrackSelectionTool :: InDetAmbiTrackSelectionToolinstall ;

install :: InDetAmbiTrackSelectionToolinstall ;

InDetAmbiTrackSelectionToolinstall :: $(install_dir)/$(InDetAmbiTrackSelectionToolinstallname)
ifdef CMTINSTALLAREA
	$(echo) "installation done"
endif

$(install_dir)/$(InDetAmbiTrackSelectionToolinstallname) :: $(bin)$(InDetAmbiTrackSelectionToolinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(InDetAmbiTrackSelectionToolinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##InDetAmbiTrackSelectionToolclean :: InDetAmbiTrackSelectionTooluninstall

uninstall :: InDetAmbiTrackSelectionTooluninstall ;

InDetAmbiTrackSelectionTooluninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(InDetAmbiTrackSelectionToolinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#-- end of library_no_static ------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetAmbiTrackSelectionToolclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)InDetAmbiTrackSelectionTool.d

$(bin)$(binobj)InDetAmbiTrackSelectionTool.d :

$(bin)$(binobj)InDetAmbiTrackSelectionTool.o : $(cmt_final_setup_InDetAmbiTrackSelectionTool)

$(bin)$(binobj)InDetAmbiTrackSelectionTool.o : $(src)InDetAmbiTrackSelectionTool.cxx
	$(cpp_echo) $(src)InDetAmbiTrackSelectionTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetAmbiTrackSelectionTool_pp_cppflags) $(lib_InDetAmbiTrackSelectionTool_pp_cppflags) $(InDetAmbiTrackSelectionTool_pp_cppflags) $(use_cppflags) $(InDetAmbiTrackSelectionTool_cppflags) $(lib_InDetAmbiTrackSelectionTool_cppflags) $(InDetAmbiTrackSelectionTool_cppflags) $(InDetAmbiTrackSelectionTool_cxx_cppflags)  $(src)InDetAmbiTrackSelectionTool.cxx
endif
endif

else
$(bin)InDetAmbiTrackSelectionTool_dependencies.make : $(InDetAmbiTrackSelectionTool_cxx_dependencies)

$(bin)InDetAmbiTrackSelectionTool_dependencies.make : $(src)InDetAmbiTrackSelectionTool.cxx

$(bin)$(binobj)InDetAmbiTrackSelectionTool.o : $(InDetAmbiTrackSelectionTool_cxx_dependencies)
	$(cpp_echo) $(src)InDetAmbiTrackSelectionTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetAmbiTrackSelectionTool_pp_cppflags) $(lib_InDetAmbiTrackSelectionTool_pp_cppflags) $(InDetAmbiTrackSelectionTool_pp_cppflags) $(use_cppflags) $(InDetAmbiTrackSelectionTool_cppflags) $(lib_InDetAmbiTrackSelectionTool_cppflags) $(InDetAmbiTrackSelectionTool_cppflags) $(InDetAmbiTrackSelectionTool_cxx_cppflags)  $(src)InDetAmbiTrackSelectionTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetAmbiTrackSelectionToolclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)InDetDenseEnvAmbiTrackSelectionTool.d

$(bin)$(binobj)InDetDenseEnvAmbiTrackSelectionTool.d :

$(bin)$(binobj)InDetDenseEnvAmbiTrackSelectionTool.o : $(cmt_final_setup_InDetAmbiTrackSelectionTool)

$(bin)$(binobj)InDetDenseEnvAmbiTrackSelectionTool.o : $(src)InDetDenseEnvAmbiTrackSelectionTool.cxx
	$(cpp_echo) $(src)InDetDenseEnvAmbiTrackSelectionTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetAmbiTrackSelectionTool_pp_cppflags) $(lib_InDetAmbiTrackSelectionTool_pp_cppflags) $(InDetDenseEnvAmbiTrackSelectionTool_pp_cppflags) $(use_cppflags) $(InDetAmbiTrackSelectionTool_cppflags) $(lib_InDetAmbiTrackSelectionTool_cppflags) $(InDetDenseEnvAmbiTrackSelectionTool_cppflags) $(InDetDenseEnvAmbiTrackSelectionTool_cxx_cppflags)  $(src)InDetDenseEnvAmbiTrackSelectionTool.cxx
endif
endif

else
$(bin)InDetAmbiTrackSelectionTool_dependencies.make : $(InDetDenseEnvAmbiTrackSelectionTool_cxx_dependencies)

$(bin)InDetAmbiTrackSelectionTool_dependencies.make : $(src)InDetDenseEnvAmbiTrackSelectionTool.cxx

$(bin)$(binobj)InDetDenseEnvAmbiTrackSelectionTool.o : $(InDetDenseEnvAmbiTrackSelectionTool_cxx_dependencies)
	$(cpp_echo) $(src)InDetDenseEnvAmbiTrackSelectionTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetAmbiTrackSelectionTool_pp_cppflags) $(lib_InDetAmbiTrackSelectionTool_pp_cppflags) $(InDetDenseEnvAmbiTrackSelectionTool_pp_cppflags) $(use_cppflags) $(InDetAmbiTrackSelectionTool_cppflags) $(lib_InDetAmbiTrackSelectionTool_cppflags) $(InDetDenseEnvAmbiTrackSelectionTool_cppflags) $(InDetDenseEnvAmbiTrackSelectionTool_cxx_cppflags)  $(src)InDetDenseEnvAmbiTrackSelectionTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetAmbiTrackSelectionToolclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)InDetAmbiTrackSelectionTool_entries.d

$(bin)$(binobj)InDetAmbiTrackSelectionTool_entries.d :

$(bin)$(binobj)InDetAmbiTrackSelectionTool_entries.o : $(cmt_final_setup_InDetAmbiTrackSelectionTool)

$(bin)$(binobj)InDetAmbiTrackSelectionTool_entries.o : $(src)components/InDetAmbiTrackSelectionTool_entries.cxx
	$(cpp_echo) $(src)components/InDetAmbiTrackSelectionTool_entries.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetAmbiTrackSelectionTool_pp_cppflags) $(lib_InDetAmbiTrackSelectionTool_pp_cppflags) $(InDetAmbiTrackSelectionTool_entries_pp_cppflags) $(use_cppflags) $(InDetAmbiTrackSelectionTool_cppflags) $(lib_InDetAmbiTrackSelectionTool_cppflags) $(InDetAmbiTrackSelectionTool_entries_cppflags) $(InDetAmbiTrackSelectionTool_entries_cxx_cppflags) -I../src/components $(src)components/InDetAmbiTrackSelectionTool_entries.cxx
endif
endif

else
$(bin)InDetAmbiTrackSelectionTool_dependencies.make : $(InDetAmbiTrackSelectionTool_entries_cxx_dependencies)

$(bin)InDetAmbiTrackSelectionTool_dependencies.make : $(src)components/InDetAmbiTrackSelectionTool_entries.cxx

$(bin)$(binobj)InDetAmbiTrackSelectionTool_entries.o : $(InDetAmbiTrackSelectionTool_entries_cxx_dependencies)
	$(cpp_echo) $(src)components/InDetAmbiTrackSelectionTool_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetAmbiTrackSelectionTool_pp_cppflags) $(lib_InDetAmbiTrackSelectionTool_pp_cppflags) $(InDetAmbiTrackSelectionTool_entries_pp_cppflags) $(use_cppflags) $(InDetAmbiTrackSelectionTool_cppflags) $(lib_InDetAmbiTrackSelectionTool_cppflags) $(InDetAmbiTrackSelectionTool_entries_cppflags) $(InDetAmbiTrackSelectionTool_entries_cxx_cppflags) -I../src/components $(src)components/InDetAmbiTrackSelectionTool_entries.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),InDetAmbiTrackSelectionToolclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)InDetAmbiTrackSelectionTool_load.d

$(bin)$(binobj)InDetAmbiTrackSelectionTool_load.d :

$(bin)$(binobj)InDetAmbiTrackSelectionTool_load.o : $(cmt_final_setup_InDetAmbiTrackSelectionTool)

$(bin)$(binobj)InDetAmbiTrackSelectionTool_load.o : $(src)components/InDetAmbiTrackSelectionTool_load.cxx
	$(cpp_echo) $(src)components/InDetAmbiTrackSelectionTool_load.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(InDetAmbiTrackSelectionTool_pp_cppflags) $(lib_InDetAmbiTrackSelectionTool_pp_cppflags) $(InDetAmbiTrackSelectionTool_load_pp_cppflags) $(use_cppflags) $(InDetAmbiTrackSelectionTool_cppflags) $(lib_InDetAmbiTrackSelectionTool_cppflags) $(InDetAmbiTrackSelectionTool_load_cppflags) $(InDetAmbiTrackSelectionTool_load_cxx_cppflags) -I../src/components $(src)components/InDetAmbiTrackSelectionTool_load.cxx
endif
endif

else
$(bin)InDetAmbiTrackSelectionTool_dependencies.make : $(InDetAmbiTrackSelectionTool_load_cxx_dependencies)

$(bin)InDetAmbiTrackSelectionTool_dependencies.make : $(src)components/InDetAmbiTrackSelectionTool_load.cxx

$(bin)$(binobj)InDetAmbiTrackSelectionTool_load.o : $(InDetAmbiTrackSelectionTool_load_cxx_dependencies)
	$(cpp_echo) $(src)components/InDetAmbiTrackSelectionTool_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(InDetAmbiTrackSelectionTool_pp_cppflags) $(lib_InDetAmbiTrackSelectionTool_pp_cppflags) $(InDetAmbiTrackSelectionTool_load_pp_cppflags) $(use_cppflags) $(InDetAmbiTrackSelectionTool_cppflags) $(lib_InDetAmbiTrackSelectionTool_cppflags) $(InDetAmbiTrackSelectionTool_load_cppflags) $(InDetAmbiTrackSelectionTool_load_cxx_cppflags) -I../src/components $(src)components/InDetAmbiTrackSelectionTool_load.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: InDetAmbiTrackSelectionToolclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetAmbiTrackSelectionTool.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetAmbiTrackSelectionToolclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library InDetAmbiTrackSelectionTool
	-$(cleanup_silent) cd $(bin) && \rm -f $(library_prefix)InDetAmbiTrackSelectionTool$(library_suffix).a $(library_prefix)InDetAmbiTrackSelectionTool$(library_suffix).$(shlibsuffix) InDetAmbiTrackSelectionTool.stamp InDetAmbiTrackSelectionTool.shstamp
#-- end of cleanup_library ---------------
