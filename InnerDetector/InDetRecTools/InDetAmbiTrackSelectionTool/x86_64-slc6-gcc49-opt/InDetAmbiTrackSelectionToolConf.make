#-- start of make_header -----------------

#====================================
#  Document InDetAmbiTrackSelectionToolConf
#
#   Generated Thu Apr 14 11:57:27 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetAmbiTrackSelectionToolConf_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetAmbiTrackSelectionToolConf_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetAmbiTrackSelectionToolConf

InDetAmbiTrackSelectionTool_tag = $(tag)

#cmt_local_tagfile_InDetAmbiTrackSelectionToolConf = $(InDetAmbiTrackSelectionTool_tag)_InDetAmbiTrackSelectionToolConf.make
cmt_local_tagfile_InDetAmbiTrackSelectionToolConf = $(bin)$(InDetAmbiTrackSelectionTool_tag)_InDetAmbiTrackSelectionToolConf.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetAmbiTrackSelectionTool_tag = $(tag)

#cmt_local_tagfile_InDetAmbiTrackSelectionToolConf = $(InDetAmbiTrackSelectionTool_tag).make
cmt_local_tagfile_InDetAmbiTrackSelectionToolConf = $(bin)$(InDetAmbiTrackSelectionTool_tag).make

endif

include $(cmt_local_tagfile_InDetAmbiTrackSelectionToolConf)
#-include $(cmt_local_tagfile_InDetAmbiTrackSelectionToolConf)

ifdef cmt_InDetAmbiTrackSelectionToolConf_has_target_tag

cmt_final_setup_InDetAmbiTrackSelectionToolConf = $(bin)setup_InDetAmbiTrackSelectionToolConf.make
cmt_dependencies_in_InDetAmbiTrackSelectionToolConf = $(bin)dependencies_InDetAmbiTrackSelectionToolConf.in
#cmt_final_setup_InDetAmbiTrackSelectionToolConf = $(bin)InDetAmbiTrackSelectionTool_InDetAmbiTrackSelectionToolConfsetup.make
cmt_local_InDetAmbiTrackSelectionToolConf_makefile = $(bin)InDetAmbiTrackSelectionToolConf.make

else

cmt_final_setup_InDetAmbiTrackSelectionToolConf = $(bin)setup.make
cmt_dependencies_in_InDetAmbiTrackSelectionToolConf = $(bin)dependencies.in
#cmt_final_setup_InDetAmbiTrackSelectionToolConf = $(bin)InDetAmbiTrackSelectionToolsetup.make
cmt_local_InDetAmbiTrackSelectionToolConf_makefile = $(bin)InDetAmbiTrackSelectionToolConf.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetAmbiTrackSelectionToolsetup.make

#InDetAmbiTrackSelectionToolConf :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetAmbiTrackSelectionToolConf'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetAmbiTrackSelectionToolConf/
#InDetAmbiTrackSelectionToolConf::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genconfig_header
# Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

# Use genconf.exe to create configurables python modules, then have the
# normal python install procedure take over.

.PHONY: InDetAmbiTrackSelectionToolConf InDetAmbiTrackSelectionToolConfclean

confpy  := InDetAmbiTrackSelectionToolConf.py
conflib := $(bin)$(library_prefix)InDetAmbiTrackSelectionTool.$(shlibsuffix)
confdb  := InDetAmbiTrackSelectionTool.confdb
instdir := $(CMTINSTALLAREA)$(shared_install_subdir)/python/$(package)
product := $(instdir)/$(confpy)
initpy  := $(instdir)/__init__.py

ifdef GENCONF_ECHO
genconf_silent =
else
genconf_silent = $(silent)
endif

InDetAmbiTrackSelectionToolConf :: InDetAmbiTrackSelectionToolConfinstall

install :: InDetAmbiTrackSelectionToolConfinstall

InDetAmbiTrackSelectionToolConfinstall : /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool/genConf/InDetAmbiTrackSelectionTool/$(confpy)
	@echo "Installing /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool/genConf/InDetAmbiTrackSelectionTool in /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InstallArea/python" ; \
	 $(install_command) --exclude="*.py?" --exclude="__init__.py" --exclude="*.confdb" /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool/genConf/InDetAmbiTrackSelectionTool /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InstallArea/python ; \

/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool/genConf/InDetAmbiTrackSelectionTool/$(confpy) : $(conflib) /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool/genConf/InDetAmbiTrackSelectionTool
	$(genconf_silent) $(genconfig_cmd)   -o /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool/genConf/InDetAmbiTrackSelectionTool -p $(package) \
	  --configurable-module=GaudiKernel.Proxy \
	  --configurable-default-name=Configurable.DefaultName \
	  --configurable-algorithm=ConfigurableAlgorithm \
	  --configurable-algtool=ConfigurableAlgTool \
	  --configurable-auditor=ConfigurableAuditor \
          --configurable-service=ConfigurableService \
	  -i ../$(tag)/$(library_prefix)InDetAmbiTrackSelectionTool.$(shlibsuffix)

/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool/genConf/InDetAmbiTrackSelectionTool:
	@ if [ ! -d /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool/genConf/InDetAmbiTrackSelectionTool ] ; then mkdir -p /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool/genConf/InDetAmbiTrackSelectionTool ; fi ;

InDetAmbiTrackSelectionToolConfclean :: InDetAmbiTrackSelectionToolConfuninstall
	$(cleanup_silent) $(remove_command) /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool/genConf/InDetAmbiTrackSelectionTool/$(confpy) /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool/genConf/InDetAmbiTrackSelectionTool/$(confdb)

uninstall :: InDetAmbiTrackSelectionToolConfuninstall

InDetAmbiTrackSelectionToolConfuninstall ::
	@$(uninstall_command) /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InstallArea/python
libInDetAmbiTrackSelectionTool_so_dependencies = ../x86_64-slc6-gcc49-opt/libInDetAmbiTrackSelectionTool.so
#-- start of cleanup_header --------------

clean :: InDetAmbiTrackSelectionToolConfclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetAmbiTrackSelectionToolConf.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetAmbiTrackSelectionToolConfclean ::
#-- end of cleanup_header ---------------
