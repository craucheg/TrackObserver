#-- start of make_header -----------------

#====================================
#  Document InDetAmbiTrackSelectionToolConfDbMerge
#
#   Generated Thu Apr 14 11:57:28 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetAmbiTrackSelectionToolConfDbMerge_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetAmbiTrackSelectionToolConfDbMerge_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetAmbiTrackSelectionToolConfDbMerge

InDetAmbiTrackSelectionTool_tag = $(tag)

#cmt_local_tagfile_InDetAmbiTrackSelectionToolConfDbMerge = $(InDetAmbiTrackSelectionTool_tag)_InDetAmbiTrackSelectionToolConfDbMerge.make
cmt_local_tagfile_InDetAmbiTrackSelectionToolConfDbMerge = $(bin)$(InDetAmbiTrackSelectionTool_tag)_InDetAmbiTrackSelectionToolConfDbMerge.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetAmbiTrackSelectionTool_tag = $(tag)

#cmt_local_tagfile_InDetAmbiTrackSelectionToolConfDbMerge = $(InDetAmbiTrackSelectionTool_tag).make
cmt_local_tagfile_InDetAmbiTrackSelectionToolConfDbMerge = $(bin)$(InDetAmbiTrackSelectionTool_tag).make

endif

include $(cmt_local_tagfile_InDetAmbiTrackSelectionToolConfDbMerge)
#-include $(cmt_local_tagfile_InDetAmbiTrackSelectionToolConfDbMerge)

ifdef cmt_InDetAmbiTrackSelectionToolConfDbMerge_has_target_tag

cmt_final_setup_InDetAmbiTrackSelectionToolConfDbMerge = $(bin)setup_InDetAmbiTrackSelectionToolConfDbMerge.make
cmt_dependencies_in_InDetAmbiTrackSelectionToolConfDbMerge = $(bin)dependencies_InDetAmbiTrackSelectionToolConfDbMerge.in
#cmt_final_setup_InDetAmbiTrackSelectionToolConfDbMerge = $(bin)InDetAmbiTrackSelectionTool_InDetAmbiTrackSelectionToolConfDbMergesetup.make
cmt_local_InDetAmbiTrackSelectionToolConfDbMerge_makefile = $(bin)InDetAmbiTrackSelectionToolConfDbMerge.make

else

cmt_final_setup_InDetAmbiTrackSelectionToolConfDbMerge = $(bin)setup.make
cmt_dependencies_in_InDetAmbiTrackSelectionToolConfDbMerge = $(bin)dependencies.in
#cmt_final_setup_InDetAmbiTrackSelectionToolConfDbMerge = $(bin)InDetAmbiTrackSelectionToolsetup.make
cmt_local_InDetAmbiTrackSelectionToolConfDbMerge_makefile = $(bin)InDetAmbiTrackSelectionToolConfDbMerge.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetAmbiTrackSelectionToolsetup.make

#InDetAmbiTrackSelectionToolConfDbMerge :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetAmbiTrackSelectionToolConfDbMerge'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetAmbiTrackSelectionToolConfDbMerge/
#InDetAmbiTrackSelectionToolConfDbMerge::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_genconfDb_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.confdb file into a single
# <project>.confdb file in the (lib) install area

.PHONY: InDetAmbiTrackSelectionToolConfDbMerge InDetAmbiTrackSelectionToolConfDbMergeclean

# default is already '#'
#genconfDb_comment_char := "'#'"

instdir      := ${CMTINSTALLAREA}/$(tag)
confDbRef    := /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InnerDetector/InDetRecTools/InDetAmbiTrackSelectionTool/genConf/InDetAmbiTrackSelectionTool/InDetAmbiTrackSelectionTool.confdb
stampConfDb  := $(confDbRef).stamp
mergedConfDb := $(instdir)/lib/$(project).confdb

InDetAmbiTrackSelectionToolConfDbMerge :: $(stampConfDb) $(mergedConfDb)
	@:

.NOTPARALLEL : $(stampConfDb) $(mergedConfDb)

$(stampConfDb) $(mergedConfDb) :: $(confDbRef)
	@echo "Running merge_genconfDb  InDetAmbiTrackSelectionToolConfDbMerge"
	$(merge_genconfDb_cmd) \
          --do-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)

InDetAmbiTrackSelectionToolConfDbMergeclean ::
	$(cleanup_silent) $(merge_genconfDb_cmd) \
          --un-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)	;
	$(cleanup_silent) $(remove_command) $(stampConfDb)
libInDetAmbiTrackSelectionTool_so_dependencies = ../x86_64-slc6-gcc49-opt/libInDetAmbiTrackSelectionTool.so
#-- start of cleanup_header --------------

clean :: InDetAmbiTrackSelectionToolConfDbMergeclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetAmbiTrackSelectionToolConfDbMerge.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetAmbiTrackSelectionToolConfDbMergeclean ::
#-- end of cleanup_header ---------------
