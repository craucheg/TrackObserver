#-- start of make_header -----------------

#====================================
#  Document InDetAmbiTrackSelectionToolCLIDDB
#
#   Generated Thu Apr 14 11:57:24 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetAmbiTrackSelectionToolCLIDDB_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetAmbiTrackSelectionToolCLIDDB_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetAmbiTrackSelectionToolCLIDDB

InDetAmbiTrackSelectionTool_tag = $(tag)

#cmt_local_tagfile_InDetAmbiTrackSelectionToolCLIDDB = $(InDetAmbiTrackSelectionTool_tag)_InDetAmbiTrackSelectionToolCLIDDB.make
cmt_local_tagfile_InDetAmbiTrackSelectionToolCLIDDB = $(bin)$(InDetAmbiTrackSelectionTool_tag)_InDetAmbiTrackSelectionToolCLIDDB.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetAmbiTrackSelectionTool_tag = $(tag)

#cmt_local_tagfile_InDetAmbiTrackSelectionToolCLIDDB = $(InDetAmbiTrackSelectionTool_tag).make
cmt_local_tagfile_InDetAmbiTrackSelectionToolCLIDDB = $(bin)$(InDetAmbiTrackSelectionTool_tag).make

endif

include $(cmt_local_tagfile_InDetAmbiTrackSelectionToolCLIDDB)
#-include $(cmt_local_tagfile_InDetAmbiTrackSelectionToolCLIDDB)

ifdef cmt_InDetAmbiTrackSelectionToolCLIDDB_has_target_tag

cmt_final_setup_InDetAmbiTrackSelectionToolCLIDDB = $(bin)setup_InDetAmbiTrackSelectionToolCLIDDB.make
cmt_dependencies_in_InDetAmbiTrackSelectionToolCLIDDB = $(bin)dependencies_InDetAmbiTrackSelectionToolCLIDDB.in
#cmt_final_setup_InDetAmbiTrackSelectionToolCLIDDB = $(bin)InDetAmbiTrackSelectionTool_InDetAmbiTrackSelectionToolCLIDDBsetup.make
cmt_local_InDetAmbiTrackSelectionToolCLIDDB_makefile = $(bin)InDetAmbiTrackSelectionToolCLIDDB.make

else

cmt_final_setup_InDetAmbiTrackSelectionToolCLIDDB = $(bin)setup.make
cmt_dependencies_in_InDetAmbiTrackSelectionToolCLIDDB = $(bin)dependencies.in
#cmt_final_setup_InDetAmbiTrackSelectionToolCLIDDB = $(bin)InDetAmbiTrackSelectionToolsetup.make
cmt_local_InDetAmbiTrackSelectionToolCLIDDB_makefile = $(bin)InDetAmbiTrackSelectionToolCLIDDB.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetAmbiTrackSelectionToolsetup.make

#InDetAmbiTrackSelectionToolCLIDDB :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetAmbiTrackSelectionToolCLIDDB'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetAmbiTrackSelectionToolCLIDDB/
#InDetAmbiTrackSelectionToolCLIDDB::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genCLIDDB_header
# Author: Paolo Calafiura
# derived from genconf_header

# Use genCLIDDB_cmd to create package clid.db files

.PHONY: InDetAmbiTrackSelectionToolCLIDDB InDetAmbiTrackSelectionToolCLIDDBclean

outname := clid.db
cliddb  := InDetAmbiTrackSelectionTool_$(outname)
instdir := $(CMTINSTALLAREA)/share
result  := $(instdir)/$(cliddb)
product := $(instdir)/$(outname)
conflib := $(bin)$(library_prefix)InDetAmbiTrackSelectionTool.$(shlibsuffix)

InDetAmbiTrackSelectionToolCLIDDB :: $(result)

$(instdir) :
	$(mkdir) -p $(instdir)

$(result) : $(conflib) $(product)
	@$(genCLIDDB_cmd) -p InDetAmbiTrackSelectionTool -i$(product) -o $(result)

$(product) : $(instdir)
	touch $(product)

InDetAmbiTrackSelectionToolCLIDDBclean ::
	$(cleanup_silent) $(uninstall_command) $(product) $(result)
	$(cleanup_silent) $(cmt_uninstallarea_command) $(product) $(result)

#-- start of cleanup_header --------------

clean :: InDetAmbiTrackSelectionToolCLIDDBclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetAmbiTrackSelectionToolCLIDDB.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetAmbiTrackSelectionToolCLIDDBclean ::
#-- end of cleanup_header ---------------
