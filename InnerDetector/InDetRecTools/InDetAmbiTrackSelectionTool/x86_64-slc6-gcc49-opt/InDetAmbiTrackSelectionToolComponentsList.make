#-- start of make_header -----------------

#====================================
#  Document InDetAmbiTrackSelectionToolComponentsList
#
#   Generated Thu Apr 14 11:57:27 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetAmbiTrackSelectionToolComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetAmbiTrackSelectionToolComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetAmbiTrackSelectionToolComponentsList

InDetAmbiTrackSelectionTool_tag = $(tag)

#cmt_local_tagfile_InDetAmbiTrackSelectionToolComponentsList = $(InDetAmbiTrackSelectionTool_tag)_InDetAmbiTrackSelectionToolComponentsList.make
cmt_local_tagfile_InDetAmbiTrackSelectionToolComponentsList = $(bin)$(InDetAmbiTrackSelectionTool_tag)_InDetAmbiTrackSelectionToolComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetAmbiTrackSelectionTool_tag = $(tag)

#cmt_local_tagfile_InDetAmbiTrackSelectionToolComponentsList = $(InDetAmbiTrackSelectionTool_tag).make
cmt_local_tagfile_InDetAmbiTrackSelectionToolComponentsList = $(bin)$(InDetAmbiTrackSelectionTool_tag).make

endif

include $(cmt_local_tagfile_InDetAmbiTrackSelectionToolComponentsList)
#-include $(cmt_local_tagfile_InDetAmbiTrackSelectionToolComponentsList)

ifdef cmt_InDetAmbiTrackSelectionToolComponentsList_has_target_tag

cmt_final_setup_InDetAmbiTrackSelectionToolComponentsList = $(bin)setup_InDetAmbiTrackSelectionToolComponentsList.make
cmt_dependencies_in_InDetAmbiTrackSelectionToolComponentsList = $(bin)dependencies_InDetAmbiTrackSelectionToolComponentsList.in
#cmt_final_setup_InDetAmbiTrackSelectionToolComponentsList = $(bin)InDetAmbiTrackSelectionTool_InDetAmbiTrackSelectionToolComponentsListsetup.make
cmt_local_InDetAmbiTrackSelectionToolComponentsList_makefile = $(bin)InDetAmbiTrackSelectionToolComponentsList.make

else

cmt_final_setup_InDetAmbiTrackSelectionToolComponentsList = $(bin)setup.make
cmt_dependencies_in_InDetAmbiTrackSelectionToolComponentsList = $(bin)dependencies.in
#cmt_final_setup_InDetAmbiTrackSelectionToolComponentsList = $(bin)InDetAmbiTrackSelectionToolsetup.make
cmt_local_InDetAmbiTrackSelectionToolComponentsList_makefile = $(bin)InDetAmbiTrackSelectionToolComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetAmbiTrackSelectionToolsetup.make

#InDetAmbiTrackSelectionToolComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetAmbiTrackSelectionToolComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetAmbiTrackSelectionToolComponentsList/
#InDetAmbiTrackSelectionToolComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
##
componentslistfile = InDetAmbiTrackSelectionTool.components
COMPONENTSLIST_DIR = ../$(tag)
fulllibname = libInDetAmbiTrackSelectionTool.$(shlibsuffix)

InDetAmbiTrackSelectionToolComponentsList :: ${COMPONENTSLIST_DIR}/$(componentslistfile)
	@:

${COMPONENTSLIST_DIR}/$(componentslistfile) :: $(bin)$(fulllibname)
	@echo 'Generating componentslist file for $(fulllibname)'
	cd ../$(tag);$(listcomponents_cmd) --output ${COMPONENTSLIST_DIR}/$(componentslistfile) $(fulllibname)

install :: InDetAmbiTrackSelectionToolComponentsListinstall
InDetAmbiTrackSelectionToolComponentsListinstall :: InDetAmbiTrackSelectionToolComponentsList

uninstall :: InDetAmbiTrackSelectionToolComponentsListuninstall
InDetAmbiTrackSelectionToolComponentsListuninstall :: InDetAmbiTrackSelectionToolComponentsListclean

InDetAmbiTrackSelectionToolComponentsListclean ::
	@echo 'Deleting $(componentslistfile)'
	@rm -f ${COMPONENTSLIST_DIR}/$(componentslistfile)

#-- start of cleanup_header --------------

clean :: InDetAmbiTrackSelectionToolComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetAmbiTrackSelectionToolComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetAmbiTrackSelectionToolComponentsListclean ::
#-- end of cleanup_header ---------------
