#-- start of make_header -----------------

#====================================
#  Document InDetAmbiTrackSelectionToolMergeComponentsList
#
#   Generated Thu Apr 14 11:57:28 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_InDetAmbiTrackSelectionToolMergeComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_InDetAmbiTrackSelectionToolMergeComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_InDetAmbiTrackSelectionToolMergeComponentsList

InDetAmbiTrackSelectionTool_tag = $(tag)

#cmt_local_tagfile_InDetAmbiTrackSelectionToolMergeComponentsList = $(InDetAmbiTrackSelectionTool_tag)_InDetAmbiTrackSelectionToolMergeComponentsList.make
cmt_local_tagfile_InDetAmbiTrackSelectionToolMergeComponentsList = $(bin)$(InDetAmbiTrackSelectionTool_tag)_InDetAmbiTrackSelectionToolMergeComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

InDetAmbiTrackSelectionTool_tag = $(tag)

#cmt_local_tagfile_InDetAmbiTrackSelectionToolMergeComponentsList = $(InDetAmbiTrackSelectionTool_tag).make
cmt_local_tagfile_InDetAmbiTrackSelectionToolMergeComponentsList = $(bin)$(InDetAmbiTrackSelectionTool_tag).make

endif

include $(cmt_local_tagfile_InDetAmbiTrackSelectionToolMergeComponentsList)
#-include $(cmt_local_tagfile_InDetAmbiTrackSelectionToolMergeComponentsList)

ifdef cmt_InDetAmbiTrackSelectionToolMergeComponentsList_has_target_tag

cmt_final_setup_InDetAmbiTrackSelectionToolMergeComponentsList = $(bin)setup_InDetAmbiTrackSelectionToolMergeComponentsList.make
cmt_dependencies_in_InDetAmbiTrackSelectionToolMergeComponentsList = $(bin)dependencies_InDetAmbiTrackSelectionToolMergeComponentsList.in
#cmt_final_setup_InDetAmbiTrackSelectionToolMergeComponentsList = $(bin)InDetAmbiTrackSelectionTool_InDetAmbiTrackSelectionToolMergeComponentsListsetup.make
cmt_local_InDetAmbiTrackSelectionToolMergeComponentsList_makefile = $(bin)InDetAmbiTrackSelectionToolMergeComponentsList.make

else

cmt_final_setup_InDetAmbiTrackSelectionToolMergeComponentsList = $(bin)setup.make
cmt_dependencies_in_InDetAmbiTrackSelectionToolMergeComponentsList = $(bin)dependencies.in
#cmt_final_setup_InDetAmbiTrackSelectionToolMergeComponentsList = $(bin)InDetAmbiTrackSelectionToolsetup.make
cmt_local_InDetAmbiTrackSelectionToolMergeComponentsList_makefile = $(bin)InDetAmbiTrackSelectionToolMergeComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)InDetAmbiTrackSelectionToolsetup.make

#InDetAmbiTrackSelectionToolMergeComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'InDetAmbiTrackSelectionToolMergeComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = InDetAmbiTrackSelectionToolMergeComponentsList/
#InDetAmbiTrackSelectionToolMergeComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_componentslist_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.components file into a single
# <project>.components file in the (lib) install area
# If no InstallArea is present the fragment is dummy


.PHONY: InDetAmbiTrackSelectionToolMergeComponentsList InDetAmbiTrackSelectionToolMergeComponentsListclean

# default is already '#'
#genmap_comment_char := "'#'"

componentsListRef    := ../$(tag)/InDetAmbiTrackSelectionTool.components

ifdef CMTINSTALLAREA
componentsListDir    := ${CMTINSTALLAREA}/$(tag)/lib
mergedComponentsList := $(componentsListDir)/$(project).components
stampComponentsList  := $(componentsListRef).stamp
else
componentsListDir    := ../$(tag)
mergedComponentsList :=
stampComponentsList  :=
endif

InDetAmbiTrackSelectionToolMergeComponentsList :: $(stampComponentsList) $(mergedComponentsList)
	@:

.NOTPARALLEL : $(stampComponentsList) $(mergedComponentsList)

$(stampComponentsList) $(mergedComponentsList) :: $(componentsListRef)
	@echo "Running merge_componentslist  InDetAmbiTrackSelectionToolMergeComponentsList"
	$(merge_componentslist_cmd) --do-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList)

InDetAmbiTrackSelectionToolMergeComponentsListclean ::
	$(cleanup_silent) $(merge_componentslist_cmd) --un-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList) ;
	$(cleanup_silent) $(remove_command) $(stampComponentsList)
libInDetAmbiTrackSelectionTool_so_dependencies = ../x86_64-slc6-gcc49-opt/libInDetAmbiTrackSelectionTool.so
#-- start of cleanup_header --------------

clean :: InDetAmbiTrackSelectionToolMergeComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(InDetAmbiTrackSelectionToolMergeComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

InDetAmbiTrackSelectionToolMergeComponentsListclean ::
#-- end of cleanup_header ---------------
