#Thu Apr 14 11:57:27 2016"""Automatically generated. DO NOT EDIT please"""
from GaudiKernel.GaudiHandles import *
from GaudiKernel.Proxy.Configurable import *

class InDet__InDetAmbiTrackSelectionTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'AssociationTool' : PublicToolHandle('Trk::PRD_AssociationTool/PRD_AssociationTool'), # GaudiHandle
    'DriftCircleCutTool' : PublicToolHandle('InDet::InDetTrtDriftCircleCutTool'), # GaudiHandle
    'minHits' : 5, # int
    'minTRTHits' : 0, # int
    'maxShared' : 1, # int
    'minScoreShareTracks' : 0.00000, # float
    'maxTracksPerSharedPRD' : 2, # int
    'minNotShared' : 6, # int
    'Cosmics' : False, # bool
    'UseParameterization' : True, # bool
    'doPixelSplitting' : False, # bool
    'sharedProbCut' : 0.0200000, # float
    'MaximalSplitSize' : 49, # int
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(InDet__InDetAmbiTrackSelectionTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'InDetAmbiTrackSelectionTool'
  def getType( self ):
      return 'InDet::InDetAmbiTrackSelectionTool'
  pass # class InDet__InDetAmbiTrackSelectionTool

class InDet__InDetDenseEnvAmbiTrackSelectionTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'AssociationTool' : PublicToolHandle('Trk::PRD_AssociationTool/PRD_AssociationTool'), # GaudiHandle
    'DriftCircleCutTool' : PublicToolHandle('InDet::InDetTrtDriftCircleCutTool'), # GaudiHandle
    'IncidentService' : ServiceHandle('IncidentSvc'), # GaudiHandle
    'minHits' : 5, # int
    'minTRTHits' : 0, # int
    'maxShared' : 1, # int
    'maxSharedModulesInROI' : 1, # int
    'minScoreShareTracks' : 0.00000, # float
    'maxTracksPerSharedPRD' : 2, # int
    'minNotShared' : 6, # int
    'Cosmics' : False, # bool
    'UseParameterization' : True, # bool
    'doPixelSplitting' : False, # bool
    'sharedProbCut' : 0.300000, # float
    'sharedProbCut2' : 0.300000, # float
    'minSharedProbCut' : 0.0500000, # float
    'minTrackChi2ForSharedHits' : 3.00000, # float
    'minUniqueSCTHits' : 2, # int
    'minSiHitsToAllowSplitting' : 9, # int
    'maxPixMultiCluster' : 4, # int
    'doHadCaloSeed' : False, # bool
    'minPtSplit' : 0.00000, # float
    'phiWidth' : 0.300000, # float
    'etaWidth' : 0.300000, # float
    'InputHadClusterContainerName' : '', # str
    'MonitorAmbiguitySolving' : False, # bool
    'ObserverTool' : PublicToolHandle('Trk::TrkObserverTool/TrkObserverTool'), # GaudiHandle
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(InDet__InDetDenseEnvAmbiTrackSelectionTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'InDetAmbiTrackSelectionTool'
  def getType( self ):
      return 'InDet::InDetDenseEnvAmbiTrackSelectionTool'
  pass # class InDet__InDetDenseEnvAmbiTrackSelectionTool
