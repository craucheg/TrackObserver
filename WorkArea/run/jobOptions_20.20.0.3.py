#--------------------------------------------------------------
# Event related parameters and input files
#--------------------------------------------------------------

# --- read BS 
doReadBS = False

from AthenaCommon.AthenaCommonFlags import athenaCommonFlags

if not doReadBS:
  #ITK Ex Barrel@4.0 ... singe pi-
  #athenaCommonFlags.FilesInput = ["/afs/cern.ch/work/c/craucheg/data/mc15_14TeV.428002.ParticleGun_single_piminus_logE0p2to2000.recon.RDO.e3895_s2844_s2848_r7641/RDO.07777856._000001.pool.root.1"]
  #athenaCommonFlags.FilesInput = ["/afs/cern.ch/user/p/picazio/public/ForChristophR/OUT.RDO_ExtBrl4.pool.root"]		# Sample attilio
	#athenaCommonFlags.FilesInput = ["/afs/cern.ch/work/c/craucheg/data/tmp/mc15_14TeV.117050.PowhegPythia_P2011C_ttbar.recon.RDO.e2176_s2845_s2849_r7642_tid07777889_00/RDO.07777889._000001.pool.root.1"]		#test tid_...
	#athenaCommonFlags.FilesInput = ["/afs/cern.ch/work/c/craucheg/data/tmp/mc15_14TeV.428002.ParticleGun_single_piminus_logE0p2to2000.recon.RDO.e3895_s2845_s2849_r7642_tid07777933_00/RDO.07777933._000001.pool.root.1"]		#test single pi minus tid_sample

	#ITK Inclined pi+
	athenaCommonFlags.FilesInput = ["/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000001.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000003.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000005.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000006.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000009.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000011.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000015.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000016.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000017.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000030.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000033.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000037.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000045.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000046.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000049.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000050.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000076.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000081.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000087.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000089.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000092.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000095.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000102.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000112.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000113.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000121.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000124.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000130.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000131.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000133.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000138.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000143.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000144.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000154.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000166.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000167.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000171.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000175.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000177.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000192.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000234.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000243.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000251.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000253.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000256.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000269.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000274.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000275.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000279.pool.root.1",
"/afs/cern.ch/work/c/craucheg/data/inclined/mc15_14TeV.428001.ParticleGun_single_piplus_logE0p2to2000.recon.RDO.e3895_s2846_s2850_r7643_tid07778003_00/RDO.07778003._000280.pool.root.1"]


if doReadBS:
  athenaCommonFlags.FilesInput = ["root://eosatlas//eos/atlas/atlascerngroupdisk/perf-idtracking/InDetRecExample/data10_7TeV.00154817.physics_MinBias.merge.RAW/data10_7TeV.00154817.physics_MinBias.merge.RAW._lb0100._0001.1"]
  
import AthenaPython.ConfigLib as apcl
cfg = apcl.AutoCfg(name = 'InDetRecExampleAutoConfig', input_files=athenaCommonFlags.FilesInput())
cfg.configure_job()

if doReadBS:
  from AthenaCommon.GlobalFlags import globalflags
  if len(globalflags.ConditionsTag())!=0:
    from IOVDbSvc.CondDB import conddb
    conddb.setGlobalTag(globalflags.ConditionsTag())

import MagFieldServices.SetupField

theApp.EvtMax = -1
#theApp.EvtMax = 100
#theApp.EvtMax = 500
#theApp.EvtMax = 5000

#--------------------------------------------------------------
# Control
#--------------------------------------------------------------

# --- Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
OutputLevel     = WARNING
# --- produce an atlantis data file
doJiveXML       = False
# --- run the Virtual Point 1 event visualisation
doVP1           = False
# --- controls what is written out. ESD includes AOD, so it's normally enough
doWriteESD      = True
doWriteAOD      = True
# --- do auditors ?
doAuditors      = True

import os
if os.environ['CMTCONFIG'].endswith('-dbg'):
  # --- do EDM monitor (debug mode only)
  doEdmMonitor    = True 
  # --- write out a short message upon entering or leaving each algorithm
  doNameAuditor   = True
else:
  doEdmMonitor    = False
  doNameAuditor   = False

#--------------------------------------------------------------
# Additional Detector Setup
#--------------------------------------------------------------

from RecExConfig.RecFlags import rec
rec.Commissioning=False

# --- for heavy ion running
#rec.doHeavyIon = True

from AthenaCommon.DetFlags import DetFlags 
# --- switch on InnerDetector
DetFlags.ID_setOn()
#DetFlags.makeRIO.pixel_setOff()
#DetFlags.makeRIO.SCT_setOff()
#DetFlags.makeRIO.TRT_setOff()

# --- and switch off all the rest
DetFlags.Calo_setOff()
DetFlags.Muon_setOff()

# ---- switch parts of ID off/on as follows (always use both lines)
#DetFlags.pixel_setOff()
#DetFlags.detdescr.pixel_setOn()
#DetFlags.SCT_setOff()
#DetFlags.detdescr.SCT_setOn()
#DetFlags.TRT_setOff()
#DetFlags.detdescr.TRT_setOn()

# --- switch off DCS
#DetFlags.dcs.pixel_setOff()
#DetFlags.dcs.SCT_setOff()
#DetFlags.dcs.TRT_setOff()

# --- printout
#DetFlags.Print()

# --- output level
#OutputLevel          = DEBUG

#--------------------------------------------------------------
# Load InDet configuration
#--------------------------------------------------------------

from AthenaCommon.GlobalFlags import globalflags



#globalflags.ConditionsTag.set_Value_and_Lock('OFLCOND-MC15c-SDR-06')		# for ITK ... geht nicht (falsch laut nora)
globalflags.ConditionsTag.set_Value_and_Lock('OFLCOND-MC12-ITK-27-00')		# for ITK

DetDescrVersion = "ATLAS-P2-ITK-07-00-00"		#inclined layout step 1
globalflags.DetDescrVersion = DetDescrVersion

#from AthenaCommon.GlobalFlags import jobproperties
#jobproperties.Global.DetDescrVersion = DetDescrVersion

# --- setup InDetJobProperties
from InDetRecExample.InDetJobProperties import InDetFlags
InDetFlags.doTruth       = (globalflags.DataSource == 'geant4' and globalflags.InputFormat() == 'pool')

InDetFlags.doTIDE_AmbiTrackMonitoring = True		#enable observer tool

#pre includes
include("InDetSLHC_Example/preInclude.SLHC.SiliconOnly.Reco.py")
#include("InDetSLHC_Example/preInclude.SLHC_Setup_ExtBrl_4.py")
include("InDetSLHC_Example/preInclude.SLHC_Setup_IExtBrl_4.py")		# inclined
include("InDetSLHC_Example/SLHC_Setup_Reco_Alpine.py")

# --- special setup for Beate
# InDetFlags.doVtxLumi = True 

# --- enable xKalman and iPatRec
# InDetFlags.doxKalman              = True
# InDetFlags.doiPatRec              = True

# --- uncomment to change the default of one of the following options:
#InDetFlags.doNewTracking          = False
#InDetFlags.doLowPt                = True
#InDetFlags.doBeamGas              = True
#InDetFlags.doBeamHalo             = True
#InDetFlags.doBackTracking         = False
#InDetFlags.doTRTStandalone        = False
#InDetFlags.doLowBetaFinder        = False


#InDetFlags.cutLevel.set_Value_and_Lock                             (10)

#InDetFlags.cutLevel       = 6 # test for TRTonly in PU
#InDetFlags.priVtxCutLevel = 2 # test for PU

# --- enable brem recovery
# InDetFlags.doBremRecovery   = True

# --- calo seeding does not work standalone 
# InDetFlags.doCaloSeededBrem = False

# --- Turn off track slimming
#InDetFlags.doSlimming = False

#InDetFlags.doIBL = True
#InDetFlags.doHighPileup = True
InDetFlags.doSLHC = True

# --- possibility to run tracking on subdetectors separately (and independent from each other)
#InDetFlags.doTrackSegmentsPixel = True
#InDetFlags.doTrackSegmentsSCT   = True
#InDetFlags.doTrackSegmentsTRT   = True

# --- possibility to change the trackfitter
#InDetFlags.trackFitterType = 'KalmanFitter'

# --- activate monitorings
InDetFlags.doMonitoringGlobal    = False
InDetFlags.doMonitoringPrimaryVertexingEnhanced = False
InDetFlags.doMonitoringPixel     = False
InDetFlags.doMonitoringSCT       = False
InDetFlags.doMonitoringTRT       = False
InDetFlags.doMonitoringAlignment = False

# --- activate (memory/cpu) monitoring
#InDetFlags.doPerfMon        = True
# --- activate creation of standard plots
#InDetFlags.doStandardPlots  = True
# --- activate physics validation monitoring
#InDetFlags.doPhysValMon  = True
# --- active storegate delection
#InDetFlags.doSGDeletion  = True

# --- produce various ntuples (all in one root file)
#InDetFlags.doTrkNtuple      = True
#InDetFlags.doPixelTrkNtuple = True
#InDetFlags.doSctTrkNtuple   = True
#InDetFlags.doTrtTrkNtuple   = True
#InDetFlags.doPixelClusterNtuple = True
#InDetFlags.doSctClusterNtuple   = True
#InDetFlags.doTrtDriftCircleNtuple = True
#InDetFlags.doVtxNtuple      = True
#InDetFlags.doConvVtxNtuple  = True
#InDetFlags.doV0VtxNtuple    = True

#InDetFlags.doMinBias   = True

# activate the print InDetXYZAlgorithm statements
InDetFlags.doPrintConfigurables = True

#Turn off filling of TRT occupacy to EventInfo (does not work for Standalone)
#InDetFlags.doTRTGlobalOccupancy = False
#

# IMPORTANT NOTE: initialization of the flags and locking them is done in InDetRec_jobOptions.py!
# This way RecExCommon just needs to import the properties without doing anything else!
# DO NOT SET JOBPROPERTIES AFTER THIS LINE! The change will be ignored!

#--------------------------------------------------------------
# load master joboptions file
#--------------------------------------------------------------

#preExec
InDetFlags.doSLHCVeryForward.set_Value_and_Lock(True)
#InDetFlags.writeRDOs.set_Value_and_Lock(True)
#rec.doDPD.set_Value_and_Lock(True)
#rec.DPDMakerScripts.set_Value_and_Lock(["InDetPrepRawDataToxAOD/InDetDxAOD.py","PrimaryDPDMaker/PrimaryDPDMaker.py"])
#from InDetPrepRawDataToxAOD.InDetDxAODJobProperties import InDetDxAODFlags
#InDetDxAODFlags.DumpUnassociatedHits.set_Value_and_Lock(False)
#InDetDxAODFlags.DumpPixelInfo.set_Value_and_Lock(True)
#InDetDxAODFlags.DumpPixelRdoInfo.set_Value_and_Lock(True)
#InDetDxAODFlags.DumpSctInfo.set_Value_and_Lock(True)
#InDetDxAODFlags.DumpSctRdoInfo.set_Value_and_Lock(True)

#pre execute (old)
InDetFlags.doPixelClusterSplitting.set_Value_and_Lock(True)
InDetFlags.doTIDE_Ambi.set_Value_and_Lock(True)

include("InDetRecExample/InDetRec_all.py")

#post includes
#include("InDetSLHC_Example/postInclude.SLHC_Setup_ExtBrl_4.py")
include("InDetSLHC_Example/postInclude.SLHC_Setup_IExtBrl_4.py")	#inclined layout step1
include("InDetSLHC_Example/postInclude.DigitalClustering.py")
ServiceMgr.PixelOfflineCalibSvc.HDCFromCOOL = False

# --- dump MC truth into logfile
#from TruthExamples.TruthExamplesConf import DumpMC
#topSequence += DumpMC(McEventKey = "TruthEvent")

from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
outStream = MSMgr.NewPoolRootStream( "MyXAODStream", "inclinedSinglePiPlus.pool.root" )
outStream.AddItem(['xAOD::TrackParticleContainer#*'])
outStream.AddItem(['xAOD::TrackParticleAuxContainer#*'])
outStream.AddItem(['xAOD::TruthParticleContainer#*'])
outStream.AddItem(['xAOD::TruthParticleAuxContainer#*'])


#outStream.OutputLevel = INFO

#outStream.AddItem(['xAOD::TrackParticleContainer#ObservedTracks'])
#outStream.AddItem(['xAOD::TrackParticleAuxContainer#ObservedTracksAux.'])
#print "END OF THE JOB OPTIONS!!!"
#print outStream


