#-- start of make_header -----------------

#====================================
#  Document TrkAmbiguityProcessorComponentsList
#
#   Generated Thu Apr 14 11:56:00 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_TrkAmbiguityProcessorComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_TrkAmbiguityProcessorComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_TrkAmbiguityProcessorComponentsList

TrkAmbiguityProcessor_tag = $(tag)

#cmt_local_tagfile_TrkAmbiguityProcessorComponentsList = $(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessorComponentsList.make
cmt_local_tagfile_TrkAmbiguityProcessorComponentsList = $(bin)$(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessorComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

TrkAmbiguityProcessor_tag = $(tag)

#cmt_local_tagfile_TrkAmbiguityProcessorComponentsList = $(TrkAmbiguityProcessor_tag).make
cmt_local_tagfile_TrkAmbiguityProcessorComponentsList = $(bin)$(TrkAmbiguityProcessor_tag).make

endif

include $(cmt_local_tagfile_TrkAmbiguityProcessorComponentsList)
#-include $(cmt_local_tagfile_TrkAmbiguityProcessorComponentsList)

ifdef cmt_TrkAmbiguityProcessorComponentsList_has_target_tag

cmt_final_setup_TrkAmbiguityProcessorComponentsList = $(bin)setup_TrkAmbiguityProcessorComponentsList.make
cmt_dependencies_in_TrkAmbiguityProcessorComponentsList = $(bin)dependencies_TrkAmbiguityProcessorComponentsList.in
#cmt_final_setup_TrkAmbiguityProcessorComponentsList = $(bin)TrkAmbiguityProcessor_TrkAmbiguityProcessorComponentsListsetup.make
cmt_local_TrkAmbiguityProcessorComponentsList_makefile = $(bin)TrkAmbiguityProcessorComponentsList.make

else

cmt_final_setup_TrkAmbiguityProcessorComponentsList = $(bin)setup.make
cmt_dependencies_in_TrkAmbiguityProcessorComponentsList = $(bin)dependencies.in
#cmt_final_setup_TrkAmbiguityProcessorComponentsList = $(bin)TrkAmbiguityProcessorsetup.make
cmt_local_TrkAmbiguityProcessorComponentsList_makefile = $(bin)TrkAmbiguityProcessorComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)TrkAmbiguityProcessorsetup.make

#TrkAmbiguityProcessorComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'TrkAmbiguityProcessorComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = TrkAmbiguityProcessorComponentsList/
#TrkAmbiguityProcessorComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
##
componentslistfile = TrkAmbiguityProcessor.components
COMPONENTSLIST_DIR = ../$(tag)
fulllibname = libTrkAmbiguityProcessor.$(shlibsuffix)

TrkAmbiguityProcessorComponentsList :: ${COMPONENTSLIST_DIR}/$(componentslistfile)
	@:

${COMPONENTSLIST_DIR}/$(componentslistfile) :: $(bin)$(fulllibname)
	@echo 'Generating componentslist file for $(fulllibname)'
	cd ../$(tag);$(listcomponents_cmd) --output ${COMPONENTSLIST_DIR}/$(componentslistfile) $(fulllibname)

install :: TrkAmbiguityProcessorComponentsListinstall
TrkAmbiguityProcessorComponentsListinstall :: TrkAmbiguityProcessorComponentsList

uninstall :: TrkAmbiguityProcessorComponentsListuninstall
TrkAmbiguityProcessorComponentsListuninstall :: TrkAmbiguityProcessorComponentsListclean

TrkAmbiguityProcessorComponentsListclean ::
	@echo 'Deleting $(componentslistfile)'
	@rm -f ${COMPONENTSLIST_DIR}/$(componentslistfile)

#-- start of cleanup_header --------------

clean :: TrkAmbiguityProcessorComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(TrkAmbiguityProcessorComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

TrkAmbiguityProcessorComponentsListclean ::
#-- end of cleanup_header ---------------
