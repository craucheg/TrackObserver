#-- start of make_header -----------------

#====================================
#  Document TrkAmbiguityProcessorMergeComponentsList
#
#   Generated Thu Apr 14 11:56:03 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_TrkAmbiguityProcessorMergeComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_TrkAmbiguityProcessorMergeComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_TrkAmbiguityProcessorMergeComponentsList

TrkAmbiguityProcessor_tag = $(tag)

#cmt_local_tagfile_TrkAmbiguityProcessorMergeComponentsList = $(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessorMergeComponentsList.make
cmt_local_tagfile_TrkAmbiguityProcessorMergeComponentsList = $(bin)$(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessorMergeComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

TrkAmbiguityProcessor_tag = $(tag)

#cmt_local_tagfile_TrkAmbiguityProcessorMergeComponentsList = $(TrkAmbiguityProcessor_tag).make
cmt_local_tagfile_TrkAmbiguityProcessorMergeComponentsList = $(bin)$(TrkAmbiguityProcessor_tag).make

endif

include $(cmt_local_tagfile_TrkAmbiguityProcessorMergeComponentsList)
#-include $(cmt_local_tagfile_TrkAmbiguityProcessorMergeComponentsList)

ifdef cmt_TrkAmbiguityProcessorMergeComponentsList_has_target_tag

cmt_final_setup_TrkAmbiguityProcessorMergeComponentsList = $(bin)setup_TrkAmbiguityProcessorMergeComponentsList.make
cmt_dependencies_in_TrkAmbiguityProcessorMergeComponentsList = $(bin)dependencies_TrkAmbiguityProcessorMergeComponentsList.in
#cmt_final_setup_TrkAmbiguityProcessorMergeComponentsList = $(bin)TrkAmbiguityProcessor_TrkAmbiguityProcessorMergeComponentsListsetup.make
cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile = $(bin)TrkAmbiguityProcessorMergeComponentsList.make

else

cmt_final_setup_TrkAmbiguityProcessorMergeComponentsList = $(bin)setup.make
cmt_dependencies_in_TrkAmbiguityProcessorMergeComponentsList = $(bin)dependencies.in
#cmt_final_setup_TrkAmbiguityProcessorMergeComponentsList = $(bin)TrkAmbiguityProcessorsetup.make
cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile = $(bin)TrkAmbiguityProcessorMergeComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)TrkAmbiguityProcessorsetup.make

#TrkAmbiguityProcessorMergeComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'TrkAmbiguityProcessorMergeComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = TrkAmbiguityProcessorMergeComponentsList/
#TrkAmbiguityProcessorMergeComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_componentslist_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.components file into a single
# <project>.components file in the (lib) install area
# If no InstallArea is present the fragment is dummy


.PHONY: TrkAmbiguityProcessorMergeComponentsList TrkAmbiguityProcessorMergeComponentsListclean

# default is already '#'
#genmap_comment_char := "'#'"

componentsListRef    := ../$(tag)/TrkAmbiguityProcessor.components

ifdef CMTINSTALLAREA
componentsListDir    := ${CMTINSTALLAREA}/$(tag)/lib
mergedComponentsList := $(componentsListDir)/$(project).components
stampComponentsList  := $(componentsListRef).stamp
else
componentsListDir    := ../$(tag)
mergedComponentsList :=
stampComponentsList  :=
endif

TrkAmbiguityProcessorMergeComponentsList :: $(stampComponentsList) $(mergedComponentsList)
	@:

.NOTPARALLEL : $(stampComponentsList) $(mergedComponentsList)

$(stampComponentsList) $(mergedComponentsList) :: $(componentsListRef)
	@echo "Running merge_componentslist  TrkAmbiguityProcessorMergeComponentsList"
	$(merge_componentslist_cmd) --do-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList)

TrkAmbiguityProcessorMergeComponentsListclean ::
	$(cleanup_silent) $(merge_componentslist_cmd) --un-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList) ;
	$(cleanup_silent) $(remove_command) $(stampComponentsList)
libTrkAmbiguityProcessor_so_dependencies = ../x86_64-slc6-gcc49-opt/libTrkAmbiguityProcessor.so
#-- start of cleanup_header --------------

clean :: TrkAmbiguityProcessorMergeComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(TrkAmbiguityProcessorMergeComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

TrkAmbiguityProcessorMergeComponentsListclean ::
#-- end of cleanup_header ---------------
