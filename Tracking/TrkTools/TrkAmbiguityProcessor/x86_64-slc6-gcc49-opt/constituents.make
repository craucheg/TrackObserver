
#-- start of constituents_header ------

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

tags      = $(tag),$(CMTEXTRATAGS)

TrkAmbiguityProcessor_tag = $(tag)

#cmt_local_tagfile = $(TrkAmbiguityProcessor_tag).make
cmt_local_tagfile = $(bin)$(TrkAmbiguityProcessor_tag).make

#-include $(cmt_local_tagfile)
include $(cmt_local_tagfile)

#cmt_local_setup = $(bin)setup$$$$.make
#cmt_local_setup = $(bin)$(package)setup$$$$.make
#cmt_final_setup = $(bin)TrkAmbiguityProcessorsetup.make
cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)$(package)setup.make

cmt_build_library_linksstamp = $(bin)cmt_build_library_links.stamp
#--------------------------------------------------------

#cmt_lock_setup = /tmp/lock$(cmt_lock_pid).make
#cmt_temp_tag = /tmp/tag$(cmt_lock_pid).make

#first :: $(cmt_local_tagfile)
#	@echo $(cmt_local_tagfile) ok
#ifndef QUICK
#first :: $(cmt_final_setup) ;
#else
#first :: ;
#endif

##	@bin=`$(cmtexe) show macro_value bin`

#$(cmt_local_tagfile) : $(cmt_lock_setup)
#	@echo "#CMT> Error: $@: No such file" >&2; exit 1
#$(cmt_local_tagfile) :
#	@echo "#CMT> Warning: $@: No such file" >&2; exit
#	@echo "#CMT> Info: $@: No need to rebuild file" >&2; exit

#$(cmt_final_setup) : $(cmt_local_tagfile) 
#	$(echo) "(constituents.make) Rebuilding $@"
#	@if test ! -d $(@D); then $(mkdir) -p $(@D); fi; \
#	  if test -f $(cmt_local_setup); then /bin/rm -f $(cmt_local_setup); fi; \
#	  trap '/bin/rm -f $(cmt_local_setup)' 0 1 2 15; \
#	  $(cmtexe) -tag=$(tags) show setup >>$(cmt_local_setup); \
#	  if test ! -f $@; then \
#	    mv $(cmt_local_setup) $@; \
#	  else \
#	    if /usr/bin/diff $(cmt_local_setup) $@ >/dev/null ; then \
#	      : ; \
#	    else \
#	      mv $(cmt_local_setup) $@; \
#	    fi; \
#	  fi

#	@/bin/echo $@ ok   

#config :: checkuses
#	@exit 0
#checkuses : ;

env.make ::
	printenv >env.make.tmp; $(cmtexe) check files env.make.tmp env.make

ifndef QUICK
all :: build_library_links ;
else
all :: $(cmt_build_library_linksstamp) ;
endif

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

dirs :: requirements
	@if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi
#	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
#	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

#requirements :
#	@if test ! -r requirements ; then echo "No requirements file" ; fi

build_library_links : dirs
	$(echo) "(constituents.make) Rebuilding library links"; \
	 $(build_library_links)
#	if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi; \
#	$(build_library_links)

$(cmt_build_library_linksstamp) : $(cmt_final_setup) $(cmt_local_tagfile) $(bin)library_links.in
	$(echo) "(constituents.make) Rebuilding library links"; \
	 $(build_library_links) -f=$(bin)library_links.in -without_cmt
	$(silent) \touch $@

ifndef PEDANTIC
.DEFAULT ::
#.DEFAULT :
	$(echo) "(constituents.make) $@: No rule for such target" >&2
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of constituents_header ------
#-- start of group ------

all_groups :: all

all :: $(all_dependencies)  $(all_pre_constituents) $(all_constituents)  $(all_post_constituents)
	$(echo) "all ok."

#	@/bin/echo " all ok."

clean :: allclean

allclean ::  $(all_constituentsclean)
	$(echo) $(all_constituentsclean)
	$(echo) "allclean ok."

#	@echo $(all_constituentsclean)
#	@/bin/echo " allclean ok."

#-- end of group ------
#-- start of group ------

all_groups :: cmt_actions

cmt_actions :: $(cmt_actions_dependencies)  $(cmt_actions_pre_constituents) $(cmt_actions_constituents)  $(cmt_actions_post_constituents)
	$(echo) "cmt_actions ok."

#	@/bin/echo " cmt_actions ok."

clean :: allclean

cmt_actionsclean ::  $(cmt_actions_constituentsclean)
	$(echo) $(cmt_actions_constituentsclean)
	$(echo) "cmt_actionsclean ok."

#	@echo $(cmt_actions_constituentsclean)
#	@/bin/echo " cmt_actionsclean ok."

#-- end of group ------
#-- start of group ------

all_groups :: rulechecker

rulechecker :: $(rulechecker_dependencies)  $(rulechecker_pre_constituents) $(rulechecker_constituents)  $(rulechecker_post_constituents)
	$(echo) "rulechecker ok."

#	@/bin/echo " rulechecker ok."

clean :: allclean

rulecheckerclean ::  $(rulechecker_constituentsclean)
	$(echo) $(rulechecker_constituentsclean)
	$(echo) "rulecheckerclean ok."

#	@echo $(rulechecker_constituentsclean)
#	@/bin/echo " rulecheckerclean ok."

#-- end of group ------
#-- start of constituent ------

cmt_TrkAmbiguityProcessor_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkAmbiguityProcessor_has_target_tag

cmt_local_tagfile_TrkAmbiguityProcessor = $(bin)$(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessor.make
cmt_final_setup_TrkAmbiguityProcessor = $(bin)setup_TrkAmbiguityProcessor.make
cmt_local_TrkAmbiguityProcessor_makefile = $(bin)TrkAmbiguityProcessor.make

TrkAmbiguityProcessor_extratags = -tag_add=target_TrkAmbiguityProcessor

else

cmt_local_tagfile_TrkAmbiguityProcessor = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_TrkAmbiguityProcessor = $(bin)setup.make
cmt_local_TrkAmbiguityProcessor_makefile = $(bin)TrkAmbiguityProcessor.make

endif

not_TrkAmbiguityProcessor_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkAmbiguityProcessor_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkAmbiguityProcessordirs :
	@if test ! -d $(bin)TrkAmbiguityProcessor; then $(mkdir) -p $(bin)TrkAmbiguityProcessor; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkAmbiguityProcessor
else
TrkAmbiguityProcessordirs : ;
endif

ifdef cmt_TrkAmbiguityProcessor_has_target_tag

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessor_makefile) : $(TrkAmbiguityProcessor_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessor.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessor_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessor_makefile) TrkAmbiguityProcessor
else
$(cmt_local_TrkAmbiguityProcessor_makefile) : $(TrkAmbiguityProcessor_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessor) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessor) ] || \
	  $(not_TrkAmbiguityProcessor_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessor.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessor_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessor_makefile) TrkAmbiguityProcessor; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessor_makefile) : $(TrkAmbiguityProcessor_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessor.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessor.in -tag=$(tags) $(TrkAmbiguityProcessor_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessor_makefile) TrkAmbiguityProcessor
else
$(cmt_local_TrkAmbiguityProcessor_makefile) : $(TrkAmbiguityProcessor_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkAmbiguityProcessor.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessor) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessor) ] || \
	  $(not_TrkAmbiguityProcessor_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessor.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessor.in -tag=$(tags) $(TrkAmbiguityProcessor_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessor_makefile) TrkAmbiguityProcessor; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessor_extratags) build constituent_makefile -out=$(cmt_local_TrkAmbiguityProcessor_makefile) TrkAmbiguityProcessor

TrkAmbiguityProcessor :: $(TrkAmbiguityProcessor_dependencies) $(cmt_local_TrkAmbiguityProcessor_makefile) dirs TrkAmbiguityProcessordirs
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessor"
	@if test -f $(cmt_local_TrkAmbiguityProcessor_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_makefile) TrkAmbiguityProcessor; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_makefile) TrkAmbiguityProcessor
	$(echo) "(constituents.make) TrkAmbiguityProcessor done"

clean :: TrkAmbiguityProcessorclean ;

TrkAmbiguityProcessorclean :: $(TrkAmbiguityProcessorclean_dependencies) ##$(cmt_local_TrkAmbiguityProcessor_makefile)
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessorclean"
	@-if test -f $(cmt_local_TrkAmbiguityProcessor_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_makefile) TrkAmbiguityProcessorclean; \
	fi
	$(echo) "(constituents.make) TrkAmbiguityProcessorclean done"
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_makefile) TrkAmbiguityProcessorclean

##	  /bin/rm -f $(cmt_local_TrkAmbiguityProcessor_makefile) $(bin)TrkAmbiguityProcessor_dependencies.make

install :: TrkAmbiguityProcessorinstall ;

TrkAmbiguityProcessorinstall :: $(TrkAmbiguityProcessor_dependencies) $(cmt_local_TrkAmbiguityProcessor_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkAmbiguityProcessor_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkAmbiguityProcessoruninstall

$(foreach d,$(TrkAmbiguityProcessor_dependencies),$(eval $(d)uninstall_dependencies += TrkAmbiguityProcessoruninstall))

TrkAmbiguityProcessoruninstall : $(TrkAmbiguityProcessoruninstall_dependencies) ##$(cmt_local_TrkAmbiguityProcessor_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkAmbiguityProcessor_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkAmbiguityProcessoruninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkAmbiguityProcessor"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkAmbiguityProcessor done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkAmbiguityProcessorConf_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkAmbiguityProcessorConf_has_target_tag

cmt_local_tagfile_TrkAmbiguityProcessorConf = $(bin)$(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessorConf.make
cmt_final_setup_TrkAmbiguityProcessorConf = $(bin)setup_TrkAmbiguityProcessorConf.make
cmt_local_TrkAmbiguityProcessorConf_makefile = $(bin)TrkAmbiguityProcessorConf.make

TrkAmbiguityProcessorConf_extratags = -tag_add=target_TrkAmbiguityProcessorConf

else

cmt_local_tagfile_TrkAmbiguityProcessorConf = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_TrkAmbiguityProcessorConf = $(bin)setup.make
cmt_local_TrkAmbiguityProcessorConf_makefile = $(bin)TrkAmbiguityProcessorConf.make

endif

not_TrkAmbiguityProcessorConf_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkAmbiguityProcessorConf_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkAmbiguityProcessorConfdirs :
	@if test ! -d $(bin)TrkAmbiguityProcessorConf; then $(mkdir) -p $(bin)TrkAmbiguityProcessorConf; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkAmbiguityProcessorConf
else
TrkAmbiguityProcessorConfdirs : ;
endif

ifdef cmt_TrkAmbiguityProcessorConf_has_target_tag

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessorConf_makefile) : $(TrkAmbiguityProcessorConf_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessorConf.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorConf_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessorConf_makefile) TrkAmbiguityProcessorConf
else
$(cmt_local_TrkAmbiguityProcessorConf_makefile) : $(TrkAmbiguityProcessorConf_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessorConf) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessorConf) ] || \
	  $(not_TrkAmbiguityProcessorConf_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessorConf.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorConf_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessorConf_makefile) TrkAmbiguityProcessorConf; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessorConf_makefile) : $(TrkAmbiguityProcessorConf_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessorConf.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessorConf.in -tag=$(tags) $(TrkAmbiguityProcessorConf_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessorConf_makefile) TrkAmbiguityProcessorConf
else
$(cmt_local_TrkAmbiguityProcessorConf_makefile) : $(TrkAmbiguityProcessorConf_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkAmbiguityProcessorConf.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessorConf) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessorConf) ] || \
	  $(not_TrkAmbiguityProcessorConf_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessorConf.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessorConf.in -tag=$(tags) $(TrkAmbiguityProcessorConf_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessorConf_makefile) TrkAmbiguityProcessorConf; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorConf_extratags) build constituent_makefile -out=$(cmt_local_TrkAmbiguityProcessorConf_makefile) TrkAmbiguityProcessorConf

TrkAmbiguityProcessorConf :: $(TrkAmbiguityProcessorConf_dependencies) $(cmt_local_TrkAmbiguityProcessorConf_makefile) dirs TrkAmbiguityProcessorConfdirs
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessorConf"
	@if test -f $(cmt_local_TrkAmbiguityProcessorConf_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConf_makefile) TrkAmbiguityProcessorConf; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConf_makefile) TrkAmbiguityProcessorConf
	$(echo) "(constituents.make) TrkAmbiguityProcessorConf done"

clean :: TrkAmbiguityProcessorConfclean ;

TrkAmbiguityProcessorConfclean :: $(TrkAmbiguityProcessorConfclean_dependencies) ##$(cmt_local_TrkAmbiguityProcessorConf_makefile)
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessorConfclean"
	@-if test -f $(cmt_local_TrkAmbiguityProcessorConf_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConf_makefile) TrkAmbiguityProcessorConfclean; \
	fi
	$(echo) "(constituents.make) TrkAmbiguityProcessorConfclean done"
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConf_makefile) TrkAmbiguityProcessorConfclean

##	  /bin/rm -f $(cmt_local_TrkAmbiguityProcessorConf_makefile) $(bin)TrkAmbiguityProcessorConf_dependencies.make

install :: TrkAmbiguityProcessorConfinstall ;

TrkAmbiguityProcessorConfinstall :: $(TrkAmbiguityProcessorConf_dependencies) $(cmt_local_TrkAmbiguityProcessorConf_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkAmbiguityProcessorConf_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConf_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConf_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkAmbiguityProcessorConfuninstall

$(foreach d,$(TrkAmbiguityProcessorConf_dependencies),$(eval $(d)uninstall_dependencies += TrkAmbiguityProcessorConfuninstall))

TrkAmbiguityProcessorConfuninstall : $(TrkAmbiguityProcessorConfuninstall_dependencies) ##$(cmt_local_TrkAmbiguityProcessorConf_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkAmbiguityProcessorConf_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConf_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConf_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkAmbiguityProcessorConfuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkAmbiguityProcessorConf"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkAmbiguityProcessorConf done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkAmbiguityProcessor_python_init_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkAmbiguityProcessor_python_init_has_target_tag

cmt_local_tagfile_TrkAmbiguityProcessor_python_init = $(bin)$(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessor_python_init.make
cmt_final_setup_TrkAmbiguityProcessor_python_init = $(bin)setup_TrkAmbiguityProcessor_python_init.make
cmt_local_TrkAmbiguityProcessor_python_init_makefile = $(bin)TrkAmbiguityProcessor_python_init.make

TrkAmbiguityProcessor_python_init_extratags = -tag_add=target_TrkAmbiguityProcessor_python_init

else

cmt_local_tagfile_TrkAmbiguityProcessor_python_init = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_TrkAmbiguityProcessor_python_init = $(bin)setup.make
cmt_local_TrkAmbiguityProcessor_python_init_makefile = $(bin)TrkAmbiguityProcessor_python_init.make

endif

not_TrkAmbiguityProcessor_python_init_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkAmbiguityProcessor_python_init_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkAmbiguityProcessor_python_initdirs :
	@if test ! -d $(bin)TrkAmbiguityProcessor_python_init; then $(mkdir) -p $(bin)TrkAmbiguityProcessor_python_init; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkAmbiguityProcessor_python_init
else
TrkAmbiguityProcessor_python_initdirs : ;
endif

ifdef cmt_TrkAmbiguityProcessor_python_init_has_target_tag

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessor_python_init_makefile) : $(TrkAmbiguityProcessor_python_init_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessor_python_init.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessor_python_init_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessor_python_init_makefile) TrkAmbiguityProcessor_python_init
else
$(cmt_local_TrkAmbiguityProcessor_python_init_makefile) : $(TrkAmbiguityProcessor_python_init_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessor_python_init) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessor_python_init) ] || \
	  $(not_TrkAmbiguityProcessor_python_init_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessor_python_init.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessor_python_init_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessor_python_init_makefile) TrkAmbiguityProcessor_python_init; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessor_python_init_makefile) : $(TrkAmbiguityProcessor_python_init_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessor_python_init.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessor_python_init.in -tag=$(tags) $(TrkAmbiguityProcessor_python_init_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessor_python_init_makefile) TrkAmbiguityProcessor_python_init
else
$(cmt_local_TrkAmbiguityProcessor_python_init_makefile) : $(TrkAmbiguityProcessor_python_init_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkAmbiguityProcessor_python_init.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessor_python_init) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessor_python_init) ] || \
	  $(not_TrkAmbiguityProcessor_python_init_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessor_python_init.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessor_python_init.in -tag=$(tags) $(TrkAmbiguityProcessor_python_init_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessor_python_init_makefile) TrkAmbiguityProcessor_python_init; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessor_python_init_extratags) build constituent_makefile -out=$(cmt_local_TrkAmbiguityProcessor_python_init_makefile) TrkAmbiguityProcessor_python_init

TrkAmbiguityProcessor_python_init :: $(TrkAmbiguityProcessor_python_init_dependencies) $(cmt_local_TrkAmbiguityProcessor_python_init_makefile) dirs TrkAmbiguityProcessor_python_initdirs
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessor_python_init"
	@if test -f $(cmt_local_TrkAmbiguityProcessor_python_init_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_python_init_makefile) TrkAmbiguityProcessor_python_init; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_python_init_makefile) TrkAmbiguityProcessor_python_init
	$(echo) "(constituents.make) TrkAmbiguityProcessor_python_init done"

clean :: TrkAmbiguityProcessor_python_initclean ;

TrkAmbiguityProcessor_python_initclean :: $(TrkAmbiguityProcessor_python_initclean_dependencies) ##$(cmt_local_TrkAmbiguityProcessor_python_init_makefile)
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessor_python_initclean"
	@-if test -f $(cmt_local_TrkAmbiguityProcessor_python_init_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_python_init_makefile) TrkAmbiguityProcessor_python_initclean; \
	fi
	$(echo) "(constituents.make) TrkAmbiguityProcessor_python_initclean done"
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_python_init_makefile) TrkAmbiguityProcessor_python_initclean

##	  /bin/rm -f $(cmt_local_TrkAmbiguityProcessor_python_init_makefile) $(bin)TrkAmbiguityProcessor_python_init_dependencies.make

install :: TrkAmbiguityProcessor_python_initinstall ;

TrkAmbiguityProcessor_python_initinstall :: $(TrkAmbiguityProcessor_python_init_dependencies) $(cmt_local_TrkAmbiguityProcessor_python_init_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkAmbiguityProcessor_python_init_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_python_init_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_python_init_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkAmbiguityProcessor_python_inituninstall

$(foreach d,$(TrkAmbiguityProcessor_python_init_dependencies),$(eval $(d)uninstall_dependencies += TrkAmbiguityProcessor_python_inituninstall))

TrkAmbiguityProcessor_python_inituninstall : $(TrkAmbiguityProcessor_python_inituninstall_dependencies) ##$(cmt_local_TrkAmbiguityProcessor_python_init_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkAmbiguityProcessor_python_init_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_python_init_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_python_init_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkAmbiguityProcessor_python_inituninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkAmbiguityProcessor_python_init"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkAmbiguityProcessor_python_init done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkAmbiguityProcessorConfDbMerge_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkAmbiguityProcessorConfDbMerge_has_target_tag

cmt_local_tagfile_TrkAmbiguityProcessorConfDbMerge = $(bin)$(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessorConfDbMerge.make
cmt_final_setup_TrkAmbiguityProcessorConfDbMerge = $(bin)setup_TrkAmbiguityProcessorConfDbMerge.make
cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile = $(bin)TrkAmbiguityProcessorConfDbMerge.make

TrkAmbiguityProcessorConfDbMerge_extratags = -tag_add=target_TrkAmbiguityProcessorConfDbMerge

else

cmt_local_tagfile_TrkAmbiguityProcessorConfDbMerge = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_TrkAmbiguityProcessorConfDbMerge = $(bin)setup.make
cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile = $(bin)TrkAmbiguityProcessorConfDbMerge.make

endif

not_TrkAmbiguityProcessorConfDbMerge_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkAmbiguityProcessorConfDbMerge_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkAmbiguityProcessorConfDbMergedirs :
	@if test ! -d $(bin)TrkAmbiguityProcessorConfDbMerge; then $(mkdir) -p $(bin)TrkAmbiguityProcessorConfDbMerge; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkAmbiguityProcessorConfDbMerge
else
TrkAmbiguityProcessorConfDbMergedirs : ;
endif

ifdef cmt_TrkAmbiguityProcessorConfDbMerge_has_target_tag

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) : $(TrkAmbiguityProcessorConfDbMerge_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessorConfDbMerge.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorConfDbMerge_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) TrkAmbiguityProcessorConfDbMerge
else
$(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) : $(TrkAmbiguityProcessorConfDbMerge_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessorConfDbMerge) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessorConfDbMerge) ] || \
	  $(not_TrkAmbiguityProcessorConfDbMerge_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessorConfDbMerge.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorConfDbMerge_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) TrkAmbiguityProcessorConfDbMerge; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) : $(TrkAmbiguityProcessorConfDbMerge_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessorConfDbMerge.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessorConfDbMerge.in -tag=$(tags) $(TrkAmbiguityProcessorConfDbMerge_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) TrkAmbiguityProcessorConfDbMerge
else
$(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) : $(TrkAmbiguityProcessorConfDbMerge_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkAmbiguityProcessorConfDbMerge.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessorConfDbMerge) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessorConfDbMerge) ] || \
	  $(not_TrkAmbiguityProcessorConfDbMerge_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessorConfDbMerge.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessorConfDbMerge.in -tag=$(tags) $(TrkAmbiguityProcessorConfDbMerge_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) TrkAmbiguityProcessorConfDbMerge; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorConfDbMerge_extratags) build constituent_makefile -out=$(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) TrkAmbiguityProcessorConfDbMerge

TrkAmbiguityProcessorConfDbMerge :: $(TrkAmbiguityProcessorConfDbMerge_dependencies) $(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) dirs TrkAmbiguityProcessorConfDbMergedirs
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessorConfDbMerge"
	@if test -f $(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) TrkAmbiguityProcessorConfDbMerge; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) TrkAmbiguityProcessorConfDbMerge
	$(echo) "(constituents.make) TrkAmbiguityProcessorConfDbMerge done"

clean :: TrkAmbiguityProcessorConfDbMergeclean ;

TrkAmbiguityProcessorConfDbMergeclean :: $(TrkAmbiguityProcessorConfDbMergeclean_dependencies) ##$(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile)
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessorConfDbMergeclean"
	@-if test -f $(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) TrkAmbiguityProcessorConfDbMergeclean; \
	fi
	$(echo) "(constituents.make) TrkAmbiguityProcessorConfDbMergeclean done"
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) TrkAmbiguityProcessorConfDbMergeclean

##	  /bin/rm -f $(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) $(bin)TrkAmbiguityProcessorConfDbMerge_dependencies.make

install :: TrkAmbiguityProcessorConfDbMergeinstall ;

TrkAmbiguityProcessorConfDbMergeinstall :: $(TrkAmbiguityProcessorConfDbMerge_dependencies) $(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkAmbiguityProcessorConfDbMergeuninstall

$(foreach d,$(TrkAmbiguityProcessorConfDbMerge_dependencies),$(eval $(d)uninstall_dependencies += TrkAmbiguityProcessorConfDbMergeuninstall))

TrkAmbiguityProcessorConfDbMergeuninstall : $(TrkAmbiguityProcessorConfDbMergeuninstall_dependencies) ##$(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorConfDbMerge_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkAmbiguityProcessorConfDbMergeuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkAmbiguityProcessorConfDbMerge"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkAmbiguityProcessorConfDbMerge done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkAmbiguityProcessorComponentsList_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkAmbiguityProcessorComponentsList_has_target_tag

cmt_local_tagfile_TrkAmbiguityProcessorComponentsList = $(bin)$(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessorComponentsList.make
cmt_final_setup_TrkAmbiguityProcessorComponentsList = $(bin)setup_TrkAmbiguityProcessorComponentsList.make
cmt_local_TrkAmbiguityProcessorComponentsList_makefile = $(bin)TrkAmbiguityProcessorComponentsList.make

TrkAmbiguityProcessorComponentsList_extratags = -tag_add=target_TrkAmbiguityProcessorComponentsList

else

cmt_local_tagfile_TrkAmbiguityProcessorComponentsList = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_TrkAmbiguityProcessorComponentsList = $(bin)setup.make
cmt_local_TrkAmbiguityProcessorComponentsList_makefile = $(bin)TrkAmbiguityProcessorComponentsList.make

endif

not_TrkAmbiguityProcessorComponentsList_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkAmbiguityProcessorComponentsList_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkAmbiguityProcessorComponentsListdirs :
	@if test ! -d $(bin)TrkAmbiguityProcessorComponentsList; then $(mkdir) -p $(bin)TrkAmbiguityProcessorComponentsList; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkAmbiguityProcessorComponentsList
else
TrkAmbiguityProcessorComponentsListdirs : ;
endif

ifdef cmt_TrkAmbiguityProcessorComponentsList_has_target_tag

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) : $(TrkAmbiguityProcessorComponentsList_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessorComponentsList.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorComponentsList_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) TrkAmbiguityProcessorComponentsList
else
$(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) : $(TrkAmbiguityProcessorComponentsList_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessorComponentsList) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessorComponentsList) ] || \
	  $(not_TrkAmbiguityProcessorComponentsList_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessorComponentsList.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorComponentsList_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) TrkAmbiguityProcessorComponentsList; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) : $(TrkAmbiguityProcessorComponentsList_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessorComponentsList.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessorComponentsList.in -tag=$(tags) $(TrkAmbiguityProcessorComponentsList_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) TrkAmbiguityProcessorComponentsList
else
$(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) : $(TrkAmbiguityProcessorComponentsList_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkAmbiguityProcessorComponentsList.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessorComponentsList) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessorComponentsList) ] || \
	  $(not_TrkAmbiguityProcessorComponentsList_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessorComponentsList.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessorComponentsList.in -tag=$(tags) $(TrkAmbiguityProcessorComponentsList_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) TrkAmbiguityProcessorComponentsList; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorComponentsList_extratags) build constituent_makefile -out=$(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) TrkAmbiguityProcessorComponentsList

TrkAmbiguityProcessorComponentsList :: $(TrkAmbiguityProcessorComponentsList_dependencies) $(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) dirs TrkAmbiguityProcessorComponentsListdirs
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessorComponentsList"
	@if test -f $(cmt_local_TrkAmbiguityProcessorComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) TrkAmbiguityProcessorComponentsList; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) TrkAmbiguityProcessorComponentsList
	$(echo) "(constituents.make) TrkAmbiguityProcessorComponentsList done"

clean :: TrkAmbiguityProcessorComponentsListclean ;

TrkAmbiguityProcessorComponentsListclean :: $(TrkAmbiguityProcessorComponentsListclean_dependencies) ##$(cmt_local_TrkAmbiguityProcessorComponentsList_makefile)
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessorComponentsListclean"
	@-if test -f $(cmt_local_TrkAmbiguityProcessorComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) TrkAmbiguityProcessorComponentsListclean; \
	fi
	$(echo) "(constituents.make) TrkAmbiguityProcessorComponentsListclean done"
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) TrkAmbiguityProcessorComponentsListclean

##	  /bin/rm -f $(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) $(bin)TrkAmbiguityProcessorComponentsList_dependencies.make

install :: TrkAmbiguityProcessorComponentsListinstall ;

TrkAmbiguityProcessorComponentsListinstall :: $(TrkAmbiguityProcessorComponentsList_dependencies) $(cmt_local_TrkAmbiguityProcessorComponentsList_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkAmbiguityProcessorComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkAmbiguityProcessorComponentsListuninstall

$(foreach d,$(TrkAmbiguityProcessorComponentsList_dependencies),$(eval $(d)uninstall_dependencies += TrkAmbiguityProcessorComponentsListuninstall))

TrkAmbiguityProcessorComponentsListuninstall : $(TrkAmbiguityProcessorComponentsListuninstall_dependencies) ##$(cmt_local_TrkAmbiguityProcessorComponentsList_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkAmbiguityProcessorComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorComponentsList_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkAmbiguityProcessorComponentsListuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkAmbiguityProcessorComponentsList"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkAmbiguityProcessorComponentsList done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkAmbiguityProcessorMergeComponentsList_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkAmbiguityProcessorMergeComponentsList_has_target_tag

cmt_local_tagfile_TrkAmbiguityProcessorMergeComponentsList = $(bin)$(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessorMergeComponentsList.make
cmt_final_setup_TrkAmbiguityProcessorMergeComponentsList = $(bin)setup_TrkAmbiguityProcessorMergeComponentsList.make
cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile = $(bin)TrkAmbiguityProcessorMergeComponentsList.make

TrkAmbiguityProcessorMergeComponentsList_extratags = -tag_add=target_TrkAmbiguityProcessorMergeComponentsList

else

cmt_local_tagfile_TrkAmbiguityProcessorMergeComponentsList = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_TrkAmbiguityProcessorMergeComponentsList = $(bin)setup.make
cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile = $(bin)TrkAmbiguityProcessorMergeComponentsList.make

endif

not_TrkAmbiguityProcessorMergeComponentsList_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkAmbiguityProcessorMergeComponentsList_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkAmbiguityProcessorMergeComponentsListdirs :
	@if test ! -d $(bin)TrkAmbiguityProcessorMergeComponentsList; then $(mkdir) -p $(bin)TrkAmbiguityProcessorMergeComponentsList; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkAmbiguityProcessorMergeComponentsList
else
TrkAmbiguityProcessorMergeComponentsListdirs : ;
endif

ifdef cmt_TrkAmbiguityProcessorMergeComponentsList_has_target_tag

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) : $(TrkAmbiguityProcessorMergeComponentsList_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessorMergeComponentsList.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorMergeComponentsList_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) TrkAmbiguityProcessorMergeComponentsList
else
$(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) : $(TrkAmbiguityProcessorMergeComponentsList_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessorMergeComponentsList) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessorMergeComponentsList) ] || \
	  $(not_TrkAmbiguityProcessorMergeComponentsList_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessorMergeComponentsList.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorMergeComponentsList_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) TrkAmbiguityProcessorMergeComponentsList; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) : $(TrkAmbiguityProcessorMergeComponentsList_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessorMergeComponentsList.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessorMergeComponentsList.in -tag=$(tags) $(TrkAmbiguityProcessorMergeComponentsList_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) TrkAmbiguityProcessorMergeComponentsList
else
$(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) : $(TrkAmbiguityProcessorMergeComponentsList_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkAmbiguityProcessorMergeComponentsList.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessorMergeComponentsList) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessorMergeComponentsList) ] || \
	  $(not_TrkAmbiguityProcessorMergeComponentsList_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessorMergeComponentsList.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessorMergeComponentsList.in -tag=$(tags) $(TrkAmbiguityProcessorMergeComponentsList_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) TrkAmbiguityProcessorMergeComponentsList; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorMergeComponentsList_extratags) build constituent_makefile -out=$(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) TrkAmbiguityProcessorMergeComponentsList

TrkAmbiguityProcessorMergeComponentsList :: $(TrkAmbiguityProcessorMergeComponentsList_dependencies) $(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) dirs TrkAmbiguityProcessorMergeComponentsListdirs
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessorMergeComponentsList"
	@if test -f $(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) TrkAmbiguityProcessorMergeComponentsList; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) TrkAmbiguityProcessorMergeComponentsList
	$(echo) "(constituents.make) TrkAmbiguityProcessorMergeComponentsList done"

clean :: TrkAmbiguityProcessorMergeComponentsListclean ;

TrkAmbiguityProcessorMergeComponentsListclean :: $(TrkAmbiguityProcessorMergeComponentsListclean_dependencies) ##$(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile)
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessorMergeComponentsListclean"
	@-if test -f $(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) TrkAmbiguityProcessorMergeComponentsListclean; \
	fi
	$(echo) "(constituents.make) TrkAmbiguityProcessorMergeComponentsListclean done"
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) TrkAmbiguityProcessorMergeComponentsListclean

##	  /bin/rm -f $(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) $(bin)TrkAmbiguityProcessorMergeComponentsList_dependencies.make

install :: TrkAmbiguityProcessorMergeComponentsListinstall ;

TrkAmbiguityProcessorMergeComponentsListinstall :: $(TrkAmbiguityProcessorMergeComponentsList_dependencies) $(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkAmbiguityProcessorMergeComponentsListuninstall

$(foreach d,$(TrkAmbiguityProcessorMergeComponentsList_dependencies),$(eval $(d)uninstall_dependencies += TrkAmbiguityProcessorMergeComponentsListuninstall))

TrkAmbiguityProcessorMergeComponentsListuninstall : $(TrkAmbiguityProcessorMergeComponentsListuninstall_dependencies) ##$(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorMergeComponentsList_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkAmbiguityProcessorMergeComponentsListuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkAmbiguityProcessorMergeComponentsList"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkAmbiguityProcessorMergeComponentsList done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkAmbiguityProcessor_optdebug_library_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkAmbiguityProcessor_optdebug_library_has_target_tag

cmt_local_tagfile_TrkAmbiguityProcessor_optdebug_library = $(bin)$(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessor_optdebug_library.make
cmt_final_setup_TrkAmbiguityProcessor_optdebug_library = $(bin)setup_TrkAmbiguityProcessor_optdebug_library.make
cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile = $(bin)TrkAmbiguityProcessor_optdebug_library.make

TrkAmbiguityProcessor_optdebug_library_extratags = -tag_add=target_TrkAmbiguityProcessor_optdebug_library

else

cmt_local_tagfile_TrkAmbiguityProcessor_optdebug_library = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_TrkAmbiguityProcessor_optdebug_library = $(bin)setup.make
cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile = $(bin)TrkAmbiguityProcessor_optdebug_library.make

endif

not_TrkAmbiguityProcessor_optdebug_library_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkAmbiguityProcessor_optdebug_library_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkAmbiguityProcessor_optdebug_librarydirs :
	@if test ! -d $(bin)TrkAmbiguityProcessor_optdebug_library; then $(mkdir) -p $(bin)TrkAmbiguityProcessor_optdebug_library; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkAmbiguityProcessor_optdebug_library
else
TrkAmbiguityProcessor_optdebug_librarydirs : ;
endif

ifdef cmt_TrkAmbiguityProcessor_optdebug_library_has_target_tag

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) : $(TrkAmbiguityProcessor_optdebug_library_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessor_optdebug_library.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessor_optdebug_library_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) TrkAmbiguityProcessor_optdebug_library
else
$(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) : $(TrkAmbiguityProcessor_optdebug_library_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessor_optdebug_library) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessor_optdebug_library) ] || \
	  $(not_TrkAmbiguityProcessor_optdebug_library_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessor_optdebug_library.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessor_optdebug_library_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) TrkAmbiguityProcessor_optdebug_library; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) : $(TrkAmbiguityProcessor_optdebug_library_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessor_optdebug_library.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessor_optdebug_library.in -tag=$(tags) $(TrkAmbiguityProcessor_optdebug_library_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) TrkAmbiguityProcessor_optdebug_library
else
$(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) : $(TrkAmbiguityProcessor_optdebug_library_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkAmbiguityProcessor_optdebug_library.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessor_optdebug_library) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessor_optdebug_library) ] || \
	  $(not_TrkAmbiguityProcessor_optdebug_library_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessor_optdebug_library.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessor_optdebug_library.in -tag=$(tags) $(TrkAmbiguityProcessor_optdebug_library_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) TrkAmbiguityProcessor_optdebug_library; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessor_optdebug_library_extratags) build constituent_makefile -out=$(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) TrkAmbiguityProcessor_optdebug_library

TrkAmbiguityProcessor_optdebug_library :: $(TrkAmbiguityProcessor_optdebug_library_dependencies) $(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) dirs TrkAmbiguityProcessor_optdebug_librarydirs
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessor_optdebug_library"
	@if test -f $(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) TrkAmbiguityProcessor_optdebug_library; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) TrkAmbiguityProcessor_optdebug_library
	$(echo) "(constituents.make) TrkAmbiguityProcessor_optdebug_library done"

clean :: TrkAmbiguityProcessor_optdebug_libraryclean ;

TrkAmbiguityProcessor_optdebug_libraryclean :: $(TrkAmbiguityProcessor_optdebug_libraryclean_dependencies) ##$(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile)
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessor_optdebug_libraryclean"
	@-if test -f $(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) TrkAmbiguityProcessor_optdebug_libraryclean; \
	fi
	$(echo) "(constituents.make) TrkAmbiguityProcessor_optdebug_libraryclean done"
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) TrkAmbiguityProcessor_optdebug_libraryclean

##	  /bin/rm -f $(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) $(bin)TrkAmbiguityProcessor_optdebug_library_dependencies.make

install :: TrkAmbiguityProcessor_optdebug_libraryinstall ;

TrkAmbiguityProcessor_optdebug_libraryinstall :: $(TrkAmbiguityProcessor_optdebug_library_dependencies) $(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkAmbiguityProcessor_optdebug_libraryuninstall

$(foreach d,$(TrkAmbiguityProcessor_optdebug_library_dependencies),$(eval $(d)uninstall_dependencies += TrkAmbiguityProcessor_optdebug_libraryuninstall))

TrkAmbiguityProcessor_optdebug_libraryuninstall : $(TrkAmbiguityProcessor_optdebug_libraryuninstall_dependencies) ##$(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_optdebug_library_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkAmbiguityProcessor_optdebug_libraryuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkAmbiguityProcessor_optdebug_library"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkAmbiguityProcessor_optdebug_library done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkAmbiguityProcessorCLIDDB_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkAmbiguityProcessorCLIDDB_has_target_tag

cmt_local_tagfile_TrkAmbiguityProcessorCLIDDB = $(bin)$(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessorCLIDDB.make
cmt_final_setup_TrkAmbiguityProcessorCLIDDB = $(bin)setup_TrkAmbiguityProcessorCLIDDB.make
cmt_local_TrkAmbiguityProcessorCLIDDB_makefile = $(bin)TrkAmbiguityProcessorCLIDDB.make

TrkAmbiguityProcessorCLIDDB_extratags = -tag_add=target_TrkAmbiguityProcessorCLIDDB

else

cmt_local_tagfile_TrkAmbiguityProcessorCLIDDB = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_TrkAmbiguityProcessorCLIDDB = $(bin)setup.make
cmt_local_TrkAmbiguityProcessorCLIDDB_makefile = $(bin)TrkAmbiguityProcessorCLIDDB.make

endif

not_TrkAmbiguityProcessorCLIDDB_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkAmbiguityProcessorCLIDDB_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkAmbiguityProcessorCLIDDBdirs :
	@if test ! -d $(bin)TrkAmbiguityProcessorCLIDDB; then $(mkdir) -p $(bin)TrkAmbiguityProcessorCLIDDB; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkAmbiguityProcessorCLIDDB
else
TrkAmbiguityProcessorCLIDDBdirs : ;
endif

ifdef cmt_TrkAmbiguityProcessorCLIDDB_has_target_tag

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) : $(TrkAmbiguityProcessorCLIDDB_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessorCLIDDB.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorCLIDDB_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) TrkAmbiguityProcessorCLIDDB
else
$(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) : $(TrkAmbiguityProcessorCLIDDB_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessorCLIDDB) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessorCLIDDB) ] || \
	  $(not_TrkAmbiguityProcessorCLIDDB_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessorCLIDDB.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorCLIDDB_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) TrkAmbiguityProcessorCLIDDB; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) : $(TrkAmbiguityProcessorCLIDDB_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessorCLIDDB.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessorCLIDDB.in -tag=$(tags) $(TrkAmbiguityProcessorCLIDDB_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) TrkAmbiguityProcessorCLIDDB
else
$(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) : $(TrkAmbiguityProcessorCLIDDB_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkAmbiguityProcessorCLIDDB.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessorCLIDDB) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessorCLIDDB) ] || \
	  $(not_TrkAmbiguityProcessorCLIDDB_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessorCLIDDB.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessorCLIDDB.in -tag=$(tags) $(TrkAmbiguityProcessorCLIDDB_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) TrkAmbiguityProcessorCLIDDB; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorCLIDDB_extratags) build constituent_makefile -out=$(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) TrkAmbiguityProcessorCLIDDB

TrkAmbiguityProcessorCLIDDB :: $(TrkAmbiguityProcessorCLIDDB_dependencies) $(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) dirs TrkAmbiguityProcessorCLIDDBdirs
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessorCLIDDB"
	@if test -f $(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) TrkAmbiguityProcessorCLIDDB; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) TrkAmbiguityProcessorCLIDDB
	$(echo) "(constituents.make) TrkAmbiguityProcessorCLIDDB done"

clean :: TrkAmbiguityProcessorCLIDDBclean ;

TrkAmbiguityProcessorCLIDDBclean :: $(TrkAmbiguityProcessorCLIDDBclean_dependencies) ##$(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile)
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessorCLIDDBclean"
	@-if test -f $(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) TrkAmbiguityProcessorCLIDDBclean; \
	fi
	$(echo) "(constituents.make) TrkAmbiguityProcessorCLIDDBclean done"
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) TrkAmbiguityProcessorCLIDDBclean

##	  /bin/rm -f $(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) $(bin)TrkAmbiguityProcessorCLIDDB_dependencies.make

install :: TrkAmbiguityProcessorCLIDDBinstall ;

TrkAmbiguityProcessorCLIDDBinstall :: $(TrkAmbiguityProcessorCLIDDB_dependencies) $(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkAmbiguityProcessorCLIDDBuninstall

$(foreach d,$(TrkAmbiguityProcessorCLIDDB_dependencies),$(eval $(d)uninstall_dependencies += TrkAmbiguityProcessorCLIDDBuninstall))

TrkAmbiguityProcessorCLIDDBuninstall : $(TrkAmbiguityProcessorCLIDDBuninstall_dependencies) ##$(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorCLIDDB_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkAmbiguityProcessorCLIDDBuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkAmbiguityProcessorCLIDDB"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkAmbiguityProcessorCLIDDB done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkAmbiguityProcessorrchk_has_target_tag = 1

#--------------------------------------

ifdef cmt_TrkAmbiguityProcessorrchk_has_target_tag

cmt_local_tagfile_TrkAmbiguityProcessorrchk = $(bin)$(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessorrchk.make
cmt_final_setup_TrkAmbiguityProcessorrchk = $(bin)setup_TrkAmbiguityProcessorrchk.make
cmt_local_TrkAmbiguityProcessorrchk_makefile = $(bin)TrkAmbiguityProcessorrchk.make

TrkAmbiguityProcessorrchk_extratags = -tag_add=target_TrkAmbiguityProcessorrchk

else

cmt_local_tagfile_TrkAmbiguityProcessorrchk = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_TrkAmbiguityProcessorrchk = $(bin)setup.make
cmt_local_TrkAmbiguityProcessorrchk_makefile = $(bin)TrkAmbiguityProcessorrchk.make

endif

not_TrkAmbiguityProcessorrchk_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkAmbiguityProcessorrchk_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkAmbiguityProcessorrchkdirs :
	@if test ! -d $(bin)TrkAmbiguityProcessorrchk; then $(mkdir) -p $(bin)TrkAmbiguityProcessorrchk; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkAmbiguityProcessorrchk
else
TrkAmbiguityProcessorrchkdirs : ;
endif

ifdef cmt_TrkAmbiguityProcessorrchk_has_target_tag

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessorrchk_makefile) : $(TrkAmbiguityProcessorrchk_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessorrchk.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorrchk_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessorrchk_makefile) TrkAmbiguityProcessorrchk
else
$(cmt_local_TrkAmbiguityProcessorrchk_makefile) : $(TrkAmbiguityProcessorrchk_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessorrchk) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessorrchk) ] || \
	  $(not_TrkAmbiguityProcessorrchk_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessorrchk.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorrchk_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessorrchk_makefile) TrkAmbiguityProcessorrchk; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessorrchk_makefile) : $(TrkAmbiguityProcessorrchk_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessorrchk.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessorrchk.in -tag=$(tags) $(TrkAmbiguityProcessorrchk_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessorrchk_makefile) TrkAmbiguityProcessorrchk
else
$(cmt_local_TrkAmbiguityProcessorrchk_makefile) : $(TrkAmbiguityProcessorrchk_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkAmbiguityProcessorrchk.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessorrchk) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessorrchk) ] || \
	  $(not_TrkAmbiguityProcessorrchk_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessorrchk.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessorrchk.in -tag=$(tags) $(TrkAmbiguityProcessorrchk_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessorrchk_makefile) TrkAmbiguityProcessorrchk; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessorrchk_extratags) build constituent_makefile -out=$(cmt_local_TrkAmbiguityProcessorrchk_makefile) TrkAmbiguityProcessorrchk

TrkAmbiguityProcessorrchk :: $(TrkAmbiguityProcessorrchk_dependencies) $(cmt_local_TrkAmbiguityProcessorrchk_makefile) dirs TrkAmbiguityProcessorrchkdirs
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessorrchk"
	@if test -f $(cmt_local_TrkAmbiguityProcessorrchk_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorrchk_makefile) TrkAmbiguityProcessorrchk; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorrchk_makefile) TrkAmbiguityProcessorrchk
	$(echo) "(constituents.make) TrkAmbiguityProcessorrchk done"

clean :: TrkAmbiguityProcessorrchkclean ;

TrkAmbiguityProcessorrchkclean :: $(TrkAmbiguityProcessorrchkclean_dependencies) ##$(cmt_local_TrkAmbiguityProcessorrchk_makefile)
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessorrchkclean"
	@-if test -f $(cmt_local_TrkAmbiguityProcessorrchk_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorrchk_makefile) TrkAmbiguityProcessorrchkclean; \
	fi
	$(echo) "(constituents.make) TrkAmbiguityProcessorrchkclean done"
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorrchk_makefile) TrkAmbiguityProcessorrchkclean

##	  /bin/rm -f $(cmt_local_TrkAmbiguityProcessorrchk_makefile) $(bin)TrkAmbiguityProcessorrchk_dependencies.make

install :: TrkAmbiguityProcessorrchkinstall ;

TrkAmbiguityProcessorrchkinstall :: $(TrkAmbiguityProcessorrchk_dependencies) $(cmt_local_TrkAmbiguityProcessorrchk_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkAmbiguityProcessorrchk_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorrchk_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorrchk_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkAmbiguityProcessorrchkuninstall

$(foreach d,$(TrkAmbiguityProcessorrchk_dependencies),$(eval $(d)uninstall_dependencies += TrkAmbiguityProcessorrchkuninstall))

TrkAmbiguityProcessorrchkuninstall : $(TrkAmbiguityProcessorrchkuninstall_dependencies) ##$(cmt_local_TrkAmbiguityProcessorrchk_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkAmbiguityProcessorrchk_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessorrchk_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessorrchk_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkAmbiguityProcessorrchkuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkAmbiguityProcessorrchk"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkAmbiguityProcessorrchk done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_install_root_include_path_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_install_root_include_path_has_target_tag

cmt_local_tagfile_install_root_include_path = $(bin)$(TrkAmbiguityProcessor_tag)_install_root_include_path.make
cmt_final_setup_install_root_include_path = $(bin)setup_install_root_include_path.make
cmt_local_install_root_include_path_makefile = $(bin)install_root_include_path.make

install_root_include_path_extratags = -tag_add=target_install_root_include_path

else

cmt_local_tagfile_install_root_include_path = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_install_root_include_path = $(bin)setup.make
cmt_local_install_root_include_path_makefile = $(bin)install_root_include_path.make

endif

not_install_root_include_path_dependencies = { n=0; for p in $?; do m=0; for d in $(install_root_include_path_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
install_root_include_pathdirs :
	@if test ! -d $(bin)install_root_include_path; then $(mkdir) -p $(bin)install_root_include_path; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)install_root_include_path
else
install_root_include_pathdirs : ;
endif

ifdef cmt_install_root_include_path_has_target_tag

ifndef QUICK
$(cmt_local_install_root_include_path_makefile) : $(install_root_include_path_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_root_include_path.make"; \
	  $(cmtexe) -tag=$(tags) $(install_root_include_path_extratags) build constituent_config -out=$(cmt_local_install_root_include_path_makefile) install_root_include_path
else
$(cmt_local_install_root_include_path_makefile) : $(install_root_include_path_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_root_include_path) ] || \
	  [ ! -f $(cmt_final_setup_install_root_include_path) ] || \
	  $(not_install_root_include_path_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_root_include_path.make"; \
	  $(cmtexe) -tag=$(tags) $(install_root_include_path_extratags) build constituent_config -out=$(cmt_local_install_root_include_path_makefile) install_root_include_path; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_install_root_include_path_makefile) : $(install_root_include_path_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_root_include_path.make"; \
	  $(cmtexe) -f=$(bin)install_root_include_path.in -tag=$(tags) $(install_root_include_path_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_root_include_path_makefile) install_root_include_path
else
$(cmt_local_install_root_include_path_makefile) : $(install_root_include_path_dependencies) $(cmt_build_library_linksstamp) $(bin)install_root_include_path.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_root_include_path) ] || \
	  [ ! -f $(cmt_final_setup_install_root_include_path) ] || \
	  $(not_install_root_include_path_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_root_include_path.make"; \
	  $(cmtexe) -f=$(bin)install_root_include_path.in -tag=$(tags) $(install_root_include_path_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_root_include_path_makefile) install_root_include_path; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(install_root_include_path_extratags) build constituent_makefile -out=$(cmt_local_install_root_include_path_makefile) install_root_include_path

install_root_include_path :: $(install_root_include_path_dependencies) $(cmt_local_install_root_include_path_makefile) dirs install_root_include_pathdirs
	$(echo) "(constituents.make) Starting install_root_include_path"
	@if test -f $(cmt_local_install_root_include_path_makefile); then \
	  $(MAKE) -f $(cmt_local_install_root_include_path_makefile) install_root_include_path; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_root_include_path_makefile) install_root_include_path
	$(echo) "(constituents.make) install_root_include_path done"

clean :: install_root_include_pathclean ;

install_root_include_pathclean :: $(install_root_include_pathclean_dependencies) ##$(cmt_local_install_root_include_path_makefile)
	$(echo) "(constituents.make) Starting install_root_include_pathclean"
	@-if test -f $(cmt_local_install_root_include_path_makefile); then \
	  $(MAKE) -f $(cmt_local_install_root_include_path_makefile) install_root_include_pathclean; \
	fi
	$(echo) "(constituents.make) install_root_include_pathclean done"
#	@-$(MAKE) -f $(cmt_local_install_root_include_path_makefile) install_root_include_pathclean

##	  /bin/rm -f $(cmt_local_install_root_include_path_makefile) $(bin)install_root_include_path_dependencies.make

install :: install_root_include_pathinstall ;

install_root_include_pathinstall :: $(install_root_include_path_dependencies) $(cmt_local_install_root_include_path_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_install_root_include_path_makefile); then \
	  $(MAKE) -f $(cmt_local_install_root_include_path_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_install_root_include_path_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : install_root_include_pathuninstall

$(foreach d,$(install_root_include_path_dependencies),$(eval $(d)uninstall_dependencies += install_root_include_pathuninstall))

install_root_include_pathuninstall : $(install_root_include_pathuninstall_dependencies) ##$(cmt_local_install_root_include_path_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_install_root_include_path_makefile); then \
	  $(MAKE) -f $(cmt_local_install_root_include_path_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_root_include_path_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: install_root_include_pathuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ install_root_include_path"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ install_root_include_path done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_install_includes_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_install_includes_has_target_tag

cmt_local_tagfile_install_includes = $(bin)$(TrkAmbiguityProcessor_tag)_install_includes.make
cmt_final_setup_install_includes = $(bin)setup_install_includes.make
cmt_local_install_includes_makefile = $(bin)install_includes.make

install_includes_extratags = -tag_add=target_install_includes

else

cmt_local_tagfile_install_includes = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_install_includes = $(bin)setup.make
cmt_local_install_includes_makefile = $(bin)install_includes.make

endif

not_install_includes_dependencies = { n=0; for p in $?; do m=0; for d in $(install_includes_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
install_includesdirs :
	@if test ! -d $(bin)install_includes; then $(mkdir) -p $(bin)install_includes; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)install_includes
else
install_includesdirs : ;
endif

ifdef cmt_install_includes_has_target_tag

ifndef QUICK
$(cmt_local_install_includes_makefile) : $(install_includes_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_includes.make"; \
	  $(cmtexe) -tag=$(tags) $(install_includes_extratags) build constituent_config -out=$(cmt_local_install_includes_makefile) install_includes
else
$(cmt_local_install_includes_makefile) : $(install_includes_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_includes) ] || \
	  [ ! -f $(cmt_final_setup_install_includes) ] || \
	  $(not_install_includes_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_includes.make"; \
	  $(cmtexe) -tag=$(tags) $(install_includes_extratags) build constituent_config -out=$(cmt_local_install_includes_makefile) install_includes; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_install_includes_makefile) : $(install_includes_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_includes.make"; \
	  $(cmtexe) -f=$(bin)install_includes.in -tag=$(tags) $(install_includes_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_includes_makefile) install_includes
else
$(cmt_local_install_includes_makefile) : $(install_includes_dependencies) $(cmt_build_library_linksstamp) $(bin)install_includes.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_includes) ] || \
	  [ ! -f $(cmt_final_setup_install_includes) ] || \
	  $(not_install_includes_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_includes.make"; \
	  $(cmtexe) -f=$(bin)install_includes.in -tag=$(tags) $(install_includes_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_includes_makefile) install_includes; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(install_includes_extratags) build constituent_makefile -out=$(cmt_local_install_includes_makefile) install_includes

install_includes :: $(install_includes_dependencies) $(cmt_local_install_includes_makefile) dirs install_includesdirs
	$(echo) "(constituents.make) Starting install_includes"
	@if test -f $(cmt_local_install_includes_makefile); then \
	  $(MAKE) -f $(cmt_local_install_includes_makefile) install_includes; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_includes_makefile) install_includes
	$(echo) "(constituents.make) install_includes done"

clean :: install_includesclean ;

install_includesclean :: $(install_includesclean_dependencies) ##$(cmt_local_install_includes_makefile)
	$(echo) "(constituents.make) Starting install_includesclean"
	@-if test -f $(cmt_local_install_includes_makefile); then \
	  $(MAKE) -f $(cmt_local_install_includes_makefile) install_includesclean; \
	fi
	$(echo) "(constituents.make) install_includesclean done"
#	@-$(MAKE) -f $(cmt_local_install_includes_makefile) install_includesclean

##	  /bin/rm -f $(cmt_local_install_includes_makefile) $(bin)install_includes_dependencies.make

install :: install_includesinstall ;

install_includesinstall :: $(install_includes_dependencies) $(cmt_local_install_includes_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_install_includes_makefile); then \
	  $(MAKE) -f $(cmt_local_install_includes_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_install_includes_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : install_includesuninstall

$(foreach d,$(install_includes_dependencies),$(eval $(d)uninstall_dependencies += install_includesuninstall))

install_includesuninstall : $(install_includesuninstall_dependencies) ##$(cmt_local_install_includes_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_install_includes_makefile); then \
	  $(MAKE) -f $(cmt_local_install_includes_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_includes_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: install_includesuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ install_includes"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ install_includes done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_make_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_make_has_target_tag

cmt_local_tagfile_make = $(bin)$(TrkAmbiguityProcessor_tag)_make.make
cmt_final_setup_make = $(bin)setup_make.make
cmt_local_make_makefile = $(bin)make.make

make_extratags = -tag_add=target_make

else

cmt_local_tagfile_make = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_make = $(bin)setup.make
cmt_local_make_makefile = $(bin)make.make

endif

not_make_dependencies = { n=0; for p in $?; do m=0; for d in $(make_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
makedirs :
	@if test ! -d $(bin)make; then $(mkdir) -p $(bin)make; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)make
else
makedirs : ;
endif

ifdef cmt_make_has_target_tag

ifndef QUICK
$(cmt_local_make_makefile) : $(make_dependencies) build_library_links
	$(echo) "(constituents.make) Building make.make"; \
	  $(cmtexe) -tag=$(tags) $(make_extratags) build constituent_config -out=$(cmt_local_make_makefile) make
else
$(cmt_local_make_makefile) : $(make_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_make) ] || \
	  [ ! -f $(cmt_final_setup_make) ] || \
	  $(not_make_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building make.make"; \
	  $(cmtexe) -tag=$(tags) $(make_extratags) build constituent_config -out=$(cmt_local_make_makefile) make; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_make_makefile) : $(make_dependencies) build_library_links
	$(echo) "(constituents.make) Building make.make"; \
	  $(cmtexe) -f=$(bin)make.in -tag=$(tags) $(make_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_make_makefile) make
else
$(cmt_local_make_makefile) : $(make_dependencies) $(cmt_build_library_linksstamp) $(bin)make.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_make) ] || \
	  [ ! -f $(cmt_final_setup_make) ] || \
	  $(not_make_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building make.make"; \
	  $(cmtexe) -f=$(bin)make.in -tag=$(tags) $(make_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_make_makefile) make; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(make_extratags) build constituent_makefile -out=$(cmt_local_make_makefile) make

make :: $(make_dependencies) $(cmt_local_make_makefile) dirs makedirs
	$(echo) "(constituents.make) Starting make"
	@if test -f $(cmt_local_make_makefile); then \
	  $(MAKE) -f $(cmt_local_make_makefile) make; \
	  fi
#	@$(MAKE) -f $(cmt_local_make_makefile) make
	$(echo) "(constituents.make) make done"

clean :: makeclean ;

makeclean :: $(makeclean_dependencies) ##$(cmt_local_make_makefile)
	$(echo) "(constituents.make) Starting makeclean"
	@-if test -f $(cmt_local_make_makefile); then \
	  $(MAKE) -f $(cmt_local_make_makefile) makeclean; \
	fi
	$(echo) "(constituents.make) makeclean done"
#	@-$(MAKE) -f $(cmt_local_make_makefile) makeclean

##	  /bin/rm -f $(cmt_local_make_makefile) $(bin)make_dependencies.make

install :: makeinstall ;

makeinstall :: $(make_dependencies) $(cmt_local_make_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_make_makefile); then \
	  $(MAKE) -f $(cmt_local_make_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_make_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : makeuninstall

$(foreach d,$(make_dependencies),$(eval $(d)uninstall_dependencies += makeuninstall))

makeuninstall : $(makeuninstall_dependencies) ##$(cmt_local_make_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_make_makefile); then \
	  $(MAKE) -f $(cmt_local_make_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_make_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: makeuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ make"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ make done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_CompilePython_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_CompilePython_has_target_tag

cmt_local_tagfile_CompilePython = $(bin)$(TrkAmbiguityProcessor_tag)_CompilePython.make
cmt_final_setup_CompilePython = $(bin)setup_CompilePython.make
cmt_local_CompilePython_makefile = $(bin)CompilePython.make

CompilePython_extratags = -tag_add=target_CompilePython

else

cmt_local_tagfile_CompilePython = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_CompilePython = $(bin)setup.make
cmt_local_CompilePython_makefile = $(bin)CompilePython.make

endif

not_CompilePython_dependencies = { n=0; for p in $?; do m=0; for d in $(CompilePython_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
CompilePythondirs :
	@if test ! -d $(bin)CompilePython; then $(mkdir) -p $(bin)CompilePython; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)CompilePython
else
CompilePythondirs : ;
endif

ifdef cmt_CompilePython_has_target_tag

ifndef QUICK
$(cmt_local_CompilePython_makefile) : $(CompilePython_dependencies) build_library_links
	$(echo) "(constituents.make) Building CompilePython.make"; \
	  $(cmtexe) -tag=$(tags) $(CompilePython_extratags) build constituent_config -out=$(cmt_local_CompilePython_makefile) CompilePython
else
$(cmt_local_CompilePython_makefile) : $(CompilePython_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_CompilePython) ] || \
	  [ ! -f $(cmt_final_setup_CompilePython) ] || \
	  $(not_CompilePython_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building CompilePython.make"; \
	  $(cmtexe) -tag=$(tags) $(CompilePython_extratags) build constituent_config -out=$(cmt_local_CompilePython_makefile) CompilePython; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_CompilePython_makefile) : $(CompilePython_dependencies) build_library_links
	$(echo) "(constituents.make) Building CompilePython.make"; \
	  $(cmtexe) -f=$(bin)CompilePython.in -tag=$(tags) $(CompilePython_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_CompilePython_makefile) CompilePython
else
$(cmt_local_CompilePython_makefile) : $(CompilePython_dependencies) $(cmt_build_library_linksstamp) $(bin)CompilePython.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_CompilePython) ] || \
	  [ ! -f $(cmt_final_setup_CompilePython) ] || \
	  $(not_CompilePython_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building CompilePython.make"; \
	  $(cmtexe) -f=$(bin)CompilePython.in -tag=$(tags) $(CompilePython_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_CompilePython_makefile) CompilePython; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(CompilePython_extratags) build constituent_makefile -out=$(cmt_local_CompilePython_makefile) CompilePython

CompilePython :: $(CompilePython_dependencies) $(cmt_local_CompilePython_makefile) dirs CompilePythondirs
	$(echo) "(constituents.make) Starting CompilePython"
	@if test -f $(cmt_local_CompilePython_makefile); then \
	  $(MAKE) -f $(cmt_local_CompilePython_makefile) CompilePython; \
	  fi
#	@$(MAKE) -f $(cmt_local_CompilePython_makefile) CompilePython
	$(echo) "(constituents.make) CompilePython done"

clean :: CompilePythonclean ;

CompilePythonclean :: $(CompilePythonclean_dependencies) ##$(cmt_local_CompilePython_makefile)
	$(echo) "(constituents.make) Starting CompilePythonclean"
	@-if test -f $(cmt_local_CompilePython_makefile); then \
	  $(MAKE) -f $(cmt_local_CompilePython_makefile) CompilePythonclean; \
	fi
	$(echo) "(constituents.make) CompilePythonclean done"
#	@-$(MAKE) -f $(cmt_local_CompilePython_makefile) CompilePythonclean

##	  /bin/rm -f $(cmt_local_CompilePython_makefile) $(bin)CompilePython_dependencies.make

install :: CompilePythoninstall ;

CompilePythoninstall :: $(CompilePython_dependencies) $(cmt_local_CompilePython_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_CompilePython_makefile); then \
	  $(MAKE) -f $(cmt_local_CompilePython_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_CompilePython_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : CompilePythonuninstall

$(foreach d,$(CompilePython_dependencies),$(eval $(d)uninstall_dependencies += CompilePythonuninstall))

CompilePythonuninstall : $(CompilePythonuninstall_dependencies) ##$(cmt_local_CompilePython_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_CompilePython_makefile); then \
	  $(MAKE) -f $(cmt_local_CompilePython_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_CompilePython_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: CompilePythonuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ CompilePython"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ CompilePython done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_qmtest_run_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_qmtest_run_has_target_tag

cmt_local_tagfile_qmtest_run = $(bin)$(TrkAmbiguityProcessor_tag)_qmtest_run.make
cmt_final_setup_qmtest_run = $(bin)setup_qmtest_run.make
cmt_local_qmtest_run_makefile = $(bin)qmtest_run.make

qmtest_run_extratags = -tag_add=target_qmtest_run

else

cmt_local_tagfile_qmtest_run = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_qmtest_run = $(bin)setup.make
cmt_local_qmtest_run_makefile = $(bin)qmtest_run.make

endif

not_qmtest_run_dependencies = { n=0; for p in $?; do m=0; for d in $(qmtest_run_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
qmtest_rundirs :
	@if test ! -d $(bin)qmtest_run; then $(mkdir) -p $(bin)qmtest_run; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)qmtest_run
else
qmtest_rundirs : ;
endif

ifdef cmt_qmtest_run_has_target_tag

ifndef QUICK
$(cmt_local_qmtest_run_makefile) : $(qmtest_run_dependencies) build_library_links
	$(echo) "(constituents.make) Building qmtest_run.make"; \
	  $(cmtexe) -tag=$(tags) $(qmtest_run_extratags) build constituent_config -out=$(cmt_local_qmtest_run_makefile) qmtest_run
else
$(cmt_local_qmtest_run_makefile) : $(qmtest_run_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_qmtest_run) ] || \
	  [ ! -f $(cmt_final_setup_qmtest_run) ] || \
	  $(not_qmtest_run_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building qmtest_run.make"; \
	  $(cmtexe) -tag=$(tags) $(qmtest_run_extratags) build constituent_config -out=$(cmt_local_qmtest_run_makefile) qmtest_run; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_qmtest_run_makefile) : $(qmtest_run_dependencies) build_library_links
	$(echo) "(constituents.make) Building qmtest_run.make"; \
	  $(cmtexe) -f=$(bin)qmtest_run.in -tag=$(tags) $(qmtest_run_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_qmtest_run_makefile) qmtest_run
else
$(cmt_local_qmtest_run_makefile) : $(qmtest_run_dependencies) $(cmt_build_library_linksstamp) $(bin)qmtest_run.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_qmtest_run) ] || \
	  [ ! -f $(cmt_final_setup_qmtest_run) ] || \
	  $(not_qmtest_run_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building qmtest_run.make"; \
	  $(cmtexe) -f=$(bin)qmtest_run.in -tag=$(tags) $(qmtest_run_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_qmtest_run_makefile) qmtest_run; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(qmtest_run_extratags) build constituent_makefile -out=$(cmt_local_qmtest_run_makefile) qmtest_run

qmtest_run :: $(qmtest_run_dependencies) $(cmt_local_qmtest_run_makefile) dirs qmtest_rundirs
	$(echo) "(constituents.make) Starting qmtest_run"
	@if test -f $(cmt_local_qmtest_run_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_run_makefile) qmtest_run; \
	  fi
#	@$(MAKE) -f $(cmt_local_qmtest_run_makefile) qmtest_run
	$(echo) "(constituents.make) qmtest_run done"

clean :: qmtest_runclean ;

qmtest_runclean :: $(qmtest_runclean_dependencies) ##$(cmt_local_qmtest_run_makefile)
	$(echo) "(constituents.make) Starting qmtest_runclean"
	@-if test -f $(cmt_local_qmtest_run_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_run_makefile) qmtest_runclean; \
	fi
	$(echo) "(constituents.make) qmtest_runclean done"
#	@-$(MAKE) -f $(cmt_local_qmtest_run_makefile) qmtest_runclean

##	  /bin/rm -f $(cmt_local_qmtest_run_makefile) $(bin)qmtest_run_dependencies.make

install :: qmtest_runinstall ;

qmtest_runinstall :: $(qmtest_run_dependencies) $(cmt_local_qmtest_run_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_qmtest_run_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_run_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_qmtest_run_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : qmtest_rununinstall

$(foreach d,$(qmtest_run_dependencies),$(eval $(d)uninstall_dependencies += qmtest_rununinstall))

qmtest_rununinstall : $(qmtest_rununinstall_dependencies) ##$(cmt_local_qmtest_run_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_qmtest_run_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_run_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_qmtest_run_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: qmtest_rununinstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ qmtest_run"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ qmtest_run done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_qmtest_summarize_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_qmtest_summarize_has_target_tag

cmt_local_tagfile_qmtest_summarize = $(bin)$(TrkAmbiguityProcessor_tag)_qmtest_summarize.make
cmt_final_setup_qmtest_summarize = $(bin)setup_qmtest_summarize.make
cmt_local_qmtest_summarize_makefile = $(bin)qmtest_summarize.make

qmtest_summarize_extratags = -tag_add=target_qmtest_summarize

else

cmt_local_tagfile_qmtest_summarize = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_qmtest_summarize = $(bin)setup.make
cmt_local_qmtest_summarize_makefile = $(bin)qmtest_summarize.make

endif

not_qmtest_summarize_dependencies = { n=0; for p in $?; do m=0; for d in $(qmtest_summarize_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
qmtest_summarizedirs :
	@if test ! -d $(bin)qmtest_summarize; then $(mkdir) -p $(bin)qmtest_summarize; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)qmtest_summarize
else
qmtest_summarizedirs : ;
endif

ifdef cmt_qmtest_summarize_has_target_tag

ifndef QUICK
$(cmt_local_qmtest_summarize_makefile) : $(qmtest_summarize_dependencies) build_library_links
	$(echo) "(constituents.make) Building qmtest_summarize.make"; \
	  $(cmtexe) -tag=$(tags) $(qmtest_summarize_extratags) build constituent_config -out=$(cmt_local_qmtest_summarize_makefile) qmtest_summarize
else
$(cmt_local_qmtest_summarize_makefile) : $(qmtest_summarize_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_qmtest_summarize) ] || \
	  [ ! -f $(cmt_final_setup_qmtest_summarize) ] || \
	  $(not_qmtest_summarize_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building qmtest_summarize.make"; \
	  $(cmtexe) -tag=$(tags) $(qmtest_summarize_extratags) build constituent_config -out=$(cmt_local_qmtest_summarize_makefile) qmtest_summarize; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_qmtest_summarize_makefile) : $(qmtest_summarize_dependencies) build_library_links
	$(echo) "(constituents.make) Building qmtest_summarize.make"; \
	  $(cmtexe) -f=$(bin)qmtest_summarize.in -tag=$(tags) $(qmtest_summarize_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_qmtest_summarize_makefile) qmtest_summarize
else
$(cmt_local_qmtest_summarize_makefile) : $(qmtest_summarize_dependencies) $(cmt_build_library_linksstamp) $(bin)qmtest_summarize.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_qmtest_summarize) ] || \
	  [ ! -f $(cmt_final_setup_qmtest_summarize) ] || \
	  $(not_qmtest_summarize_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building qmtest_summarize.make"; \
	  $(cmtexe) -f=$(bin)qmtest_summarize.in -tag=$(tags) $(qmtest_summarize_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_qmtest_summarize_makefile) qmtest_summarize; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(qmtest_summarize_extratags) build constituent_makefile -out=$(cmt_local_qmtest_summarize_makefile) qmtest_summarize

qmtest_summarize :: $(qmtest_summarize_dependencies) $(cmt_local_qmtest_summarize_makefile) dirs qmtest_summarizedirs
	$(echo) "(constituents.make) Starting qmtest_summarize"
	@if test -f $(cmt_local_qmtest_summarize_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_summarize_makefile) qmtest_summarize; \
	  fi
#	@$(MAKE) -f $(cmt_local_qmtest_summarize_makefile) qmtest_summarize
	$(echo) "(constituents.make) qmtest_summarize done"

clean :: qmtest_summarizeclean ;

qmtest_summarizeclean :: $(qmtest_summarizeclean_dependencies) ##$(cmt_local_qmtest_summarize_makefile)
	$(echo) "(constituents.make) Starting qmtest_summarizeclean"
	@-if test -f $(cmt_local_qmtest_summarize_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_summarize_makefile) qmtest_summarizeclean; \
	fi
	$(echo) "(constituents.make) qmtest_summarizeclean done"
#	@-$(MAKE) -f $(cmt_local_qmtest_summarize_makefile) qmtest_summarizeclean

##	  /bin/rm -f $(cmt_local_qmtest_summarize_makefile) $(bin)qmtest_summarize_dependencies.make

install :: qmtest_summarizeinstall ;

qmtest_summarizeinstall :: $(qmtest_summarize_dependencies) $(cmt_local_qmtest_summarize_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_qmtest_summarize_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_summarize_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_qmtest_summarize_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : qmtest_summarizeuninstall

$(foreach d,$(qmtest_summarize_dependencies),$(eval $(d)uninstall_dependencies += qmtest_summarizeuninstall))

qmtest_summarizeuninstall : $(qmtest_summarizeuninstall_dependencies) ##$(cmt_local_qmtest_summarize_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_qmtest_summarize_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_summarize_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_qmtest_summarize_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: qmtest_summarizeuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ qmtest_summarize"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ qmtest_summarize done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TestPackage_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TestPackage_has_target_tag

cmt_local_tagfile_TestPackage = $(bin)$(TrkAmbiguityProcessor_tag)_TestPackage.make
cmt_final_setup_TestPackage = $(bin)setup_TestPackage.make
cmt_local_TestPackage_makefile = $(bin)TestPackage.make

TestPackage_extratags = -tag_add=target_TestPackage

else

cmt_local_tagfile_TestPackage = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_TestPackage = $(bin)setup.make
cmt_local_TestPackage_makefile = $(bin)TestPackage.make

endif

not_TestPackage_dependencies = { n=0; for p in $?; do m=0; for d in $(TestPackage_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TestPackagedirs :
	@if test ! -d $(bin)TestPackage; then $(mkdir) -p $(bin)TestPackage; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TestPackage
else
TestPackagedirs : ;
endif

ifdef cmt_TestPackage_has_target_tag

ifndef QUICK
$(cmt_local_TestPackage_makefile) : $(TestPackage_dependencies) build_library_links
	$(echo) "(constituents.make) Building TestPackage.make"; \
	  $(cmtexe) -tag=$(tags) $(TestPackage_extratags) build constituent_config -out=$(cmt_local_TestPackage_makefile) TestPackage
else
$(cmt_local_TestPackage_makefile) : $(TestPackage_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TestPackage) ] || \
	  [ ! -f $(cmt_final_setup_TestPackage) ] || \
	  $(not_TestPackage_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TestPackage.make"; \
	  $(cmtexe) -tag=$(tags) $(TestPackage_extratags) build constituent_config -out=$(cmt_local_TestPackage_makefile) TestPackage; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TestPackage_makefile) : $(TestPackage_dependencies) build_library_links
	$(echo) "(constituents.make) Building TestPackage.make"; \
	  $(cmtexe) -f=$(bin)TestPackage.in -tag=$(tags) $(TestPackage_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TestPackage_makefile) TestPackage
else
$(cmt_local_TestPackage_makefile) : $(TestPackage_dependencies) $(cmt_build_library_linksstamp) $(bin)TestPackage.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TestPackage) ] || \
	  [ ! -f $(cmt_final_setup_TestPackage) ] || \
	  $(not_TestPackage_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TestPackage.make"; \
	  $(cmtexe) -f=$(bin)TestPackage.in -tag=$(tags) $(TestPackage_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TestPackage_makefile) TestPackage; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TestPackage_extratags) build constituent_makefile -out=$(cmt_local_TestPackage_makefile) TestPackage

TestPackage :: $(TestPackage_dependencies) $(cmt_local_TestPackage_makefile) dirs TestPackagedirs
	$(echo) "(constituents.make) Starting TestPackage"
	@if test -f $(cmt_local_TestPackage_makefile); then \
	  $(MAKE) -f $(cmt_local_TestPackage_makefile) TestPackage; \
	  fi
#	@$(MAKE) -f $(cmt_local_TestPackage_makefile) TestPackage
	$(echo) "(constituents.make) TestPackage done"

clean :: TestPackageclean ;

TestPackageclean :: $(TestPackageclean_dependencies) ##$(cmt_local_TestPackage_makefile)
	$(echo) "(constituents.make) Starting TestPackageclean"
	@-if test -f $(cmt_local_TestPackage_makefile); then \
	  $(MAKE) -f $(cmt_local_TestPackage_makefile) TestPackageclean; \
	fi
	$(echo) "(constituents.make) TestPackageclean done"
#	@-$(MAKE) -f $(cmt_local_TestPackage_makefile) TestPackageclean

##	  /bin/rm -f $(cmt_local_TestPackage_makefile) $(bin)TestPackage_dependencies.make

install :: TestPackageinstall ;

TestPackageinstall :: $(TestPackage_dependencies) $(cmt_local_TestPackage_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TestPackage_makefile); then \
	  $(MAKE) -f $(cmt_local_TestPackage_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TestPackage_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TestPackageuninstall

$(foreach d,$(TestPackage_dependencies),$(eval $(d)uninstall_dependencies += TestPackageuninstall))

TestPackageuninstall : $(TestPackageuninstall_dependencies) ##$(cmt_local_TestPackage_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TestPackage_makefile); then \
	  $(MAKE) -f $(cmt_local_TestPackage_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TestPackage_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TestPackageuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TestPackage"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TestPackage done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TestProject_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TestProject_has_target_tag

cmt_local_tagfile_TestProject = $(bin)$(TrkAmbiguityProcessor_tag)_TestProject.make
cmt_final_setup_TestProject = $(bin)setup_TestProject.make
cmt_local_TestProject_makefile = $(bin)TestProject.make

TestProject_extratags = -tag_add=target_TestProject

else

cmt_local_tagfile_TestProject = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_TestProject = $(bin)setup.make
cmt_local_TestProject_makefile = $(bin)TestProject.make

endif

not_TestProject_dependencies = { n=0; for p in $?; do m=0; for d in $(TestProject_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TestProjectdirs :
	@if test ! -d $(bin)TestProject; then $(mkdir) -p $(bin)TestProject; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TestProject
else
TestProjectdirs : ;
endif

ifdef cmt_TestProject_has_target_tag

ifndef QUICK
$(cmt_local_TestProject_makefile) : $(TestProject_dependencies) build_library_links
	$(echo) "(constituents.make) Building TestProject.make"; \
	  $(cmtexe) -tag=$(tags) $(TestProject_extratags) build constituent_config -out=$(cmt_local_TestProject_makefile) TestProject
else
$(cmt_local_TestProject_makefile) : $(TestProject_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TestProject) ] || \
	  [ ! -f $(cmt_final_setup_TestProject) ] || \
	  $(not_TestProject_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TestProject.make"; \
	  $(cmtexe) -tag=$(tags) $(TestProject_extratags) build constituent_config -out=$(cmt_local_TestProject_makefile) TestProject; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TestProject_makefile) : $(TestProject_dependencies) build_library_links
	$(echo) "(constituents.make) Building TestProject.make"; \
	  $(cmtexe) -f=$(bin)TestProject.in -tag=$(tags) $(TestProject_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TestProject_makefile) TestProject
else
$(cmt_local_TestProject_makefile) : $(TestProject_dependencies) $(cmt_build_library_linksstamp) $(bin)TestProject.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TestProject) ] || \
	  [ ! -f $(cmt_final_setup_TestProject) ] || \
	  $(not_TestProject_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TestProject.make"; \
	  $(cmtexe) -f=$(bin)TestProject.in -tag=$(tags) $(TestProject_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TestProject_makefile) TestProject; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TestProject_extratags) build constituent_makefile -out=$(cmt_local_TestProject_makefile) TestProject

TestProject :: $(TestProject_dependencies) $(cmt_local_TestProject_makefile) dirs TestProjectdirs
	$(echo) "(constituents.make) Starting TestProject"
	@if test -f $(cmt_local_TestProject_makefile); then \
	  $(MAKE) -f $(cmt_local_TestProject_makefile) TestProject; \
	  fi
#	@$(MAKE) -f $(cmt_local_TestProject_makefile) TestProject
	$(echo) "(constituents.make) TestProject done"

clean :: TestProjectclean ;

TestProjectclean :: $(TestProjectclean_dependencies) ##$(cmt_local_TestProject_makefile)
	$(echo) "(constituents.make) Starting TestProjectclean"
	@-if test -f $(cmt_local_TestProject_makefile); then \
	  $(MAKE) -f $(cmt_local_TestProject_makefile) TestProjectclean; \
	fi
	$(echo) "(constituents.make) TestProjectclean done"
#	@-$(MAKE) -f $(cmt_local_TestProject_makefile) TestProjectclean

##	  /bin/rm -f $(cmt_local_TestProject_makefile) $(bin)TestProject_dependencies.make

install :: TestProjectinstall ;

TestProjectinstall :: $(TestProject_dependencies) $(cmt_local_TestProject_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TestProject_makefile); then \
	  $(MAKE) -f $(cmt_local_TestProject_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TestProject_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TestProjectuninstall

$(foreach d,$(TestProject_dependencies),$(eval $(d)uninstall_dependencies += TestProjectuninstall))

TestProjectuninstall : $(TestProjectuninstall_dependencies) ##$(cmt_local_TestProject_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TestProject_makefile); then \
	  $(MAKE) -f $(cmt_local_TestProject_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TestProject_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TestProjectuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TestProject"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TestProject done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_new_rootsys_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_new_rootsys_has_target_tag

cmt_local_tagfile_new_rootsys = $(bin)$(TrkAmbiguityProcessor_tag)_new_rootsys.make
cmt_final_setup_new_rootsys = $(bin)setup_new_rootsys.make
cmt_local_new_rootsys_makefile = $(bin)new_rootsys.make

new_rootsys_extratags = -tag_add=target_new_rootsys

else

cmt_local_tagfile_new_rootsys = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_new_rootsys = $(bin)setup.make
cmt_local_new_rootsys_makefile = $(bin)new_rootsys.make

endif

not_new_rootsys_dependencies = { n=0; for p in $?; do m=0; for d in $(new_rootsys_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
new_rootsysdirs :
	@if test ! -d $(bin)new_rootsys; then $(mkdir) -p $(bin)new_rootsys; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)new_rootsys
else
new_rootsysdirs : ;
endif

ifdef cmt_new_rootsys_has_target_tag

ifndef QUICK
$(cmt_local_new_rootsys_makefile) : $(new_rootsys_dependencies) build_library_links
	$(echo) "(constituents.make) Building new_rootsys.make"; \
	  $(cmtexe) -tag=$(tags) $(new_rootsys_extratags) build constituent_config -out=$(cmt_local_new_rootsys_makefile) new_rootsys
else
$(cmt_local_new_rootsys_makefile) : $(new_rootsys_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_new_rootsys) ] || \
	  [ ! -f $(cmt_final_setup_new_rootsys) ] || \
	  $(not_new_rootsys_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building new_rootsys.make"; \
	  $(cmtexe) -tag=$(tags) $(new_rootsys_extratags) build constituent_config -out=$(cmt_local_new_rootsys_makefile) new_rootsys; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_new_rootsys_makefile) : $(new_rootsys_dependencies) build_library_links
	$(echo) "(constituents.make) Building new_rootsys.make"; \
	  $(cmtexe) -f=$(bin)new_rootsys.in -tag=$(tags) $(new_rootsys_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_new_rootsys_makefile) new_rootsys
else
$(cmt_local_new_rootsys_makefile) : $(new_rootsys_dependencies) $(cmt_build_library_linksstamp) $(bin)new_rootsys.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_new_rootsys) ] || \
	  [ ! -f $(cmt_final_setup_new_rootsys) ] || \
	  $(not_new_rootsys_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building new_rootsys.make"; \
	  $(cmtexe) -f=$(bin)new_rootsys.in -tag=$(tags) $(new_rootsys_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_new_rootsys_makefile) new_rootsys; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(new_rootsys_extratags) build constituent_makefile -out=$(cmt_local_new_rootsys_makefile) new_rootsys

new_rootsys :: $(new_rootsys_dependencies) $(cmt_local_new_rootsys_makefile) dirs new_rootsysdirs
	$(echo) "(constituents.make) Starting new_rootsys"
	@if test -f $(cmt_local_new_rootsys_makefile); then \
	  $(MAKE) -f $(cmt_local_new_rootsys_makefile) new_rootsys; \
	  fi
#	@$(MAKE) -f $(cmt_local_new_rootsys_makefile) new_rootsys
	$(echo) "(constituents.make) new_rootsys done"

clean :: new_rootsysclean ;

new_rootsysclean :: $(new_rootsysclean_dependencies) ##$(cmt_local_new_rootsys_makefile)
	$(echo) "(constituents.make) Starting new_rootsysclean"
	@-if test -f $(cmt_local_new_rootsys_makefile); then \
	  $(MAKE) -f $(cmt_local_new_rootsys_makefile) new_rootsysclean; \
	fi
	$(echo) "(constituents.make) new_rootsysclean done"
#	@-$(MAKE) -f $(cmt_local_new_rootsys_makefile) new_rootsysclean

##	  /bin/rm -f $(cmt_local_new_rootsys_makefile) $(bin)new_rootsys_dependencies.make

install :: new_rootsysinstall ;

new_rootsysinstall :: $(new_rootsys_dependencies) $(cmt_local_new_rootsys_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_new_rootsys_makefile); then \
	  $(MAKE) -f $(cmt_local_new_rootsys_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_new_rootsys_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : new_rootsysuninstall

$(foreach d,$(new_rootsys_dependencies),$(eval $(d)uninstall_dependencies += new_rootsysuninstall))

new_rootsysuninstall : $(new_rootsysuninstall_dependencies) ##$(cmt_local_new_rootsys_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_new_rootsys_makefile); then \
	  $(MAKE) -f $(cmt_local_new_rootsys_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_new_rootsys_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: new_rootsysuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ new_rootsys"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ new_rootsys done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_doxygen_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_doxygen_has_target_tag

cmt_local_tagfile_doxygen = $(bin)$(TrkAmbiguityProcessor_tag)_doxygen.make
cmt_final_setup_doxygen = $(bin)setup_doxygen.make
cmt_local_doxygen_makefile = $(bin)doxygen.make

doxygen_extratags = -tag_add=target_doxygen

else

cmt_local_tagfile_doxygen = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_doxygen = $(bin)setup.make
cmt_local_doxygen_makefile = $(bin)doxygen.make

endif

not_doxygen_dependencies = { n=0; for p in $?; do m=0; for d in $(doxygen_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
doxygendirs :
	@if test ! -d $(bin)doxygen; then $(mkdir) -p $(bin)doxygen; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)doxygen
else
doxygendirs : ;
endif

ifdef cmt_doxygen_has_target_tag

ifndef QUICK
$(cmt_local_doxygen_makefile) : $(doxygen_dependencies) build_library_links
	$(echo) "(constituents.make) Building doxygen.make"; \
	  $(cmtexe) -tag=$(tags) $(doxygen_extratags) build constituent_config -out=$(cmt_local_doxygen_makefile) doxygen
else
$(cmt_local_doxygen_makefile) : $(doxygen_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_doxygen) ] || \
	  [ ! -f $(cmt_final_setup_doxygen) ] || \
	  $(not_doxygen_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building doxygen.make"; \
	  $(cmtexe) -tag=$(tags) $(doxygen_extratags) build constituent_config -out=$(cmt_local_doxygen_makefile) doxygen; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_doxygen_makefile) : $(doxygen_dependencies) build_library_links
	$(echo) "(constituents.make) Building doxygen.make"; \
	  $(cmtexe) -f=$(bin)doxygen.in -tag=$(tags) $(doxygen_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_doxygen_makefile) doxygen
else
$(cmt_local_doxygen_makefile) : $(doxygen_dependencies) $(cmt_build_library_linksstamp) $(bin)doxygen.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_doxygen) ] || \
	  [ ! -f $(cmt_final_setup_doxygen) ] || \
	  $(not_doxygen_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building doxygen.make"; \
	  $(cmtexe) -f=$(bin)doxygen.in -tag=$(tags) $(doxygen_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_doxygen_makefile) doxygen; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(doxygen_extratags) build constituent_makefile -out=$(cmt_local_doxygen_makefile) doxygen

doxygen :: $(doxygen_dependencies) $(cmt_local_doxygen_makefile) dirs doxygendirs
	$(echo) "(constituents.make) Starting doxygen"
	@if test -f $(cmt_local_doxygen_makefile); then \
	  $(MAKE) -f $(cmt_local_doxygen_makefile) doxygen; \
	  fi
#	@$(MAKE) -f $(cmt_local_doxygen_makefile) doxygen
	$(echo) "(constituents.make) doxygen done"

clean :: doxygenclean ;

doxygenclean :: $(doxygenclean_dependencies) ##$(cmt_local_doxygen_makefile)
	$(echo) "(constituents.make) Starting doxygenclean"
	@-if test -f $(cmt_local_doxygen_makefile); then \
	  $(MAKE) -f $(cmt_local_doxygen_makefile) doxygenclean; \
	fi
	$(echo) "(constituents.make) doxygenclean done"
#	@-$(MAKE) -f $(cmt_local_doxygen_makefile) doxygenclean

##	  /bin/rm -f $(cmt_local_doxygen_makefile) $(bin)doxygen_dependencies.make

install :: doxygeninstall ;

doxygeninstall :: $(doxygen_dependencies) $(cmt_local_doxygen_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_doxygen_makefile); then \
	  $(MAKE) -f $(cmt_local_doxygen_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_doxygen_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : doxygenuninstall

$(foreach d,$(doxygen_dependencies),$(eval $(d)uninstall_dependencies += doxygenuninstall))

doxygenuninstall : $(doxygenuninstall_dependencies) ##$(cmt_local_doxygen_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_doxygen_makefile); then \
	  $(MAKE) -f $(cmt_local_doxygen_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_doxygen_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: doxygenuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ doxygen"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ doxygen done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_post_install_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_post_install_has_target_tag

cmt_local_tagfile_post_install = $(bin)$(TrkAmbiguityProcessor_tag)_post_install.make
cmt_final_setup_post_install = $(bin)setup_post_install.make
cmt_local_post_install_makefile = $(bin)post_install.make

post_install_extratags = -tag_add=target_post_install

else

cmt_local_tagfile_post_install = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_post_install = $(bin)setup.make
cmt_local_post_install_makefile = $(bin)post_install.make

endif

not_post_install_dependencies = { n=0; for p in $?; do m=0; for d in $(post_install_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
post_installdirs :
	@if test ! -d $(bin)post_install; then $(mkdir) -p $(bin)post_install; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)post_install
else
post_installdirs : ;
endif

ifdef cmt_post_install_has_target_tag

ifndef QUICK
$(cmt_local_post_install_makefile) : $(post_install_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_install.make"; \
	  $(cmtexe) -tag=$(tags) $(post_install_extratags) build constituent_config -out=$(cmt_local_post_install_makefile) post_install
else
$(cmt_local_post_install_makefile) : $(post_install_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_install) ] || \
	  [ ! -f $(cmt_final_setup_post_install) ] || \
	  $(not_post_install_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_install.make"; \
	  $(cmtexe) -tag=$(tags) $(post_install_extratags) build constituent_config -out=$(cmt_local_post_install_makefile) post_install; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_post_install_makefile) : $(post_install_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_install.make"; \
	  $(cmtexe) -f=$(bin)post_install.in -tag=$(tags) $(post_install_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_install_makefile) post_install
else
$(cmt_local_post_install_makefile) : $(post_install_dependencies) $(cmt_build_library_linksstamp) $(bin)post_install.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_install) ] || \
	  [ ! -f $(cmt_final_setup_post_install) ] || \
	  $(not_post_install_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_install.make"; \
	  $(cmtexe) -f=$(bin)post_install.in -tag=$(tags) $(post_install_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_install_makefile) post_install; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(post_install_extratags) build constituent_makefile -out=$(cmt_local_post_install_makefile) post_install

post_install :: $(post_install_dependencies) $(cmt_local_post_install_makefile) dirs post_installdirs
	$(echo) "(constituents.make) Starting post_install"
	@if test -f $(cmt_local_post_install_makefile); then \
	  $(MAKE) -f $(cmt_local_post_install_makefile) post_install; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_install_makefile) post_install
	$(echo) "(constituents.make) post_install done"

clean :: post_installclean ;

post_installclean :: $(post_installclean_dependencies) ##$(cmt_local_post_install_makefile)
	$(echo) "(constituents.make) Starting post_installclean"
	@-if test -f $(cmt_local_post_install_makefile); then \
	  $(MAKE) -f $(cmt_local_post_install_makefile) post_installclean; \
	fi
	$(echo) "(constituents.make) post_installclean done"
#	@-$(MAKE) -f $(cmt_local_post_install_makefile) post_installclean

##	  /bin/rm -f $(cmt_local_post_install_makefile) $(bin)post_install_dependencies.make

install :: post_installinstall ;

post_installinstall :: $(post_install_dependencies) $(cmt_local_post_install_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_post_install_makefile); then \
	  $(MAKE) -f $(cmt_local_post_install_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_post_install_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : post_installuninstall

$(foreach d,$(post_install_dependencies),$(eval $(d)uninstall_dependencies += post_installuninstall))

post_installuninstall : $(post_installuninstall_dependencies) ##$(cmt_local_post_install_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_post_install_makefile); then \
	  $(MAKE) -f $(cmt_local_post_install_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_install_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: post_installuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ post_install"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ post_install done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_post_merge_rootmap_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_post_merge_rootmap_has_target_tag

cmt_local_tagfile_post_merge_rootmap = $(bin)$(TrkAmbiguityProcessor_tag)_post_merge_rootmap.make
cmt_final_setup_post_merge_rootmap = $(bin)setup_post_merge_rootmap.make
cmt_local_post_merge_rootmap_makefile = $(bin)post_merge_rootmap.make

post_merge_rootmap_extratags = -tag_add=target_post_merge_rootmap

else

cmt_local_tagfile_post_merge_rootmap = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_post_merge_rootmap = $(bin)setup.make
cmt_local_post_merge_rootmap_makefile = $(bin)post_merge_rootmap.make

endif

not_post_merge_rootmap_dependencies = { n=0; for p in $?; do m=0; for d in $(post_merge_rootmap_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
post_merge_rootmapdirs :
	@if test ! -d $(bin)post_merge_rootmap; then $(mkdir) -p $(bin)post_merge_rootmap; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)post_merge_rootmap
else
post_merge_rootmapdirs : ;
endif

ifdef cmt_post_merge_rootmap_has_target_tag

ifndef QUICK
$(cmt_local_post_merge_rootmap_makefile) : $(post_merge_rootmap_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_merge_rootmap.make"; \
	  $(cmtexe) -tag=$(tags) $(post_merge_rootmap_extratags) build constituent_config -out=$(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap
else
$(cmt_local_post_merge_rootmap_makefile) : $(post_merge_rootmap_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_merge_rootmap) ] || \
	  [ ! -f $(cmt_final_setup_post_merge_rootmap) ] || \
	  $(not_post_merge_rootmap_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_merge_rootmap.make"; \
	  $(cmtexe) -tag=$(tags) $(post_merge_rootmap_extratags) build constituent_config -out=$(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_post_merge_rootmap_makefile) : $(post_merge_rootmap_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_merge_rootmap.make"; \
	  $(cmtexe) -f=$(bin)post_merge_rootmap.in -tag=$(tags) $(post_merge_rootmap_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap
else
$(cmt_local_post_merge_rootmap_makefile) : $(post_merge_rootmap_dependencies) $(cmt_build_library_linksstamp) $(bin)post_merge_rootmap.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_merge_rootmap) ] || \
	  [ ! -f $(cmt_final_setup_post_merge_rootmap) ] || \
	  $(not_post_merge_rootmap_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_merge_rootmap.make"; \
	  $(cmtexe) -f=$(bin)post_merge_rootmap.in -tag=$(tags) $(post_merge_rootmap_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(post_merge_rootmap_extratags) build constituent_makefile -out=$(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap

post_merge_rootmap :: $(post_merge_rootmap_dependencies) $(cmt_local_post_merge_rootmap_makefile) dirs post_merge_rootmapdirs
	$(echo) "(constituents.make) Starting post_merge_rootmap"
	@if test -f $(cmt_local_post_merge_rootmap_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap
	$(echo) "(constituents.make) post_merge_rootmap done"

clean :: post_merge_rootmapclean ;

post_merge_rootmapclean :: $(post_merge_rootmapclean_dependencies) ##$(cmt_local_post_merge_rootmap_makefile)
	$(echo) "(constituents.make) Starting post_merge_rootmapclean"
	@-if test -f $(cmt_local_post_merge_rootmap_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) post_merge_rootmapclean; \
	fi
	$(echo) "(constituents.make) post_merge_rootmapclean done"
#	@-$(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) post_merge_rootmapclean

##	  /bin/rm -f $(cmt_local_post_merge_rootmap_makefile) $(bin)post_merge_rootmap_dependencies.make

install :: post_merge_rootmapinstall ;

post_merge_rootmapinstall :: $(post_merge_rootmap_dependencies) $(cmt_local_post_merge_rootmap_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_post_merge_rootmap_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : post_merge_rootmapuninstall

$(foreach d,$(post_merge_rootmap_dependencies),$(eval $(d)uninstall_dependencies += post_merge_rootmapuninstall))

post_merge_rootmapuninstall : $(post_merge_rootmapuninstall_dependencies) ##$(cmt_local_post_merge_rootmap_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_post_merge_rootmap_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: post_merge_rootmapuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ post_merge_rootmap"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ post_merge_rootmap done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_post_merge_genconfdb_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_post_merge_genconfdb_has_target_tag

cmt_local_tagfile_post_merge_genconfdb = $(bin)$(TrkAmbiguityProcessor_tag)_post_merge_genconfdb.make
cmt_final_setup_post_merge_genconfdb = $(bin)setup_post_merge_genconfdb.make
cmt_local_post_merge_genconfdb_makefile = $(bin)post_merge_genconfdb.make

post_merge_genconfdb_extratags = -tag_add=target_post_merge_genconfdb

else

cmt_local_tagfile_post_merge_genconfdb = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_post_merge_genconfdb = $(bin)setup.make
cmt_local_post_merge_genconfdb_makefile = $(bin)post_merge_genconfdb.make

endif

not_post_merge_genconfdb_dependencies = { n=0; for p in $?; do m=0; for d in $(post_merge_genconfdb_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
post_merge_genconfdbdirs :
	@if test ! -d $(bin)post_merge_genconfdb; then $(mkdir) -p $(bin)post_merge_genconfdb; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)post_merge_genconfdb
else
post_merge_genconfdbdirs : ;
endif

ifdef cmt_post_merge_genconfdb_has_target_tag

ifndef QUICK
$(cmt_local_post_merge_genconfdb_makefile) : $(post_merge_genconfdb_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_merge_genconfdb.make"; \
	  $(cmtexe) -tag=$(tags) $(post_merge_genconfdb_extratags) build constituent_config -out=$(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb
else
$(cmt_local_post_merge_genconfdb_makefile) : $(post_merge_genconfdb_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_merge_genconfdb) ] || \
	  [ ! -f $(cmt_final_setup_post_merge_genconfdb) ] || \
	  $(not_post_merge_genconfdb_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_merge_genconfdb.make"; \
	  $(cmtexe) -tag=$(tags) $(post_merge_genconfdb_extratags) build constituent_config -out=$(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_post_merge_genconfdb_makefile) : $(post_merge_genconfdb_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_merge_genconfdb.make"; \
	  $(cmtexe) -f=$(bin)post_merge_genconfdb.in -tag=$(tags) $(post_merge_genconfdb_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb
else
$(cmt_local_post_merge_genconfdb_makefile) : $(post_merge_genconfdb_dependencies) $(cmt_build_library_linksstamp) $(bin)post_merge_genconfdb.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_merge_genconfdb) ] || \
	  [ ! -f $(cmt_final_setup_post_merge_genconfdb) ] || \
	  $(not_post_merge_genconfdb_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_merge_genconfdb.make"; \
	  $(cmtexe) -f=$(bin)post_merge_genconfdb.in -tag=$(tags) $(post_merge_genconfdb_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(post_merge_genconfdb_extratags) build constituent_makefile -out=$(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb

post_merge_genconfdb :: $(post_merge_genconfdb_dependencies) $(cmt_local_post_merge_genconfdb_makefile) dirs post_merge_genconfdbdirs
	$(echo) "(constituents.make) Starting post_merge_genconfdb"
	@if test -f $(cmt_local_post_merge_genconfdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb
	$(echo) "(constituents.make) post_merge_genconfdb done"

clean :: post_merge_genconfdbclean ;

post_merge_genconfdbclean :: $(post_merge_genconfdbclean_dependencies) ##$(cmt_local_post_merge_genconfdb_makefile)
	$(echo) "(constituents.make) Starting post_merge_genconfdbclean"
	@-if test -f $(cmt_local_post_merge_genconfdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdbclean; \
	fi
	$(echo) "(constituents.make) post_merge_genconfdbclean done"
#	@-$(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdbclean

##	  /bin/rm -f $(cmt_local_post_merge_genconfdb_makefile) $(bin)post_merge_genconfdb_dependencies.make

install :: post_merge_genconfdbinstall ;

post_merge_genconfdbinstall :: $(post_merge_genconfdb_dependencies) $(cmt_local_post_merge_genconfdb_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_post_merge_genconfdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : post_merge_genconfdbuninstall

$(foreach d,$(post_merge_genconfdb_dependencies),$(eval $(d)uninstall_dependencies += post_merge_genconfdbuninstall))

post_merge_genconfdbuninstall : $(post_merge_genconfdbuninstall_dependencies) ##$(cmt_local_post_merge_genconfdb_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_post_merge_genconfdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: post_merge_genconfdbuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ post_merge_genconfdb"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ post_merge_genconfdb done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_post_build_tpcnvdb_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_post_build_tpcnvdb_has_target_tag

cmt_local_tagfile_post_build_tpcnvdb = $(bin)$(TrkAmbiguityProcessor_tag)_post_build_tpcnvdb.make
cmt_final_setup_post_build_tpcnvdb = $(bin)setup_post_build_tpcnvdb.make
cmt_local_post_build_tpcnvdb_makefile = $(bin)post_build_tpcnvdb.make

post_build_tpcnvdb_extratags = -tag_add=target_post_build_tpcnvdb

else

cmt_local_tagfile_post_build_tpcnvdb = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_post_build_tpcnvdb = $(bin)setup.make
cmt_local_post_build_tpcnvdb_makefile = $(bin)post_build_tpcnvdb.make

endif

not_post_build_tpcnvdb_dependencies = { n=0; for p in $?; do m=0; for d in $(post_build_tpcnvdb_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
post_build_tpcnvdbdirs :
	@if test ! -d $(bin)post_build_tpcnvdb; then $(mkdir) -p $(bin)post_build_tpcnvdb; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)post_build_tpcnvdb
else
post_build_tpcnvdbdirs : ;
endif

ifdef cmt_post_build_tpcnvdb_has_target_tag

ifndef QUICK
$(cmt_local_post_build_tpcnvdb_makefile) : $(post_build_tpcnvdb_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_build_tpcnvdb.make"; \
	  $(cmtexe) -tag=$(tags) $(post_build_tpcnvdb_extratags) build constituent_config -out=$(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb
else
$(cmt_local_post_build_tpcnvdb_makefile) : $(post_build_tpcnvdb_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_build_tpcnvdb) ] || \
	  [ ! -f $(cmt_final_setup_post_build_tpcnvdb) ] || \
	  $(not_post_build_tpcnvdb_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_build_tpcnvdb.make"; \
	  $(cmtexe) -tag=$(tags) $(post_build_tpcnvdb_extratags) build constituent_config -out=$(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_post_build_tpcnvdb_makefile) : $(post_build_tpcnvdb_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_build_tpcnvdb.make"; \
	  $(cmtexe) -f=$(bin)post_build_tpcnvdb.in -tag=$(tags) $(post_build_tpcnvdb_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb
else
$(cmt_local_post_build_tpcnvdb_makefile) : $(post_build_tpcnvdb_dependencies) $(cmt_build_library_linksstamp) $(bin)post_build_tpcnvdb.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_build_tpcnvdb) ] || \
	  [ ! -f $(cmt_final_setup_post_build_tpcnvdb) ] || \
	  $(not_post_build_tpcnvdb_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_build_tpcnvdb.make"; \
	  $(cmtexe) -f=$(bin)post_build_tpcnvdb.in -tag=$(tags) $(post_build_tpcnvdb_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(post_build_tpcnvdb_extratags) build constituent_makefile -out=$(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb

post_build_tpcnvdb :: $(post_build_tpcnvdb_dependencies) $(cmt_local_post_build_tpcnvdb_makefile) dirs post_build_tpcnvdbdirs
	$(echo) "(constituents.make) Starting post_build_tpcnvdb"
	@if test -f $(cmt_local_post_build_tpcnvdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb
	$(echo) "(constituents.make) post_build_tpcnvdb done"

clean :: post_build_tpcnvdbclean ;

post_build_tpcnvdbclean :: $(post_build_tpcnvdbclean_dependencies) ##$(cmt_local_post_build_tpcnvdb_makefile)
	$(echo) "(constituents.make) Starting post_build_tpcnvdbclean"
	@-if test -f $(cmt_local_post_build_tpcnvdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdbclean; \
	fi
	$(echo) "(constituents.make) post_build_tpcnvdbclean done"
#	@-$(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdbclean

##	  /bin/rm -f $(cmt_local_post_build_tpcnvdb_makefile) $(bin)post_build_tpcnvdb_dependencies.make

install :: post_build_tpcnvdbinstall ;

post_build_tpcnvdbinstall :: $(post_build_tpcnvdb_dependencies) $(cmt_local_post_build_tpcnvdb_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_post_build_tpcnvdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : post_build_tpcnvdbuninstall

$(foreach d,$(post_build_tpcnvdb_dependencies),$(eval $(d)uninstall_dependencies += post_build_tpcnvdbuninstall))

post_build_tpcnvdbuninstall : $(post_build_tpcnvdbuninstall_dependencies) ##$(cmt_local_post_build_tpcnvdb_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_post_build_tpcnvdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: post_build_tpcnvdbuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ post_build_tpcnvdb"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ post_build_tpcnvdb done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_all_post_constituents_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_all_post_constituents_has_target_tag

cmt_local_tagfile_all_post_constituents = $(bin)$(TrkAmbiguityProcessor_tag)_all_post_constituents.make
cmt_final_setup_all_post_constituents = $(bin)setup_all_post_constituents.make
cmt_local_all_post_constituents_makefile = $(bin)all_post_constituents.make

all_post_constituents_extratags = -tag_add=target_all_post_constituents

else

cmt_local_tagfile_all_post_constituents = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_all_post_constituents = $(bin)setup.make
cmt_local_all_post_constituents_makefile = $(bin)all_post_constituents.make

endif

not_all_post_constituents_dependencies = { n=0; for p in $?; do m=0; for d in $(all_post_constituents_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
all_post_constituentsdirs :
	@if test ! -d $(bin)all_post_constituents; then $(mkdir) -p $(bin)all_post_constituents; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)all_post_constituents
else
all_post_constituentsdirs : ;
endif

ifdef cmt_all_post_constituents_has_target_tag

ifndef QUICK
$(cmt_local_all_post_constituents_makefile) : $(all_post_constituents_dependencies) build_library_links
	$(echo) "(constituents.make) Building all_post_constituents.make"; \
	  $(cmtexe) -tag=$(tags) $(all_post_constituents_extratags) build constituent_config -out=$(cmt_local_all_post_constituents_makefile) all_post_constituents
else
$(cmt_local_all_post_constituents_makefile) : $(all_post_constituents_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_all_post_constituents) ] || \
	  [ ! -f $(cmt_final_setup_all_post_constituents) ] || \
	  $(not_all_post_constituents_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building all_post_constituents.make"; \
	  $(cmtexe) -tag=$(tags) $(all_post_constituents_extratags) build constituent_config -out=$(cmt_local_all_post_constituents_makefile) all_post_constituents; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_all_post_constituents_makefile) : $(all_post_constituents_dependencies) build_library_links
	$(echo) "(constituents.make) Building all_post_constituents.make"; \
	  $(cmtexe) -f=$(bin)all_post_constituents.in -tag=$(tags) $(all_post_constituents_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_all_post_constituents_makefile) all_post_constituents
else
$(cmt_local_all_post_constituents_makefile) : $(all_post_constituents_dependencies) $(cmt_build_library_linksstamp) $(bin)all_post_constituents.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_all_post_constituents) ] || \
	  [ ! -f $(cmt_final_setup_all_post_constituents) ] || \
	  $(not_all_post_constituents_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building all_post_constituents.make"; \
	  $(cmtexe) -f=$(bin)all_post_constituents.in -tag=$(tags) $(all_post_constituents_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_all_post_constituents_makefile) all_post_constituents; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(all_post_constituents_extratags) build constituent_makefile -out=$(cmt_local_all_post_constituents_makefile) all_post_constituents

all_post_constituents :: $(all_post_constituents_dependencies) $(cmt_local_all_post_constituents_makefile) dirs all_post_constituentsdirs
	$(echo) "(constituents.make) Starting all_post_constituents"
	@if test -f $(cmt_local_all_post_constituents_makefile); then \
	  $(MAKE) -f $(cmt_local_all_post_constituents_makefile) all_post_constituents; \
	  fi
#	@$(MAKE) -f $(cmt_local_all_post_constituents_makefile) all_post_constituents
	$(echo) "(constituents.make) all_post_constituents done"

clean :: all_post_constituentsclean ;

all_post_constituentsclean :: $(all_post_constituentsclean_dependencies) ##$(cmt_local_all_post_constituents_makefile)
	$(echo) "(constituents.make) Starting all_post_constituentsclean"
	@-if test -f $(cmt_local_all_post_constituents_makefile); then \
	  $(MAKE) -f $(cmt_local_all_post_constituents_makefile) all_post_constituentsclean; \
	fi
	$(echo) "(constituents.make) all_post_constituentsclean done"
#	@-$(MAKE) -f $(cmt_local_all_post_constituents_makefile) all_post_constituentsclean

##	  /bin/rm -f $(cmt_local_all_post_constituents_makefile) $(bin)all_post_constituents_dependencies.make

install :: all_post_constituentsinstall ;

all_post_constituentsinstall :: $(all_post_constituents_dependencies) $(cmt_local_all_post_constituents_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_all_post_constituents_makefile); then \
	  $(MAKE) -f $(cmt_local_all_post_constituents_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_all_post_constituents_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : all_post_constituentsuninstall

$(foreach d,$(all_post_constituents_dependencies),$(eval $(d)uninstall_dependencies += all_post_constituentsuninstall))

all_post_constituentsuninstall : $(all_post_constituentsuninstall_dependencies) ##$(cmt_local_all_post_constituents_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_all_post_constituents_makefile); then \
	  $(MAKE) -f $(cmt_local_all_post_constituents_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_all_post_constituents_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: all_post_constituentsuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ all_post_constituents"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ all_post_constituents done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_checkreq_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_checkreq_has_target_tag

cmt_local_tagfile_checkreq = $(bin)$(TrkAmbiguityProcessor_tag)_checkreq.make
cmt_final_setup_checkreq = $(bin)setup_checkreq.make
cmt_local_checkreq_makefile = $(bin)checkreq.make

checkreq_extratags = -tag_add=target_checkreq

else

cmt_local_tagfile_checkreq = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_checkreq = $(bin)setup.make
cmt_local_checkreq_makefile = $(bin)checkreq.make

endif

not_checkreq_dependencies = { n=0; for p in $?; do m=0; for d in $(checkreq_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
checkreqdirs :
	@if test ! -d $(bin)checkreq; then $(mkdir) -p $(bin)checkreq; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)checkreq
else
checkreqdirs : ;
endif

ifdef cmt_checkreq_has_target_tag

ifndef QUICK
$(cmt_local_checkreq_makefile) : $(checkreq_dependencies) build_library_links
	$(echo) "(constituents.make) Building checkreq.make"; \
	  $(cmtexe) -tag=$(tags) $(checkreq_extratags) build constituent_config -out=$(cmt_local_checkreq_makefile) checkreq
else
$(cmt_local_checkreq_makefile) : $(checkreq_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_checkreq) ] || \
	  [ ! -f $(cmt_final_setup_checkreq) ] || \
	  $(not_checkreq_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building checkreq.make"; \
	  $(cmtexe) -tag=$(tags) $(checkreq_extratags) build constituent_config -out=$(cmt_local_checkreq_makefile) checkreq; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_checkreq_makefile) : $(checkreq_dependencies) build_library_links
	$(echo) "(constituents.make) Building checkreq.make"; \
	  $(cmtexe) -f=$(bin)checkreq.in -tag=$(tags) $(checkreq_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_checkreq_makefile) checkreq
else
$(cmt_local_checkreq_makefile) : $(checkreq_dependencies) $(cmt_build_library_linksstamp) $(bin)checkreq.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_checkreq) ] || \
	  [ ! -f $(cmt_final_setup_checkreq) ] || \
	  $(not_checkreq_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building checkreq.make"; \
	  $(cmtexe) -f=$(bin)checkreq.in -tag=$(tags) $(checkreq_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_checkreq_makefile) checkreq; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(checkreq_extratags) build constituent_makefile -out=$(cmt_local_checkreq_makefile) checkreq

checkreq :: $(checkreq_dependencies) $(cmt_local_checkreq_makefile) dirs checkreqdirs
	$(echo) "(constituents.make) Starting checkreq"
	@if test -f $(cmt_local_checkreq_makefile); then \
	  $(MAKE) -f $(cmt_local_checkreq_makefile) checkreq; \
	  fi
#	@$(MAKE) -f $(cmt_local_checkreq_makefile) checkreq
	$(echo) "(constituents.make) checkreq done"

clean :: checkreqclean ;

checkreqclean :: $(checkreqclean_dependencies) ##$(cmt_local_checkreq_makefile)
	$(echo) "(constituents.make) Starting checkreqclean"
	@-if test -f $(cmt_local_checkreq_makefile); then \
	  $(MAKE) -f $(cmt_local_checkreq_makefile) checkreqclean; \
	fi
	$(echo) "(constituents.make) checkreqclean done"
#	@-$(MAKE) -f $(cmt_local_checkreq_makefile) checkreqclean

##	  /bin/rm -f $(cmt_local_checkreq_makefile) $(bin)checkreq_dependencies.make

install :: checkreqinstall ;

checkreqinstall :: $(checkreq_dependencies) $(cmt_local_checkreq_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_checkreq_makefile); then \
	  $(MAKE) -f $(cmt_local_checkreq_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_checkreq_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : checkrequninstall

$(foreach d,$(checkreq_dependencies),$(eval $(d)uninstall_dependencies += checkrequninstall))

checkrequninstall : $(checkrequninstall_dependencies) ##$(cmt_local_checkreq_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_checkreq_makefile); then \
	  $(MAKE) -f $(cmt_local_checkreq_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_checkreq_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: checkrequninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ checkreq"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ checkreq done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_has_target_tag

cmt_local_tagfile_TrkAmbiguityProcessor_NICOS_Fix_debuginfo = $(bin)$(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessor_NICOS_Fix_debuginfo.make
cmt_final_setup_TrkAmbiguityProcessor_NICOS_Fix_debuginfo = $(bin)setup_TrkAmbiguityProcessor_NICOS_Fix_debuginfo.make
cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile = $(bin)TrkAmbiguityProcessor_NICOS_Fix_debuginfo.make

TrkAmbiguityProcessor_NICOS_Fix_debuginfo_extratags = -tag_add=target_TrkAmbiguityProcessor_NICOS_Fix_debuginfo

else

cmt_local_tagfile_TrkAmbiguityProcessor_NICOS_Fix_debuginfo = $(bin)$(TrkAmbiguityProcessor_tag).make
cmt_final_setup_TrkAmbiguityProcessor_NICOS_Fix_debuginfo = $(bin)setup.make
cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile = $(bin)TrkAmbiguityProcessor_NICOS_Fix_debuginfo.make

endif

not_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkAmbiguityProcessor_NICOS_Fix_debuginfo_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkAmbiguityProcessor_NICOS_Fix_debuginfodirs :
	@if test ! -d $(bin)TrkAmbiguityProcessor_NICOS_Fix_debuginfo; then $(mkdir) -p $(bin)TrkAmbiguityProcessor_NICOS_Fix_debuginfo; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkAmbiguityProcessor_NICOS_Fix_debuginfo
else
TrkAmbiguityProcessor_NICOS_Fix_debuginfodirs : ;
endif

ifdef cmt_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_has_target_tag

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) : $(TrkAmbiguityProcessor_NICOS_Fix_debuginfo_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessor_NICOS_Fix_debuginfo.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessor_NICOS_Fix_debuginfo_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) TrkAmbiguityProcessor_NICOS_Fix_debuginfo
else
$(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) : $(TrkAmbiguityProcessor_NICOS_Fix_debuginfo_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessor_NICOS_Fix_debuginfo) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessor_NICOS_Fix_debuginfo) ] || \
	  $(not_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessor_NICOS_Fix_debuginfo.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessor_NICOS_Fix_debuginfo_extratags) build constituent_config -out=$(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) TrkAmbiguityProcessor_NICOS_Fix_debuginfo; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) : $(TrkAmbiguityProcessor_NICOS_Fix_debuginfo_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkAmbiguityProcessor_NICOS_Fix_debuginfo.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessor_NICOS_Fix_debuginfo.in -tag=$(tags) $(TrkAmbiguityProcessor_NICOS_Fix_debuginfo_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) TrkAmbiguityProcessor_NICOS_Fix_debuginfo
else
$(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) : $(TrkAmbiguityProcessor_NICOS_Fix_debuginfo_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkAmbiguityProcessor_NICOS_Fix_debuginfo.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkAmbiguityProcessor_NICOS_Fix_debuginfo) ] || \
	  [ ! -f $(cmt_final_setup_TrkAmbiguityProcessor_NICOS_Fix_debuginfo) ] || \
	  $(not_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkAmbiguityProcessor_NICOS_Fix_debuginfo.make"; \
	  $(cmtexe) -f=$(bin)TrkAmbiguityProcessor_NICOS_Fix_debuginfo.in -tag=$(tags) $(TrkAmbiguityProcessor_NICOS_Fix_debuginfo_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) TrkAmbiguityProcessor_NICOS_Fix_debuginfo; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkAmbiguityProcessor_NICOS_Fix_debuginfo_extratags) build constituent_makefile -out=$(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) TrkAmbiguityProcessor_NICOS_Fix_debuginfo

TrkAmbiguityProcessor_NICOS_Fix_debuginfo :: $(TrkAmbiguityProcessor_NICOS_Fix_debuginfo_dependencies) $(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) dirs TrkAmbiguityProcessor_NICOS_Fix_debuginfodirs
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessor_NICOS_Fix_debuginfo"
	@if test -f $(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) TrkAmbiguityProcessor_NICOS_Fix_debuginfo; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) TrkAmbiguityProcessor_NICOS_Fix_debuginfo
	$(echo) "(constituents.make) TrkAmbiguityProcessor_NICOS_Fix_debuginfo done"

clean :: TrkAmbiguityProcessor_NICOS_Fix_debuginfoclean ;

TrkAmbiguityProcessor_NICOS_Fix_debuginfoclean :: $(TrkAmbiguityProcessor_NICOS_Fix_debuginfoclean_dependencies) ##$(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile)
	$(echo) "(constituents.make) Starting TrkAmbiguityProcessor_NICOS_Fix_debuginfoclean"
	@-if test -f $(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) TrkAmbiguityProcessor_NICOS_Fix_debuginfoclean; \
	fi
	$(echo) "(constituents.make) TrkAmbiguityProcessor_NICOS_Fix_debuginfoclean done"
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) TrkAmbiguityProcessor_NICOS_Fix_debuginfoclean

##	  /bin/rm -f $(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) $(bin)TrkAmbiguityProcessor_NICOS_Fix_debuginfo_dependencies.make

install :: TrkAmbiguityProcessor_NICOS_Fix_debuginfoinstall ;

TrkAmbiguityProcessor_NICOS_Fix_debuginfoinstall :: $(TrkAmbiguityProcessor_NICOS_Fix_debuginfo_dependencies) $(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkAmbiguityProcessor_NICOS_Fix_debuginfouninstall

$(foreach d,$(TrkAmbiguityProcessor_NICOS_Fix_debuginfo_dependencies),$(eval $(d)uninstall_dependencies += TrkAmbiguityProcessor_NICOS_Fix_debuginfouninstall))

TrkAmbiguityProcessor_NICOS_Fix_debuginfouninstall : $(TrkAmbiguityProcessor_NICOS_Fix_debuginfouninstall_dependencies) ##$(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkAmbiguityProcessor_NICOS_Fix_debuginfo_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkAmbiguityProcessor_NICOS_Fix_debuginfouninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkAmbiguityProcessor_NICOS_Fix_debuginfo"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkAmbiguityProcessor_NICOS_Fix_debuginfo done"
endif

#-- end of constituent ------
#-- start of constituents_trailer ------

uninstall : remove_library_links ;
clean ::
	$(cleanup_echo) $(cmt_build_library_linksstamp)
	-$(cleanup_silent) \rm -f $(cmt_build_library_linksstamp)
#clean :: remove_library_links

remove_library_links ::
ifndef QUICK
	$(echo) "(constituents.make) Removing library links"; \
	  $(remove_library_links)
else
	$(echo) "(constituents.make) Removing library links"; \
	  $(remove_library_links) -f=$(bin)library_links.in -without_cmt
endif

#-- end of constituents_trailer ------
