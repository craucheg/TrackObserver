#-- start of make_header -----------------

#====================================
#  Document TrkAmbiguityProcessorCLIDDB
#
#   Generated Thu Apr 14 11:55:52 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_TrkAmbiguityProcessorCLIDDB_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_TrkAmbiguityProcessorCLIDDB_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_TrkAmbiguityProcessorCLIDDB

TrkAmbiguityProcessor_tag = $(tag)

#cmt_local_tagfile_TrkAmbiguityProcessorCLIDDB = $(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessorCLIDDB.make
cmt_local_tagfile_TrkAmbiguityProcessorCLIDDB = $(bin)$(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessorCLIDDB.make

else

tags      = $(tag),$(CMTEXTRATAGS)

TrkAmbiguityProcessor_tag = $(tag)

#cmt_local_tagfile_TrkAmbiguityProcessorCLIDDB = $(TrkAmbiguityProcessor_tag).make
cmt_local_tagfile_TrkAmbiguityProcessorCLIDDB = $(bin)$(TrkAmbiguityProcessor_tag).make

endif

include $(cmt_local_tagfile_TrkAmbiguityProcessorCLIDDB)
#-include $(cmt_local_tagfile_TrkAmbiguityProcessorCLIDDB)

ifdef cmt_TrkAmbiguityProcessorCLIDDB_has_target_tag

cmt_final_setup_TrkAmbiguityProcessorCLIDDB = $(bin)setup_TrkAmbiguityProcessorCLIDDB.make
cmt_dependencies_in_TrkAmbiguityProcessorCLIDDB = $(bin)dependencies_TrkAmbiguityProcessorCLIDDB.in
#cmt_final_setup_TrkAmbiguityProcessorCLIDDB = $(bin)TrkAmbiguityProcessor_TrkAmbiguityProcessorCLIDDBsetup.make
cmt_local_TrkAmbiguityProcessorCLIDDB_makefile = $(bin)TrkAmbiguityProcessorCLIDDB.make

else

cmt_final_setup_TrkAmbiguityProcessorCLIDDB = $(bin)setup.make
cmt_dependencies_in_TrkAmbiguityProcessorCLIDDB = $(bin)dependencies.in
#cmt_final_setup_TrkAmbiguityProcessorCLIDDB = $(bin)TrkAmbiguityProcessorsetup.make
cmt_local_TrkAmbiguityProcessorCLIDDB_makefile = $(bin)TrkAmbiguityProcessorCLIDDB.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)TrkAmbiguityProcessorsetup.make

#TrkAmbiguityProcessorCLIDDB :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'TrkAmbiguityProcessorCLIDDB'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = TrkAmbiguityProcessorCLIDDB/
#TrkAmbiguityProcessorCLIDDB::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genCLIDDB_header
# Author: Paolo Calafiura
# derived from genconf_header

# Use genCLIDDB_cmd to create package clid.db files

.PHONY: TrkAmbiguityProcessorCLIDDB TrkAmbiguityProcessorCLIDDBclean

outname := clid.db
cliddb  := TrkAmbiguityProcessor_$(outname)
instdir := $(CMTINSTALLAREA)/share
result  := $(instdir)/$(cliddb)
product := $(instdir)/$(outname)
conflib := $(bin)$(library_prefix)TrkAmbiguityProcessor.$(shlibsuffix)

TrkAmbiguityProcessorCLIDDB :: $(result)

$(instdir) :
	$(mkdir) -p $(instdir)

$(result) : $(conflib) $(product)
	@$(genCLIDDB_cmd) -p TrkAmbiguityProcessor -i$(product) -o $(result)

$(product) : $(instdir)
	touch $(product)

TrkAmbiguityProcessorCLIDDBclean ::
	$(cleanup_silent) $(uninstall_command) $(product) $(result)
	$(cleanup_silent) $(cmt_uninstallarea_command) $(product) $(result)

#-- start of cleanup_header --------------

clean :: TrkAmbiguityProcessorCLIDDBclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(TrkAmbiguityProcessorCLIDDB.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

TrkAmbiguityProcessorCLIDDBclean ::
#-- end of cleanup_header ---------------
