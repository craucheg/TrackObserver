#-- start of make_header -----------------

#====================================
#  Document TrkAmbiguityProcessorConf
#
#   Generated Thu Apr 14 11:56:00 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_TrkAmbiguityProcessorConf_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_TrkAmbiguityProcessorConf_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_TrkAmbiguityProcessorConf

TrkAmbiguityProcessor_tag = $(tag)

#cmt_local_tagfile_TrkAmbiguityProcessorConf = $(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessorConf.make
cmt_local_tagfile_TrkAmbiguityProcessorConf = $(bin)$(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessorConf.make

else

tags      = $(tag),$(CMTEXTRATAGS)

TrkAmbiguityProcessor_tag = $(tag)

#cmt_local_tagfile_TrkAmbiguityProcessorConf = $(TrkAmbiguityProcessor_tag).make
cmt_local_tagfile_TrkAmbiguityProcessorConf = $(bin)$(TrkAmbiguityProcessor_tag).make

endif

include $(cmt_local_tagfile_TrkAmbiguityProcessorConf)
#-include $(cmt_local_tagfile_TrkAmbiguityProcessorConf)

ifdef cmt_TrkAmbiguityProcessorConf_has_target_tag

cmt_final_setup_TrkAmbiguityProcessorConf = $(bin)setup_TrkAmbiguityProcessorConf.make
cmt_dependencies_in_TrkAmbiguityProcessorConf = $(bin)dependencies_TrkAmbiguityProcessorConf.in
#cmt_final_setup_TrkAmbiguityProcessorConf = $(bin)TrkAmbiguityProcessor_TrkAmbiguityProcessorConfsetup.make
cmt_local_TrkAmbiguityProcessorConf_makefile = $(bin)TrkAmbiguityProcessorConf.make

else

cmt_final_setup_TrkAmbiguityProcessorConf = $(bin)setup.make
cmt_dependencies_in_TrkAmbiguityProcessorConf = $(bin)dependencies.in
#cmt_final_setup_TrkAmbiguityProcessorConf = $(bin)TrkAmbiguityProcessorsetup.make
cmt_local_TrkAmbiguityProcessorConf_makefile = $(bin)TrkAmbiguityProcessorConf.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)TrkAmbiguityProcessorsetup.make

#TrkAmbiguityProcessorConf :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'TrkAmbiguityProcessorConf'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = TrkAmbiguityProcessorConf/
#TrkAmbiguityProcessorConf::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genconfig_header
# Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

# Use genconf.exe to create configurables python modules, then have the
# normal python install procedure take over.

.PHONY: TrkAmbiguityProcessorConf TrkAmbiguityProcessorConfclean

confpy  := TrkAmbiguityProcessorConf.py
conflib := $(bin)$(library_prefix)TrkAmbiguityProcessor.$(shlibsuffix)
confdb  := TrkAmbiguityProcessor.confdb
instdir := $(CMTINSTALLAREA)$(shared_install_subdir)/python/$(package)
product := $(instdir)/$(confpy)
initpy  := $(instdir)/__init__.py

ifdef GENCONF_ECHO
genconf_silent =
else
genconf_silent = $(silent)
endif

TrkAmbiguityProcessorConf :: TrkAmbiguityProcessorConfinstall

install :: TrkAmbiguityProcessorConfinstall

TrkAmbiguityProcessorConfinstall : /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools/TrkAmbiguityProcessor/genConf/TrkAmbiguityProcessor/$(confpy)
	@echo "Installing /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools/TrkAmbiguityProcessor/genConf/TrkAmbiguityProcessor in /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InstallArea/python" ; \
	 $(install_command) --exclude="*.py?" --exclude="__init__.py" --exclude="*.confdb" /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools/TrkAmbiguityProcessor/genConf/TrkAmbiguityProcessor /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InstallArea/python ; \

/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools/TrkAmbiguityProcessor/genConf/TrkAmbiguityProcessor/$(confpy) : $(conflib) /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools/TrkAmbiguityProcessor/genConf/TrkAmbiguityProcessor
	$(genconf_silent) $(genconfig_cmd)   -o /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools/TrkAmbiguityProcessor/genConf/TrkAmbiguityProcessor -p $(package) \
	  --configurable-module=GaudiKernel.Proxy \
	  --configurable-default-name=Configurable.DefaultName \
	  --configurable-algorithm=ConfigurableAlgorithm \
	  --configurable-algtool=ConfigurableAlgTool \
	  --configurable-auditor=ConfigurableAuditor \
          --configurable-service=ConfigurableService \
	  -i ../$(tag)/$(library_prefix)TrkAmbiguityProcessor.$(shlibsuffix)

/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools/TrkAmbiguityProcessor/genConf/TrkAmbiguityProcessor:
	@ if [ ! -d /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools/TrkAmbiguityProcessor/genConf/TrkAmbiguityProcessor ] ; then mkdir -p /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools/TrkAmbiguityProcessor/genConf/TrkAmbiguityProcessor ; fi ;

TrkAmbiguityProcessorConfclean :: TrkAmbiguityProcessorConfuninstall
	$(cleanup_silent) $(remove_command) /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools/TrkAmbiguityProcessor/genConf/TrkAmbiguityProcessor/$(confpy) /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools/TrkAmbiguityProcessor/genConf/TrkAmbiguityProcessor/$(confdb)

uninstall :: TrkAmbiguityProcessorConfuninstall

TrkAmbiguityProcessorConfuninstall ::
	@$(uninstall_command) /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InstallArea/python
libTrkAmbiguityProcessor_so_dependencies = ../x86_64-slc6-gcc49-opt/libTrkAmbiguityProcessor.so
#-- start of cleanup_header --------------

clean :: TrkAmbiguityProcessorConfclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(TrkAmbiguityProcessorConf.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

TrkAmbiguityProcessorConfclean ::
#-- end of cleanup_header ---------------
