#-- start of make_header -----------------

#====================================
#  Library TrkAmbiguityProcessor
#
#   Generated Thu Apr 14 11:53:27 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_TrkAmbiguityProcessor_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_TrkAmbiguityProcessor_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_TrkAmbiguityProcessor

TrkAmbiguityProcessor_tag = $(tag)

#cmt_local_tagfile_TrkAmbiguityProcessor = $(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessor.make
cmt_local_tagfile_TrkAmbiguityProcessor = $(bin)$(TrkAmbiguityProcessor_tag)_TrkAmbiguityProcessor.make

else

tags      = $(tag),$(CMTEXTRATAGS)

TrkAmbiguityProcessor_tag = $(tag)

#cmt_local_tagfile_TrkAmbiguityProcessor = $(TrkAmbiguityProcessor_tag).make
cmt_local_tagfile_TrkAmbiguityProcessor = $(bin)$(TrkAmbiguityProcessor_tag).make

endif

include $(cmt_local_tagfile_TrkAmbiguityProcessor)
#-include $(cmt_local_tagfile_TrkAmbiguityProcessor)

ifdef cmt_TrkAmbiguityProcessor_has_target_tag

cmt_final_setup_TrkAmbiguityProcessor = $(bin)setup_TrkAmbiguityProcessor.make
cmt_dependencies_in_TrkAmbiguityProcessor = $(bin)dependencies_TrkAmbiguityProcessor.in
#cmt_final_setup_TrkAmbiguityProcessor = $(bin)TrkAmbiguityProcessor_TrkAmbiguityProcessorsetup.make
cmt_local_TrkAmbiguityProcessor_makefile = $(bin)TrkAmbiguityProcessor.make

else

cmt_final_setup_TrkAmbiguityProcessor = $(bin)setup.make
cmt_dependencies_in_TrkAmbiguityProcessor = $(bin)dependencies.in
#cmt_final_setup_TrkAmbiguityProcessor = $(bin)TrkAmbiguityProcessorsetup.make
cmt_local_TrkAmbiguityProcessor_makefile = $(bin)TrkAmbiguityProcessor.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)TrkAmbiguityProcessorsetup.make

#TrkAmbiguityProcessor :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'TrkAmbiguityProcessor'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = TrkAmbiguityProcessor/
#TrkAmbiguityProcessor::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

TrkAmbiguityProcessorlibname   = $(bin)$(library_prefix)TrkAmbiguityProcessor$(library_suffix)
TrkAmbiguityProcessorlib       = $(TrkAmbiguityProcessorlibname).a
TrkAmbiguityProcessorstamp     = $(bin)TrkAmbiguityProcessor.stamp
TrkAmbiguityProcessorshstamp   = $(bin)TrkAmbiguityProcessor.shstamp

TrkAmbiguityProcessor :: dirs  TrkAmbiguityProcessorLIB
	$(echo) "TrkAmbiguityProcessor ok"

#-- end of libary_header ----------------
#-- start of library_no_static ------

#TrkAmbiguityProcessorLIB :: $(TrkAmbiguityProcessorlib) $(TrkAmbiguityProcessorshstamp)
TrkAmbiguityProcessorLIB :: $(TrkAmbiguityProcessorshstamp)
	$(echo) "TrkAmbiguityProcessor : library ok"

$(TrkAmbiguityProcessorlib) :: $(bin)DenseEnvironmentsAmbiguityProcessorTool.o $(bin)SimpleAmbiguityProcessorTool.o $(bin)TrackScoringTool.o $(bin)TrackSelectionProcessorTool.o $(bin)TrkAmbiguityProcessor_entries.o $(bin)TrkAmbiguityProcessor_load.o
	$(lib_echo) "static library $@"
	$(lib_silent) cd $(bin); \
	  $(ar) $(TrkAmbiguityProcessorlib) $?
	$(lib_silent) $(ranlib) $(TrkAmbiguityProcessorlib)
	$(lib_silent) cat /dev/null >$(TrkAmbiguityProcessorstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

#
# We add one level of dependency upon the true shared library 
# (rather than simply upon the stamp file)
# this is for cases where the shared library has not been built
# while the stamp was created (error??) 
#

$(TrkAmbiguityProcessorlibname).$(shlibsuffix) :: $(bin)DenseEnvironmentsAmbiguityProcessorTool.o $(bin)SimpleAmbiguityProcessorTool.o $(bin)TrackScoringTool.o $(bin)TrackSelectionProcessorTool.o $(bin)TrkAmbiguityProcessor_entries.o $(bin)TrkAmbiguityProcessor_load.o $(use_requirements) $(TrkAmbiguityProcessorstamps)
	$(lib_echo) "shared library $@"
	$(lib_silent) $(shlibbuilder) $(shlibflags) -o $@ $(bin)DenseEnvironmentsAmbiguityProcessorTool.o $(bin)SimpleAmbiguityProcessorTool.o $(bin)TrackScoringTool.o $(bin)TrackSelectionProcessorTool.o $(bin)TrkAmbiguityProcessor_entries.o $(bin)TrkAmbiguityProcessor_load.o $(TrkAmbiguityProcessor_shlibflags)
	$(lib_silent) cat /dev/null >$(TrkAmbiguityProcessorstamp) && \
	  cat /dev/null >$(TrkAmbiguityProcessorshstamp)

$(TrkAmbiguityProcessorshstamp) :: $(TrkAmbiguityProcessorlibname).$(shlibsuffix)
	$(lib_silent) if test -f $(TrkAmbiguityProcessorlibname).$(shlibsuffix) ; then \
	  cat /dev/null >$(TrkAmbiguityProcessorstamp) && \
	  cat /dev/null >$(TrkAmbiguityProcessorshstamp) ; fi

TrkAmbiguityProcessorclean ::
	$(cleanup_echo) objects TrkAmbiguityProcessor
	$(cleanup_silent) /bin/rm -f $(bin)DenseEnvironmentsAmbiguityProcessorTool.o $(bin)SimpleAmbiguityProcessorTool.o $(bin)TrackScoringTool.o $(bin)TrackSelectionProcessorTool.o $(bin)TrkAmbiguityProcessor_entries.o $(bin)TrkAmbiguityProcessor_load.o
	$(cleanup_silent) /bin/rm -f $(patsubst %.o,%.d,$(bin)DenseEnvironmentsAmbiguityProcessorTool.o $(bin)SimpleAmbiguityProcessorTool.o $(bin)TrackScoringTool.o $(bin)TrackSelectionProcessorTool.o $(bin)TrkAmbiguityProcessor_entries.o $(bin)TrkAmbiguityProcessor_load.o) $(patsubst %.o,%.dep,$(bin)DenseEnvironmentsAmbiguityProcessorTool.o $(bin)SimpleAmbiguityProcessorTool.o $(bin)TrackScoringTool.o $(bin)TrackSelectionProcessorTool.o $(bin)TrkAmbiguityProcessor_entries.o $(bin)TrkAmbiguityProcessor_load.o) $(patsubst %.o,%.d.stamp,$(bin)DenseEnvironmentsAmbiguityProcessorTool.o $(bin)SimpleAmbiguityProcessorTool.o $(bin)TrackScoringTool.o $(bin)TrackSelectionProcessorTool.o $(bin)TrkAmbiguityProcessor_entries.o $(bin)TrkAmbiguityProcessor_load.o)
	$(cleanup_silent) cd $(bin); /bin/rm -rf TrkAmbiguityProcessor_deps TrkAmbiguityProcessor_dependencies.make

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/lib
TrkAmbiguityProcessorinstallname = $(library_prefix)TrkAmbiguityProcessor$(library_suffix).$(shlibsuffix)

TrkAmbiguityProcessor :: TrkAmbiguityProcessorinstall ;

install :: TrkAmbiguityProcessorinstall ;

TrkAmbiguityProcessorinstall :: $(install_dir)/$(TrkAmbiguityProcessorinstallname)
ifdef CMTINSTALLAREA
	$(echo) "installation done"
endif

$(install_dir)/$(TrkAmbiguityProcessorinstallname) :: $(bin)$(TrkAmbiguityProcessorinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(TrkAmbiguityProcessorinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##TrkAmbiguityProcessorclean :: TrkAmbiguityProcessoruninstall

uninstall :: TrkAmbiguityProcessoruninstall ;

TrkAmbiguityProcessoruninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(TrkAmbiguityProcessorinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#-- end of library_no_static ------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkAmbiguityProcessorclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)DenseEnvironmentsAmbiguityProcessorTool.d

$(bin)$(binobj)DenseEnvironmentsAmbiguityProcessorTool.d :

$(bin)$(binobj)DenseEnvironmentsAmbiguityProcessorTool.o : $(cmt_final_setup_TrkAmbiguityProcessor)

$(bin)$(binobj)DenseEnvironmentsAmbiguityProcessorTool.o : $(src)DenseEnvironmentsAmbiguityProcessorTool.cxx
	$(cpp_echo) $(src)DenseEnvironmentsAmbiguityProcessorTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkAmbiguityProcessor_pp_cppflags) $(lib_TrkAmbiguityProcessor_pp_cppflags) $(DenseEnvironmentsAmbiguityProcessorTool_pp_cppflags) $(use_cppflags) $(TrkAmbiguityProcessor_cppflags) $(lib_TrkAmbiguityProcessor_cppflags) $(DenseEnvironmentsAmbiguityProcessorTool_cppflags) $(DenseEnvironmentsAmbiguityProcessorTool_cxx_cppflags)  $(src)DenseEnvironmentsAmbiguityProcessorTool.cxx
endif
endif

else
$(bin)TrkAmbiguityProcessor_dependencies.make : $(DenseEnvironmentsAmbiguityProcessorTool_cxx_dependencies)

$(bin)TrkAmbiguityProcessor_dependencies.make : $(src)DenseEnvironmentsAmbiguityProcessorTool.cxx

$(bin)$(binobj)DenseEnvironmentsAmbiguityProcessorTool.o : $(DenseEnvironmentsAmbiguityProcessorTool_cxx_dependencies)
	$(cpp_echo) $(src)DenseEnvironmentsAmbiguityProcessorTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkAmbiguityProcessor_pp_cppflags) $(lib_TrkAmbiguityProcessor_pp_cppflags) $(DenseEnvironmentsAmbiguityProcessorTool_pp_cppflags) $(use_cppflags) $(TrkAmbiguityProcessor_cppflags) $(lib_TrkAmbiguityProcessor_cppflags) $(DenseEnvironmentsAmbiguityProcessorTool_cppflags) $(DenseEnvironmentsAmbiguityProcessorTool_cxx_cppflags)  $(src)DenseEnvironmentsAmbiguityProcessorTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkAmbiguityProcessorclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)SimpleAmbiguityProcessorTool.d

$(bin)$(binobj)SimpleAmbiguityProcessorTool.d :

$(bin)$(binobj)SimpleAmbiguityProcessorTool.o : $(cmt_final_setup_TrkAmbiguityProcessor)

$(bin)$(binobj)SimpleAmbiguityProcessorTool.o : $(src)SimpleAmbiguityProcessorTool.cxx
	$(cpp_echo) $(src)SimpleAmbiguityProcessorTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkAmbiguityProcessor_pp_cppflags) $(lib_TrkAmbiguityProcessor_pp_cppflags) $(SimpleAmbiguityProcessorTool_pp_cppflags) $(use_cppflags) $(TrkAmbiguityProcessor_cppflags) $(lib_TrkAmbiguityProcessor_cppflags) $(SimpleAmbiguityProcessorTool_cppflags) $(SimpleAmbiguityProcessorTool_cxx_cppflags)  $(src)SimpleAmbiguityProcessorTool.cxx
endif
endif

else
$(bin)TrkAmbiguityProcessor_dependencies.make : $(SimpleAmbiguityProcessorTool_cxx_dependencies)

$(bin)TrkAmbiguityProcessor_dependencies.make : $(src)SimpleAmbiguityProcessorTool.cxx

$(bin)$(binobj)SimpleAmbiguityProcessorTool.o : $(SimpleAmbiguityProcessorTool_cxx_dependencies)
	$(cpp_echo) $(src)SimpleAmbiguityProcessorTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkAmbiguityProcessor_pp_cppflags) $(lib_TrkAmbiguityProcessor_pp_cppflags) $(SimpleAmbiguityProcessorTool_pp_cppflags) $(use_cppflags) $(TrkAmbiguityProcessor_cppflags) $(lib_TrkAmbiguityProcessor_cppflags) $(SimpleAmbiguityProcessorTool_cppflags) $(SimpleAmbiguityProcessorTool_cxx_cppflags)  $(src)SimpleAmbiguityProcessorTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkAmbiguityProcessorclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)TrackScoringTool.d

$(bin)$(binobj)TrackScoringTool.d :

$(bin)$(binobj)TrackScoringTool.o : $(cmt_final_setup_TrkAmbiguityProcessor)

$(bin)$(binobj)TrackScoringTool.o : $(src)TrackScoringTool.cxx
	$(cpp_echo) $(src)TrackScoringTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkAmbiguityProcessor_pp_cppflags) $(lib_TrkAmbiguityProcessor_pp_cppflags) $(TrackScoringTool_pp_cppflags) $(use_cppflags) $(TrkAmbiguityProcessor_cppflags) $(lib_TrkAmbiguityProcessor_cppflags) $(TrackScoringTool_cppflags) $(TrackScoringTool_cxx_cppflags)  $(src)TrackScoringTool.cxx
endif
endif

else
$(bin)TrkAmbiguityProcessor_dependencies.make : $(TrackScoringTool_cxx_dependencies)

$(bin)TrkAmbiguityProcessor_dependencies.make : $(src)TrackScoringTool.cxx

$(bin)$(binobj)TrackScoringTool.o : $(TrackScoringTool_cxx_dependencies)
	$(cpp_echo) $(src)TrackScoringTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkAmbiguityProcessor_pp_cppflags) $(lib_TrkAmbiguityProcessor_pp_cppflags) $(TrackScoringTool_pp_cppflags) $(use_cppflags) $(TrkAmbiguityProcessor_cppflags) $(lib_TrkAmbiguityProcessor_cppflags) $(TrackScoringTool_cppflags) $(TrackScoringTool_cxx_cppflags)  $(src)TrackScoringTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkAmbiguityProcessorclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)TrackSelectionProcessorTool.d

$(bin)$(binobj)TrackSelectionProcessorTool.d :

$(bin)$(binobj)TrackSelectionProcessorTool.o : $(cmt_final_setup_TrkAmbiguityProcessor)

$(bin)$(binobj)TrackSelectionProcessorTool.o : $(src)TrackSelectionProcessorTool.cxx
	$(cpp_echo) $(src)TrackSelectionProcessorTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkAmbiguityProcessor_pp_cppflags) $(lib_TrkAmbiguityProcessor_pp_cppflags) $(TrackSelectionProcessorTool_pp_cppflags) $(use_cppflags) $(TrkAmbiguityProcessor_cppflags) $(lib_TrkAmbiguityProcessor_cppflags) $(TrackSelectionProcessorTool_cppflags) $(TrackSelectionProcessorTool_cxx_cppflags)  $(src)TrackSelectionProcessorTool.cxx
endif
endif

else
$(bin)TrkAmbiguityProcessor_dependencies.make : $(TrackSelectionProcessorTool_cxx_dependencies)

$(bin)TrkAmbiguityProcessor_dependencies.make : $(src)TrackSelectionProcessorTool.cxx

$(bin)$(binobj)TrackSelectionProcessorTool.o : $(TrackSelectionProcessorTool_cxx_dependencies)
	$(cpp_echo) $(src)TrackSelectionProcessorTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkAmbiguityProcessor_pp_cppflags) $(lib_TrkAmbiguityProcessor_pp_cppflags) $(TrackSelectionProcessorTool_pp_cppflags) $(use_cppflags) $(TrkAmbiguityProcessor_cppflags) $(lib_TrkAmbiguityProcessor_cppflags) $(TrackSelectionProcessorTool_cppflags) $(TrackSelectionProcessorTool_cxx_cppflags)  $(src)TrackSelectionProcessorTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkAmbiguityProcessorclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)TrkAmbiguityProcessor_entries.d

$(bin)$(binobj)TrkAmbiguityProcessor_entries.d :

$(bin)$(binobj)TrkAmbiguityProcessor_entries.o : $(cmt_final_setup_TrkAmbiguityProcessor)

$(bin)$(binobj)TrkAmbiguityProcessor_entries.o : $(src)components/TrkAmbiguityProcessor_entries.cxx
	$(cpp_echo) $(src)components/TrkAmbiguityProcessor_entries.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkAmbiguityProcessor_pp_cppflags) $(lib_TrkAmbiguityProcessor_pp_cppflags) $(TrkAmbiguityProcessor_entries_pp_cppflags) $(use_cppflags) $(TrkAmbiguityProcessor_cppflags) $(lib_TrkAmbiguityProcessor_cppflags) $(TrkAmbiguityProcessor_entries_cppflags) $(TrkAmbiguityProcessor_entries_cxx_cppflags) -I../src/components $(src)components/TrkAmbiguityProcessor_entries.cxx
endif
endif

else
$(bin)TrkAmbiguityProcessor_dependencies.make : $(TrkAmbiguityProcessor_entries_cxx_dependencies)

$(bin)TrkAmbiguityProcessor_dependencies.make : $(src)components/TrkAmbiguityProcessor_entries.cxx

$(bin)$(binobj)TrkAmbiguityProcessor_entries.o : $(TrkAmbiguityProcessor_entries_cxx_dependencies)
	$(cpp_echo) $(src)components/TrkAmbiguityProcessor_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkAmbiguityProcessor_pp_cppflags) $(lib_TrkAmbiguityProcessor_pp_cppflags) $(TrkAmbiguityProcessor_entries_pp_cppflags) $(use_cppflags) $(TrkAmbiguityProcessor_cppflags) $(lib_TrkAmbiguityProcessor_cppflags) $(TrkAmbiguityProcessor_entries_cppflags) $(TrkAmbiguityProcessor_entries_cxx_cppflags) -I../src/components $(src)components/TrkAmbiguityProcessor_entries.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkAmbiguityProcessorclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)TrkAmbiguityProcessor_load.d

$(bin)$(binobj)TrkAmbiguityProcessor_load.d :

$(bin)$(binobj)TrkAmbiguityProcessor_load.o : $(cmt_final_setup_TrkAmbiguityProcessor)

$(bin)$(binobj)TrkAmbiguityProcessor_load.o : $(src)components/TrkAmbiguityProcessor_load.cxx
	$(cpp_echo) $(src)components/TrkAmbiguityProcessor_load.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkAmbiguityProcessor_pp_cppflags) $(lib_TrkAmbiguityProcessor_pp_cppflags) $(TrkAmbiguityProcessor_load_pp_cppflags) $(use_cppflags) $(TrkAmbiguityProcessor_cppflags) $(lib_TrkAmbiguityProcessor_cppflags) $(TrkAmbiguityProcessor_load_cppflags) $(TrkAmbiguityProcessor_load_cxx_cppflags) -I../src/components $(src)components/TrkAmbiguityProcessor_load.cxx
endif
endif

else
$(bin)TrkAmbiguityProcessor_dependencies.make : $(TrkAmbiguityProcessor_load_cxx_dependencies)

$(bin)TrkAmbiguityProcessor_dependencies.make : $(src)components/TrkAmbiguityProcessor_load.cxx

$(bin)$(binobj)TrkAmbiguityProcessor_load.o : $(TrkAmbiguityProcessor_load_cxx_dependencies)
	$(cpp_echo) $(src)components/TrkAmbiguityProcessor_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkAmbiguityProcessor_pp_cppflags) $(lib_TrkAmbiguityProcessor_pp_cppflags) $(TrkAmbiguityProcessor_load_pp_cppflags) $(use_cppflags) $(TrkAmbiguityProcessor_cppflags) $(lib_TrkAmbiguityProcessor_cppflags) $(TrkAmbiguityProcessor_load_cppflags) $(TrkAmbiguityProcessor_load_cxx_cppflags) -I../src/components $(src)components/TrkAmbiguityProcessor_load.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: TrkAmbiguityProcessorclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(TrkAmbiguityProcessor.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

TrkAmbiguityProcessorclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library TrkAmbiguityProcessor
	-$(cleanup_silent) cd $(bin) && \rm -f $(library_prefix)TrkAmbiguityProcessor$(library_suffix).a $(library_prefix)TrkAmbiguityProcessor$(library_suffix).$(shlibsuffix) TrkAmbiguityProcessor.stamp TrkAmbiguityProcessor.shstamp
#-- end of cleanup_library ---------------
