#Thu Apr 14 11:56:02 2016"""Automatically generated. DO NOT EDIT please"""
from GaudiKernel.GaudiHandles import *
from GaudiKernel.Proxy.Configurable import *

class Trk__DenseEnvironmentsAmbiguityProcessorTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'DropDouble' : True, # bool
    'ForceRefit' : True, # bool
    'RefitPrds' : True, # bool
    'MatEffects' : 3, # int
    'IncidentService' : ServiceHandle('IncidentSvc'), # GaudiHandle
    'ScoringTool' : PublicToolHandle('Trk::TrackScoringTool/TrackScoringTool'), # GaudiHandle
    'ObserverTool' : PublicToolHandle('Trk::TrkObserverTool/TrkObserverTool'), # GaudiHandle
    'SelectionTool' : PublicToolHandle('InDet::InDetDenseEnvAmbiTrackSelectionTool/InDetAmbiTrackSelectionTool'), # GaudiHandle
    'Fitter' : PublicToolHandle('Trk::KalmanFitter/InDetTrackFitter'), # GaudiHandle
    'SplitProbTool' : PublicToolHandle('InDet::NnPixelClusterSplitProbTool/NnPixelClusterSplitProbTool'), # GaudiHandle
    'AssociationTool' : PublicToolHandle('Trk::PRD_AssociationTool/DEAmbi_PRD_AssociationTool'), # GaudiHandle
    'SuppressHoleSearch' : False, # bool
    'SuppressTrackFit' : False, # bool
    'tryBremFit' : False, # bool
    'caloSeededBrem' : False, # bool
    'pTminBrem' : 1000.00, # float
    'etaBounds' : [ 0.800000 , 1.600000 , 2.500000 ], # list
    'sharedProbCut' : 0.300000, # float
    'sharedProbCut2' : 0.300000, # float
    'SplitClusterAmbiguityMap' : 'SplitClusterAmbiguityMap', # str
    'MonitorAmbiguitySolving' : False, # bool
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'etaBounds' : """ eta intervals for internal monitoring """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__DenseEnvironmentsAmbiguityProcessorTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkAmbiguityProcessor'
  def getType( self ):
      return 'Trk::DenseEnvironmentsAmbiguityProcessorTool'
  pass # class Trk__DenseEnvironmentsAmbiguityProcessorTool

class Trk__SimpleAmbiguityProcessorTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'DropDouble' : True, # bool
    'ForceRefit' : True, # bool
    'RefitPrds' : False, # bool
    'MatEffects' : 3, # int
    'ScoringTool' : PublicToolHandle('Trk::TrackScoringTool/TrackScoringTool'), # GaudiHandle
    'SelectionTool' : PublicToolHandle('InDet::InDetAmbiTrackSelectionTool/InDetAmbiTrackSelectionTool'), # GaudiHandle
    'Fitter' : PublicToolHandle('Trk::KalmanFitter/InDetTrackFitter'), # GaudiHandle
    'SuppressHoleSearch' : False, # bool
    'SuppressTrackFit' : False, # bool
    'tryBremFit' : False, # bool
    'caloSeededBrem' : False, # bool
    'pTminBrem' : 1000.00, # float
    'etaBounds' : [ 0.800000 , 1.600000 , 2.500000 , 2.500000 , 10.000000 ], # list
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'etaBounds' : """ eta intervals for internal monitoring """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__SimpleAmbiguityProcessorTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkAmbiguityProcessor'
  def getType( self ):
      return 'Trk::SimpleAmbiguityProcessorTool'
  pass # class Trk__SimpleAmbiguityProcessorTool

class Trk__TrackScoringTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'SumHelpTool' : PublicToolHandle('Trk::TrackSummaryTool'), # GaudiHandle
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__TrackScoringTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkAmbiguityProcessor'
  def getType( self ):
      return 'Trk::TrackScoringTool'
  pass # class Trk__TrackScoringTool

class Trk__TrackSelectionProcessorTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'DropDouble' : True, # bool
    'ScoringTool' : PublicToolHandle('Trk::TrackScoringTool/TrackScoringTool'), # GaudiHandle
    'SelectionTool' : PublicToolHandle('InDet::InDetAmbiTrackSelectionTool/InDetAmbiTrackSelectionTool'), # GaudiHandle
    'DisableSorting' : False, # bool
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__TrackSelectionProcessorTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkAmbiguityProcessor'
  def getType( self ):
      return 'Trk::TrackSelectionProcessorTool'
  pass # class Trk__TrackSelectionProcessorTool
