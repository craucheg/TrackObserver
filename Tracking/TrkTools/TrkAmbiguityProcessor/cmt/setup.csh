# echo "setup TrkAmbiguityProcessor TrkAmbiguityProcessor-01-00-09 in /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.7.5/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtTrkAmbiguityProcessortempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtTrkAmbiguityProcessortempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=TrkAmbiguityProcessor -version=TrkAmbiguityProcessor-01-00-09 -path=/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools  -no_cleanup $* >${cmtTrkAmbiguityProcessortempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=TrkAmbiguityProcessor -version=TrkAmbiguityProcessor-01-00-09 -path=/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools  -no_cleanup $* >${cmtTrkAmbiguityProcessortempfile}"
  set cmtsetupstatus=2
  /bin/rm -f ${cmtTrkAmbiguityProcessortempfile}
  unset cmtTrkAmbiguityProcessortempfile
  exit $cmtsetupstatus
endif
set cmtsetupstatus=0
source ${cmtTrkAmbiguityProcessortempfile}
if ( $status != 0 ) then
  set cmtsetupstatus=2
endif
/bin/rm -f ${cmtTrkAmbiguityProcessortempfile}
unset cmtTrkAmbiguityProcessortempfile
exit $cmtsetupstatus

