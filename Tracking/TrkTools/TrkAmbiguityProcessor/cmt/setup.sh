# echo "setup TrkAmbiguityProcessor TrkAmbiguityProcessor-01-00-09 in /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.7.5/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtTrkAmbiguityProcessortempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtTrkAmbiguityProcessortempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=TrkAmbiguityProcessor -version=TrkAmbiguityProcessor-01-00-09 -path=/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools  -no_cleanup $* >${cmtTrkAmbiguityProcessortempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe setup -sh -pack=TrkAmbiguityProcessor -version=TrkAmbiguityProcessor-01-00-09 -path=/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkTools  -no_cleanup $* >${cmtTrkAmbiguityProcessortempfile}"
  cmtsetupstatus=2
  /bin/rm -f ${cmtTrkAmbiguityProcessortempfile}
  unset cmtTrkAmbiguityProcessortempfile
  return $cmtsetupstatus
fi
cmtsetupstatus=0
. ${cmtTrkAmbiguityProcessortempfile}
if test $? != 0 ; then
  cmtsetupstatus=2
fi
/bin/rm -f ${cmtTrkAmbiguityProcessortempfile}
unset cmtTrkAmbiguityProcessortempfile
return $cmtsetupstatus

