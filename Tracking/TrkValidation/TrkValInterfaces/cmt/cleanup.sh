# echo "cleanup TrkValInterfaces TrkValInterfaces-01-00-03 in /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.7.5/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtTrkValInterfacestempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtTrkValInterfacestempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=TrkValInterfaces -version=TrkValInterfaces-01-00-03 -path=/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation  $* >${cmtTrkValInterfacestempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=TrkValInterfaces -version=TrkValInterfaces-01-00-03 -path=/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation  $* >${cmtTrkValInterfacestempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtTrkValInterfacestempfile}
  unset cmtTrkValInterfacestempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtTrkValInterfacestempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtTrkValInterfacestempfile}
unset cmtTrkValInterfacestempfile
return $cmtcleanupstatus

