# echo "setup TrkValTools TrkValTools-00-10-04 in /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation"

if ( $?CMTROOT == 0 ) then
  setenv CMTROOT /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.7.5/CMT/v1r25p20140131
endif
source ${CMTROOT}/mgr/setup.csh
set cmtTrkValToolstempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if $status != 0 then
  set cmtTrkValToolstempfile=/tmp/cmt.$$
endif
${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=TrkValTools -version=TrkValTools-00-10-04 -path=/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation  -no_cleanup $* >${cmtTrkValToolstempfile}
if ( $status != 0 ) then
  echo "${CMTROOT}/${CMTBIN}/cmt.exe setup -csh -pack=TrkValTools -version=TrkValTools-00-10-04 -path=/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation  -no_cleanup $* >${cmtTrkValToolstempfile}"
  set cmtsetupstatus=2
  /bin/rm -f ${cmtTrkValToolstempfile}
  unset cmtTrkValToolstempfile
  exit $cmtsetupstatus
endif
set cmtsetupstatus=0
source ${cmtTrkValToolstempfile}
if ( $status != 0 ) then
  set cmtsetupstatus=2
endif
/bin/rm -f ${cmtTrkValToolstempfile}
unset cmtTrkValToolstempfile
exit $cmtsetupstatus

