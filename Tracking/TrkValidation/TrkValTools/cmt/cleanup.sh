# echo "cleanup TrkValTools TrkValTools-00-10-04 in /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc49-opt/20.7.5/CMT/v1r25p20140131; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh
cmtTrkValToolstempfile=`${CMTROOT}/${CMTBIN}/cmt.exe -quiet build temporary_name`
if test ! $? = 0 ; then cmtTrkValToolstempfile=/tmp/cmt.$$; fi
${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=TrkValTools -version=TrkValTools-00-10-04 -path=/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation  $* >${cmtTrkValToolstempfile}
if test $? != 0 ; then
  echo >&2 "${CMTROOT}/${CMTBIN}/cmt.exe cleanup -sh -pack=TrkValTools -version=TrkValTools-00-10-04 -path=/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation  $* >${cmtTrkValToolstempfile}"
  cmtcleanupstatus=2
  /bin/rm -f ${cmtTrkValToolstempfile}
  unset cmtTrkValToolstempfile
  return $cmtcleanupstatus
fi
cmtcleanupstatus=0
. ${cmtTrkValToolstempfile}
if test $? != 0 ; then
  cmtcleanupstatus=2
fi
/bin/rm -f ${cmtTrkValToolstempfile}
unset cmtTrkValToolstempfile
return $cmtcleanupstatus

