#-- start of make_header -----------------

#====================================
#  Library TrkValTools
#
#   Generated Thu Apr 14 11:49:06 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_TrkValTools_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_TrkValTools_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_TrkValTools

TrkValTools_tag = $(tag)

#cmt_local_tagfile_TrkValTools = $(TrkValTools_tag)_TrkValTools.make
cmt_local_tagfile_TrkValTools = $(bin)$(TrkValTools_tag)_TrkValTools.make

else

tags      = $(tag),$(CMTEXTRATAGS)

TrkValTools_tag = $(tag)

#cmt_local_tagfile_TrkValTools = $(TrkValTools_tag).make
cmt_local_tagfile_TrkValTools = $(bin)$(TrkValTools_tag).make

endif

include $(cmt_local_tagfile_TrkValTools)
#-include $(cmt_local_tagfile_TrkValTools)

ifdef cmt_TrkValTools_has_target_tag

cmt_final_setup_TrkValTools = $(bin)setup_TrkValTools.make
cmt_dependencies_in_TrkValTools = $(bin)dependencies_TrkValTools.in
#cmt_final_setup_TrkValTools = $(bin)TrkValTools_TrkValToolssetup.make
cmt_local_TrkValTools_makefile = $(bin)TrkValTools.make

else

cmt_final_setup_TrkValTools = $(bin)setup.make
cmt_dependencies_in_TrkValTools = $(bin)dependencies.in
#cmt_final_setup_TrkValTools = $(bin)TrkValToolssetup.make
cmt_local_TrkValTools_makefile = $(bin)TrkValTools.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)TrkValToolssetup.make

#TrkValTools :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'TrkValTools'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = TrkValTools/
#TrkValTools::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
#-- start of libary_header ---------------

TrkValToolslibname   = $(bin)$(library_prefix)TrkValTools$(library_suffix)
TrkValToolslib       = $(TrkValToolslibname).a
TrkValToolsstamp     = $(bin)TrkValTools.stamp
TrkValToolsshstamp   = $(bin)TrkValTools.shstamp

TrkValTools :: dirs  TrkValToolsLIB
	$(echo) "TrkValTools ok"

#-- end of libary_header ----------------
#-- start of library_no_static ------

#TrkValToolsLIB :: $(TrkValToolslib) $(TrkValToolsshstamp)
TrkValToolsLIB :: $(TrkValToolsshstamp)
	$(echo) "TrkValTools : library ok"

$(TrkValToolslib) :: $(bin)BasicValTrkParticleNtupleTool.o $(bin)BremPositionNtupleHelper.o $(bin)DAF_ValidationNtupleHelper.o $(bin)DirectTrackNtupleWriterTool.o $(bin)EnergyLossMonitor.o $(bin)EventPropertyNtupleTool.o $(bin)EventToTrackLinkNtupleTool.o $(bin)GenParticleJetFinder.o $(bin)HitPositionNtupleHelper.o $(bin)InDetHaloSelector.o $(bin)InDetPrimaryConversionSelector.o $(bin)InDetReconstructableSelector.o $(bin)JetTruthNtupleTool.o $(bin)MeasurementVectorNtupleTool.o $(bin)PerigeeParametersNtupleTool.o $(bin)PositionMomentumWriter.o $(bin)PrimaryTruthClassifier.o $(bin)ResidualValidationNtupleHelper.o $(bin)TrackDiff.o $(bin)TrackInformationNtupleTool.o $(bin)TrackPositionNtupleHelper.o $(bin)TrackSummaryNtupleTool.o $(bin)TrkObserverTool.o $(bin)TruthNtupleTool.o $(bin)TrkValTools_entries.o $(bin)TrkValTools_load.o
	$(lib_echo) "static library $@"
	$(lib_silent) cd $(bin); \
	  $(ar) $(TrkValToolslib) $?
	$(lib_silent) $(ranlib) $(TrkValToolslib)
	$(lib_silent) cat /dev/null >$(TrkValToolsstamp)

#------------------------------------------------------------------
#  Future improvement? to empty the object files after
#  storing in the library
#
##	  for f in $?; do \
##	    rm $${f}; touch $${f}; \
##	  done
#------------------------------------------------------------------

#
# We add one level of dependency upon the true shared library 
# (rather than simply upon the stamp file)
# this is for cases where the shared library has not been built
# while the stamp was created (error??) 
#

$(TrkValToolslibname).$(shlibsuffix) :: $(bin)BasicValTrkParticleNtupleTool.o $(bin)BremPositionNtupleHelper.o $(bin)DAF_ValidationNtupleHelper.o $(bin)DirectTrackNtupleWriterTool.o $(bin)EnergyLossMonitor.o $(bin)EventPropertyNtupleTool.o $(bin)EventToTrackLinkNtupleTool.o $(bin)GenParticleJetFinder.o $(bin)HitPositionNtupleHelper.o $(bin)InDetHaloSelector.o $(bin)InDetPrimaryConversionSelector.o $(bin)InDetReconstructableSelector.o $(bin)JetTruthNtupleTool.o $(bin)MeasurementVectorNtupleTool.o $(bin)PerigeeParametersNtupleTool.o $(bin)PositionMomentumWriter.o $(bin)PrimaryTruthClassifier.o $(bin)ResidualValidationNtupleHelper.o $(bin)TrackDiff.o $(bin)TrackInformationNtupleTool.o $(bin)TrackPositionNtupleHelper.o $(bin)TrackSummaryNtupleTool.o $(bin)TrkObserverTool.o $(bin)TruthNtupleTool.o $(bin)TrkValTools_entries.o $(bin)TrkValTools_load.o $(use_requirements) $(TrkValToolsstamps)
	$(lib_echo) "shared library $@"
	$(lib_silent) $(shlibbuilder) $(shlibflags) -o $@ $(bin)BasicValTrkParticleNtupleTool.o $(bin)BremPositionNtupleHelper.o $(bin)DAF_ValidationNtupleHelper.o $(bin)DirectTrackNtupleWriterTool.o $(bin)EnergyLossMonitor.o $(bin)EventPropertyNtupleTool.o $(bin)EventToTrackLinkNtupleTool.o $(bin)GenParticleJetFinder.o $(bin)HitPositionNtupleHelper.o $(bin)InDetHaloSelector.o $(bin)InDetPrimaryConversionSelector.o $(bin)InDetReconstructableSelector.o $(bin)JetTruthNtupleTool.o $(bin)MeasurementVectorNtupleTool.o $(bin)PerigeeParametersNtupleTool.o $(bin)PositionMomentumWriter.o $(bin)PrimaryTruthClassifier.o $(bin)ResidualValidationNtupleHelper.o $(bin)TrackDiff.o $(bin)TrackInformationNtupleTool.o $(bin)TrackPositionNtupleHelper.o $(bin)TrackSummaryNtupleTool.o $(bin)TrkObserverTool.o $(bin)TruthNtupleTool.o $(bin)TrkValTools_entries.o $(bin)TrkValTools_load.o $(TrkValTools_shlibflags)
	$(lib_silent) cat /dev/null >$(TrkValToolsstamp) && \
	  cat /dev/null >$(TrkValToolsshstamp)

$(TrkValToolsshstamp) :: $(TrkValToolslibname).$(shlibsuffix)
	$(lib_silent) if test -f $(TrkValToolslibname).$(shlibsuffix) ; then \
	  cat /dev/null >$(TrkValToolsstamp) && \
	  cat /dev/null >$(TrkValToolsshstamp) ; fi

TrkValToolsclean ::
	$(cleanup_echo) objects TrkValTools
	$(cleanup_silent) /bin/rm -f $(bin)BasicValTrkParticleNtupleTool.o $(bin)BremPositionNtupleHelper.o $(bin)DAF_ValidationNtupleHelper.o $(bin)DirectTrackNtupleWriterTool.o $(bin)EnergyLossMonitor.o $(bin)EventPropertyNtupleTool.o $(bin)EventToTrackLinkNtupleTool.o $(bin)GenParticleJetFinder.o $(bin)HitPositionNtupleHelper.o $(bin)InDetHaloSelector.o $(bin)InDetPrimaryConversionSelector.o $(bin)InDetReconstructableSelector.o $(bin)JetTruthNtupleTool.o $(bin)MeasurementVectorNtupleTool.o $(bin)PerigeeParametersNtupleTool.o $(bin)PositionMomentumWriter.o $(bin)PrimaryTruthClassifier.o $(bin)ResidualValidationNtupleHelper.o $(bin)TrackDiff.o $(bin)TrackInformationNtupleTool.o $(bin)TrackPositionNtupleHelper.o $(bin)TrackSummaryNtupleTool.o $(bin)TrkObserverTool.o $(bin)TruthNtupleTool.o $(bin)TrkValTools_entries.o $(bin)TrkValTools_load.o
	$(cleanup_silent) /bin/rm -f $(patsubst %.o,%.d,$(bin)BasicValTrkParticleNtupleTool.o $(bin)BremPositionNtupleHelper.o $(bin)DAF_ValidationNtupleHelper.o $(bin)DirectTrackNtupleWriterTool.o $(bin)EnergyLossMonitor.o $(bin)EventPropertyNtupleTool.o $(bin)EventToTrackLinkNtupleTool.o $(bin)GenParticleJetFinder.o $(bin)HitPositionNtupleHelper.o $(bin)InDetHaloSelector.o $(bin)InDetPrimaryConversionSelector.o $(bin)InDetReconstructableSelector.o $(bin)JetTruthNtupleTool.o $(bin)MeasurementVectorNtupleTool.o $(bin)PerigeeParametersNtupleTool.o $(bin)PositionMomentumWriter.o $(bin)PrimaryTruthClassifier.o $(bin)ResidualValidationNtupleHelper.o $(bin)TrackDiff.o $(bin)TrackInformationNtupleTool.o $(bin)TrackPositionNtupleHelper.o $(bin)TrackSummaryNtupleTool.o $(bin)TrkObserverTool.o $(bin)TruthNtupleTool.o $(bin)TrkValTools_entries.o $(bin)TrkValTools_load.o) $(patsubst %.o,%.dep,$(bin)BasicValTrkParticleNtupleTool.o $(bin)BremPositionNtupleHelper.o $(bin)DAF_ValidationNtupleHelper.o $(bin)DirectTrackNtupleWriterTool.o $(bin)EnergyLossMonitor.o $(bin)EventPropertyNtupleTool.o $(bin)EventToTrackLinkNtupleTool.o $(bin)GenParticleJetFinder.o $(bin)HitPositionNtupleHelper.o $(bin)InDetHaloSelector.o $(bin)InDetPrimaryConversionSelector.o $(bin)InDetReconstructableSelector.o $(bin)JetTruthNtupleTool.o $(bin)MeasurementVectorNtupleTool.o $(bin)PerigeeParametersNtupleTool.o $(bin)PositionMomentumWriter.o $(bin)PrimaryTruthClassifier.o $(bin)ResidualValidationNtupleHelper.o $(bin)TrackDiff.o $(bin)TrackInformationNtupleTool.o $(bin)TrackPositionNtupleHelper.o $(bin)TrackSummaryNtupleTool.o $(bin)TrkObserverTool.o $(bin)TruthNtupleTool.o $(bin)TrkValTools_entries.o $(bin)TrkValTools_load.o) $(patsubst %.o,%.d.stamp,$(bin)BasicValTrkParticleNtupleTool.o $(bin)BremPositionNtupleHelper.o $(bin)DAF_ValidationNtupleHelper.o $(bin)DirectTrackNtupleWriterTool.o $(bin)EnergyLossMonitor.o $(bin)EventPropertyNtupleTool.o $(bin)EventToTrackLinkNtupleTool.o $(bin)GenParticleJetFinder.o $(bin)HitPositionNtupleHelper.o $(bin)InDetHaloSelector.o $(bin)InDetPrimaryConversionSelector.o $(bin)InDetReconstructableSelector.o $(bin)JetTruthNtupleTool.o $(bin)MeasurementVectorNtupleTool.o $(bin)PerigeeParametersNtupleTool.o $(bin)PositionMomentumWriter.o $(bin)PrimaryTruthClassifier.o $(bin)ResidualValidationNtupleHelper.o $(bin)TrackDiff.o $(bin)TrackInformationNtupleTool.o $(bin)TrackPositionNtupleHelper.o $(bin)TrackSummaryNtupleTool.o $(bin)TrkObserverTool.o $(bin)TruthNtupleTool.o $(bin)TrkValTools_entries.o $(bin)TrkValTools_load.o)
	$(cleanup_silent) cd $(bin); /bin/rm -rf TrkValTools_deps TrkValTools_dependencies.make

#-----------------------------------------------------------------
#
#  New section for automatic installation
#
#-----------------------------------------------------------------

install_dir = ${CMTINSTALLAREA}/$(tag)/lib
TrkValToolsinstallname = $(library_prefix)TrkValTools$(library_suffix).$(shlibsuffix)

TrkValTools :: TrkValToolsinstall ;

install :: TrkValToolsinstall ;

TrkValToolsinstall :: $(install_dir)/$(TrkValToolsinstallname)
ifdef CMTINSTALLAREA
	$(echo) "installation done"
endif

$(install_dir)/$(TrkValToolsinstallname) :: $(bin)$(TrkValToolsinstallname)
ifdef CMTINSTALLAREA
	$(install_silent) $(cmt_install_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(TrkValToolsinstallname)" \
	    -out "$(install_dir)" \
	    -cmd "$(cmt_installarea_command)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

##TrkValToolsclean :: TrkValToolsuninstall

uninstall :: TrkValToolsuninstall ;

TrkValToolsuninstall ::
ifdef CMTINSTALLAREA
	$(cleanup_silent) $(cmt_uninstall_action) \
	    -source "`(cd $(bin); pwd)`" \
	    -name "$(TrkValToolsinstallname)" \
	    -out "$(install_dir)" \
	    -cmtpath "$($(package)_cmtpath)"
endif

#-- end of library_no_static ------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BasicValTrkParticleNtupleTool.d

$(bin)$(binobj)BasicValTrkParticleNtupleTool.d :

$(bin)$(binobj)BasicValTrkParticleNtupleTool.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)BasicValTrkParticleNtupleTool.o : $(src)BasicValTrkParticleNtupleTool.cxx
	$(cpp_echo) $(src)BasicValTrkParticleNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(BasicValTrkParticleNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(BasicValTrkParticleNtupleTool_cppflags) $(BasicValTrkParticleNtupleTool_cxx_cppflags)  $(src)BasicValTrkParticleNtupleTool.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(BasicValTrkParticleNtupleTool_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)BasicValTrkParticleNtupleTool.cxx

$(bin)$(binobj)BasicValTrkParticleNtupleTool.o : $(BasicValTrkParticleNtupleTool_cxx_dependencies)
	$(cpp_echo) $(src)BasicValTrkParticleNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(BasicValTrkParticleNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(BasicValTrkParticleNtupleTool_cppflags) $(BasicValTrkParticleNtupleTool_cxx_cppflags)  $(src)BasicValTrkParticleNtupleTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)BremPositionNtupleHelper.d

$(bin)$(binobj)BremPositionNtupleHelper.d :

$(bin)$(binobj)BremPositionNtupleHelper.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)BremPositionNtupleHelper.o : $(src)BremPositionNtupleHelper.cxx
	$(cpp_echo) $(src)BremPositionNtupleHelper.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(BremPositionNtupleHelper_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(BremPositionNtupleHelper_cppflags) $(BremPositionNtupleHelper_cxx_cppflags)  $(src)BremPositionNtupleHelper.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(BremPositionNtupleHelper_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)BremPositionNtupleHelper.cxx

$(bin)$(binobj)BremPositionNtupleHelper.o : $(BremPositionNtupleHelper_cxx_dependencies)
	$(cpp_echo) $(src)BremPositionNtupleHelper.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(BremPositionNtupleHelper_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(BremPositionNtupleHelper_cppflags) $(BremPositionNtupleHelper_cxx_cppflags)  $(src)BremPositionNtupleHelper.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)DAF_ValidationNtupleHelper.d

$(bin)$(binobj)DAF_ValidationNtupleHelper.d :

$(bin)$(binobj)DAF_ValidationNtupleHelper.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)DAF_ValidationNtupleHelper.o : $(src)DAF_ValidationNtupleHelper.cxx
	$(cpp_echo) $(src)DAF_ValidationNtupleHelper.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(DAF_ValidationNtupleHelper_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(DAF_ValidationNtupleHelper_cppflags) $(DAF_ValidationNtupleHelper_cxx_cppflags)  $(src)DAF_ValidationNtupleHelper.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(DAF_ValidationNtupleHelper_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)DAF_ValidationNtupleHelper.cxx

$(bin)$(binobj)DAF_ValidationNtupleHelper.o : $(DAF_ValidationNtupleHelper_cxx_dependencies)
	$(cpp_echo) $(src)DAF_ValidationNtupleHelper.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(DAF_ValidationNtupleHelper_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(DAF_ValidationNtupleHelper_cppflags) $(DAF_ValidationNtupleHelper_cxx_cppflags)  $(src)DAF_ValidationNtupleHelper.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)DirectTrackNtupleWriterTool.d

$(bin)$(binobj)DirectTrackNtupleWriterTool.d :

$(bin)$(binobj)DirectTrackNtupleWriterTool.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)DirectTrackNtupleWriterTool.o : $(src)DirectTrackNtupleWriterTool.cxx
	$(cpp_echo) $(src)DirectTrackNtupleWriterTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(DirectTrackNtupleWriterTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(DirectTrackNtupleWriterTool_cppflags) $(DirectTrackNtupleWriterTool_cxx_cppflags)  $(src)DirectTrackNtupleWriterTool.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(DirectTrackNtupleWriterTool_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)DirectTrackNtupleWriterTool.cxx

$(bin)$(binobj)DirectTrackNtupleWriterTool.o : $(DirectTrackNtupleWriterTool_cxx_dependencies)
	$(cpp_echo) $(src)DirectTrackNtupleWriterTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(DirectTrackNtupleWriterTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(DirectTrackNtupleWriterTool_cppflags) $(DirectTrackNtupleWriterTool_cxx_cppflags)  $(src)DirectTrackNtupleWriterTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)EnergyLossMonitor.d

$(bin)$(binobj)EnergyLossMonitor.d :

$(bin)$(binobj)EnergyLossMonitor.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)EnergyLossMonitor.o : $(src)EnergyLossMonitor.cxx
	$(cpp_echo) $(src)EnergyLossMonitor.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(EnergyLossMonitor_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(EnergyLossMonitor_cppflags) $(EnergyLossMonitor_cxx_cppflags)  $(src)EnergyLossMonitor.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(EnergyLossMonitor_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)EnergyLossMonitor.cxx

$(bin)$(binobj)EnergyLossMonitor.o : $(EnergyLossMonitor_cxx_dependencies)
	$(cpp_echo) $(src)EnergyLossMonitor.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(EnergyLossMonitor_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(EnergyLossMonitor_cppflags) $(EnergyLossMonitor_cxx_cppflags)  $(src)EnergyLossMonitor.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)EventPropertyNtupleTool.d

$(bin)$(binobj)EventPropertyNtupleTool.d :

$(bin)$(binobj)EventPropertyNtupleTool.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)EventPropertyNtupleTool.o : $(src)EventPropertyNtupleTool.cxx
	$(cpp_echo) $(src)EventPropertyNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(EventPropertyNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(EventPropertyNtupleTool_cppflags) $(EventPropertyNtupleTool_cxx_cppflags)  $(src)EventPropertyNtupleTool.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(EventPropertyNtupleTool_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)EventPropertyNtupleTool.cxx

$(bin)$(binobj)EventPropertyNtupleTool.o : $(EventPropertyNtupleTool_cxx_dependencies)
	$(cpp_echo) $(src)EventPropertyNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(EventPropertyNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(EventPropertyNtupleTool_cppflags) $(EventPropertyNtupleTool_cxx_cppflags)  $(src)EventPropertyNtupleTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)EventToTrackLinkNtupleTool.d

$(bin)$(binobj)EventToTrackLinkNtupleTool.d :

$(bin)$(binobj)EventToTrackLinkNtupleTool.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)EventToTrackLinkNtupleTool.o : $(src)EventToTrackLinkNtupleTool.cxx
	$(cpp_echo) $(src)EventToTrackLinkNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(EventToTrackLinkNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(EventToTrackLinkNtupleTool_cppflags) $(EventToTrackLinkNtupleTool_cxx_cppflags)  $(src)EventToTrackLinkNtupleTool.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(EventToTrackLinkNtupleTool_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)EventToTrackLinkNtupleTool.cxx

$(bin)$(binobj)EventToTrackLinkNtupleTool.o : $(EventToTrackLinkNtupleTool_cxx_dependencies)
	$(cpp_echo) $(src)EventToTrackLinkNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(EventToTrackLinkNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(EventToTrackLinkNtupleTool_cppflags) $(EventToTrackLinkNtupleTool_cxx_cppflags)  $(src)EventToTrackLinkNtupleTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)GenParticleJetFinder.d

$(bin)$(binobj)GenParticleJetFinder.d :

$(bin)$(binobj)GenParticleJetFinder.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)GenParticleJetFinder.o : $(src)GenParticleJetFinder.cxx
	$(cpp_echo) $(src)GenParticleJetFinder.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(GenParticleJetFinder_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(GenParticleJetFinder_cppflags) $(GenParticleJetFinder_cxx_cppflags)  $(src)GenParticleJetFinder.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(GenParticleJetFinder_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)GenParticleJetFinder.cxx

$(bin)$(binobj)GenParticleJetFinder.o : $(GenParticleJetFinder_cxx_dependencies)
	$(cpp_echo) $(src)GenParticleJetFinder.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(GenParticleJetFinder_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(GenParticleJetFinder_cppflags) $(GenParticleJetFinder_cxx_cppflags)  $(src)GenParticleJetFinder.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)HitPositionNtupleHelper.d

$(bin)$(binobj)HitPositionNtupleHelper.d :

$(bin)$(binobj)HitPositionNtupleHelper.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)HitPositionNtupleHelper.o : $(src)HitPositionNtupleHelper.cxx
	$(cpp_echo) $(src)HitPositionNtupleHelper.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(HitPositionNtupleHelper_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(HitPositionNtupleHelper_cppflags) $(HitPositionNtupleHelper_cxx_cppflags)  $(src)HitPositionNtupleHelper.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(HitPositionNtupleHelper_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)HitPositionNtupleHelper.cxx

$(bin)$(binobj)HitPositionNtupleHelper.o : $(HitPositionNtupleHelper_cxx_dependencies)
	$(cpp_echo) $(src)HitPositionNtupleHelper.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(HitPositionNtupleHelper_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(HitPositionNtupleHelper_cppflags) $(HitPositionNtupleHelper_cxx_cppflags)  $(src)HitPositionNtupleHelper.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)InDetHaloSelector.d

$(bin)$(binobj)InDetHaloSelector.d :

$(bin)$(binobj)InDetHaloSelector.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)InDetHaloSelector.o : $(src)InDetHaloSelector.cxx
	$(cpp_echo) $(src)InDetHaloSelector.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(InDetHaloSelector_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(InDetHaloSelector_cppflags) $(InDetHaloSelector_cxx_cppflags)  $(src)InDetHaloSelector.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(InDetHaloSelector_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)InDetHaloSelector.cxx

$(bin)$(binobj)InDetHaloSelector.o : $(InDetHaloSelector_cxx_dependencies)
	$(cpp_echo) $(src)InDetHaloSelector.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(InDetHaloSelector_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(InDetHaloSelector_cppflags) $(InDetHaloSelector_cxx_cppflags)  $(src)InDetHaloSelector.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)InDetPrimaryConversionSelector.d

$(bin)$(binobj)InDetPrimaryConversionSelector.d :

$(bin)$(binobj)InDetPrimaryConversionSelector.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)InDetPrimaryConversionSelector.o : $(src)InDetPrimaryConversionSelector.cxx
	$(cpp_echo) $(src)InDetPrimaryConversionSelector.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(InDetPrimaryConversionSelector_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(InDetPrimaryConversionSelector_cppflags) $(InDetPrimaryConversionSelector_cxx_cppflags)  $(src)InDetPrimaryConversionSelector.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(InDetPrimaryConversionSelector_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)InDetPrimaryConversionSelector.cxx

$(bin)$(binobj)InDetPrimaryConversionSelector.o : $(InDetPrimaryConversionSelector_cxx_dependencies)
	$(cpp_echo) $(src)InDetPrimaryConversionSelector.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(InDetPrimaryConversionSelector_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(InDetPrimaryConversionSelector_cppflags) $(InDetPrimaryConversionSelector_cxx_cppflags)  $(src)InDetPrimaryConversionSelector.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)InDetReconstructableSelector.d

$(bin)$(binobj)InDetReconstructableSelector.d :

$(bin)$(binobj)InDetReconstructableSelector.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)InDetReconstructableSelector.o : $(src)InDetReconstructableSelector.cxx
	$(cpp_echo) $(src)InDetReconstructableSelector.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(InDetReconstructableSelector_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(InDetReconstructableSelector_cppflags) $(InDetReconstructableSelector_cxx_cppflags)  $(src)InDetReconstructableSelector.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(InDetReconstructableSelector_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)InDetReconstructableSelector.cxx

$(bin)$(binobj)InDetReconstructableSelector.o : $(InDetReconstructableSelector_cxx_dependencies)
	$(cpp_echo) $(src)InDetReconstructableSelector.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(InDetReconstructableSelector_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(InDetReconstructableSelector_cppflags) $(InDetReconstructableSelector_cxx_cppflags)  $(src)InDetReconstructableSelector.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)JetTruthNtupleTool.d

$(bin)$(binobj)JetTruthNtupleTool.d :

$(bin)$(binobj)JetTruthNtupleTool.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)JetTruthNtupleTool.o : $(src)JetTruthNtupleTool.cxx
	$(cpp_echo) $(src)JetTruthNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(JetTruthNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(JetTruthNtupleTool_cppflags) $(JetTruthNtupleTool_cxx_cppflags)  $(src)JetTruthNtupleTool.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(JetTruthNtupleTool_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)JetTruthNtupleTool.cxx

$(bin)$(binobj)JetTruthNtupleTool.o : $(JetTruthNtupleTool_cxx_dependencies)
	$(cpp_echo) $(src)JetTruthNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(JetTruthNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(JetTruthNtupleTool_cppflags) $(JetTruthNtupleTool_cxx_cppflags)  $(src)JetTruthNtupleTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)MeasurementVectorNtupleTool.d

$(bin)$(binobj)MeasurementVectorNtupleTool.d :

$(bin)$(binobj)MeasurementVectorNtupleTool.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)MeasurementVectorNtupleTool.o : $(src)MeasurementVectorNtupleTool.cxx
	$(cpp_echo) $(src)MeasurementVectorNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(MeasurementVectorNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(MeasurementVectorNtupleTool_cppflags) $(MeasurementVectorNtupleTool_cxx_cppflags)  $(src)MeasurementVectorNtupleTool.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(MeasurementVectorNtupleTool_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)MeasurementVectorNtupleTool.cxx

$(bin)$(binobj)MeasurementVectorNtupleTool.o : $(MeasurementVectorNtupleTool_cxx_dependencies)
	$(cpp_echo) $(src)MeasurementVectorNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(MeasurementVectorNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(MeasurementVectorNtupleTool_cppflags) $(MeasurementVectorNtupleTool_cxx_cppflags)  $(src)MeasurementVectorNtupleTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)PerigeeParametersNtupleTool.d

$(bin)$(binobj)PerigeeParametersNtupleTool.d :

$(bin)$(binobj)PerigeeParametersNtupleTool.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)PerigeeParametersNtupleTool.o : $(src)PerigeeParametersNtupleTool.cxx
	$(cpp_echo) $(src)PerigeeParametersNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(PerigeeParametersNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(PerigeeParametersNtupleTool_cppflags) $(PerigeeParametersNtupleTool_cxx_cppflags)  $(src)PerigeeParametersNtupleTool.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(PerigeeParametersNtupleTool_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)PerigeeParametersNtupleTool.cxx

$(bin)$(binobj)PerigeeParametersNtupleTool.o : $(PerigeeParametersNtupleTool_cxx_dependencies)
	$(cpp_echo) $(src)PerigeeParametersNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(PerigeeParametersNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(PerigeeParametersNtupleTool_cppflags) $(PerigeeParametersNtupleTool_cxx_cppflags)  $(src)PerigeeParametersNtupleTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)PositionMomentumWriter.d

$(bin)$(binobj)PositionMomentumWriter.d :

$(bin)$(binobj)PositionMomentumWriter.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)PositionMomentumWriter.o : $(src)PositionMomentumWriter.cxx
	$(cpp_echo) $(src)PositionMomentumWriter.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(PositionMomentumWriter_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(PositionMomentumWriter_cppflags) $(PositionMomentumWriter_cxx_cppflags)  $(src)PositionMomentumWriter.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(PositionMomentumWriter_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)PositionMomentumWriter.cxx

$(bin)$(binobj)PositionMomentumWriter.o : $(PositionMomentumWriter_cxx_dependencies)
	$(cpp_echo) $(src)PositionMomentumWriter.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(PositionMomentumWriter_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(PositionMomentumWriter_cppflags) $(PositionMomentumWriter_cxx_cppflags)  $(src)PositionMomentumWriter.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)PrimaryTruthClassifier.d

$(bin)$(binobj)PrimaryTruthClassifier.d :

$(bin)$(binobj)PrimaryTruthClassifier.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)PrimaryTruthClassifier.o : $(src)PrimaryTruthClassifier.cxx
	$(cpp_echo) $(src)PrimaryTruthClassifier.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(PrimaryTruthClassifier_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(PrimaryTruthClassifier_cppflags) $(PrimaryTruthClassifier_cxx_cppflags)  $(src)PrimaryTruthClassifier.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(PrimaryTruthClassifier_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)PrimaryTruthClassifier.cxx

$(bin)$(binobj)PrimaryTruthClassifier.o : $(PrimaryTruthClassifier_cxx_dependencies)
	$(cpp_echo) $(src)PrimaryTruthClassifier.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(PrimaryTruthClassifier_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(PrimaryTruthClassifier_cppflags) $(PrimaryTruthClassifier_cxx_cppflags)  $(src)PrimaryTruthClassifier.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)ResidualValidationNtupleHelper.d

$(bin)$(binobj)ResidualValidationNtupleHelper.d :

$(bin)$(binobj)ResidualValidationNtupleHelper.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)ResidualValidationNtupleHelper.o : $(src)ResidualValidationNtupleHelper.cxx
	$(cpp_echo) $(src)ResidualValidationNtupleHelper.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(ResidualValidationNtupleHelper_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(ResidualValidationNtupleHelper_cppflags) $(ResidualValidationNtupleHelper_cxx_cppflags)  $(src)ResidualValidationNtupleHelper.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(ResidualValidationNtupleHelper_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)ResidualValidationNtupleHelper.cxx

$(bin)$(binobj)ResidualValidationNtupleHelper.o : $(ResidualValidationNtupleHelper_cxx_dependencies)
	$(cpp_echo) $(src)ResidualValidationNtupleHelper.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(ResidualValidationNtupleHelper_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(ResidualValidationNtupleHelper_cppflags) $(ResidualValidationNtupleHelper_cxx_cppflags)  $(src)ResidualValidationNtupleHelper.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)TrackDiff.d

$(bin)$(binobj)TrackDiff.d :

$(bin)$(binobj)TrackDiff.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)TrackDiff.o : $(src)TrackDiff.cxx
	$(cpp_echo) $(src)TrackDiff.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TrackDiff_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TrackDiff_cppflags) $(TrackDiff_cxx_cppflags)  $(src)TrackDiff.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(TrackDiff_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)TrackDiff.cxx

$(bin)$(binobj)TrackDiff.o : $(TrackDiff_cxx_dependencies)
	$(cpp_echo) $(src)TrackDiff.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TrackDiff_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TrackDiff_cppflags) $(TrackDiff_cxx_cppflags)  $(src)TrackDiff.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)TrackInformationNtupleTool.d

$(bin)$(binobj)TrackInformationNtupleTool.d :

$(bin)$(binobj)TrackInformationNtupleTool.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)TrackInformationNtupleTool.o : $(src)TrackInformationNtupleTool.cxx
	$(cpp_echo) $(src)TrackInformationNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TrackInformationNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TrackInformationNtupleTool_cppflags) $(TrackInformationNtupleTool_cxx_cppflags)  $(src)TrackInformationNtupleTool.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(TrackInformationNtupleTool_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)TrackInformationNtupleTool.cxx

$(bin)$(binobj)TrackInformationNtupleTool.o : $(TrackInformationNtupleTool_cxx_dependencies)
	$(cpp_echo) $(src)TrackInformationNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TrackInformationNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TrackInformationNtupleTool_cppflags) $(TrackInformationNtupleTool_cxx_cppflags)  $(src)TrackInformationNtupleTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)TrackPositionNtupleHelper.d

$(bin)$(binobj)TrackPositionNtupleHelper.d :

$(bin)$(binobj)TrackPositionNtupleHelper.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)TrackPositionNtupleHelper.o : $(src)TrackPositionNtupleHelper.cxx
	$(cpp_echo) $(src)TrackPositionNtupleHelper.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TrackPositionNtupleHelper_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TrackPositionNtupleHelper_cppflags) $(TrackPositionNtupleHelper_cxx_cppflags)  $(src)TrackPositionNtupleHelper.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(TrackPositionNtupleHelper_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)TrackPositionNtupleHelper.cxx

$(bin)$(binobj)TrackPositionNtupleHelper.o : $(TrackPositionNtupleHelper_cxx_dependencies)
	$(cpp_echo) $(src)TrackPositionNtupleHelper.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TrackPositionNtupleHelper_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TrackPositionNtupleHelper_cppflags) $(TrackPositionNtupleHelper_cxx_cppflags)  $(src)TrackPositionNtupleHelper.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)TrackSummaryNtupleTool.d

$(bin)$(binobj)TrackSummaryNtupleTool.d :

$(bin)$(binobj)TrackSummaryNtupleTool.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)TrackSummaryNtupleTool.o : $(src)TrackSummaryNtupleTool.cxx
	$(cpp_echo) $(src)TrackSummaryNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TrackSummaryNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TrackSummaryNtupleTool_cppflags) $(TrackSummaryNtupleTool_cxx_cppflags)  $(src)TrackSummaryNtupleTool.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(TrackSummaryNtupleTool_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)TrackSummaryNtupleTool.cxx

$(bin)$(binobj)TrackSummaryNtupleTool.o : $(TrackSummaryNtupleTool_cxx_dependencies)
	$(cpp_echo) $(src)TrackSummaryNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TrackSummaryNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TrackSummaryNtupleTool_cppflags) $(TrackSummaryNtupleTool_cxx_cppflags)  $(src)TrackSummaryNtupleTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)TrkObserverTool.d

$(bin)$(binobj)TrkObserverTool.d :

$(bin)$(binobj)TrkObserverTool.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)TrkObserverTool.o : $(src)TrkObserverTool.cxx
	$(cpp_echo) $(src)TrkObserverTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TrkObserverTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TrkObserverTool_cppflags) $(TrkObserverTool_cxx_cppflags)  $(src)TrkObserverTool.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(TrkObserverTool_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)TrkObserverTool.cxx

$(bin)$(binobj)TrkObserverTool.o : $(TrkObserverTool_cxx_dependencies)
	$(cpp_echo) $(src)TrkObserverTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TrkObserverTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TrkObserverTool_cppflags) $(TrkObserverTool_cxx_cppflags)  $(src)TrkObserverTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)TruthNtupleTool.d

$(bin)$(binobj)TruthNtupleTool.d :

$(bin)$(binobj)TruthNtupleTool.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)TruthNtupleTool.o : $(src)TruthNtupleTool.cxx
	$(cpp_echo) $(src)TruthNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TruthNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TruthNtupleTool_cppflags) $(TruthNtupleTool_cxx_cppflags)  $(src)TruthNtupleTool.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(TruthNtupleTool_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)TruthNtupleTool.cxx

$(bin)$(binobj)TruthNtupleTool.o : $(TruthNtupleTool_cxx_dependencies)
	$(cpp_echo) $(src)TruthNtupleTool.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TruthNtupleTool_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TruthNtupleTool_cppflags) $(TruthNtupleTool_cxx_cppflags)  $(src)TruthNtupleTool.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)TrkValTools_entries.d

$(bin)$(binobj)TrkValTools_entries.d :

$(bin)$(binobj)TrkValTools_entries.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)TrkValTools_entries.o : $(src)components/TrkValTools_entries.cxx
	$(cpp_echo) $(src)components/TrkValTools_entries.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TrkValTools_entries_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TrkValTools_entries_cppflags) $(TrkValTools_entries_cxx_cppflags) -I../src/components $(src)components/TrkValTools_entries.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(TrkValTools_entries_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)components/TrkValTools_entries.cxx

$(bin)$(binobj)TrkValTools_entries.o : $(TrkValTools_entries_cxx_dependencies)
	$(cpp_echo) $(src)components/TrkValTools_entries.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TrkValTools_entries_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TrkValTools_entries_cppflags) $(TrkValTools_entries_cxx_cppflags) -I../src/components $(src)components/TrkValTools_entries.cxx

endif

#-- end of cpp_library ------------------
#-- start of cpp_library -----------------

ifneq (-MMD -MP -MF $*.d -MQ $@,)

ifneq ($(MAKECMDGOALS),TrkValToolsclean)
ifneq ($(MAKECMDGOALS),uninstall)
-include $(bin)$(binobj)TrkValTools_load.d

$(bin)$(binobj)TrkValTools_load.d :

$(bin)$(binobj)TrkValTools_load.o : $(cmt_final_setup_TrkValTools)

$(bin)$(binobj)TrkValTools_load.o : $(src)components/TrkValTools_load.cxx
	$(cpp_echo) $(src)components/TrkValTools_load.cxx
	$(cpp_silent) $(cppcomp) -MMD -MP -MF $*.d -MQ $@ -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TrkValTools_load_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TrkValTools_load_cppflags) $(TrkValTools_load_cxx_cppflags) -I../src/components $(src)components/TrkValTools_load.cxx
endif
endif

else
$(bin)TrkValTools_dependencies.make : $(TrkValTools_load_cxx_dependencies)

$(bin)TrkValTools_dependencies.make : $(src)components/TrkValTools_load.cxx

$(bin)$(binobj)TrkValTools_load.o : $(TrkValTools_load_cxx_dependencies)
	$(cpp_echo) $(src)components/TrkValTools_load.cxx
	$(cpp_silent) $(cppcomp) -o $@ $(use_pp_cppflags) $(TrkValTools_pp_cppflags) $(lib_TrkValTools_pp_cppflags) $(TrkValTools_load_pp_cppflags) $(use_cppflags) $(TrkValTools_cppflags) $(lib_TrkValTools_cppflags) $(TrkValTools_load_cppflags) $(TrkValTools_load_cxx_cppflags) -I../src/components $(src)components/TrkValTools_load.cxx

endif

#-- end of cpp_library ------------------
#-- start of cleanup_header --------------

clean :: TrkValToolsclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(TrkValTools.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

TrkValToolsclean ::
#-- end of cleanup_header ---------------
#-- start of cleanup_library -------------
	$(cleanup_echo) library TrkValTools
	-$(cleanup_silent) cd $(bin) && \rm -f $(library_prefix)TrkValTools$(library_suffix).a $(library_prefix)TrkValTools$(library_suffix).$(shlibsuffix) TrkValTools.stamp TrkValTools.shstamp
#-- end of cleanup_library ---------------
