#-- start of make_header -----------------

#====================================
#  Document TrkValToolsComponentsList
#
#   Generated Thu Apr 14 11:53:23 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_TrkValToolsComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_TrkValToolsComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_TrkValToolsComponentsList

TrkValTools_tag = $(tag)

#cmt_local_tagfile_TrkValToolsComponentsList = $(TrkValTools_tag)_TrkValToolsComponentsList.make
cmt_local_tagfile_TrkValToolsComponentsList = $(bin)$(TrkValTools_tag)_TrkValToolsComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

TrkValTools_tag = $(tag)

#cmt_local_tagfile_TrkValToolsComponentsList = $(TrkValTools_tag).make
cmt_local_tagfile_TrkValToolsComponentsList = $(bin)$(TrkValTools_tag).make

endif

include $(cmt_local_tagfile_TrkValToolsComponentsList)
#-include $(cmt_local_tagfile_TrkValToolsComponentsList)

ifdef cmt_TrkValToolsComponentsList_has_target_tag

cmt_final_setup_TrkValToolsComponentsList = $(bin)setup_TrkValToolsComponentsList.make
cmt_dependencies_in_TrkValToolsComponentsList = $(bin)dependencies_TrkValToolsComponentsList.in
#cmt_final_setup_TrkValToolsComponentsList = $(bin)TrkValTools_TrkValToolsComponentsListsetup.make
cmt_local_TrkValToolsComponentsList_makefile = $(bin)TrkValToolsComponentsList.make

else

cmt_final_setup_TrkValToolsComponentsList = $(bin)setup.make
cmt_dependencies_in_TrkValToolsComponentsList = $(bin)dependencies.in
#cmt_final_setup_TrkValToolsComponentsList = $(bin)TrkValToolssetup.make
cmt_local_TrkValToolsComponentsList_makefile = $(bin)TrkValToolsComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)TrkValToolssetup.make

#TrkValToolsComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'TrkValToolsComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = TrkValToolsComponentsList/
#TrkValToolsComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
##
componentslistfile = TrkValTools.components
COMPONENTSLIST_DIR = ../$(tag)
fulllibname = libTrkValTools.$(shlibsuffix)

TrkValToolsComponentsList :: ${COMPONENTSLIST_DIR}/$(componentslistfile)
	@:

${COMPONENTSLIST_DIR}/$(componentslistfile) :: $(bin)$(fulllibname)
	@echo 'Generating componentslist file for $(fulllibname)'
	cd ../$(tag);$(listcomponents_cmd) --output ${COMPONENTSLIST_DIR}/$(componentslistfile) $(fulllibname)

install :: TrkValToolsComponentsListinstall
TrkValToolsComponentsListinstall :: TrkValToolsComponentsList

uninstall :: TrkValToolsComponentsListuninstall
TrkValToolsComponentsListuninstall :: TrkValToolsComponentsListclean

TrkValToolsComponentsListclean ::
	@echo 'Deleting $(componentslistfile)'
	@rm -f ${COMPONENTSLIST_DIR}/$(componentslistfile)

#-- start of cleanup_header --------------

clean :: TrkValToolsComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(TrkValToolsComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

TrkValToolsComponentsListclean ::
#-- end of cleanup_header ---------------
