
#-- start of constituents_header ------

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

tags      = $(tag),$(CMTEXTRATAGS)

TrkValTools_tag = $(tag)

#cmt_local_tagfile = $(TrkValTools_tag).make
cmt_local_tagfile = $(bin)$(TrkValTools_tag).make

#-include $(cmt_local_tagfile)
include $(cmt_local_tagfile)

#cmt_local_setup = $(bin)setup$$$$.make
#cmt_local_setup = $(bin)$(package)setup$$$$.make
#cmt_final_setup = $(bin)TrkValToolssetup.make
cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)$(package)setup.make

cmt_build_library_linksstamp = $(bin)cmt_build_library_links.stamp
#--------------------------------------------------------

#cmt_lock_setup = /tmp/lock$(cmt_lock_pid).make
#cmt_temp_tag = /tmp/tag$(cmt_lock_pid).make

#first :: $(cmt_local_tagfile)
#	@echo $(cmt_local_tagfile) ok
#ifndef QUICK
#first :: $(cmt_final_setup) ;
#else
#first :: ;
#endif

##	@bin=`$(cmtexe) show macro_value bin`

#$(cmt_local_tagfile) : $(cmt_lock_setup)
#	@echo "#CMT> Error: $@: No such file" >&2; exit 1
#$(cmt_local_tagfile) :
#	@echo "#CMT> Warning: $@: No such file" >&2; exit
#	@echo "#CMT> Info: $@: No need to rebuild file" >&2; exit

#$(cmt_final_setup) : $(cmt_local_tagfile) 
#	$(echo) "(constituents.make) Rebuilding $@"
#	@if test ! -d $(@D); then $(mkdir) -p $(@D); fi; \
#	  if test -f $(cmt_local_setup); then /bin/rm -f $(cmt_local_setup); fi; \
#	  trap '/bin/rm -f $(cmt_local_setup)' 0 1 2 15; \
#	  $(cmtexe) -tag=$(tags) show setup >>$(cmt_local_setup); \
#	  if test ! -f $@; then \
#	    mv $(cmt_local_setup) $@; \
#	  else \
#	    if /usr/bin/diff $(cmt_local_setup) $@ >/dev/null ; then \
#	      : ; \
#	    else \
#	      mv $(cmt_local_setup) $@; \
#	    fi; \
#	  fi

#	@/bin/echo $@ ok   

#config :: checkuses
#	@exit 0
#checkuses : ;

env.make ::
	printenv >env.make.tmp; $(cmtexe) check files env.make.tmp env.make

ifndef QUICK
all :: build_library_links ;
else
all :: $(cmt_build_library_linksstamp) ;
endif

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

dirs :: requirements
	@if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi
#	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
#	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

#requirements :
#	@if test ! -r requirements ; then echo "No requirements file" ; fi

build_library_links : dirs
	$(echo) "(constituents.make) Rebuilding library links"; \
	 $(build_library_links)
#	if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi; \
#	$(build_library_links)

$(cmt_build_library_linksstamp) : $(cmt_final_setup) $(cmt_local_tagfile) $(bin)library_links.in
	$(echo) "(constituents.make) Rebuilding library links"; \
	 $(build_library_links) -f=$(bin)library_links.in -without_cmt
	$(silent) \touch $@

ifndef PEDANTIC
.DEFAULT ::
#.DEFAULT :
	$(echo) "(constituents.make) $@: No rule for such target" >&2
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of constituents_header ------
#-- start of group ------

all_groups :: all

all :: $(all_dependencies)  $(all_pre_constituents) $(all_constituents)  $(all_post_constituents)
	$(echo) "all ok."

#	@/bin/echo " all ok."

clean :: allclean

allclean ::  $(all_constituentsclean)
	$(echo) $(all_constituentsclean)
	$(echo) "allclean ok."

#	@echo $(all_constituentsclean)
#	@/bin/echo " allclean ok."

#-- end of group ------
#-- start of group ------

all_groups :: cmt_actions

cmt_actions :: $(cmt_actions_dependencies)  $(cmt_actions_pre_constituents) $(cmt_actions_constituents)  $(cmt_actions_post_constituents)
	$(echo) "cmt_actions ok."

#	@/bin/echo " cmt_actions ok."

clean :: allclean

cmt_actionsclean ::  $(cmt_actions_constituentsclean)
	$(echo) $(cmt_actions_constituentsclean)
	$(echo) "cmt_actionsclean ok."

#	@echo $(cmt_actions_constituentsclean)
#	@/bin/echo " cmt_actionsclean ok."

#-- end of group ------
#-- start of group ------

all_groups :: rulechecker

rulechecker :: $(rulechecker_dependencies)  $(rulechecker_pre_constituents) $(rulechecker_constituents)  $(rulechecker_post_constituents)
	$(echo) "rulechecker ok."

#	@/bin/echo " rulechecker ok."

clean :: allclean

rulecheckerclean ::  $(rulechecker_constituentsclean)
	$(echo) $(rulechecker_constituentsclean)
	$(echo) "rulecheckerclean ok."

#	@echo $(rulechecker_constituentsclean)
#	@/bin/echo " rulecheckerclean ok."

#-- end of group ------
#-- start of constituent ------

cmt_TrkValTools_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkValTools_has_target_tag

cmt_local_tagfile_TrkValTools = $(bin)$(TrkValTools_tag)_TrkValTools.make
cmt_final_setup_TrkValTools = $(bin)setup_TrkValTools.make
cmt_local_TrkValTools_makefile = $(bin)TrkValTools.make

TrkValTools_extratags = -tag_add=target_TrkValTools

else

cmt_local_tagfile_TrkValTools = $(bin)$(TrkValTools_tag).make
cmt_final_setup_TrkValTools = $(bin)setup.make
cmt_local_TrkValTools_makefile = $(bin)TrkValTools.make

endif

not_TrkValTools_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkValTools_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkValToolsdirs :
	@if test ! -d $(bin)TrkValTools; then $(mkdir) -p $(bin)TrkValTools; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkValTools
else
TrkValToolsdirs : ;
endif

ifdef cmt_TrkValTools_has_target_tag

ifndef QUICK
$(cmt_local_TrkValTools_makefile) : $(TrkValTools_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValTools.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValTools_extratags) build constituent_config -out=$(cmt_local_TrkValTools_makefile) TrkValTools
else
$(cmt_local_TrkValTools_makefile) : $(TrkValTools_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValTools) ] || \
	  [ ! -f $(cmt_final_setup_TrkValTools) ] || \
	  $(not_TrkValTools_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValTools.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValTools_extratags) build constituent_config -out=$(cmt_local_TrkValTools_makefile) TrkValTools; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkValTools_makefile) : $(TrkValTools_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValTools.make"; \
	  $(cmtexe) -f=$(bin)TrkValTools.in -tag=$(tags) $(TrkValTools_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValTools_makefile) TrkValTools
else
$(cmt_local_TrkValTools_makefile) : $(TrkValTools_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkValTools.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValTools) ] || \
	  [ ! -f $(cmt_final_setup_TrkValTools) ] || \
	  $(not_TrkValTools_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValTools.make"; \
	  $(cmtexe) -f=$(bin)TrkValTools.in -tag=$(tags) $(TrkValTools_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValTools_makefile) TrkValTools; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkValTools_extratags) build constituent_makefile -out=$(cmt_local_TrkValTools_makefile) TrkValTools

TrkValTools :: $(TrkValTools_dependencies) $(cmt_local_TrkValTools_makefile) dirs TrkValToolsdirs
	$(echo) "(constituents.make) Starting TrkValTools"
	@if test -f $(cmt_local_TrkValTools_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_makefile) TrkValTools; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValTools_makefile) TrkValTools
	$(echo) "(constituents.make) TrkValTools done"

clean :: TrkValToolsclean ;

TrkValToolsclean :: $(TrkValToolsclean_dependencies) ##$(cmt_local_TrkValTools_makefile)
	$(echo) "(constituents.make) Starting TrkValToolsclean"
	@-if test -f $(cmt_local_TrkValTools_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_makefile) TrkValToolsclean; \
	fi
	$(echo) "(constituents.make) TrkValToolsclean done"
#	@-$(MAKE) -f $(cmt_local_TrkValTools_makefile) TrkValToolsclean

##	  /bin/rm -f $(cmt_local_TrkValTools_makefile) $(bin)TrkValTools_dependencies.make

install :: TrkValToolsinstall ;

TrkValToolsinstall :: $(TrkValTools_dependencies) $(cmt_local_TrkValTools_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkValTools_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkValTools_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkValToolsuninstall

$(foreach d,$(TrkValTools_dependencies),$(eval $(d)uninstall_dependencies += TrkValToolsuninstall))

TrkValToolsuninstall : $(TrkValToolsuninstall_dependencies) ##$(cmt_local_TrkValTools_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkValTools_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValTools_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkValToolsuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkValTools"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkValTools done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkValToolsConf_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkValToolsConf_has_target_tag

cmt_local_tagfile_TrkValToolsConf = $(bin)$(TrkValTools_tag)_TrkValToolsConf.make
cmt_final_setup_TrkValToolsConf = $(bin)setup_TrkValToolsConf.make
cmt_local_TrkValToolsConf_makefile = $(bin)TrkValToolsConf.make

TrkValToolsConf_extratags = -tag_add=target_TrkValToolsConf

else

cmt_local_tagfile_TrkValToolsConf = $(bin)$(TrkValTools_tag).make
cmt_final_setup_TrkValToolsConf = $(bin)setup.make
cmt_local_TrkValToolsConf_makefile = $(bin)TrkValToolsConf.make

endif

not_TrkValToolsConf_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkValToolsConf_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkValToolsConfdirs :
	@if test ! -d $(bin)TrkValToolsConf; then $(mkdir) -p $(bin)TrkValToolsConf; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkValToolsConf
else
TrkValToolsConfdirs : ;
endif

ifdef cmt_TrkValToolsConf_has_target_tag

ifndef QUICK
$(cmt_local_TrkValToolsConf_makefile) : $(TrkValToolsConf_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValToolsConf.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValToolsConf_extratags) build constituent_config -out=$(cmt_local_TrkValToolsConf_makefile) TrkValToolsConf
else
$(cmt_local_TrkValToolsConf_makefile) : $(TrkValToolsConf_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValToolsConf) ] || \
	  [ ! -f $(cmt_final_setup_TrkValToolsConf) ] || \
	  $(not_TrkValToolsConf_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValToolsConf.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValToolsConf_extratags) build constituent_config -out=$(cmt_local_TrkValToolsConf_makefile) TrkValToolsConf; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkValToolsConf_makefile) : $(TrkValToolsConf_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValToolsConf.make"; \
	  $(cmtexe) -f=$(bin)TrkValToolsConf.in -tag=$(tags) $(TrkValToolsConf_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValToolsConf_makefile) TrkValToolsConf
else
$(cmt_local_TrkValToolsConf_makefile) : $(TrkValToolsConf_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkValToolsConf.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValToolsConf) ] || \
	  [ ! -f $(cmt_final_setup_TrkValToolsConf) ] || \
	  $(not_TrkValToolsConf_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValToolsConf.make"; \
	  $(cmtexe) -f=$(bin)TrkValToolsConf.in -tag=$(tags) $(TrkValToolsConf_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValToolsConf_makefile) TrkValToolsConf; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkValToolsConf_extratags) build constituent_makefile -out=$(cmt_local_TrkValToolsConf_makefile) TrkValToolsConf

TrkValToolsConf :: $(TrkValToolsConf_dependencies) $(cmt_local_TrkValToolsConf_makefile) dirs TrkValToolsConfdirs
	$(echo) "(constituents.make) Starting TrkValToolsConf"
	@if test -f $(cmt_local_TrkValToolsConf_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsConf_makefile) TrkValToolsConf; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValToolsConf_makefile) TrkValToolsConf
	$(echo) "(constituents.make) TrkValToolsConf done"

clean :: TrkValToolsConfclean ;

TrkValToolsConfclean :: $(TrkValToolsConfclean_dependencies) ##$(cmt_local_TrkValToolsConf_makefile)
	$(echo) "(constituents.make) Starting TrkValToolsConfclean"
	@-if test -f $(cmt_local_TrkValToolsConf_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsConf_makefile) TrkValToolsConfclean; \
	fi
	$(echo) "(constituents.make) TrkValToolsConfclean done"
#	@-$(MAKE) -f $(cmt_local_TrkValToolsConf_makefile) TrkValToolsConfclean

##	  /bin/rm -f $(cmt_local_TrkValToolsConf_makefile) $(bin)TrkValToolsConf_dependencies.make

install :: TrkValToolsConfinstall ;

TrkValToolsConfinstall :: $(TrkValToolsConf_dependencies) $(cmt_local_TrkValToolsConf_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkValToolsConf_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsConf_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkValToolsConf_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkValToolsConfuninstall

$(foreach d,$(TrkValToolsConf_dependencies),$(eval $(d)uninstall_dependencies += TrkValToolsConfuninstall))

TrkValToolsConfuninstall : $(TrkValToolsConfuninstall_dependencies) ##$(cmt_local_TrkValToolsConf_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkValToolsConf_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsConf_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValToolsConf_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkValToolsConfuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkValToolsConf"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkValToolsConf done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkValTools_python_init_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkValTools_python_init_has_target_tag

cmt_local_tagfile_TrkValTools_python_init = $(bin)$(TrkValTools_tag)_TrkValTools_python_init.make
cmt_final_setup_TrkValTools_python_init = $(bin)setup_TrkValTools_python_init.make
cmt_local_TrkValTools_python_init_makefile = $(bin)TrkValTools_python_init.make

TrkValTools_python_init_extratags = -tag_add=target_TrkValTools_python_init

else

cmt_local_tagfile_TrkValTools_python_init = $(bin)$(TrkValTools_tag).make
cmt_final_setup_TrkValTools_python_init = $(bin)setup.make
cmt_local_TrkValTools_python_init_makefile = $(bin)TrkValTools_python_init.make

endif

not_TrkValTools_python_init_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkValTools_python_init_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkValTools_python_initdirs :
	@if test ! -d $(bin)TrkValTools_python_init; then $(mkdir) -p $(bin)TrkValTools_python_init; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkValTools_python_init
else
TrkValTools_python_initdirs : ;
endif

ifdef cmt_TrkValTools_python_init_has_target_tag

ifndef QUICK
$(cmt_local_TrkValTools_python_init_makefile) : $(TrkValTools_python_init_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValTools_python_init.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValTools_python_init_extratags) build constituent_config -out=$(cmt_local_TrkValTools_python_init_makefile) TrkValTools_python_init
else
$(cmt_local_TrkValTools_python_init_makefile) : $(TrkValTools_python_init_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValTools_python_init) ] || \
	  [ ! -f $(cmt_final_setup_TrkValTools_python_init) ] || \
	  $(not_TrkValTools_python_init_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValTools_python_init.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValTools_python_init_extratags) build constituent_config -out=$(cmt_local_TrkValTools_python_init_makefile) TrkValTools_python_init; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkValTools_python_init_makefile) : $(TrkValTools_python_init_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValTools_python_init.make"; \
	  $(cmtexe) -f=$(bin)TrkValTools_python_init.in -tag=$(tags) $(TrkValTools_python_init_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValTools_python_init_makefile) TrkValTools_python_init
else
$(cmt_local_TrkValTools_python_init_makefile) : $(TrkValTools_python_init_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkValTools_python_init.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValTools_python_init) ] || \
	  [ ! -f $(cmt_final_setup_TrkValTools_python_init) ] || \
	  $(not_TrkValTools_python_init_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValTools_python_init.make"; \
	  $(cmtexe) -f=$(bin)TrkValTools_python_init.in -tag=$(tags) $(TrkValTools_python_init_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValTools_python_init_makefile) TrkValTools_python_init; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkValTools_python_init_extratags) build constituent_makefile -out=$(cmt_local_TrkValTools_python_init_makefile) TrkValTools_python_init

TrkValTools_python_init :: $(TrkValTools_python_init_dependencies) $(cmt_local_TrkValTools_python_init_makefile) dirs TrkValTools_python_initdirs
	$(echo) "(constituents.make) Starting TrkValTools_python_init"
	@if test -f $(cmt_local_TrkValTools_python_init_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_python_init_makefile) TrkValTools_python_init; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValTools_python_init_makefile) TrkValTools_python_init
	$(echo) "(constituents.make) TrkValTools_python_init done"

clean :: TrkValTools_python_initclean ;

TrkValTools_python_initclean :: $(TrkValTools_python_initclean_dependencies) ##$(cmt_local_TrkValTools_python_init_makefile)
	$(echo) "(constituents.make) Starting TrkValTools_python_initclean"
	@-if test -f $(cmt_local_TrkValTools_python_init_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_python_init_makefile) TrkValTools_python_initclean; \
	fi
	$(echo) "(constituents.make) TrkValTools_python_initclean done"
#	@-$(MAKE) -f $(cmt_local_TrkValTools_python_init_makefile) TrkValTools_python_initclean

##	  /bin/rm -f $(cmt_local_TrkValTools_python_init_makefile) $(bin)TrkValTools_python_init_dependencies.make

install :: TrkValTools_python_initinstall ;

TrkValTools_python_initinstall :: $(TrkValTools_python_init_dependencies) $(cmt_local_TrkValTools_python_init_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkValTools_python_init_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_python_init_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkValTools_python_init_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkValTools_python_inituninstall

$(foreach d,$(TrkValTools_python_init_dependencies),$(eval $(d)uninstall_dependencies += TrkValTools_python_inituninstall))

TrkValTools_python_inituninstall : $(TrkValTools_python_inituninstall_dependencies) ##$(cmt_local_TrkValTools_python_init_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkValTools_python_init_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_python_init_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValTools_python_init_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkValTools_python_inituninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkValTools_python_init"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkValTools_python_init done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkValToolsConfDbMerge_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkValToolsConfDbMerge_has_target_tag

cmt_local_tagfile_TrkValToolsConfDbMerge = $(bin)$(TrkValTools_tag)_TrkValToolsConfDbMerge.make
cmt_final_setup_TrkValToolsConfDbMerge = $(bin)setup_TrkValToolsConfDbMerge.make
cmt_local_TrkValToolsConfDbMerge_makefile = $(bin)TrkValToolsConfDbMerge.make

TrkValToolsConfDbMerge_extratags = -tag_add=target_TrkValToolsConfDbMerge

else

cmt_local_tagfile_TrkValToolsConfDbMerge = $(bin)$(TrkValTools_tag).make
cmt_final_setup_TrkValToolsConfDbMerge = $(bin)setup.make
cmt_local_TrkValToolsConfDbMerge_makefile = $(bin)TrkValToolsConfDbMerge.make

endif

not_TrkValToolsConfDbMerge_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkValToolsConfDbMerge_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkValToolsConfDbMergedirs :
	@if test ! -d $(bin)TrkValToolsConfDbMerge; then $(mkdir) -p $(bin)TrkValToolsConfDbMerge; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkValToolsConfDbMerge
else
TrkValToolsConfDbMergedirs : ;
endif

ifdef cmt_TrkValToolsConfDbMerge_has_target_tag

ifndef QUICK
$(cmt_local_TrkValToolsConfDbMerge_makefile) : $(TrkValToolsConfDbMerge_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValToolsConfDbMerge.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValToolsConfDbMerge_extratags) build constituent_config -out=$(cmt_local_TrkValToolsConfDbMerge_makefile) TrkValToolsConfDbMerge
else
$(cmt_local_TrkValToolsConfDbMerge_makefile) : $(TrkValToolsConfDbMerge_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValToolsConfDbMerge) ] || \
	  [ ! -f $(cmt_final_setup_TrkValToolsConfDbMerge) ] || \
	  $(not_TrkValToolsConfDbMerge_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValToolsConfDbMerge.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValToolsConfDbMerge_extratags) build constituent_config -out=$(cmt_local_TrkValToolsConfDbMerge_makefile) TrkValToolsConfDbMerge; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkValToolsConfDbMerge_makefile) : $(TrkValToolsConfDbMerge_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValToolsConfDbMerge.make"; \
	  $(cmtexe) -f=$(bin)TrkValToolsConfDbMerge.in -tag=$(tags) $(TrkValToolsConfDbMerge_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValToolsConfDbMerge_makefile) TrkValToolsConfDbMerge
else
$(cmt_local_TrkValToolsConfDbMerge_makefile) : $(TrkValToolsConfDbMerge_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkValToolsConfDbMerge.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValToolsConfDbMerge) ] || \
	  [ ! -f $(cmt_final_setup_TrkValToolsConfDbMerge) ] || \
	  $(not_TrkValToolsConfDbMerge_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValToolsConfDbMerge.make"; \
	  $(cmtexe) -f=$(bin)TrkValToolsConfDbMerge.in -tag=$(tags) $(TrkValToolsConfDbMerge_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValToolsConfDbMerge_makefile) TrkValToolsConfDbMerge; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkValToolsConfDbMerge_extratags) build constituent_makefile -out=$(cmt_local_TrkValToolsConfDbMerge_makefile) TrkValToolsConfDbMerge

TrkValToolsConfDbMerge :: $(TrkValToolsConfDbMerge_dependencies) $(cmt_local_TrkValToolsConfDbMerge_makefile) dirs TrkValToolsConfDbMergedirs
	$(echo) "(constituents.make) Starting TrkValToolsConfDbMerge"
	@if test -f $(cmt_local_TrkValToolsConfDbMerge_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsConfDbMerge_makefile) TrkValToolsConfDbMerge; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValToolsConfDbMerge_makefile) TrkValToolsConfDbMerge
	$(echo) "(constituents.make) TrkValToolsConfDbMerge done"

clean :: TrkValToolsConfDbMergeclean ;

TrkValToolsConfDbMergeclean :: $(TrkValToolsConfDbMergeclean_dependencies) ##$(cmt_local_TrkValToolsConfDbMerge_makefile)
	$(echo) "(constituents.make) Starting TrkValToolsConfDbMergeclean"
	@-if test -f $(cmt_local_TrkValToolsConfDbMerge_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsConfDbMerge_makefile) TrkValToolsConfDbMergeclean; \
	fi
	$(echo) "(constituents.make) TrkValToolsConfDbMergeclean done"
#	@-$(MAKE) -f $(cmt_local_TrkValToolsConfDbMerge_makefile) TrkValToolsConfDbMergeclean

##	  /bin/rm -f $(cmt_local_TrkValToolsConfDbMerge_makefile) $(bin)TrkValToolsConfDbMerge_dependencies.make

install :: TrkValToolsConfDbMergeinstall ;

TrkValToolsConfDbMergeinstall :: $(TrkValToolsConfDbMerge_dependencies) $(cmt_local_TrkValToolsConfDbMerge_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkValToolsConfDbMerge_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsConfDbMerge_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkValToolsConfDbMerge_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkValToolsConfDbMergeuninstall

$(foreach d,$(TrkValToolsConfDbMerge_dependencies),$(eval $(d)uninstall_dependencies += TrkValToolsConfDbMergeuninstall))

TrkValToolsConfDbMergeuninstall : $(TrkValToolsConfDbMergeuninstall_dependencies) ##$(cmt_local_TrkValToolsConfDbMerge_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkValToolsConfDbMerge_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsConfDbMerge_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValToolsConfDbMerge_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkValToolsConfDbMergeuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkValToolsConfDbMerge"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkValToolsConfDbMerge done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkValToolsComponentsList_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkValToolsComponentsList_has_target_tag

cmt_local_tagfile_TrkValToolsComponentsList = $(bin)$(TrkValTools_tag)_TrkValToolsComponentsList.make
cmt_final_setup_TrkValToolsComponentsList = $(bin)setup_TrkValToolsComponentsList.make
cmt_local_TrkValToolsComponentsList_makefile = $(bin)TrkValToolsComponentsList.make

TrkValToolsComponentsList_extratags = -tag_add=target_TrkValToolsComponentsList

else

cmt_local_tagfile_TrkValToolsComponentsList = $(bin)$(TrkValTools_tag).make
cmt_final_setup_TrkValToolsComponentsList = $(bin)setup.make
cmt_local_TrkValToolsComponentsList_makefile = $(bin)TrkValToolsComponentsList.make

endif

not_TrkValToolsComponentsList_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkValToolsComponentsList_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkValToolsComponentsListdirs :
	@if test ! -d $(bin)TrkValToolsComponentsList; then $(mkdir) -p $(bin)TrkValToolsComponentsList; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkValToolsComponentsList
else
TrkValToolsComponentsListdirs : ;
endif

ifdef cmt_TrkValToolsComponentsList_has_target_tag

ifndef QUICK
$(cmt_local_TrkValToolsComponentsList_makefile) : $(TrkValToolsComponentsList_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValToolsComponentsList.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValToolsComponentsList_extratags) build constituent_config -out=$(cmt_local_TrkValToolsComponentsList_makefile) TrkValToolsComponentsList
else
$(cmt_local_TrkValToolsComponentsList_makefile) : $(TrkValToolsComponentsList_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValToolsComponentsList) ] || \
	  [ ! -f $(cmt_final_setup_TrkValToolsComponentsList) ] || \
	  $(not_TrkValToolsComponentsList_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValToolsComponentsList.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValToolsComponentsList_extratags) build constituent_config -out=$(cmt_local_TrkValToolsComponentsList_makefile) TrkValToolsComponentsList; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkValToolsComponentsList_makefile) : $(TrkValToolsComponentsList_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValToolsComponentsList.make"; \
	  $(cmtexe) -f=$(bin)TrkValToolsComponentsList.in -tag=$(tags) $(TrkValToolsComponentsList_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValToolsComponentsList_makefile) TrkValToolsComponentsList
else
$(cmt_local_TrkValToolsComponentsList_makefile) : $(TrkValToolsComponentsList_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkValToolsComponentsList.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValToolsComponentsList) ] || \
	  [ ! -f $(cmt_final_setup_TrkValToolsComponentsList) ] || \
	  $(not_TrkValToolsComponentsList_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValToolsComponentsList.make"; \
	  $(cmtexe) -f=$(bin)TrkValToolsComponentsList.in -tag=$(tags) $(TrkValToolsComponentsList_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValToolsComponentsList_makefile) TrkValToolsComponentsList; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkValToolsComponentsList_extratags) build constituent_makefile -out=$(cmt_local_TrkValToolsComponentsList_makefile) TrkValToolsComponentsList

TrkValToolsComponentsList :: $(TrkValToolsComponentsList_dependencies) $(cmt_local_TrkValToolsComponentsList_makefile) dirs TrkValToolsComponentsListdirs
	$(echo) "(constituents.make) Starting TrkValToolsComponentsList"
	@if test -f $(cmt_local_TrkValToolsComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsComponentsList_makefile) TrkValToolsComponentsList; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValToolsComponentsList_makefile) TrkValToolsComponentsList
	$(echo) "(constituents.make) TrkValToolsComponentsList done"

clean :: TrkValToolsComponentsListclean ;

TrkValToolsComponentsListclean :: $(TrkValToolsComponentsListclean_dependencies) ##$(cmt_local_TrkValToolsComponentsList_makefile)
	$(echo) "(constituents.make) Starting TrkValToolsComponentsListclean"
	@-if test -f $(cmt_local_TrkValToolsComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsComponentsList_makefile) TrkValToolsComponentsListclean; \
	fi
	$(echo) "(constituents.make) TrkValToolsComponentsListclean done"
#	@-$(MAKE) -f $(cmt_local_TrkValToolsComponentsList_makefile) TrkValToolsComponentsListclean

##	  /bin/rm -f $(cmt_local_TrkValToolsComponentsList_makefile) $(bin)TrkValToolsComponentsList_dependencies.make

install :: TrkValToolsComponentsListinstall ;

TrkValToolsComponentsListinstall :: $(TrkValToolsComponentsList_dependencies) $(cmt_local_TrkValToolsComponentsList_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkValToolsComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsComponentsList_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkValToolsComponentsList_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkValToolsComponentsListuninstall

$(foreach d,$(TrkValToolsComponentsList_dependencies),$(eval $(d)uninstall_dependencies += TrkValToolsComponentsListuninstall))

TrkValToolsComponentsListuninstall : $(TrkValToolsComponentsListuninstall_dependencies) ##$(cmt_local_TrkValToolsComponentsList_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkValToolsComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsComponentsList_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValToolsComponentsList_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkValToolsComponentsListuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkValToolsComponentsList"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkValToolsComponentsList done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkValToolsMergeComponentsList_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkValToolsMergeComponentsList_has_target_tag

cmt_local_tagfile_TrkValToolsMergeComponentsList = $(bin)$(TrkValTools_tag)_TrkValToolsMergeComponentsList.make
cmt_final_setup_TrkValToolsMergeComponentsList = $(bin)setup_TrkValToolsMergeComponentsList.make
cmt_local_TrkValToolsMergeComponentsList_makefile = $(bin)TrkValToolsMergeComponentsList.make

TrkValToolsMergeComponentsList_extratags = -tag_add=target_TrkValToolsMergeComponentsList

else

cmt_local_tagfile_TrkValToolsMergeComponentsList = $(bin)$(TrkValTools_tag).make
cmt_final_setup_TrkValToolsMergeComponentsList = $(bin)setup.make
cmt_local_TrkValToolsMergeComponentsList_makefile = $(bin)TrkValToolsMergeComponentsList.make

endif

not_TrkValToolsMergeComponentsList_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkValToolsMergeComponentsList_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkValToolsMergeComponentsListdirs :
	@if test ! -d $(bin)TrkValToolsMergeComponentsList; then $(mkdir) -p $(bin)TrkValToolsMergeComponentsList; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkValToolsMergeComponentsList
else
TrkValToolsMergeComponentsListdirs : ;
endif

ifdef cmt_TrkValToolsMergeComponentsList_has_target_tag

ifndef QUICK
$(cmt_local_TrkValToolsMergeComponentsList_makefile) : $(TrkValToolsMergeComponentsList_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValToolsMergeComponentsList.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValToolsMergeComponentsList_extratags) build constituent_config -out=$(cmt_local_TrkValToolsMergeComponentsList_makefile) TrkValToolsMergeComponentsList
else
$(cmt_local_TrkValToolsMergeComponentsList_makefile) : $(TrkValToolsMergeComponentsList_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValToolsMergeComponentsList) ] || \
	  [ ! -f $(cmt_final_setup_TrkValToolsMergeComponentsList) ] || \
	  $(not_TrkValToolsMergeComponentsList_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValToolsMergeComponentsList.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValToolsMergeComponentsList_extratags) build constituent_config -out=$(cmt_local_TrkValToolsMergeComponentsList_makefile) TrkValToolsMergeComponentsList; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkValToolsMergeComponentsList_makefile) : $(TrkValToolsMergeComponentsList_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValToolsMergeComponentsList.make"; \
	  $(cmtexe) -f=$(bin)TrkValToolsMergeComponentsList.in -tag=$(tags) $(TrkValToolsMergeComponentsList_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValToolsMergeComponentsList_makefile) TrkValToolsMergeComponentsList
else
$(cmt_local_TrkValToolsMergeComponentsList_makefile) : $(TrkValToolsMergeComponentsList_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkValToolsMergeComponentsList.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValToolsMergeComponentsList) ] || \
	  [ ! -f $(cmt_final_setup_TrkValToolsMergeComponentsList) ] || \
	  $(not_TrkValToolsMergeComponentsList_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValToolsMergeComponentsList.make"; \
	  $(cmtexe) -f=$(bin)TrkValToolsMergeComponentsList.in -tag=$(tags) $(TrkValToolsMergeComponentsList_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValToolsMergeComponentsList_makefile) TrkValToolsMergeComponentsList; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkValToolsMergeComponentsList_extratags) build constituent_makefile -out=$(cmt_local_TrkValToolsMergeComponentsList_makefile) TrkValToolsMergeComponentsList

TrkValToolsMergeComponentsList :: $(TrkValToolsMergeComponentsList_dependencies) $(cmt_local_TrkValToolsMergeComponentsList_makefile) dirs TrkValToolsMergeComponentsListdirs
	$(echo) "(constituents.make) Starting TrkValToolsMergeComponentsList"
	@if test -f $(cmt_local_TrkValToolsMergeComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsMergeComponentsList_makefile) TrkValToolsMergeComponentsList; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValToolsMergeComponentsList_makefile) TrkValToolsMergeComponentsList
	$(echo) "(constituents.make) TrkValToolsMergeComponentsList done"

clean :: TrkValToolsMergeComponentsListclean ;

TrkValToolsMergeComponentsListclean :: $(TrkValToolsMergeComponentsListclean_dependencies) ##$(cmt_local_TrkValToolsMergeComponentsList_makefile)
	$(echo) "(constituents.make) Starting TrkValToolsMergeComponentsListclean"
	@-if test -f $(cmt_local_TrkValToolsMergeComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsMergeComponentsList_makefile) TrkValToolsMergeComponentsListclean; \
	fi
	$(echo) "(constituents.make) TrkValToolsMergeComponentsListclean done"
#	@-$(MAKE) -f $(cmt_local_TrkValToolsMergeComponentsList_makefile) TrkValToolsMergeComponentsListclean

##	  /bin/rm -f $(cmt_local_TrkValToolsMergeComponentsList_makefile) $(bin)TrkValToolsMergeComponentsList_dependencies.make

install :: TrkValToolsMergeComponentsListinstall ;

TrkValToolsMergeComponentsListinstall :: $(TrkValToolsMergeComponentsList_dependencies) $(cmt_local_TrkValToolsMergeComponentsList_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkValToolsMergeComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsMergeComponentsList_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkValToolsMergeComponentsList_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkValToolsMergeComponentsListuninstall

$(foreach d,$(TrkValToolsMergeComponentsList_dependencies),$(eval $(d)uninstall_dependencies += TrkValToolsMergeComponentsListuninstall))

TrkValToolsMergeComponentsListuninstall : $(TrkValToolsMergeComponentsListuninstall_dependencies) ##$(cmt_local_TrkValToolsMergeComponentsList_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkValToolsMergeComponentsList_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsMergeComponentsList_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValToolsMergeComponentsList_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkValToolsMergeComponentsListuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkValToolsMergeComponentsList"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkValToolsMergeComponentsList done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkValTools_optdebug_library_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkValTools_optdebug_library_has_target_tag

cmt_local_tagfile_TrkValTools_optdebug_library = $(bin)$(TrkValTools_tag)_TrkValTools_optdebug_library.make
cmt_final_setup_TrkValTools_optdebug_library = $(bin)setup_TrkValTools_optdebug_library.make
cmt_local_TrkValTools_optdebug_library_makefile = $(bin)TrkValTools_optdebug_library.make

TrkValTools_optdebug_library_extratags = -tag_add=target_TrkValTools_optdebug_library

else

cmt_local_tagfile_TrkValTools_optdebug_library = $(bin)$(TrkValTools_tag).make
cmt_final_setup_TrkValTools_optdebug_library = $(bin)setup.make
cmt_local_TrkValTools_optdebug_library_makefile = $(bin)TrkValTools_optdebug_library.make

endif

not_TrkValTools_optdebug_library_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkValTools_optdebug_library_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkValTools_optdebug_librarydirs :
	@if test ! -d $(bin)TrkValTools_optdebug_library; then $(mkdir) -p $(bin)TrkValTools_optdebug_library; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkValTools_optdebug_library
else
TrkValTools_optdebug_librarydirs : ;
endif

ifdef cmt_TrkValTools_optdebug_library_has_target_tag

ifndef QUICK
$(cmt_local_TrkValTools_optdebug_library_makefile) : $(TrkValTools_optdebug_library_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValTools_optdebug_library.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValTools_optdebug_library_extratags) build constituent_config -out=$(cmt_local_TrkValTools_optdebug_library_makefile) TrkValTools_optdebug_library
else
$(cmt_local_TrkValTools_optdebug_library_makefile) : $(TrkValTools_optdebug_library_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValTools_optdebug_library) ] || \
	  [ ! -f $(cmt_final_setup_TrkValTools_optdebug_library) ] || \
	  $(not_TrkValTools_optdebug_library_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValTools_optdebug_library.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValTools_optdebug_library_extratags) build constituent_config -out=$(cmt_local_TrkValTools_optdebug_library_makefile) TrkValTools_optdebug_library; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkValTools_optdebug_library_makefile) : $(TrkValTools_optdebug_library_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValTools_optdebug_library.make"; \
	  $(cmtexe) -f=$(bin)TrkValTools_optdebug_library.in -tag=$(tags) $(TrkValTools_optdebug_library_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValTools_optdebug_library_makefile) TrkValTools_optdebug_library
else
$(cmt_local_TrkValTools_optdebug_library_makefile) : $(TrkValTools_optdebug_library_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkValTools_optdebug_library.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValTools_optdebug_library) ] || \
	  [ ! -f $(cmt_final_setup_TrkValTools_optdebug_library) ] || \
	  $(not_TrkValTools_optdebug_library_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValTools_optdebug_library.make"; \
	  $(cmtexe) -f=$(bin)TrkValTools_optdebug_library.in -tag=$(tags) $(TrkValTools_optdebug_library_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValTools_optdebug_library_makefile) TrkValTools_optdebug_library; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkValTools_optdebug_library_extratags) build constituent_makefile -out=$(cmt_local_TrkValTools_optdebug_library_makefile) TrkValTools_optdebug_library

TrkValTools_optdebug_library :: $(TrkValTools_optdebug_library_dependencies) $(cmt_local_TrkValTools_optdebug_library_makefile) dirs TrkValTools_optdebug_librarydirs
	$(echo) "(constituents.make) Starting TrkValTools_optdebug_library"
	@if test -f $(cmt_local_TrkValTools_optdebug_library_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_optdebug_library_makefile) TrkValTools_optdebug_library; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValTools_optdebug_library_makefile) TrkValTools_optdebug_library
	$(echo) "(constituents.make) TrkValTools_optdebug_library done"

clean :: TrkValTools_optdebug_libraryclean ;

TrkValTools_optdebug_libraryclean :: $(TrkValTools_optdebug_libraryclean_dependencies) ##$(cmt_local_TrkValTools_optdebug_library_makefile)
	$(echo) "(constituents.make) Starting TrkValTools_optdebug_libraryclean"
	@-if test -f $(cmt_local_TrkValTools_optdebug_library_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_optdebug_library_makefile) TrkValTools_optdebug_libraryclean; \
	fi
	$(echo) "(constituents.make) TrkValTools_optdebug_libraryclean done"
#	@-$(MAKE) -f $(cmt_local_TrkValTools_optdebug_library_makefile) TrkValTools_optdebug_libraryclean

##	  /bin/rm -f $(cmt_local_TrkValTools_optdebug_library_makefile) $(bin)TrkValTools_optdebug_library_dependencies.make

install :: TrkValTools_optdebug_libraryinstall ;

TrkValTools_optdebug_libraryinstall :: $(TrkValTools_optdebug_library_dependencies) $(cmt_local_TrkValTools_optdebug_library_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkValTools_optdebug_library_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_optdebug_library_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkValTools_optdebug_library_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkValTools_optdebug_libraryuninstall

$(foreach d,$(TrkValTools_optdebug_library_dependencies),$(eval $(d)uninstall_dependencies += TrkValTools_optdebug_libraryuninstall))

TrkValTools_optdebug_libraryuninstall : $(TrkValTools_optdebug_libraryuninstall_dependencies) ##$(cmt_local_TrkValTools_optdebug_library_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkValTools_optdebug_library_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_optdebug_library_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValTools_optdebug_library_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkValTools_optdebug_libraryuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkValTools_optdebug_library"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkValTools_optdebug_library done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkValToolsCLIDDB_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkValToolsCLIDDB_has_target_tag

cmt_local_tagfile_TrkValToolsCLIDDB = $(bin)$(TrkValTools_tag)_TrkValToolsCLIDDB.make
cmt_final_setup_TrkValToolsCLIDDB = $(bin)setup_TrkValToolsCLIDDB.make
cmt_local_TrkValToolsCLIDDB_makefile = $(bin)TrkValToolsCLIDDB.make

TrkValToolsCLIDDB_extratags = -tag_add=target_TrkValToolsCLIDDB

else

cmt_local_tagfile_TrkValToolsCLIDDB = $(bin)$(TrkValTools_tag).make
cmt_final_setup_TrkValToolsCLIDDB = $(bin)setup.make
cmt_local_TrkValToolsCLIDDB_makefile = $(bin)TrkValToolsCLIDDB.make

endif

not_TrkValToolsCLIDDB_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkValToolsCLIDDB_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkValToolsCLIDDBdirs :
	@if test ! -d $(bin)TrkValToolsCLIDDB; then $(mkdir) -p $(bin)TrkValToolsCLIDDB; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkValToolsCLIDDB
else
TrkValToolsCLIDDBdirs : ;
endif

ifdef cmt_TrkValToolsCLIDDB_has_target_tag

ifndef QUICK
$(cmt_local_TrkValToolsCLIDDB_makefile) : $(TrkValToolsCLIDDB_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValToolsCLIDDB.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValToolsCLIDDB_extratags) build constituent_config -out=$(cmt_local_TrkValToolsCLIDDB_makefile) TrkValToolsCLIDDB
else
$(cmt_local_TrkValToolsCLIDDB_makefile) : $(TrkValToolsCLIDDB_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValToolsCLIDDB) ] || \
	  [ ! -f $(cmt_final_setup_TrkValToolsCLIDDB) ] || \
	  $(not_TrkValToolsCLIDDB_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValToolsCLIDDB.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValToolsCLIDDB_extratags) build constituent_config -out=$(cmt_local_TrkValToolsCLIDDB_makefile) TrkValToolsCLIDDB; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkValToolsCLIDDB_makefile) : $(TrkValToolsCLIDDB_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValToolsCLIDDB.make"; \
	  $(cmtexe) -f=$(bin)TrkValToolsCLIDDB.in -tag=$(tags) $(TrkValToolsCLIDDB_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValToolsCLIDDB_makefile) TrkValToolsCLIDDB
else
$(cmt_local_TrkValToolsCLIDDB_makefile) : $(TrkValToolsCLIDDB_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkValToolsCLIDDB.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValToolsCLIDDB) ] || \
	  [ ! -f $(cmt_final_setup_TrkValToolsCLIDDB) ] || \
	  $(not_TrkValToolsCLIDDB_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValToolsCLIDDB.make"; \
	  $(cmtexe) -f=$(bin)TrkValToolsCLIDDB.in -tag=$(tags) $(TrkValToolsCLIDDB_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValToolsCLIDDB_makefile) TrkValToolsCLIDDB; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkValToolsCLIDDB_extratags) build constituent_makefile -out=$(cmt_local_TrkValToolsCLIDDB_makefile) TrkValToolsCLIDDB

TrkValToolsCLIDDB :: $(TrkValToolsCLIDDB_dependencies) $(cmt_local_TrkValToolsCLIDDB_makefile) dirs TrkValToolsCLIDDBdirs
	$(echo) "(constituents.make) Starting TrkValToolsCLIDDB"
	@if test -f $(cmt_local_TrkValToolsCLIDDB_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsCLIDDB_makefile) TrkValToolsCLIDDB; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValToolsCLIDDB_makefile) TrkValToolsCLIDDB
	$(echo) "(constituents.make) TrkValToolsCLIDDB done"

clean :: TrkValToolsCLIDDBclean ;

TrkValToolsCLIDDBclean :: $(TrkValToolsCLIDDBclean_dependencies) ##$(cmt_local_TrkValToolsCLIDDB_makefile)
	$(echo) "(constituents.make) Starting TrkValToolsCLIDDBclean"
	@-if test -f $(cmt_local_TrkValToolsCLIDDB_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsCLIDDB_makefile) TrkValToolsCLIDDBclean; \
	fi
	$(echo) "(constituents.make) TrkValToolsCLIDDBclean done"
#	@-$(MAKE) -f $(cmt_local_TrkValToolsCLIDDB_makefile) TrkValToolsCLIDDBclean

##	  /bin/rm -f $(cmt_local_TrkValToolsCLIDDB_makefile) $(bin)TrkValToolsCLIDDB_dependencies.make

install :: TrkValToolsCLIDDBinstall ;

TrkValToolsCLIDDBinstall :: $(TrkValToolsCLIDDB_dependencies) $(cmt_local_TrkValToolsCLIDDB_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkValToolsCLIDDB_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsCLIDDB_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkValToolsCLIDDB_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkValToolsCLIDDBuninstall

$(foreach d,$(TrkValToolsCLIDDB_dependencies),$(eval $(d)uninstall_dependencies += TrkValToolsCLIDDBuninstall))

TrkValToolsCLIDDBuninstall : $(TrkValToolsCLIDDBuninstall_dependencies) ##$(cmt_local_TrkValToolsCLIDDB_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkValToolsCLIDDB_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsCLIDDB_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValToolsCLIDDB_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkValToolsCLIDDBuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkValToolsCLIDDB"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkValToolsCLIDDB done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkValToolsrchk_has_target_tag = 1

#--------------------------------------

ifdef cmt_TrkValToolsrchk_has_target_tag

cmt_local_tagfile_TrkValToolsrchk = $(bin)$(TrkValTools_tag)_TrkValToolsrchk.make
cmt_final_setup_TrkValToolsrchk = $(bin)setup_TrkValToolsrchk.make
cmt_local_TrkValToolsrchk_makefile = $(bin)TrkValToolsrchk.make

TrkValToolsrchk_extratags = -tag_add=target_TrkValToolsrchk

else

cmt_local_tagfile_TrkValToolsrchk = $(bin)$(TrkValTools_tag).make
cmt_final_setup_TrkValToolsrchk = $(bin)setup.make
cmt_local_TrkValToolsrchk_makefile = $(bin)TrkValToolsrchk.make

endif

not_TrkValToolsrchk_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkValToolsrchk_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkValToolsrchkdirs :
	@if test ! -d $(bin)TrkValToolsrchk; then $(mkdir) -p $(bin)TrkValToolsrchk; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkValToolsrchk
else
TrkValToolsrchkdirs : ;
endif

ifdef cmt_TrkValToolsrchk_has_target_tag

ifndef QUICK
$(cmt_local_TrkValToolsrchk_makefile) : $(TrkValToolsrchk_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValToolsrchk.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValToolsrchk_extratags) build constituent_config -out=$(cmt_local_TrkValToolsrchk_makefile) TrkValToolsrchk
else
$(cmt_local_TrkValToolsrchk_makefile) : $(TrkValToolsrchk_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValToolsrchk) ] || \
	  [ ! -f $(cmt_final_setup_TrkValToolsrchk) ] || \
	  $(not_TrkValToolsrchk_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValToolsrchk.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValToolsrchk_extratags) build constituent_config -out=$(cmt_local_TrkValToolsrchk_makefile) TrkValToolsrchk; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkValToolsrchk_makefile) : $(TrkValToolsrchk_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValToolsrchk.make"; \
	  $(cmtexe) -f=$(bin)TrkValToolsrchk.in -tag=$(tags) $(TrkValToolsrchk_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValToolsrchk_makefile) TrkValToolsrchk
else
$(cmt_local_TrkValToolsrchk_makefile) : $(TrkValToolsrchk_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkValToolsrchk.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValToolsrchk) ] || \
	  [ ! -f $(cmt_final_setup_TrkValToolsrchk) ] || \
	  $(not_TrkValToolsrchk_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValToolsrchk.make"; \
	  $(cmtexe) -f=$(bin)TrkValToolsrchk.in -tag=$(tags) $(TrkValToolsrchk_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValToolsrchk_makefile) TrkValToolsrchk; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkValToolsrchk_extratags) build constituent_makefile -out=$(cmt_local_TrkValToolsrchk_makefile) TrkValToolsrchk

TrkValToolsrchk :: $(TrkValToolsrchk_dependencies) $(cmt_local_TrkValToolsrchk_makefile) dirs TrkValToolsrchkdirs
	$(echo) "(constituents.make) Starting TrkValToolsrchk"
	@if test -f $(cmt_local_TrkValToolsrchk_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsrchk_makefile) TrkValToolsrchk; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValToolsrchk_makefile) TrkValToolsrchk
	$(echo) "(constituents.make) TrkValToolsrchk done"

clean :: TrkValToolsrchkclean ;

TrkValToolsrchkclean :: $(TrkValToolsrchkclean_dependencies) ##$(cmt_local_TrkValToolsrchk_makefile)
	$(echo) "(constituents.make) Starting TrkValToolsrchkclean"
	@-if test -f $(cmt_local_TrkValToolsrchk_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsrchk_makefile) TrkValToolsrchkclean; \
	fi
	$(echo) "(constituents.make) TrkValToolsrchkclean done"
#	@-$(MAKE) -f $(cmt_local_TrkValToolsrchk_makefile) TrkValToolsrchkclean

##	  /bin/rm -f $(cmt_local_TrkValToolsrchk_makefile) $(bin)TrkValToolsrchk_dependencies.make

install :: TrkValToolsrchkinstall ;

TrkValToolsrchkinstall :: $(TrkValToolsrchk_dependencies) $(cmt_local_TrkValToolsrchk_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkValToolsrchk_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsrchk_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkValToolsrchk_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkValToolsrchkuninstall

$(foreach d,$(TrkValToolsrchk_dependencies),$(eval $(d)uninstall_dependencies += TrkValToolsrchkuninstall))

TrkValToolsrchkuninstall : $(TrkValToolsrchkuninstall_dependencies) ##$(cmt_local_TrkValToolsrchk_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkValToolsrchk_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValToolsrchk_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValToolsrchk_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkValToolsrchkuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkValToolsrchk"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkValToolsrchk done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_install_root_include_path_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_install_root_include_path_has_target_tag

cmt_local_tagfile_install_root_include_path = $(bin)$(TrkValTools_tag)_install_root_include_path.make
cmt_final_setup_install_root_include_path = $(bin)setup_install_root_include_path.make
cmt_local_install_root_include_path_makefile = $(bin)install_root_include_path.make

install_root_include_path_extratags = -tag_add=target_install_root_include_path

else

cmt_local_tagfile_install_root_include_path = $(bin)$(TrkValTools_tag).make
cmt_final_setup_install_root_include_path = $(bin)setup.make
cmt_local_install_root_include_path_makefile = $(bin)install_root_include_path.make

endif

not_install_root_include_path_dependencies = { n=0; for p in $?; do m=0; for d in $(install_root_include_path_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
install_root_include_pathdirs :
	@if test ! -d $(bin)install_root_include_path; then $(mkdir) -p $(bin)install_root_include_path; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)install_root_include_path
else
install_root_include_pathdirs : ;
endif

ifdef cmt_install_root_include_path_has_target_tag

ifndef QUICK
$(cmt_local_install_root_include_path_makefile) : $(install_root_include_path_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_root_include_path.make"; \
	  $(cmtexe) -tag=$(tags) $(install_root_include_path_extratags) build constituent_config -out=$(cmt_local_install_root_include_path_makefile) install_root_include_path
else
$(cmt_local_install_root_include_path_makefile) : $(install_root_include_path_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_root_include_path) ] || \
	  [ ! -f $(cmt_final_setup_install_root_include_path) ] || \
	  $(not_install_root_include_path_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_root_include_path.make"; \
	  $(cmtexe) -tag=$(tags) $(install_root_include_path_extratags) build constituent_config -out=$(cmt_local_install_root_include_path_makefile) install_root_include_path; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_install_root_include_path_makefile) : $(install_root_include_path_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_root_include_path.make"; \
	  $(cmtexe) -f=$(bin)install_root_include_path.in -tag=$(tags) $(install_root_include_path_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_root_include_path_makefile) install_root_include_path
else
$(cmt_local_install_root_include_path_makefile) : $(install_root_include_path_dependencies) $(cmt_build_library_linksstamp) $(bin)install_root_include_path.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_root_include_path) ] || \
	  [ ! -f $(cmt_final_setup_install_root_include_path) ] || \
	  $(not_install_root_include_path_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_root_include_path.make"; \
	  $(cmtexe) -f=$(bin)install_root_include_path.in -tag=$(tags) $(install_root_include_path_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_root_include_path_makefile) install_root_include_path; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(install_root_include_path_extratags) build constituent_makefile -out=$(cmt_local_install_root_include_path_makefile) install_root_include_path

install_root_include_path :: $(install_root_include_path_dependencies) $(cmt_local_install_root_include_path_makefile) dirs install_root_include_pathdirs
	$(echo) "(constituents.make) Starting install_root_include_path"
	@if test -f $(cmt_local_install_root_include_path_makefile); then \
	  $(MAKE) -f $(cmt_local_install_root_include_path_makefile) install_root_include_path; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_root_include_path_makefile) install_root_include_path
	$(echo) "(constituents.make) install_root_include_path done"

clean :: install_root_include_pathclean ;

install_root_include_pathclean :: $(install_root_include_pathclean_dependencies) ##$(cmt_local_install_root_include_path_makefile)
	$(echo) "(constituents.make) Starting install_root_include_pathclean"
	@-if test -f $(cmt_local_install_root_include_path_makefile); then \
	  $(MAKE) -f $(cmt_local_install_root_include_path_makefile) install_root_include_pathclean; \
	fi
	$(echo) "(constituents.make) install_root_include_pathclean done"
#	@-$(MAKE) -f $(cmt_local_install_root_include_path_makefile) install_root_include_pathclean

##	  /bin/rm -f $(cmt_local_install_root_include_path_makefile) $(bin)install_root_include_path_dependencies.make

install :: install_root_include_pathinstall ;

install_root_include_pathinstall :: $(install_root_include_path_dependencies) $(cmt_local_install_root_include_path_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_install_root_include_path_makefile); then \
	  $(MAKE) -f $(cmt_local_install_root_include_path_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_install_root_include_path_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : install_root_include_pathuninstall

$(foreach d,$(install_root_include_path_dependencies),$(eval $(d)uninstall_dependencies += install_root_include_pathuninstall))

install_root_include_pathuninstall : $(install_root_include_pathuninstall_dependencies) ##$(cmt_local_install_root_include_path_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_install_root_include_path_makefile); then \
	  $(MAKE) -f $(cmt_local_install_root_include_path_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_root_include_path_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: install_root_include_pathuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ install_root_include_path"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ install_root_include_path done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_install_includes_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_install_includes_has_target_tag

cmt_local_tagfile_install_includes = $(bin)$(TrkValTools_tag)_install_includes.make
cmt_final_setup_install_includes = $(bin)setup_install_includes.make
cmt_local_install_includes_makefile = $(bin)install_includes.make

install_includes_extratags = -tag_add=target_install_includes

else

cmt_local_tagfile_install_includes = $(bin)$(TrkValTools_tag).make
cmt_final_setup_install_includes = $(bin)setup.make
cmt_local_install_includes_makefile = $(bin)install_includes.make

endif

not_install_includes_dependencies = { n=0; for p in $?; do m=0; for d in $(install_includes_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
install_includesdirs :
	@if test ! -d $(bin)install_includes; then $(mkdir) -p $(bin)install_includes; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)install_includes
else
install_includesdirs : ;
endif

ifdef cmt_install_includes_has_target_tag

ifndef QUICK
$(cmt_local_install_includes_makefile) : $(install_includes_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_includes.make"; \
	  $(cmtexe) -tag=$(tags) $(install_includes_extratags) build constituent_config -out=$(cmt_local_install_includes_makefile) install_includes
else
$(cmt_local_install_includes_makefile) : $(install_includes_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_includes) ] || \
	  [ ! -f $(cmt_final_setup_install_includes) ] || \
	  $(not_install_includes_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_includes.make"; \
	  $(cmtexe) -tag=$(tags) $(install_includes_extratags) build constituent_config -out=$(cmt_local_install_includes_makefile) install_includes; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_install_includes_makefile) : $(install_includes_dependencies) build_library_links
	$(echo) "(constituents.make) Building install_includes.make"; \
	  $(cmtexe) -f=$(bin)install_includes.in -tag=$(tags) $(install_includes_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_includes_makefile) install_includes
else
$(cmt_local_install_includes_makefile) : $(install_includes_dependencies) $(cmt_build_library_linksstamp) $(bin)install_includes.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_install_includes) ] || \
	  [ ! -f $(cmt_final_setup_install_includes) ] || \
	  $(not_install_includes_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building install_includes.make"; \
	  $(cmtexe) -f=$(bin)install_includes.in -tag=$(tags) $(install_includes_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_install_includes_makefile) install_includes; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(install_includes_extratags) build constituent_makefile -out=$(cmt_local_install_includes_makefile) install_includes

install_includes :: $(install_includes_dependencies) $(cmt_local_install_includes_makefile) dirs install_includesdirs
	$(echo) "(constituents.make) Starting install_includes"
	@if test -f $(cmt_local_install_includes_makefile); then \
	  $(MAKE) -f $(cmt_local_install_includes_makefile) install_includes; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_includes_makefile) install_includes
	$(echo) "(constituents.make) install_includes done"

clean :: install_includesclean ;

install_includesclean :: $(install_includesclean_dependencies) ##$(cmt_local_install_includes_makefile)
	$(echo) "(constituents.make) Starting install_includesclean"
	@-if test -f $(cmt_local_install_includes_makefile); then \
	  $(MAKE) -f $(cmt_local_install_includes_makefile) install_includesclean; \
	fi
	$(echo) "(constituents.make) install_includesclean done"
#	@-$(MAKE) -f $(cmt_local_install_includes_makefile) install_includesclean

##	  /bin/rm -f $(cmt_local_install_includes_makefile) $(bin)install_includes_dependencies.make

install :: install_includesinstall ;

install_includesinstall :: $(install_includes_dependencies) $(cmt_local_install_includes_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_install_includes_makefile); then \
	  $(MAKE) -f $(cmt_local_install_includes_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_install_includes_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : install_includesuninstall

$(foreach d,$(install_includes_dependencies),$(eval $(d)uninstall_dependencies += install_includesuninstall))

install_includesuninstall : $(install_includesuninstall_dependencies) ##$(cmt_local_install_includes_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_install_includes_makefile); then \
	  $(MAKE) -f $(cmt_local_install_includes_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_install_includes_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: install_includesuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ install_includes"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ install_includes done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_make_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_make_has_target_tag

cmt_local_tagfile_make = $(bin)$(TrkValTools_tag)_make.make
cmt_final_setup_make = $(bin)setup_make.make
cmt_local_make_makefile = $(bin)make.make

make_extratags = -tag_add=target_make

else

cmt_local_tagfile_make = $(bin)$(TrkValTools_tag).make
cmt_final_setup_make = $(bin)setup.make
cmt_local_make_makefile = $(bin)make.make

endif

not_make_dependencies = { n=0; for p in $?; do m=0; for d in $(make_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
makedirs :
	@if test ! -d $(bin)make; then $(mkdir) -p $(bin)make; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)make
else
makedirs : ;
endif

ifdef cmt_make_has_target_tag

ifndef QUICK
$(cmt_local_make_makefile) : $(make_dependencies) build_library_links
	$(echo) "(constituents.make) Building make.make"; \
	  $(cmtexe) -tag=$(tags) $(make_extratags) build constituent_config -out=$(cmt_local_make_makefile) make
else
$(cmt_local_make_makefile) : $(make_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_make) ] || \
	  [ ! -f $(cmt_final_setup_make) ] || \
	  $(not_make_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building make.make"; \
	  $(cmtexe) -tag=$(tags) $(make_extratags) build constituent_config -out=$(cmt_local_make_makefile) make; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_make_makefile) : $(make_dependencies) build_library_links
	$(echo) "(constituents.make) Building make.make"; \
	  $(cmtexe) -f=$(bin)make.in -tag=$(tags) $(make_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_make_makefile) make
else
$(cmt_local_make_makefile) : $(make_dependencies) $(cmt_build_library_linksstamp) $(bin)make.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_make) ] || \
	  [ ! -f $(cmt_final_setup_make) ] || \
	  $(not_make_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building make.make"; \
	  $(cmtexe) -f=$(bin)make.in -tag=$(tags) $(make_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_make_makefile) make; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(make_extratags) build constituent_makefile -out=$(cmt_local_make_makefile) make

make :: $(make_dependencies) $(cmt_local_make_makefile) dirs makedirs
	$(echo) "(constituents.make) Starting make"
	@if test -f $(cmt_local_make_makefile); then \
	  $(MAKE) -f $(cmt_local_make_makefile) make; \
	  fi
#	@$(MAKE) -f $(cmt_local_make_makefile) make
	$(echo) "(constituents.make) make done"

clean :: makeclean ;

makeclean :: $(makeclean_dependencies) ##$(cmt_local_make_makefile)
	$(echo) "(constituents.make) Starting makeclean"
	@-if test -f $(cmt_local_make_makefile); then \
	  $(MAKE) -f $(cmt_local_make_makefile) makeclean; \
	fi
	$(echo) "(constituents.make) makeclean done"
#	@-$(MAKE) -f $(cmt_local_make_makefile) makeclean

##	  /bin/rm -f $(cmt_local_make_makefile) $(bin)make_dependencies.make

install :: makeinstall ;

makeinstall :: $(make_dependencies) $(cmt_local_make_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_make_makefile); then \
	  $(MAKE) -f $(cmt_local_make_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_make_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : makeuninstall

$(foreach d,$(make_dependencies),$(eval $(d)uninstall_dependencies += makeuninstall))

makeuninstall : $(makeuninstall_dependencies) ##$(cmt_local_make_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_make_makefile); then \
	  $(MAKE) -f $(cmt_local_make_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_make_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: makeuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ make"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ make done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_CompilePython_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_CompilePython_has_target_tag

cmt_local_tagfile_CompilePython = $(bin)$(TrkValTools_tag)_CompilePython.make
cmt_final_setup_CompilePython = $(bin)setup_CompilePython.make
cmt_local_CompilePython_makefile = $(bin)CompilePython.make

CompilePython_extratags = -tag_add=target_CompilePython

else

cmt_local_tagfile_CompilePython = $(bin)$(TrkValTools_tag).make
cmt_final_setup_CompilePython = $(bin)setup.make
cmt_local_CompilePython_makefile = $(bin)CompilePython.make

endif

not_CompilePython_dependencies = { n=0; for p in $?; do m=0; for d in $(CompilePython_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
CompilePythondirs :
	@if test ! -d $(bin)CompilePython; then $(mkdir) -p $(bin)CompilePython; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)CompilePython
else
CompilePythondirs : ;
endif

ifdef cmt_CompilePython_has_target_tag

ifndef QUICK
$(cmt_local_CompilePython_makefile) : $(CompilePython_dependencies) build_library_links
	$(echo) "(constituents.make) Building CompilePython.make"; \
	  $(cmtexe) -tag=$(tags) $(CompilePython_extratags) build constituent_config -out=$(cmt_local_CompilePython_makefile) CompilePython
else
$(cmt_local_CompilePython_makefile) : $(CompilePython_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_CompilePython) ] || \
	  [ ! -f $(cmt_final_setup_CompilePython) ] || \
	  $(not_CompilePython_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building CompilePython.make"; \
	  $(cmtexe) -tag=$(tags) $(CompilePython_extratags) build constituent_config -out=$(cmt_local_CompilePython_makefile) CompilePython; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_CompilePython_makefile) : $(CompilePython_dependencies) build_library_links
	$(echo) "(constituents.make) Building CompilePython.make"; \
	  $(cmtexe) -f=$(bin)CompilePython.in -tag=$(tags) $(CompilePython_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_CompilePython_makefile) CompilePython
else
$(cmt_local_CompilePython_makefile) : $(CompilePython_dependencies) $(cmt_build_library_linksstamp) $(bin)CompilePython.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_CompilePython) ] || \
	  [ ! -f $(cmt_final_setup_CompilePython) ] || \
	  $(not_CompilePython_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building CompilePython.make"; \
	  $(cmtexe) -f=$(bin)CompilePython.in -tag=$(tags) $(CompilePython_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_CompilePython_makefile) CompilePython; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(CompilePython_extratags) build constituent_makefile -out=$(cmt_local_CompilePython_makefile) CompilePython

CompilePython :: $(CompilePython_dependencies) $(cmt_local_CompilePython_makefile) dirs CompilePythondirs
	$(echo) "(constituents.make) Starting CompilePython"
	@if test -f $(cmt_local_CompilePython_makefile); then \
	  $(MAKE) -f $(cmt_local_CompilePython_makefile) CompilePython; \
	  fi
#	@$(MAKE) -f $(cmt_local_CompilePython_makefile) CompilePython
	$(echo) "(constituents.make) CompilePython done"

clean :: CompilePythonclean ;

CompilePythonclean :: $(CompilePythonclean_dependencies) ##$(cmt_local_CompilePython_makefile)
	$(echo) "(constituents.make) Starting CompilePythonclean"
	@-if test -f $(cmt_local_CompilePython_makefile); then \
	  $(MAKE) -f $(cmt_local_CompilePython_makefile) CompilePythonclean; \
	fi
	$(echo) "(constituents.make) CompilePythonclean done"
#	@-$(MAKE) -f $(cmt_local_CompilePython_makefile) CompilePythonclean

##	  /bin/rm -f $(cmt_local_CompilePython_makefile) $(bin)CompilePython_dependencies.make

install :: CompilePythoninstall ;

CompilePythoninstall :: $(CompilePython_dependencies) $(cmt_local_CompilePython_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_CompilePython_makefile); then \
	  $(MAKE) -f $(cmt_local_CompilePython_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_CompilePython_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : CompilePythonuninstall

$(foreach d,$(CompilePython_dependencies),$(eval $(d)uninstall_dependencies += CompilePythonuninstall))

CompilePythonuninstall : $(CompilePythonuninstall_dependencies) ##$(cmt_local_CompilePython_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_CompilePython_makefile); then \
	  $(MAKE) -f $(cmt_local_CompilePython_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_CompilePython_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: CompilePythonuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ CompilePython"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ CompilePython done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_qmtest_run_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_qmtest_run_has_target_tag

cmt_local_tagfile_qmtest_run = $(bin)$(TrkValTools_tag)_qmtest_run.make
cmt_final_setup_qmtest_run = $(bin)setup_qmtest_run.make
cmt_local_qmtest_run_makefile = $(bin)qmtest_run.make

qmtest_run_extratags = -tag_add=target_qmtest_run

else

cmt_local_tagfile_qmtest_run = $(bin)$(TrkValTools_tag).make
cmt_final_setup_qmtest_run = $(bin)setup.make
cmt_local_qmtest_run_makefile = $(bin)qmtest_run.make

endif

not_qmtest_run_dependencies = { n=0; for p in $?; do m=0; for d in $(qmtest_run_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
qmtest_rundirs :
	@if test ! -d $(bin)qmtest_run; then $(mkdir) -p $(bin)qmtest_run; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)qmtest_run
else
qmtest_rundirs : ;
endif

ifdef cmt_qmtest_run_has_target_tag

ifndef QUICK
$(cmt_local_qmtest_run_makefile) : $(qmtest_run_dependencies) build_library_links
	$(echo) "(constituents.make) Building qmtest_run.make"; \
	  $(cmtexe) -tag=$(tags) $(qmtest_run_extratags) build constituent_config -out=$(cmt_local_qmtest_run_makefile) qmtest_run
else
$(cmt_local_qmtest_run_makefile) : $(qmtest_run_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_qmtest_run) ] || \
	  [ ! -f $(cmt_final_setup_qmtest_run) ] || \
	  $(not_qmtest_run_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building qmtest_run.make"; \
	  $(cmtexe) -tag=$(tags) $(qmtest_run_extratags) build constituent_config -out=$(cmt_local_qmtest_run_makefile) qmtest_run; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_qmtest_run_makefile) : $(qmtest_run_dependencies) build_library_links
	$(echo) "(constituents.make) Building qmtest_run.make"; \
	  $(cmtexe) -f=$(bin)qmtest_run.in -tag=$(tags) $(qmtest_run_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_qmtest_run_makefile) qmtest_run
else
$(cmt_local_qmtest_run_makefile) : $(qmtest_run_dependencies) $(cmt_build_library_linksstamp) $(bin)qmtest_run.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_qmtest_run) ] || \
	  [ ! -f $(cmt_final_setup_qmtest_run) ] || \
	  $(not_qmtest_run_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building qmtest_run.make"; \
	  $(cmtexe) -f=$(bin)qmtest_run.in -tag=$(tags) $(qmtest_run_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_qmtest_run_makefile) qmtest_run; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(qmtest_run_extratags) build constituent_makefile -out=$(cmt_local_qmtest_run_makefile) qmtest_run

qmtest_run :: $(qmtest_run_dependencies) $(cmt_local_qmtest_run_makefile) dirs qmtest_rundirs
	$(echo) "(constituents.make) Starting qmtest_run"
	@if test -f $(cmt_local_qmtest_run_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_run_makefile) qmtest_run; \
	  fi
#	@$(MAKE) -f $(cmt_local_qmtest_run_makefile) qmtest_run
	$(echo) "(constituents.make) qmtest_run done"

clean :: qmtest_runclean ;

qmtest_runclean :: $(qmtest_runclean_dependencies) ##$(cmt_local_qmtest_run_makefile)
	$(echo) "(constituents.make) Starting qmtest_runclean"
	@-if test -f $(cmt_local_qmtest_run_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_run_makefile) qmtest_runclean; \
	fi
	$(echo) "(constituents.make) qmtest_runclean done"
#	@-$(MAKE) -f $(cmt_local_qmtest_run_makefile) qmtest_runclean

##	  /bin/rm -f $(cmt_local_qmtest_run_makefile) $(bin)qmtest_run_dependencies.make

install :: qmtest_runinstall ;

qmtest_runinstall :: $(qmtest_run_dependencies) $(cmt_local_qmtest_run_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_qmtest_run_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_run_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_qmtest_run_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : qmtest_rununinstall

$(foreach d,$(qmtest_run_dependencies),$(eval $(d)uninstall_dependencies += qmtest_rununinstall))

qmtest_rununinstall : $(qmtest_rununinstall_dependencies) ##$(cmt_local_qmtest_run_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_qmtest_run_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_run_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_qmtest_run_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: qmtest_rununinstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ qmtest_run"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ qmtest_run done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_qmtest_summarize_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_qmtest_summarize_has_target_tag

cmt_local_tagfile_qmtest_summarize = $(bin)$(TrkValTools_tag)_qmtest_summarize.make
cmt_final_setup_qmtest_summarize = $(bin)setup_qmtest_summarize.make
cmt_local_qmtest_summarize_makefile = $(bin)qmtest_summarize.make

qmtest_summarize_extratags = -tag_add=target_qmtest_summarize

else

cmt_local_tagfile_qmtest_summarize = $(bin)$(TrkValTools_tag).make
cmt_final_setup_qmtest_summarize = $(bin)setup.make
cmt_local_qmtest_summarize_makefile = $(bin)qmtest_summarize.make

endif

not_qmtest_summarize_dependencies = { n=0; for p in $?; do m=0; for d in $(qmtest_summarize_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
qmtest_summarizedirs :
	@if test ! -d $(bin)qmtest_summarize; then $(mkdir) -p $(bin)qmtest_summarize; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)qmtest_summarize
else
qmtest_summarizedirs : ;
endif

ifdef cmt_qmtest_summarize_has_target_tag

ifndef QUICK
$(cmt_local_qmtest_summarize_makefile) : $(qmtest_summarize_dependencies) build_library_links
	$(echo) "(constituents.make) Building qmtest_summarize.make"; \
	  $(cmtexe) -tag=$(tags) $(qmtest_summarize_extratags) build constituent_config -out=$(cmt_local_qmtest_summarize_makefile) qmtest_summarize
else
$(cmt_local_qmtest_summarize_makefile) : $(qmtest_summarize_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_qmtest_summarize) ] || \
	  [ ! -f $(cmt_final_setup_qmtest_summarize) ] || \
	  $(not_qmtest_summarize_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building qmtest_summarize.make"; \
	  $(cmtexe) -tag=$(tags) $(qmtest_summarize_extratags) build constituent_config -out=$(cmt_local_qmtest_summarize_makefile) qmtest_summarize; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_qmtest_summarize_makefile) : $(qmtest_summarize_dependencies) build_library_links
	$(echo) "(constituents.make) Building qmtest_summarize.make"; \
	  $(cmtexe) -f=$(bin)qmtest_summarize.in -tag=$(tags) $(qmtest_summarize_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_qmtest_summarize_makefile) qmtest_summarize
else
$(cmt_local_qmtest_summarize_makefile) : $(qmtest_summarize_dependencies) $(cmt_build_library_linksstamp) $(bin)qmtest_summarize.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_qmtest_summarize) ] || \
	  [ ! -f $(cmt_final_setup_qmtest_summarize) ] || \
	  $(not_qmtest_summarize_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building qmtest_summarize.make"; \
	  $(cmtexe) -f=$(bin)qmtest_summarize.in -tag=$(tags) $(qmtest_summarize_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_qmtest_summarize_makefile) qmtest_summarize; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(qmtest_summarize_extratags) build constituent_makefile -out=$(cmt_local_qmtest_summarize_makefile) qmtest_summarize

qmtest_summarize :: $(qmtest_summarize_dependencies) $(cmt_local_qmtest_summarize_makefile) dirs qmtest_summarizedirs
	$(echo) "(constituents.make) Starting qmtest_summarize"
	@if test -f $(cmt_local_qmtest_summarize_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_summarize_makefile) qmtest_summarize; \
	  fi
#	@$(MAKE) -f $(cmt_local_qmtest_summarize_makefile) qmtest_summarize
	$(echo) "(constituents.make) qmtest_summarize done"

clean :: qmtest_summarizeclean ;

qmtest_summarizeclean :: $(qmtest_summarizeclean_dependencies) ##$(cmt_local_qmtest_summarize_makefile)
	$(echo) "(constituents.make) Starting qmtest_summarizeclean"
	@-if test -f $(cmt_local_qmtest_summarize_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_summarize_makefile) qmtest_summarizeclean; \
	fi
	$(echo) "(constituents.make) qmtest_summarizeclean done"
#	@-$(MAKE) -f $(cmt_local_qmtest_summarize_makefile) qmtest_summarizeclean

##	  /bin/rm -f $(cmt_local_qmtest_summarize_makefile) $(bin)qmtest_summarize_dependencies.make

install :: qmtest_summarizeinstall ;

qmtest_summarizeinstall :: $(qmtest_summarize_dependencies) $(cmt_local_qmtest_summarize_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_qmtest_summarize_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_summarize_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_qmtest_summarize_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : qmtest_summarizeuninstall

$(foreach d,$(qmtest_summarize_dependencies),$(eval $(d)uninstall_dependencies += qmtest_summarizeuninstall))

qmtest_summarizeuninstall : $(qmtest_summarizeuninstall_dependencies) ##$(cmt_local_qmtest_summarize_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_qmtest_summarize_makefile); then \
	  $(MAKE) -f $(cmt_local_qmtest_summarize_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_qmtest_summarize_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: qmtest_summarizeuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ qmtest_summarize"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ qmtest_summarize done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TestPackage_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TestPackage_has_target_tag

cmt_local_tagfile_TestPackage = $(bin)$(TrkValTools_tag)_TestPackage.make
cmt_final_setup_TestPackage = $(bin)setup_TestPackage.make
cmt_local_TestPackage_makefile = $(bin)TestPackage.make

TestPackage_extratags = -tag_add=target_TestPackage

else

cmt_local_tagfile_TestPackage = $(bin)$(TrkValTools_tag).make
cmt_final_setup_TestPackage = $(bin)setup.make
cmt_local_TestPackage_makefile = $(bin)TestPackage.make

endif

not_TestPackage_dependencies = { n=0; for p in $?; do m=0; for d in $(TestPackage_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TestPackagedirs :
	@if test ! -d $(bin)TestPackage; then $(mkdir) -p $(bin)TestPackage; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TestPackage
else
TestPackagedirs : ;
endif

ifdef cmt_TestPackage_has_target_tag

ifndef QUICK
$(cmt_local_TestPackage_makefile) : $(TestPackage_dependencies) build_library_links
	$(echo) "(constituents.make) Building TestPackage.make"; \
	  $(cmtexe) -tag=$(tags) $(TestPackage_extratags) build constituent_config -out=$(cmt_local_TestPackage_makefile) TestPackage
else
$(cmt_local_TestPackage_makefile) : $(TestPackage_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TestPackage) ] || \
	  [ ! -f $(cmt_final_setup_TestPackage) ] || \
	  $(not_TestPackage_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TestPackage.make"; \
	  $(cmtexe) -tag=$(tags) $(TestPackage_extratags) build constituent_config -out=$(cmt_local_TestPackage_makefile) TestPackage; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TestPackage_makefile) : $(TestPackage_dependencies) build_library_links
	$(echo) "(constituents.make) Building TestPackage.make"; \
	  $(cmtexe) -f=$(bin)TestPackage.in -tag=$(tags) $(TestPackage_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TestPackage_makefile) TestPackage
else
$(cmt_local_TestPackage_makefile) : $(TestPackage_dependencies) $(cmt_build_library_linksstamp) $(bin)TestPackage.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TestPackage) ] || \
	  [ ! -f $(cmt_final_setup_TestPackage) ] || \
	  $(not_TestPackage_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TestPackage.make"; \
	  $(cmtexe) -f=$(bin)TestPackage.in -tag=$(tags) $(TestPackage_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TestPackage_makefile) TestPackage; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TestPackage_extratags) build constituent_makefile -out=$(cmt_local_TestPackage_makefile) TestPackage

TestPackage :: $(TestPackage_dependencies) $(cmt_local_TestPackage_makefile) dirs TestPackagedirs
	$(echo) "(constituents.make) Starting TestPackage"
	@if test -f $(cmt_local_TestPackage_makefile); then \
	  $(MAKE) -f $(cmt_local_TestPackage_makefile) TestPackage; \
	  fi
#	@$(MAKE) -f $(cmt_local_TestPackage_makefile) TestPackage
	$(echo) "(constituents.make) TestPackage done"

clean :: TestPackageclean ;

TestPackageclean :: $(TestPackageclean_dependencies) ##$(cmt_local_TestPackage_makefile)
	$(echo) "(constituents.make) Starting TestPackageclean"
	@-if test -f $(cmt_local_TestPackage_makefile); then \
	  $(MAKE) -f $(cmt_local_TestPackage_makefile) TestPackageclean; \
	fi
	$(echo) "(constituents.make) TestPackageclean done"
#	@-$(MAKE) -f $(cmt_local_TestPackage_makefile) TestPackageclean

##	  /bin/rm -f $(cmt_local_TestPackage_makefile) $(bin)TestPackage_dependencies.make

install :: TestPackageinstall ;

TestPackageinstall :: $(TestPackage_dependencies) $(cmt_local_TestPackage_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TestPackage_makefile); then \
	  $(MAKE) -f $(cmt_local_TestPackage_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TestPackage_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TestPackageuninstall

$(foreach d,$(TestPackage_dependencies),$(eval $(d)uninstall_dependencies += TestPackageuninstall))

TestPackageuninstall : $(TestPackageuninstall_dependencies) ##$(cmt_local_TestPackage_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TestPackage_makefile); then \
	  $(MAKE) -f $(cmt_local_TestPackage_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TestPackage_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TestPackageuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TestPackage"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TestPackage done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TestProject_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TestProject_has_target_tag

cmt_local_tagfile_TestProject = $(bin)$(TrkValTools_tag)_TestProject.make
cmt_final_setup_TestProject = $(bin)setup_TestProject.make
cmt_local_TestProject_makefile = $(bin)TestProject.make

TestProject_extratags = -tag_add=target_TestProject

else

cmt_local_tagfile_TestProject = $(bin)$(TrkValTools_tag).make
cmt_final_setup_TestProject = $(bin)setup.make
cmt_local_TestProject_makefile = $(bin)TestProject.make

endif

not_TestProject_dependencies = { n=0; for p in $?; do m=0; for d in $(TestProject_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TestProjectdirs :
	@if test ! -d $(bin)TestProject; then $(mkdir) -p $(bin)TestProject; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TestProject
else
TestProjectdirs : ;
endif

ifdef cmt_TestProject_has_target_tag

ifndef QUICK
$(cmt_local_TestProject_makefile) : $(TestProject_dependencies) build_library_links
	$(echo) "(constituents.make) Building TestProject.make"; \
	  $(cmtexe) -tag=$(tags) $(TestProject_extratags) build constituent_config -out=$(cmt_local_TestProject_makefile) TestProject
else
$(cmt_local_TestProject_makefile) : $(TestProject_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TestProject) ] || \
	  [ ! -f $(cmt_final_setup_TestProject) ] || \
	  $(not_TestProject_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TestProject.make"; \
	  $(cmtexe) -tag=$(tags) $(TestProject_extratags) build constituent_config -out=$(cmt_local_TestProject_makefile) TestProject; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TestProject_makefile) : $(TestProject_dependencies) build_library_links
	$(echo) "(constituents.make) Building TestProject.make"; \
	  $(cmtexe) -f=$(bin)TestProject.in -tag=$(tags) $(TestProject_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TestProject_makefile) TestProject
else
$(cmt_local_TestProject_makefile) : $(TestProject_dependencies) $(cmt_build_library_linksstamp) $(bin)TestProject.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TestProject) ] || \
	  [ ! -f $(cmt_final_setup_TestProject) ] || \
	  $(not_TestProject_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TestProject.make"; \
	  $(cmtexe) -f=$(bin)TestProject.in -tag=$(tags) $(TestProject_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TestProject_makefile) TestProject; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TestProject_extratags) build constituent_makefile -out=$(cmt_local_TestProject_makefile) TestProject

TestProject :: $(TestProject_dependencies) $(cmt_local_TestProject_makefile) dirs TestProjectdirs
	$(echo) "(constituents.make) Starting TestProject"
	@if test -f $(cmt_local_TestProject_makefile); then \
	  $(MAKE) -f $(cmt_local_TestProject_makefile) TestProject; \
	  fi
#	@$(MAKE) -f $(cmt_local_TestProject_makefile) TestProject
	$(echo) "(constituents.make) TestProject done"

clean :: TestProjectclean ;

TestProjectclean :: $(TestProjectclean_dependencies) ##$(cmt_local_TestProject_makefile)
	$(echo) "(constituents.make) Starting TestProjectclean"
	@-if test -f $(cmt_local_TestProject_makefile); then \
	  $(MAKE) -f $(cmt_local_TestProject_makefile) TestProjectclean; \
	fi
	$(echo) "(constituents.make) TestProjectclean done"
#	@-$(MAKE) -f $(cmt_local_TestProject_makefile) TestProjectclean

##	  /bin/rm -f $(cmt_local_TestProject_makefile) $(bin)TestProject_dependencies.make

install :: TestProjectinstall ;

TestProjectinstall :: $(TestProject_dependencies) $(cmt_local_TestProject_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TestProject_makefile); then \
	  $(MAKE) -f $(cmt_local_TestProject_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TestProject_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TestProjectuninstall

$(foreach d,$(TestProject_dependencies),$(eval $(d)uninstall_dependencies += TestProjectuninstall))

TestProjectuninstall : $(TestProjectuninstall_dependencies) ##$(cmt_local_TestProject_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TestProject_makefile); then \
	  $(MAKE) -f $(cmt_local_TestProject_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TestProject_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TestProjectuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TestProject"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TestProject done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_new_rootsys_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_new_rootsys_has_target_tag

cmt_local_tagfile_new_rootsys = $(bin)$(TrkValTools_tag)_new_rootsys.make
cmt_final_setup_new_rootsys = $(bin)setup_new_rootsys.make
cmt_local_new_rootsys_makefile = $(bin)new_rootsys.make

new_rootsys_extratags = -tag_add=target_new_rootsys

else

cmt_local_tagfile_new_rootsys = $(bin)$(TrkValTools_tag).make
cmt_final_setup_new_rootsys = $(bin)setup.make
cmt_local_new_rootsys_makefile = $(bin)new_rootsys.make

endif

not_new_rootsys_dependencies = { n=0; for p in $?; do m=0; for d in $(new_rootsys_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
new_rootsysdirs :
	@if test ! -d $(bin)new_rootsys; then $(mkdir) -p $(bin)new_rootsys; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)new_rootsys
else
new_rootsysdirs : ;
endif

ifdef cmt_new_rootsys_has_target_tag

ifndef QUICK
$(cmt_local_new_rootsys_makefile) : $(new_rootsys_dependencies) build_library_links
	$(echo) "(constituents.make) Building new_rootsys.make"; \
	  $(cmtexe) -tag=$(tags) $(new_rootsys_extratags) build constituent_config -out=$(cmt_local_new_rootsys_makefile) new_rootsys
else
$(cmt_local_new_rootsys_makefile) : $(new_rootsys_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_new_rootsys) ] || \
	  [ ! -f $(cmt_final_setup_new_rootsys) ] || \
	  $(not_new_rootsys_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building new_rootsys.make"; \
	  $(cmtexe) -tag=$(tags) $(new_rootsys_extratags) build constituent_config -out=$(cmt_local_new_rootsys_makefile) new_rootsys; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_new_rootsys_makefile) : $(new_rootsys_dependencies) build_library_links
	$(echo) "(constituents.make) Building new_rootsys.make"; \
	  $(cmtexe) -f=$(bin)new_rootsys.in -tag=$(tags) $(new_rootsys_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_new_rootsys_makefile) new_rootsys
else
$(cmt_local_new_rootsys_makefile) : $(new_rootsys_dependencies) $(cmt_build_library_linksstamp) $(bin)new_rootsys.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_new_rootsys) ] || \
	  [ ! -f $(cmt_final_setup_new_rootsys) ] || \
	  $(not_new_rootsys_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building new_rootsys.make"; \
	  $(cmtexe) -f=$(bin)new_rootsys.in -tag=$(tags) $(new_rootsys_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_new_rootsys_makefile) new_rootsys; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(new_rootsys_extratags) build constituent_makefile -out=$(cmt_local_new_rootsys_makefile) new_rootsys

new_rootsys :: $(new_rootsys_dependencies) $(cmt_local_new_rootsys_makefile) dirs new_rootsysdirs
	$(echo) "(constituents.make) Starting new_rootsys"
	@if test -f $(cmt_local_new_rootsys_makefile); then \
	  $(MAKE) -f $(cmt_local_new_rootsys_makefile) new_rootsys; \
	  fi
#	@$(MAKE) -f $(cmt_local_new_rootsys_makefile) new_rootsys
	$(echo) "(constituents.make) new_rootsys done"

clean :: new_rootsysclean ;

new_rootsysclean :: $(new_rootsysclean_dependencies) ##$(cmt_local_new_rootsys_makefile)
	$(echo) "(constituents.make) Starting new_rootsysclean"
	@-if test -f $(cmt_local_new_rootsys_makefile); then \
	  $(MAKE) -f $(cmt_local_new_rootsys_makefile) new_rootsysclean; \
	fi
	$(echo) "(constituents.make) new_rootsysclean done"
#	@-$(MAKE) -f $(cmt_local_new_rootsys_makefile) new_rootsysclean

##	  /bin/rm -f $(cmt_local_new_rootsys_makefile) $(bin)new_rootsys_dependencies.make

install :: new_rootsysinstall ;

new_rootsysinstall :: $(new_rootsys_dependencies) $(cmt_local_new_rootsys_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_new_rootsys_makefile); then \
	  $(MAKE) -f $(cmt_local_new_rootsys_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_new_rootsys_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : new_rootsysuninstall

$(foreach d,$(new_rootsys_dependencies),$(eval $(d)uninstall_dependencies += new_rootsysuninstall))

new_rootsysuninstall : $(new_rootsysuninstall_dependencies) ##$(cmt_local_new_rootsys_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_new_rootsys_makefile); then \
	  $(MAKE) -f $(cmt_local_new_rootsys_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_new_rootsys_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: new_rootsysuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ new_rootsys"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ new_rootsys done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_doxygen_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_doxygen_has_target_tag

cmt_local_tagfile_doxygen = $(bin)$(TrkValTools_tag)_doxygen.make
cmt_final_setup_doxygen = $(bin)setup_doxygen.make
cmt_local_doxygen_makefile = $(bin)doxygen.make

doxygen_extratags = -tag_add=target_doxygen

else

cmt_local_tagfile_doxygen = $(bin)$(TrkValTools_tag).make
cmt_final_setup_doxygen = $(bin)setup.make
cmt_local_doxygen_makefile = $(bin)doxygen.make

endif

not_doxygen_dependencies = { n=0; for p in $?; do m=0; for d in $(doxygen_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
doxygendirs :
	@if test ! -d $(bin)doxygen; then $(mkdir) -p $(bin)doxygen; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)doxygen
else
doxygendirs : ;
endif

ifdef cmt_doxygen_has_target_tag

ifndef QUICK
$(cmt_local_doxygen_makefile) : $(doxygen_dependencies) build_library_links
	$(echo) "(constituents.make) Building doxygen.make"; \
	  $(cmtexe) -tag=$(tags) $(doxygen_extratags) build constituent_config -out=$(cmt_local_doxygen_makefile) doxygen
else
$(cmt_local_doxygen_makefile) : $(doxygen_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_doxygen) ] || \
	  [ ! -f $(cmt_final_setup_doxygen) ] || \
	  $(not_doxygen_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building doxygen.make"; \
	  $(cmtexe) -tag=$(tags) $(doxygen_extratags) build constituent_config -out=$(cmt_local_doxygen_makefile) doxygen; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_doxygen_makefile) : $(doxygen_dependencies) build_library_links
	$(echo) "(constituents.make) Building doxygen.make"; \
	  $(cmtexe) -f=$(bin)doxygen.in -tag=$(tags) $(doxygen_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_doxygen_makefile) doxygen
else
$(cmt_local_doxygen_makefile) : $(doxygen_dependencies) $(cmt_build_library_linksstamp) $(bin)doxygen.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_doxygen) ] || \
	  [ ! -f $(cmt_final_setup_doxygen) ] || \
	  $(not_doxygen_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building doxygen.make"; \
	  $(cmtexe) -f=$(bin)doxygen.in -tag=$(tags) $(doxygen_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_doxygen_makefile) doxygen; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(doxygen_extratags) build constituent_makefile -out=$(cmt_local_doxygen_makefile) doxygen

doxygen :: $(doxygen_dependencies) $(cmt_local_doxygen_makefile) dirs doxygendirs
	$(echo) "(constituents.make) Starting doxygen"
	@if test -f $(cmt_local_doxygen_makefile); then \
	  $(MAKE) -f $(cmt_local_doxygen_makefile) doxygen; \
	  fi
#	@$(MAKE) -f $(cmt_local_doxygen_makefile) doxygen
	$(echo) "(constituents.make) doxygen done"

clean :: doxygenclean ;

doxygenclean :: $(doxygenclean_dependencies) ##$(cmt_local_doxygen_makefile)
	$(echo) "(constituents.make) Starting doxygenclean"
	@-if test -f $(cmt_local_doxygen_makefile); then \
	  $(MAKE) -f $(cmt_local_doxygen_makefile) doxygenclean; \
	fi
	$(echo) "(constituents.make) doxygenclean done"
#	@-$(MAKE) -f $(cmt_local_doxygen_makefile) doxygenclean

##	  /bin/rm -f $(cmt_local_doxygen_makefile) $(bin)doxygen_dependencies.make

install :: doxygeninstall ;

doxygeninstall :: $(doxygen_dependencies) $(cmt_local_doxygen_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_doxygen_makefile); then \
	  $(MAKE) -f $(cmt_local_doxygen_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_doxygen_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : doxygenuninstall

$(foreach d,$(doxygen_dependencies),$(eval $(d)uninstall_dependencies += doxygenuninstall))

doxygenuninstall : $(doxygenuninstall_dependencies) ##$(cmt_local_doxygen_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_doxygen_makefile); then \
	  $(MAKE) -f $(cmt_local_doxygen_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_doxygen_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: doxygenuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ doxygen"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ doxygen done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_post_install_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_post_install_has_target_tag

cmt_local_tagfile_post_install = $(bin)$(TrkValTools_tag)_post_install.make
cmt_final_setup_post_install = $(bin)setup_post_install.make
cmt_local_post_install_makefile = $(bin)post_install.make

post_install_extratags = -tag_add=target_post_install

else

cmt_local_tagfile_post_install = $(bin)$(TrkValTools_tag).make
cmt_final_setup_post_install = $(bin)setup.make
cmt_local_post_install_makefile = $(bin)post_install.make

endif

not_post_install_dependencies = { n=0; for p in $?; do m=0; for d in $(post_install_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
post_installdirs :
	@if test ! -d $(bin)post_install; then $(mkdir) -p $(bin)post_install; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)post_install
else
post_installdirs : ;
endif

ifdef cmt_post_install_has_target_tag

ifndef QUICK
$(cmt_local_post_install_makefile) : $(post_install_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_install.make"; \
	  $(cmtexe) -tag=$(tags) $(post_install_extratags) build constituent_config -out=$(cmt_local_post_install_makefile) post_install
else
$(cmt_local_post_install_makefile) : $(post_install_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_install) ] || \
	  [ ! -f $(cmt_final_setup_post_install) ] || \
	  $(not_post_install_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_install.make"; \
	  $(cmtexe) -tag=$(tags) $(post_install_extratags) build constituent_config -out=$(cmt_local_post_install_makefile) post_install; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_post_install_makefile) : $(post_install_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_install.make"; \
	  $(cmtexe) -f=$(bin)post_install.in -tag=$(tags) $(post_install_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_install_makefile) post_install
else
$(cmt_local_post_install_makefile) : $(post_install_dependencies) $(cmt_build_library_linksstamp) $(bin)post_install.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_install) ] || \
	  [ ! -f $(cmt_final_setup_post_install) ] || \
	  $(not_post_install_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_install.make"; \
	  $(cmtexe) -f=$(bin)post_install.in -tag=$(tags) $(post_install_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_install_makefile) post_install; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(post_install_extratags) build constituent_makefile -out=$(cmt_local_post_install_makefile) post_install

post_install :: $(post_install_dependencies) $(cmt_local_post_install_makefile) dirs post_installdirs
	$(echo) "(constituents.make) Starting post_install"
	@if test -f $(cmt_local_post_install_makefile); then \
	  $(MAKE) -f $(cmt_local_post_install_makefile) post_install; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_install_makefile) post_install
	$(echo) "(constituents.make) post_install done"

clean :: post_installclean ;

post_installclean :: $(post_installclean_dependencies) ##$(cmt_local_post_install_makefile)
	$(echo) "(constituents.make) Starting post_installclean"
	@-if test -f $(cmt_local_post_install_makefile); then \
	  $(MAKE) -f $(cmt_local_post_install_makefile) post_installclean; \
	fi
	$(echo) "(constituents.make) post_installclean done"
#	@-$(MAKE) -f $(cmt_local_post_install_makefile) post_installclean

##	  /bin/rm -f $(cmt_local_post_install_makefile) $(bin)post_install_dependencies.make

install :: post_installinstall ;

post_installinstall :: $(post_install_dependencies) $(cmt_local_post_install_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_post_install_makefile); then \
	  $(MAKE) -f $(cmt_local_post_install_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_post_install_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : post_installuninstall

$(foreach d,$(post_install_dependencies),$(eval $(d)uninstall_dependencies += post_installuninstall))

post_installuninstall : $(post_installuninstall_dependencies) ##$(cmt_local_post_install_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_post_install_makefile); then \
	  $(MAKE) -f $(cmt_local_post_install_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_install_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: post_installuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ post_install"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ post_install done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_post_merge_rootmap_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_post_merge_rootmap_has_target_tag

cmt_local_tagfile_post_merge_rootmap = $(bin)$(TrkValTools_tag)_post_merge_rootmap.make
cmt_final_setup_post_merge_rootmap = $(bin)setup_post_merge_rootmap.make
cmt_local_post_merge_rootmap_makefile = $(bin)post_merge_rootmap.make

post_merge_rootmap_extratags = -tag_add=target_post_merge_rootmap

else

cmt_local_tagfile_post_merge_rootmap = $(bin)$(TrkValTools_tag).make
cmt_final_setup_post_merge_rootmap = $(bin)setup.make
cmt_local_post_merge_rootmap_makefile = $(bin)post_merge_rootmap.make

endif

not_post_merge_rootmap_dependencies = { n=0; for p in $?; do m=0; for d in $(post_merge_rootmap_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
post_merge_rootmapdirs :
	@if test ! -d $(bin)post_merge_rootmap; then $(mkdir) -p $(bin)post_merge_rootmap; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)post_merge_rootmap
else
post_merge_rootmapdirs : ;
endif

ifdef cmt_post_merge_rootmap_has_target_tag

ifndef QUICK
$(cmt_local_post_merge_rootmap_makefile) : $(post_merge_rootmap_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_merge_rootmap.make"; \
	  $(cmtexe) -tag=$(tags) $(post_merge_rootmap_extratags) build constituent_config -out=$(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap
else
$(cmt_local_post_merge_rootmap_makefile) : $(post_merge_rootmap_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_merge_rootmap) ] || \
	  [ ! -f $(cmt_final_setup_post_merge_rootmap) ] || \
	  $(not_post_merge_rootmap_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_merge_rootmap.make"; \
	  $(cmtexe) -tag=$(tags) $(post_merge_rootmap_extratags) build constituent_config -out=$(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_post_merge_rootmap_makefile) : $(post_merge_rootmap_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_merge_rootmap.make"; \
	  $(cmtexe) -f=$(bin)post_merge_rootmap.in -tag=$(tags) $(post_merge_rootmap_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap
else
$(cmt_local_post_merge_rootmap_makefile) : $(post_merge_rootmap_dependencies) $(cmt_build_library_linksstamp) $(bin)post_merge_rootmap.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_merge_rootmap) ] || \
	  [ ! -f $(cmt_final_setup_post_merge_rootmap) ] || \
	  $(not_post_merge_rootmap_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_merge_rootmap.make"; \
	  $(cmtexe) -f=$(bin)post_merge_rootmap.in -tag=$(tags) $(post_merge_rootmap_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(post_merge_rootmap_extratags) build constituent_makefile -out=$(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap

post_merge_rootmap :: $(post_merge_rootmap_dependencies) $(cmt_local_post_merge_rootmap_makefile) dirs post_merge_rootmapdirs
	$(echo) "(constituents.make) Starting post_merge_rootmap"
	@if test -f $(cmt_local_post_merge_rootmap_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) post_merge_rootmap
	$(echo) "(constituents.make) post_merge_rootmap done"

clean :: post_merge_rootmapclean ;

post_merge_rootmapclean :: $(post_merge_rootmapclean_dependencies) ##$(cmt_local_post_merge_rootmap_makefile)
	$(echo) "(constituents.make) Starting post_merge_rootmapclean"
	@-if test -f $(cmt_local_post_merge_rootmap_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) post_merge_rootmapclean; \
	fi
	$(echo) "(constituents.make) post_merge_rootmapclean done"
#	@-$(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) post_merge_rootmapclean

##	  /bin/rm -f $(cmt_local_post_merge_rootmap_makefile) $(bin)post_merge_rootmap_dependencies.make

install :: post_merge_rootmapinstall ;

post_merge_rootmapinstall :: $(post_merge_rootmap_dependencies) $(cmt_local_post_merge_rootmap_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_post_merge_rootmap_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : post_merge_rootmapuninstall

$(foreach d,$(post_merge_rootmap_dependencies),$(eval $(d)uninstall_dependencies += post_merge_rootmapuninstall))

post_merge_rootmapuninstall : $(post_merge_rootmapuninstall_dependencies) ##$(cmt_local_post_merge_rootmap_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_post_merge_rootmap_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_merge_rootmap_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: post_merge_rootmapuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ post_merge_rootmap"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ post_merge_rootmap done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_post_merge_genconfdb_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_post_merge_genconfdb_has_target_tag

cmt_local_tagfile_post_merge_genconfdb = $(bin)$(TrkValTools_tag)_post_merge_genconfdb.make
cmt_final_setup_post_merge_genconfdb = $(bin)setup_post_merge_genconfdb.make
cmt_local_post_merge_genconfdb_makefile = $(bin)post_merge_genconfdb.make

post_merge_genconfdb_extratags = -tag_add=target_post_merge_genconfdb

else

cmt_local_tagfile_post_merge_genconfdb = $(bin)$(TrkValTools_tag).make
cmt_final_setup_post_merge_genconfdb = $(bin)setup.make
cmt_local_post_merge_genconfdb_makefile = $(bin)post_merge_genconfdb.make

endif

not_post_merge_genconfdb_dependencies = { n=0; for p in $?; do m=0; for d in $(post_merge_genconfdb_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
post_merge_genconfdbdirs :
	@if test ! -d $(bin)post_merge_genconfdb; then $(mkdir) -p $(bin)post_merge_genconfdb; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)post_merge_genconfdb
else
post_merge_genconfdbdirs : ;
endif

ifdef cmt_post_merge_genconfdb_has_target_tag

ifndef QUICK
$(cmt_local_post_merge_genconfdb_makefile) : $(post_merge_genconfdb_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_merge_genconfdb.make"; \
	  $(cmtexe) -tag=$(tags) $(post_merge_genconfdb_extratags) build constituent_config -out=$(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb
else
$(cmt_local_post_merge_genconfdb_makefile) : $(post_merge_genconfdb_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_merge_genconfdb) ] || \
	  [ ! -f $(cmt_final_setup_post_merge_genconfdb) ] || \
	  $(not_post_merge_genconfdb_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_merge_genconfdb.make"; \
	  $(cmtexe) -tag=$(tags) $(post_merge_genconfdb_extratags) build constituent_config -out=$(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_post_merge_genconfdb_makefile) : $(post_merge_genconfdb_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_merge_genconfdb.make"; \
	  $(cmtexe) -f=$(bin)post_merge_genconfdb.in -tag=$(tags) $(post_merge_genconfdb_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb
else
$(cmt_local_post_merge_genconfdb_makefile) : $(post_merge_genconfdb_dependencies) $(cmt_build_library_linksstamp) $(bin)post_merge_genconfdb.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_merge_genconfdb) ] || \
	  [ ! -f $(cmt_final_setup_post_merge_genconfdb) ] || \
	  $(not_post_merge_genconfdb_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_merge_genconfdb.make"; \
	  $(cmtexe) -f=$(bin)post_merge_genconfdb.in -tag=$(tags) $(post_merge_genconfdb_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(post_merge_genconfdb_extratags) build constituent_makefile -out=$(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb

post_merge_genconfdb :: $(post_merge_genconfdb_dependencies) $(cmt_local_post_merge_genconfdb_makefile) dirs post_merge_genconfdbdirs
	$(echo) "(constituents.make) Starting post_merge_genconfdb"
	@if test -f $(cmt_local_post_merge_genconfdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdb
	$(echo) "(constituents.make) post_merge_genconfdb done"

clean :: post_merge_genconfdbclean ;

post_merge_genconfdbclean :: $(post_merge_genconfdbclean_dependencies) ##$(cmt_local_post_merge_genconfdb_makefile)
	$(echo) "(constituents.make) Starting post_merge_genconfdbclean"
	@-if test -f $(cmt_local_post_merge_genconfdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdbclean; \
	fi
	$(echo) "(constituents.make) post_merge_genconfdbclean done"
#	@-$(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) post_merge_genconfdbclean

##	  /bin/rm -f $(cmt_local_post_merge_genconfdb_makefile) $(bin)post_merge_genconfdb_dependencies.make

install :: post_merge_genconfdbinstall ;

post_merge_genconfdbinstall :: $(post_merge_genconfdb_dependencies) $(cmt_local_post_merge_genconfdb_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_post_merge_genconfdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : post_merge_genconfdbuninstall

$(foreach d,$(post_merge_genconfdb_dependencies),$(eval $(d)uninstall_dependencies += post_merge_genconfdbuninstall))

post_merge_genconfdbuninstall : $(post_merge_genconfdbuninstall_dependencies) ##$(cmt_local_post_merge_genconfdb_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_post_merge_genconfdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_merge_genconfdb_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: post_merge_genconfdbuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ post_merge_genconfdb"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ post_merge_genconfdb done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_post_build_tpcnvdb_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_post_build_tpcnvdb_has_target_tag

cmt_local_tagfile_post_build_tpcnvdb = $(bin)$(TrkValTools_tag)_post_build_tpcnvdb.make
cmt_final_setup_post_build_tpcnvdb = $(bin)setup_post_build_tpcnvdb.make
cmt_local_post_build_tpcnvdb_makefile = $(bin)post_build_tpcnvdb.make

post_build_tpcnvdb_extratags = -tag_add=target_post_build_tpcnvdb

else

cmt_local_tagfile_post_build_tpcnvdb = $(bin)$(TrkValTools_tag).make
cmt_final_setup_post_build_tpcnvdb = $(bin)setup.make
cmt_local_post_build_tpcnvdb_makefile = $(bin)post_build_tpcnvdb.make

endif

not_post_build_tpcnvdb_dependencies = { n=0; for p in $?; do m=0; for d in $(post_build_tpcnvdb_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
post_build_tpcnvdbdirs :
	@if test ! -d $(bin)post_build_tpcnvdb; then $(mkdir) -p $(bin)post_build_tpcnvdb; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)post_build_tpcnvdb
else
post_build_tpcnvdbdirs : ;
endif

ifdef cmt_post_build_tpcnvdb_has_target_tag

ifndef QUICK
$(cmt_local_post_build_tpcnvdb_makefile) : $(post_build_tpcnvdb_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_build_tpcnvdb.make"; \
	  $(cmtexe) -tag=$(tags) $(post_build_tpcnvdb_extratags) build constituent_config -out=$(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb
else
$(cmt_local_post_build_tpcnvdb_makefile) : $(post_build_tpcnvdb_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_build_tpcnvdb) ] || \
	  [ ! -f $(cmt_final_setup_post_build_tpcnvdb) ] || \
	  $(not_post_build_tpcnvdb_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_build_tpcnvdb.make"; \
	  $(cmtexe) -tag=$(tags) $(post_build_tpcnvdb_extratags) build constituent_config -out=$(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_post_build_tpcnvdb_makefile) : $(post_build_tpcnvdb_dependencies) build_library_links
	$(echo) "(constituents.make) Building post_build_tpcnvdb.make"; \
	  $(cmtexe) -f=$(bin)post_build_tpcnvdb.in -tag=$(tags) $(post_build_tpcnvdb_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb
else
$(cmt_local_post_build_tpcnvdb_makefile) : $(post_build_tpcnvdb_dependencies) $(cmt_build_library_linksstamp) $(bin)post_build_tpcnvdb.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_post_build_tpcnvdb) ] || \
	  [ ! -f $(cmt_final_setup_post_build_tpcnvdb) ] || \
	  $(not_post_build_tpcnvdb_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building post_build_tpcnvdb.make"; \
	  $(cmtexe) -f=$(bin)post_build_tpcnvdb.in -tag=$(tags) $(post_build_tpcnvdb_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(post_build_tpcnvdb_extratags) build constituent_makefile -out=$(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb

post_build_tpcnvdb :: $(post_build_tpcnvdb_dependencies) $(cmt_local_post_build_tpcnvdb_makefile) dirs post_build_tpcnvdbdirs
	$(echo) "(constituents.make) Starting post_build_tpcnvdb"
	@if test -f $(cmt_local_post_build_tpcnvdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdb
	$(echo) "(constituents.make) post_build_tpcnvdb done"

clean :: post_build_tpcnvdbclean ;

post_build_tpcnvdbclean :: $(post_build_tpcnvdbclean_dependencies) ##$(cmt_local_post_build_tpcnvdb_makefile)
	$(echo) "(constituents.make) Starting post_build_tpcnvdbclean"
	@-if test -f $(cmt_local_post_build_tpcnvdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdbclean; \
	fi
	$(echo) "(constituents.make) post_build_tpcnvdbclean done"
#	@-$(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) post_build_tpcnvdbclean

##	  /bin/rm -f $(cmt_local_post_build_tpcnvdb_makefile) $(bin)post_build_tpcnvdb_dependencies.make

install :: post_build_tpcnvdbinstall ;

post_build_tpcnvdbinstall :: $(post_build_tpcnvdb_dependencies) $(cmt_local_post_build_tpcnvdb_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_post_build_tpcnvdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : post_build_tpcnvdbuninstall

$(foreach d,$(post_build_tpcnvdb_dependencies),$(eval $(d)uninstall_dependencies += post_build_tpcnvdbuninstall))

post_build_tpcnvdbuninstall : $(post_build_tpcnvdbuninstall_dependencies) ##$(cmt_local_post_build_tpcnvdb_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_post_build_tpcnvdb_makefile); then \
	  $(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_post_build_tpcnvdb_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: post_build_tpcnvdbuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ post_build_tpcnvdb"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ post_build_tpcnvdb done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_all_post_constituents_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_all_post_constituents_has_target_tag

cmt_local_tagfile_all_post_constituents = $(bin)$(TrkValTools_tag)_all_post_constituents.make
cmt_final_setup_all_post_constituents = $(bin)setup_all_post_constituents.make
cmt_local_all_post_constituents_makefile = $(bin)all_post_constituents.make

all_post_constituents_extratags = -tag_add=target_all_post_constituents

else

cmt_local_tagfile_all_post_constituents = $(bin)$(TrkValTools_tag).make
cmt_final_setup_all_post_constituents = $(bin)setup.make
cmt_local_all_post_constituents_makefile = $(bin)all_post_constituents.make

endif

not_all_post_constituents_dependencies = { n=0; for p in $?; do m=0; for d in $(all_post_constituents_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
all_post_constituentsdirs :
	@if test ! -d $(bin)all_post_constituents; then $(mkdir) -p $(bin)all_post_constituents; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)all_post_constituents
else
all_post_constituentsdirs : ;
endif

ifdef cmt_all_post_constituents_has_target_tag

ifndef QUICK
$(cmt_local_all_post_constituents_makefile) : $(all_post_constituents_dependencies) build_library_links
	$(echo) "(constituents.make) Building all_post_constituents.make"; \
	  $(cmtexe) -tag=$(tags) $(all_post_constituents_extratags) build constituent_config -out=$(cmt_local_all_post_constituents_makefile) all_post_constituents
else
$(cmt_local_all_post_constituents_makefile) : $(all_post_constituents_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_all_post_constituents) ] || \
	  [ ! -f $(cmt_final_setup_all_post_constituents) ] || \
	  $(not_all_post_constituents_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building all_post_constituents.make"; \
	  $(cmtexe) -tag=$(tags) $(all_post_constituents_extratags) build constituent_config -out=$(cmt_local_all_post_constituents_makefile) all_post_constituents; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_all_post_constituents_makefile) : $(all_post_constituents_dependencies) build_library_links
	$(echo) "(constituents.make) Building all_post_constituents.make"; \
	  $(cmtexe) -f=$(bin)all_post_constituents.in -tag=$(tags) $(all_post_constituents_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_all_post_constituents_makefile) all_post_constituents
else
$(cmt_local_all_post_constituents_makefile) : $(all_post_constituents_dependencies) $(cmt_build_library_linksstamp) $(bin)all_post_constituents.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_all_post_constituents) ] || \
	  [ ! -f $(cmt_final_setup_all_post_constituents) ] || \
	  $(not_all_post_constituents_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building all_post_constituents.make"; \
	  $(cmtexe) -f=$(bin)all_post_constituents.in -tag=$(tags) $(all_post_constituents_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_all_post_constituents_makefile) all_post_constituents; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(all_post_constituents_extratags) build constituent_makefile -out=$(cmt_local_all_post_constituents_makefile) all_post_constituents

all_post_constituents :: $(all_post_constituents_dependencies) $(cmt_local_all_post_constituents_makefile) dirs all_post_constituentsdirs
	$(echo) "(constituents.make) Starting all_post_constituents"
	@if test -f $(cmt_local_all_post_constituents_makefile); then \
	  $(MAKE) -f $(cmt_local_all_post_constituents_makefile) all_post_constituents; \
	  fi
#	@$(MAKE) -f $(cmt_local_all_post_constituents_makefile) all_post_constituents
	$(echo) "(constituents.make) all_post_constituents done"

clean :: all_post_constituentsclean ;

all_post_constituentsclean :: $(all_post_constituentsclean_dependencies) ##$(cmt_local_all_post_constituents_makefile)
	$(echo) "(constituents.make) Starting all_post_constituentsclean"
	@-if test -f $(cmt_local_all_post_constituents_makefile); then \
	  $(MAKE) -f $(cmt_local_all_post_constituents_makefile) all_post_constituentsclean; \
	fi
	$(echo) "(constituents.make) all_post_constituentsclean done"
#	@-$(MAKE) -f $(cmt_local_all_post_constituents_makefile) all_post_constituentsclean

##	  /bin/rm -f $(cmt_local_all_post_constituents_makefile) $(bin)all_post_constituents_dependencies.make

install :: all_post_constituentsinstall ;

all_post_constituentsinstall :: $(all_post_constituents_dependencies) $(cmt_local_all_post_constituents_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_all_post_constituents_makefile); then \
	  $(MAKE) -f $(cmt_local_all_post_constituents_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_all_post_constituents_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : all_post_constituentsuninstall

$(foreach d,$(all_post_constituents_dependencies),$(eval $(d)uninstall_dependencies += all_post_constituentsuninstall))

all_post_constituentsuninstall : $(all_post_constituentsuninstall_dependencies) ##$(cmt_local_all_post_constituents_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_all_post_constituents_makefile); then \
	  $(MAKE) -f $(cmt_local_all_post_constituents_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_all_post_constituents_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: all_post_constituentsuninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ all_post_constituents"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ all_post_constituents done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_checkreq_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_checkreq_has_target_tag

cmt_local_tagfile_checkreq = $(bin)$(TrkValTools_tag)_checkreq.make
cmt_final_setup_checkreq = $(bin)setup_checkreq.make
cmt_local_checkreq_makefile = $(bin)checkreq.make

checkreq_extratags = -tag_add=target_checkreq

else

cmt_local_tagfile_checkreq = $(bin)$(TrkValTools_tag).make
cmt_final_setup_checkreq = $(bin)setup.make
cmt_local_checkreq_makefile = $(bin)checkreq.make

endif

not_checkreq_dependencies = { n=0; for p in $?; do m=0; for d in $(checkreq_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
checkreqdirs :
	@if test ! -d $(bin)checkreq; then $(mkdir) -p $(bin)checkreq; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)checkreq
else
checkreqdirs : ;
endif

ifdef cmt_checkreq_has_target_tag

ifndef QUICK
$(cmt_local_checkreq_makefile) : $(checkreq_dependencies) build_library_links
	$(echo) "(constituents.make) Building checkreq.make"; \
	  $(cmtexe) -tag=$(tags) $(checkreq_extratags) build constituent_config -out=$(cmt_local_checkreq_makefile) checkreq
else
$(cmt_local_checkreq_makefile) : $(checkreq_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_checkreq) ] || \
	  [ ! -f $(cmt_final_setup_checkreq) ] || \
	  $(not_checkreq_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building checkreq.make"; \
	  $(cmtexe) -tag=$(tags) $(checkreq_extratags) build constituent_config -out=$(cmt_local_checkreq_makefile) checkreq; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_checkreq_makefile) : $(checkreq_dependencies) build_library_links
	$(echo) "(constituents.make) Building checkreq.make"; \
	  $(cmtexe) -f=$(bin)checkreq.in -tag=$(tags) $(checkreq_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_checkreq_makefile) checkreq
else
$(cmt_local_checkreq_makefile) : $(checkreq_dependencies) $(cmt_build_library_linksstamp) $(bin)checkreq.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_checkreq) ] || \
	  [ ! -f $(cmt_final_setup_checkreq) ] || \
	  $(not_checkreq_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building checkreq.make"; \
	  $(cmtexe) -f=$(bin)checkreq.in -tag=$(tags) $(checkreq_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_checkreq_makefile) checkreq; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(checkreq_extratags) build constituent_makefile -out=$(cmt_local_checkreq_makefile) checkreq

checkreq :: $(checkreq_dependencies) $(cmt_local_checkreq_makefile) dirs checkreqdirs
	$(echo) "(constituents.make) Starting checkreq"
	@if test -f $(cmt_local_checkreq_makefile); then \
	  $(MAKE) -f $(cmt_local_checkreq_makefile) checkreq; \
	  fi
#	@$(MAKE) -f $(cmt_local_checkreq_makefile) checkreq
	$(echo) "(constituents.make) checkreq done"

clean :: checkreqclean ;

checkreqclean :: $(checkreqclean_dependencies) ##$(cmt_local_checkreq_makefile)
	$(echo) "(constituents.make) Starting checkreqclean"
	@-if test -f $(cmt_local_checkreq_makefile); then \
	  $(MAKE) -f $(cmt_local_checkreq_makefile) checkreqclean; \
	fi
	$(echo) "(constituents.make) checkreqclean done"
#	@-$(MAKE) -f $(cmt_local_checkreq_makefile) checkreqclean

##	  /bin/rm -f $(cmt_local_checkreq_makefile) $(bin)checkreq_dependencies.make

install :: checkreqinstall ;

checkreqinstall :: $(checkreq_dependencies) $(cmt_local_checkreq_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_checkreq_makefile); then \
	  $(MAKE) -f $(cmt_local_checkreq_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_checkreq_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : checkrequninstall

$(foreach d,$(checkreq_dependencies),$(eval $(d)uninstall_dependencies += checkrequninstall))

checkrequninstall : $(checkrequninstall_dependencies) ##$(cmt_local_checkreq_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_checkreq_makefile); then \
	  $(MAKE) -f $(cmt_local_checkreq_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_checkreq_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: checkrequninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ checkreq"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ checkreq done"
endif

#-- end of constituent ------
#-- start of constituent ------

cmt_TrkValTools_NICOS_Fix_debuginfo_has_no_target_tag = 1

#--------------------------------------

ifdef cmt_TrkValTools_NICOS_Fix_debuginfo_has_target_tag

cmt_local_tagfile_TrkValTools_NICOS_Fix_debuginfo = $(bin)$(TrkValTools_tag)_TrkValTools_NICOS_Fix_debuginfo.make
cmt_final_setup_TrkValTools_NICOS_Fix_debuginfo = $(bin)setup_TrkValTools_NICOS_Fix_debuginfo.make
cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile = $(bin)TrkValTools_NICOS_Fix_debuginfo.make

TrkValTools_NICOS_Fix_debuginfo_extratags = -tag_add=target_TrkValTools_NICOS_Fix_debuginfo

else

cmt_local_tagfile_TrkValTools_NICOS_Fix_debuginfo = $(bin)$(TrkValTools_tag).make
cmt_final_setup_TrkValTools_NICOS_Fix_debuginfo = $(bin)setup.make
cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile = $(bin)TrkValTools_NICOS_Fix_debuginfo.make

endif

not_TrkValTools_NICOS_Fix_debuginfo_dependencies = { n=0; for p in $?; do m=0; for d in $(TrkValTools_NICOS_Fix_debuginfo_dependencies); do if [ $$p = $$d ]; then m=1; break; fi; done; if [ $$m -eq 0 ]; then n=1; break; fi; done; [ $$n -eq 1 ]; }

ifdef STRUCTURED_OUTPUT
TrkValTools_NICOS_Fix_debuginfodirs :
	@if test ! -d $(bin)TrkValTools_NICOS_Fix_debuginfo; then $(mkdir) -p $(bin)TrkValTools_NICOS_Fix_debuginfo; fi
	$(echo) "STRUCTURED_OUTPUT="$(bin)TrkValTools_NICOS_Fix_debuginfo
else
TrkValTools_NICOS_Fix_debuginfodirs : ;
endif

ifdef cmt_TrkValTools_NICOS_Fix_debuginfo_has_target_tag

ifndef QUICK
$(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) : $(TrkValTools_NICOS_Fix_debuginfo_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValTools_NICOS_Fix_debuginfo.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValTools_NICOS_Fix_debuginfo_extratags) build constituent_config -out=$(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) TrkValTools_NICOS_Fix_debuginfo
else
$(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) : $(TrkValTools_NICOS_Fix_debuginfo_dependencies) $(cmt_build_library_linksstamp) $(use_requirements)
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValTools_NICOS_Fix_debuginfo) ] || \
	  [ ! -f $(cmt_final_setup_TrkValTools_NICOS_Fix_debuginfo) ] || \
	  $(not_TrkValTools_NICOS_Fix_debuginfo_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValTools_NICOS_Fix_debuginfo.make"; \
	  $(cmtexe) -tag=$(tags) $(TrkValTools_NICOS_Fix_debuginfo_extratags) build constituent_config -out=$(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) TrkValTools_NICOS_Fix_debuginfo; \
	  fi
endif

else

ifndef QUICK
$(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) : $(TrkValTools_NICOS_Fix_debuginfo_dependencies) build_library_links
	$(echo) "(constituents.make) Building TrkValTools_NICOS_Fix_debuginfo.make"; \
	  $(cmtexe) -f=$(bin)TrkValTools_NICOS_Fix_debuginfo.in -tag=$(tags) $(TrkValTools_NICOS_Fix_debuginfo_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) TrkValTools_NICOS_Fix_debuginfo
else
$(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) : $(TrkValTools_NICOS_Fix_debuginfo_dependencies) $(cmt_build_library_linksstamp) $(bin)TrkValTools_NICOS_Fix_debuginfo.in
	@if [ ! -f $@ ] || [ ! -f $(cmt_local_tagfile_TrkValTools_NICOS_Fix_debuginfo) ] || \
	  [ ! -f $(cmt_final_setup_TrkValTools_NICOS_Fix_debuginfo) ] || \
	  $(not_TrkValTools_NICOS_Fix_debuginfo_dependencies) ; then \
	  test -z "$(cmtmsg)" || \
	  echo "$(CMTMSGPREFIX)" "(constituents.make) Building TrkValTools_NICOS_Fix_debuginfo.make"; \
	  $(cmtexe) -f=$(bin)TrkValTools_NICOS_Fix_debuginfo.in -tag=$(tags) $(TrkValTools_NICOS_Fix_debuginfo_extratags) build constituent_makefile -without_cmt -out=$(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) TrkValTools_NICOS_Fix_debuginfo; \
	  fi
endif

endif

#	  $(cmtexe) -tag=$(tags) $(TrkValTools_NICOS_Fix_debuginfo_extratags) build constituent_makefile -out=$(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) TrkValTools_NICOS_Fix_debuginfo

TrkValTools_NICOS_Fix_debuginfo :: $(TrkValTools_NICOS_Fix_debuginfo_dependencies) $(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) dirs TrkValTools_NICOS_Fix_debuginfodirs
	$(echo) "(constituents.make) Starting TrkValTools_NICOS_Fix_debuginfo"
	@if test -f $(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) TrkValTools_NICOS_Fix_debuginfo; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) TrkValTools_NICOS_Fix_debuginfo
	$(echo) "(constituents.make) TrkValTools_NICOS_Fix_debuginfo done"

clean :: TrkValTools_NICOS_Fix_debuginfoclean ;

TrkValTools_NICOS_Fix_debuginfoclean :: $(TrkValTools_NICOS_Fix_debuginfoclean_dependencies) ##$(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile)
	$(echo) "(constituents.make) Starting TrkValTools_NICOS_Fix_debuginfoclean"
	@-if test -f $(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) TrkValTools_NICOS_Fix_debuginfoclean; \
	fi
	$(echo) "(constituents.make) TrkValTools_NICOS_Fix_debuginfoclean done"
#	@-$(MAKE) -f $(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) TrkValTools_NICOS_Fix_debuginfoclean

##	  /bin/rm -f $(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) $(bin)TrkValTools_NICOS_Fix_debuginfo_dependencies.make

install :: TrkValTools_NICOS_Fix_debuginfoinstall ;

TrkValTools_NICOS_Fix_debuginfoinstall :: $(TrkValTools_NICOS_Fix_debuginfo_dependencies) $(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile)
	$(echo) "(constituents.make) Starting $@"
	@if test -f $(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) install; \
	  fi
#	@-$(MAKE) -f $(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) install
	$(echo) "(constituents.make) $@ done"

uninstall : TrkValTools_NICOS_Fix_debuginfouninstall

$(foreach d,$(TrkValTools_NICOS_Fix_debuginfo_dependencies),$(eval $(d)uninstall_dependencies += TrkValTools_NICOS_Fix_debuginfouninstall))

TrkValTools_NICOS_Fix_debuginfouninstall : $(TrkValTools_NICOS_Fix_debuginfouninstall_dependencies) ##$(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile)
	$(echo) "(constituents.make) Starting $@"
	@-if test -f $(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile); then \
	  $(MAKE) -f $(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) uninstall; \
	  fi
#	@$(MAKE) -f $(cmt_local_TrkValTools_NICOS_Fix_debuginfo_makefile) uninstall
	$(echo) "(constituents.make) $@ done"

remove_library_links :: TrkValTools_NICOS_Fix_debuginfouninstall ;

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(constituents.make) Starting $@ TrkValTools_NICOS_Fix_debuginfo"
	$(echo) Using default action for $@
	$(echo) "(constituents.make) $@ TrkValTools_NICOS_Fix_debuginfo done"
endif

#-- end of constituent ------
#-- start of constituents_trailer ------

uninstall : remove_library_links ;
clean ::
	$(cleanup_echo) $(cmt_build_library_linksstamp)
	-$(cleanup_silent) \rm -f $(cmt_build_library_linksstamp)
#clean :: remove_library_links

remove_library_links ::
ifndef QUICK
	$(echo) "(constituents.make) Removing library links"; \
	  $(remove_library_links)
else
	$(echo) "(constituents.make) Removing library links"; \
	  $(remove_library_links) -f=$(bin)library_links.in -without_cmt
endif

#-- end of constituents_trailer ------
