#-- start of make_header -----------------

#====================================
#  Document TrkValToolsCLIDDB
#
#   Generated Thu Apr 14 11:53:18 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_TrkValToolsCLIDDB_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_TrkValToolsCLIDDB_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_TrkValToolsCLIDDB

TrkValTools_tag = $(tag)

#cmt_local_tagfile_TrkValToolsCLIDDB = $(TrkValTools_tag)_TrkValToolsCLIDDB.make
cmt_local_tagfile_TrkValToolsCLIDDB = $(bin)$(TrkValTools_tag)_TrkValToolsCLIDDB.make

else

tags      = $(tag),$(CMTEXTRATAGS)

TrkValTools_tag = $(tag)

#cmt_local_tagfile_TrkValToolsCLIDDB = $(TrkValTools_tag).make
cmt_local_tagfile_TrkValToolsCLIDDB = $(bin)$(TrkValTools_tag).make

endif

include $(cmt_local_tagfile_TrkValToolsCLIDDB)
#-include $(cmt_local_tagfile_TrkValToolsCLIDDB)

ifdef cmt_TrkValToolsCLIDDB_has_target_tag

cmt_final_setup_TrkValToolsCLIDDB = $(bin)setup_TrkValToolsCLIDDB.make
cmt_dependencies_in_TrkValToolsCLIDDB = $(bin)dependencies_TrkValToolsCLIDDB.in
#cmt_final_setup_TrkValToolsCLIDDB = $(bin)TrkValTools_TrkValToolsCLIDDBsetup.make
cmt_local_TrkValToolsCLIDDB_makefile = $(bin)TrkValToolsCLIDDB.make

else

cmt_final_setup_TrkValToolsCLIDDB = $(bin)setup.make
cmt_dependencies_in_TrkValToolsCLIDDB = $(bin)dependencies.in
#cmt_final_setup_TrkValToolsCLIDDB = $(bin)TrkValToolssetup.make
cmt_local_TrkValToolsCLIDDB_makefile = $(bin)TrkValToolsCLIDDB.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)TrkValToolssetup.make

#TrkValToolsCLIDDB :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'TrkValToolsCLIDDB'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = TrkValToolsCLIDDB/
#TrkValToolsCLIDDB::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genCLIDDB_header
# Author: Paolo Calafiura
# derived from genconf_header

# Use genCLIDDB_cmd to create package clid.db files

.PHONY: TrkValToolsCLIDDB TrkValToolsCLIDDBclean

outname := clid.db
cliddb  := TrkValTools_$(outname)
instdir := $(CMTINSTALLAREA)/share
result  := $(instdir)/$(cliddb)
product := $(instdir)/$(outname)
conflib := $(bin)$(library_prefix)TrkValTools.$(shlibsuffix)

TrkValToolsCLIDDB :: $(result)

$(instdir) :
	$(mkdir) -p $(instdir)

$(result) : $(conflib) $(product)
	@$(genCLIDDB_cmd) -p TrkValTools -i$(product) -o $(result)

$(product) : $(instdir)
	touch $(product)

TrkValToolsCLIDDBclean ::
	$(cleanup_silent) $(uninstall_command) $(product) $(result)
	$(cleanup_silent) $(cmt_uninstallarea_command) $(product) $(result)

#-- start of cleanup_header --------------

clean :: TrkValToolsCLIDDBclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(TrkValToolsCLIDDB.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

TrkValToolsCLIDDBclean ::
#-- end of cleanup_header ---------------
