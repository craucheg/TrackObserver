#-- start of make_header -----------------

#====================================
#  Document TrkValToolsConf
#
#   Generated Thu Apr 14 11:53:23 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_TrkValToolsConf_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_TrkValToolsConf_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_TrkValToolsConf

TrkValTools_tag = $(tag)

#cmt_local_tagfile_TrkValToolsConf = $(TrkValTools_tag)_TrkValToolsConf.make
cmt_local_tagfile_TrkValToolsConf = $(bin)$(TrkValTools_tag)_TrkValToolsConf.make

else

tags      = $(tag),$(CMTEXTRATAGS)

TrkValTools_tag = $(tag)

#cmt_local_tagfile_TrkValToolsConf = $(TrkValTools_tag).make
cmt_local_tagfile_TrkValToolsConf = $(bin)$(TrkValTools_tag).make

endif

include $(cmt_local_tagfile_TrkValToolsConf)
#-include $(cmt_local_tagfile_TrkValToolsConf)

ifdef cmt_TrkValToolsConf_has_target_tag

cmt_final_setup_TrkValToolsConf = $(bin)setup_TrkValToolsConf.make
cmt_dependencies_in_TrkValToolsConf = $(bin)dependencies_TrkValToolsConf.in
#cmt_final_setup_TrkValToolsConf = $(bin)TrkValTools_TrkValToolsConfsetup.make
cmt_local_TrkValToolsConf_makefile = $(bin)TrkValToolsConf.make

else

cmt_final_setup_TrkValToolsConf = $(bin)setup.make
cmt_dependencies_in_TrkValToolsConf = $(bin)dependencies.in
#cmt_final_setup_TrkValToolsConf = $(bin)TrkValToolssetup.make
cmt_local_TrkValToolsConf_makefile = $(bin)TrkValToolsConf.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)TrkValToolssetup.make

#TrkValToolsConf :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'TrkValToolsConf'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = TrkValToolsConf/
#TrkValToolsConf::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/genconfig_header
# Author: Wim Lavrijsen (WLavrijsen@lbl.gov)

# Use genconf.exe to create configurables python modules, then have the
# normal python install procedure take over.

.PHONY: TrkValToolsConf TrkValToolsConfclean

confpy  := TrkValToolsConf.py
conflib := $(bin)$(library_prefix)TrkValTools.$(shlibsuffix)
confdb  := TrkValTools.confdb
instdir := $(CMTINSTALLAREA)$(shared_install_subdir)/python/$(package)
product := $(instdir)/$(confpy)
initpy  := $(instdir)/__init__.py

ifdef GENCONF_ECHO
genconf_silent =
else
genconf_silent = $(silent)
endif

TrkValToolsConf :: TrkValToolsConfinstall

install :: TrkValToolsConfinstall

TrkValToolsConfinstall : /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation/TrkValTools/genConf/TrkValTools/$(confpy)
	@echo "Installing /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation/TrkValTools/genConf/TrkValTools in /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InstallArea/python" ; \
	 $(install_command) --exclude="*.py?" --exclude="__init__.py" --exclude="*.confdb" /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation/TrkValTools/genConf/TrkValTools /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InstallArea/python ; \

/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation/TrkValTools/genConf/TrkValTools/$(confpy) : $(conflib) /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation/TrkValTools/genConf/TrkValTools
	$(genconf_silent) $(genconfig_cmd)   -o /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation/TrkValTools/genConf/TrkValTools -p $(package) \
	  --configurable-module=GaudiKernel.Proxy \
	  --configurable-default-name=Configurable.DefaultName \
	  --configurable-algorithm=ConfigurableAlgorithm \
	  --configurable-algtool=ConfigurableAlgTool \
	  --configurable-auditor=ConfigurableAuditor \
          --configurable-service=ConfigurableService \
	  -i ../$(tag)/$(library_prefix)TrkValTools.$(shlibsuffix)

/afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation/TrkValTools/genConf/TrkValTools:
	@ if [ ! -d /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation/TrkValTools/genConf/TrkValTools ] ; then mkdir -p /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation/TrkValTools/genConf/TrkValTools ; fi ;

TrkValToolsConfclean :: TrkValToolsConfuninstall
	$(cleanup_silent) $(remove_command) /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation/TrkValTools/genConf/TrkValTools/$(confpy) /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation/TrkValTools/genConf/TrkValTools/$(confdb)

uninstall :: TrkValToolsConfuninstall

TrkValToolsConfuninstall ::
	@$(uninstall_command) /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/InstallArea/python
libTrkValTools_so_dependencies = ../x86_64-slc6-gcc49-opt/libTrkValTools.so
#-- start of cleanup_header --------------

clean :: TrkValToolsConfclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(TrkValToolsConf.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

TrkValToolsConfclean ::
#-- end of cleanup_header ---------------
