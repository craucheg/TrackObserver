#-- start of make_header -----------------

#====================================
#  Document TrkValToolsMergeComponentsList
#
#   Generated Thu Apr 14 11:53:24 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_TrkValToolsMergeComponentsList_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_TrkValToolsMergeComponentsList_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_TrkValToolsMergeComponentsList

TrkValTools_tag = $(tag)

#cmt_local_tagfile_TrkValToolsMergeComponentsList = $(TrkValTools_tag)_TrkValToolsMergeComponentsList.make
cmt_local_tagfile_TrkValToolsMergeComponentsList = $(bin)$(TrkValTools_tag)_TrkValToolsMergeComponentsList.make

else

tags      = $(tag),$(CMTEXTRATAGS)

TrkValTools_tag = $(tag)

#cmt_local_tagfile_TrkValToolsMergeComponentsList = $(TrkValTools_tag).make
cmt_local_tagfile_TrkValToolsMergeComponentsList = $(bin)$(TrkValTools_tag).make

endif

include $(cmt_local_tagfile_TrkValToolsMergeComponentsList)
#-include $(cmt_local_tagfile_TrkValToolsMergeComponentsList)

ifdef cmt_TrkValToolsMergeComponentsList_has_target_tag

cmt_final_setup_TrkValToolsMergeComponentsList = $(bin)setup_TrkValToolsMergeComponentsList.make
cmt_dependencies_in_TrkValToolsMergeComponentsList = $(bin)dependencies_TrkValToolsMergeComponentsList.in
#cmt_final_setup_TrkValToolsMergeComponentsList = $(bin)TrkValTools_TrkValToolsMergeComponentsListsetup.make
cmt_local_TrkValToolsMergeComponentsList_makefile = $(bin)TrkValToolsMergeComponentsList.make

else

cmt_final_setup_TrkValToolsMergeComponentsList = $(bin)setup.make
cmt_dependencies_in_TrkValToolsMergeComponentsList = $(bin)dependencies.in
#cmt_final_setup_TrkValToolsMergeComponentsList = $(bin)TrkValToolssetup.make
cmt_local_TrkValToolsMergeComponentsList_makefile = $(bin)TrkValToolsMergeComponentsList.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)TrkValToolssetup.make

#TrkValToolsMergeComponentsList :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'TrkValToolsMergeComponentsList'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = TrkValToolsMergeComponentsList/
#TrkValToolsMergeComponentsList::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_componentslist_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.components file into a single
# <project>.components file in the (lib) install area
# If no InstallArea is present the fragment is dummy


.PHONY: TrkValToolsMergeComponentsList TrkValToolsMergeComponentsListclean

# default is already '#'
#genmap_comment_char := "'#'"

componentsListRef    := ../$(tag)/TrkValTools.components

ifdef CMTINSTALLAREA
componentsListDir    := ${CMTINSTALLAREA}/$(tag)/lib
mergedComponentsList := $(componentsListDir)/$(project).components
stampComponentsList  := $(componentsListRef).stamp
else
componentsListDir    := ../$(tag)
mergedComponentsList :=
stampComponentsList  :=
endif

TrkValToolsMergeComponentsList :: $(stampComponentsList) $(mergedComponentsList)
	@:

.NOTPARALLEL : $(stampComponentsList) $(mergedComponentsList)

$(stampComponentsList) $(mergedComponentsList) :: $(componentsListRef)
	@echo "Running merge_componentslist  TrkValToolsMergeComponentsList"
	$(merge_componentslist_cmd) --do-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList)

TrkValToolsMergeComponentsListclean ::
	$(cleanup_silent) $(merge_componentslist_cmd) --un-merge \
         --input-file $(componentsListRef) --merged-file $(mergedComponentsList) ;
	$(cleanup_silent) $(remove_command) $(stampComponentsList)
libTrkValTools_so_dependencies = ../x86_64-slc6-gcc49-opt/libTrkValTools.so
#-- start of cleanup_header --------------

clean :: TrkValToolsMergeComponentsListclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(TrkValToolsMergeComponentsList.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

TrkValToolsMergeComponentsListclean ::
#-- end of cleanup_header ---------------
