#-- start of make_header -----------------

#====================================
#  Document TrkValToolsConfDbMerge
#
#   Generated Thu Apr 14 11:53:24 2016  by craucheg
#
#====================================

include ${CMTROOT}/src/Makefile.core

ifdef tag
CMTEXTRATAGS = $(tag)
else
tag       = $(CMTCONFIG)
endif

cmt_TrkValToolsConfDbMerge_has_no_target_tag = 1

#--------------------------------------------------------

ifdef cmt_TrkValToolsConfDbMerge_has_target_tag

tags      = $(tag),$(CMTEXTRATAGS),target_TrkValToolsConfDbMerge

TrkValTools_tag = $(tag)

#cmt_local_tagfile_TrkValToolsConfDbMerge = $(TrkValTools_tag)_TrkValToolsConfDbMerge.make
cmt_local_tagfile_TrkValToolsConfDbMerge = $(bin)$(TrkValTools_tag)_TrkValToolsConfDbMerge.make

else

tags      = $(tag),$(CMTEXTRATAGS)

TrkValTools_tag = $(tag)

#cmt_local_tagfile_TrkValToolsConfDbMerge = $(TrkValTools_tag).make
cmt_local_tagfile_TrkValToolsConfDbMerge = $(bin)$(TrkValTools_tag).make

endif

include $(cmt_local_tagfile_TrkValToolsConfDbMerge)
#-include $(cmt_local_tagfile_TrkValToolsConfDbMerge)

ifdef cmt_TrkValToolsConfDbMerge_has_target_tag

cmt_final_setup_TrkValToolsConfDbMerge = $(bin)setup_TrkValToolsConfDbMerge.make
cmt_dependencies_in_TrkValToolsConfDbMerge = $(bin)dependencies_TrkValToolsConfDbMerge.in
#cmt_final_setup_TrkValToolsConfDbMerge = $(bin)TrkValTools_TrkValToolsConfDbMergesetup.make
cmt_local_TrkValToolsConfDbMerge_makefile = $(bin)TrkValToolsConfDbMerge.make

else

cmt_final_setup_TrkValToolsConfDbMerge = $(bin)setup.make
cmt_dependencies_in_TrkValToolsConfDbMerge = $(bin)dependencies.in
#cmt_final_setup_TrkValToolsConfDbMerge = $(bin)TrkValToolssetup.make
cmt_local_TrkValToolsConfDbMerge_makefile = $(bin)TrkValToolsConfDbMerge.make

endif

#cmt_final_setup = $(bin)setup.make
#cmt_final_setup = $(bin)TrkValToolssetup.make

#TrkValToolsConfDbMerge :: ;

dirs ::
	@if test ! -r requirements ; then echo "No requirements file" ; fi; \
	  if test ! -d $(bin) ; then $(mkdir) -p $(bin) ; fi

javadirs ::
	@if test ! -d $(javabin) ; then $(mkdir) -p $(javabin) ; fi

srcdirs ::
	@if test ! -d $(src) ; then $(mkdir) -p $(src) ; fi

help ::
	$(echo) 'TrkValToolsConfDbMerge'

binobj = 
ifdef STRUCTURED_OUTPUT
binobj = TrkValToolsConfDbMerge/
#TrkValToolsConfDbMerge::
#	@if test ! -d $(bin)$(binobj) ; then $(mkdir) -p $(bin)$(binobj) ; fi
#	$(echo) "STRUCTURED_OUTPUT="$(bin)$(binobj)
endif

${CMTROOT}/src/Makefile.core : ;
ifdef use_requirements
$(use_requirements) : ;
endif

#-- end of make_header ------------------
# File: cmt/fragments/merge_genconfDb_header
# Author: Sebastien Binet (binet@cern.ch)

# Makefile fragment to merge a <library>.confdb file into a single
# <project>.confdb file in the (lib) install area

.PHONY: TrkValToolsConfDbMerge TrkValToolsConfDbMergeclean

# default is already '#'
#genconfDb_comment_char := "'#'"

instdir      := ${CMTINSTALLAREA}/$(tag)
confDbRef    := /afs/cern.ch/user/c/craucheg/atlas/TrackObserver/Tracking/TrkValidation/TrkValTools/genConf/TrkValTools/TrkValTools.confdb
stampConfDb  := $(confDbRef).stamp
mergedConfDb := $(instdir)/lib/$(project).confdb

TrkValToolsConfDbMerge :: $(stampConfDb) $(mergedConfDb)
	@:

.NOTPARALLEL : $(stampConfDb) $(mergedConfDb)

$(stampConfDb) $(mergedConfDb) :: $(confDbRef)
	@echo "Running merge_genconfDb  TrkValToolsConfDbMerge"
	$(merge_genconfDb_cmd) \
          --do-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)

TrkValToolsConfDbMergeclean ::
	$(cleanup_silent) $(merge_genconfDb_cmd) \
          --un-merge \
          --input-file $(confDbRef) \
          --merged-file $(mergedConfDb)	;
	$(cleanup_silent) $(remove_command) $(stampConfDb)
libTrkValTools_so_dependencies = ../x86_64-slc6-gcc49-opt/libTrkValTools.so
#-- start of cleanup_header --------------

clean :: TrkValToolsConfDbMergeclean ;
#	@cd .

ifndef PEDANTIC
.DEFAULT::
	$(echo) "(TrkValToolsConfDbMerge.make) $@: No rule for such target" >&2
else
.DEFAULT::
	$(error PEDANTIC: $@: No rule for such target)
endif

TrkValToolsConfDbMergeclean ::
#-- end of cleanup_header ---------------
