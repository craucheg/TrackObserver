#Thu Apr 14 11:53:23 2016"""Automatically generated. DO NOT EDIT please"""
from GaudiKernel.GaudiHandles import *
from GaudiKernel.Proxy.Configurable import *

class Trk__BasicValTrkParticleNtupleTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'BookNewNtuple' : False, # bool
    'NtupleFileName' : '/NTUPLES/FILE1', # str
    'NtupleDirectoryName' : 'FitterValidation', # str
    'NtupleTreeName' : 'Validation', # str
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'NtupleTreeName' : """ Name of the ntuple tree """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
    'NtupleDirectoryName' : """ dircetory name for ntuple tree """,
    'BookNewNtuple' : """ Create the ntuple tree or use existing one? """,
    'NtupleFileName' : """ Ntuple file handle """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__BasicValTrkParticleNtupleTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::BasicValTrkParticleNtupleTool'
  pass # class Trk__BasicValTrkParticleNtupleTool

class Trk__DAF_ValidationNtupleHelper( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'IgnoreMissingTrackCovarianceForPulls' : False, # bool
    'WriteMeasurementPositionOfROTs' : True, # bool
    'ResidualPullCalculatorTool' : PublicToolHandle('Trk::ResidualPullCalculator/ResidualPullCalculator'), # GaudiHandle
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'WriteMeasurementPositionOfROTs' : """ Write measurement positions? """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
    'IgnoreMissingTrackCovarianceForPulls' : """ Do not warn, if track states do not have covariance matries when calculating pulls """,
    'ResidualPullCalculatorTool' : """ Tool to calculate residuals and pulls """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__DAF_ValidationNtupleHelper, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::DAF_ValidationNtupleHelper'
  pass # class Trk__DAF_ValidationNtupleHelper

class Trk__DirectTrackNtupleWriterTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'ValidationNtupleTools' : PublicToolHandleArray([]), # GaudiHandleArray
    'ExtrapolatorTool' : PublicToolHandle('Trk::Extrapolator/InDetExtrapolator'), # GaudiHandle
    'NtupleFileName' : '/NTUPLES/FILE1', # str
    'NtupleDirectoryName' : 'FitterValidation', # str
    'NtupleTreeName' : 'Validation', # str
    'DoTruth' : True, # bool
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'NtupleTreeName' : """ Name of the ntuple tree """,
    'ValidationNtupleTools' : """ set of tools to write the validation ntuple """,
    'ExtrapolatorTool' : """ Extrapolator, eg for tracks without Pergiee parameters """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
    'NtupleDirectoryName' : """ dircetory name for ntuple tree """,
    'NtupleFileName' : """ Ntuple file handle """,
    'DoTruth' : """ Write truth data? """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__DirectTrackNtupleWriterTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::DirectTrackNtupleWriterTool'
  pass # class Trk__DirectTrackNtupleWriterTool

class Trk__EnergyLossMonitor( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'NtuplePath' : '/NTUPLES/FILE1/EnergyLossMonitor/SingleTrackEnergyLoss', # str
    'NtupleDescription' : 'Output of the Trk::EnergyLossMonitor AlgTool', # str
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__EnergyLossMonitor, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::EnergyLossMonitor'
  pass # class Trk__EnergyLossMonitor

class Trk__EventPropertyNtupleTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'FillTrtPhase' : False, # bool
    'FillTrigger' : True, # bool
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__EventPropertyNtupleTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::EventPropertyNtupleTool'
  pass # class Trk__EventPropertyNtupleTool

class Trk__EventToTrackLinkNtupleTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'CollectionType' : 'Trk::Track', # str
  }
  _propertyDocDct = { 
    'CollectionType' : """ Type of collection which associated with this instance [Trk::Track, Rec::TrackParticle, Rec::TrackParticle_Trig, InDetTrack_Trig] """,
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__EventToTrackLinkNtupleTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::EventToTrackLinkNtupleTool'
  pass # class Trk__EventToTrackLinkNtupleTool

class Trk__GenParticleJetFinder( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'yCut' : 0.00300000, # float
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__GenParticleJetFinder, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::GenParticleJetFinder'
  pass # class Trk__GenParticleJetFinder

class Trk__HitPositionNtupleHelper( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__HitPositionNtupleHelper, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::HitPositionNtupleHelper'
  pass # class Trk__HitPositionNtupleHelper

class Trk__InDetHaloSelector( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__InDetHaloSelector, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::InDetHaloSelector'
  pass # class Trk__InDetHaloSelector

class Trk__InDetPrimaryConversionSelector( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'MinPt' : 500.000, # float
    'MaxEta' : 2.50000, # float
    'MaxRStartAll' : 800.000, # float
    'MaxZStartAll' : 2000.00, # float
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'MaxRStartAll' : """ production vtx r for all """,
    'MaxZStartAll' : """ production vtx z for all """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__InDetPrimaryConversionSelector, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::InDetPrimaryConversionSelector'
  pass # class Trk__InDetPrimaryConversionSelector

class Trk__InDetReconstructableSelector( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'MinPt' : 1000.00, # float
    'MaxEta' : 3.00000, # float
    'SelectPrimariesOnly' : False, # bool
    'MaxRStartPrimary' : 25.0000, # float
    'MaxZStartPrimary' : 200.000, # float
    'MaxRStartAll' : 360.000, # float
    'MaxZStartAll' : 2000.00, # float
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'MaxZStartPrimary' : """ production vtx z for primaries """,
    'MaxRStartAll' : """ production vtx r for all """,
    'MaxZStartAll' : """ production vtx z for all """,
    'MaxRStartPrimary' : """ production vtx r for primaries """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
    'SelectPrimariesOnly' : """ flag to restrict to primaries or not """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__InDetReconstructableSelector, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::InDetReconstructableSelector'
  pass # class Trk__InDetReconstructableSelector

class Trk__JetTruthNtupleTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'NtupleFileName' : 'TRKVAL/Validation', # str
    'NtupleTreeName' : 'TruthJets', # str
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'NtupleTreeName' : """ Name of the ntuple tree """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
    'NtupleFileName' : """ Ntuple file handle """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__JetTruthNtupleTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::JetTruthNtupleTool'
  pass # class Trk__JetTruthNtupleTool

class Trk__MeasurementVectorNtupleTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'UseROTwithMaxAssgnProb' : True, # bool
    'IgnoreMissingTrackCovarianceForPulls' : False, # bool
    'UpdatorTool' : PublicToolHandle('Trk::KalmanUpdator/TrkKalmanUpdator'), # GaudiHandle
    'ResidualPullCalculatorTool' : PublicToolHandle('Trk::ResidualPullCalculator/ResidualPullCalculator'), # GaudiHandle
    'HoleSearchTool' : PublicToolHandle('InDet::InDetTrackHoleSearchTool/InDetHoleSearchTool'), # GaudiHandle
    'PixelNtupleHelperTools' : PrivateToolHandleArray([]), # GaudiHandleArray
    'SCTNtupleHelperTools' : PrivateToolHandleArray([]), # GaudiHandleArray
    'TRTNtupleHelperTools' : PrivateToolHandleArray([]), # GaudiHandleArray
    'MDTNtupleHelperTools' : PrivateToolHandleArray([]), # GaudiHandleArray
    'CSCNtupleHelperTools' : PrivateToolHandleArray([]), # GaudiHandleArray
    'RPCNtupleHelperTools' : PrivateToolHandleArray([]), # GaudiHandleArray
    'TGCNtupleHelperTools' : PrivateToolHandleArray([]), # GaudiHandleArray
    'GeneralNtupleHelperTools' : PrivateToolHandleArray([]), # GaudiHandleArray
    'DoTruth' : False, # bool
    'DoHoleSearch' : True, # bool
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UpdatorTool' : """ Measurement updator to calculate unbiased track states """,
    'RPCNtupleHelperTools' : """ List of RPC validation tools """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'TRTNtupleHelperTools' : """ List of TRT validation tools """,
    'MDTNtupleHelperTools' : """ List of MDT validation tools """,
    'DoHoleSearch' : """ Write hole data? """,
    'TGCNtupleHelperTools' : """ List of TGC validation tools """,
    'HoleSearchTool' : """ Tool to search for holes on track """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
    'IgnoreMissingTrackCovarianceForPulls' : """ Do not warn, if track states do not have covariance matries when calculating pulls """,
    'GeneralNtupleHelperTools' : """ List of detector independent validation tools """,
    'SCTNtupleHelperTools' : """ List of SCT validation tools """,
    'PixelNtupleHelperTools' : """ List of Pixel validation tools """,
    'ResidualPullCalculatorTool' : """ Tool to calculate residuals and pulls """,
    'UseROTwithMaxAssgnProb' : """ In case of CompetingRIOsOnTrack: Use the ROT with maximum assignment probabilty to calculate residuals, etc or use mean measurement? """,
    'CSCNtupleHelperTools' : """ List of CSC validation tools """,
    'DoTruth' : """ Write truth data? """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__MeasurementVectorNtupleTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::MeasurementVectorNtupleTool'
  pass # class Trk__MeasurementVectorNtupleTool

class Trk__PerigeeParametersNtupleTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'ExtrapolatorTool' : PublicToolHandle(''), # GaudiHandle
    'DoTruth' : True, # bool
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'ExtrapolatorTool' : """ Extrapolator, eg for tracks without Perigee """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
    'DoTruth' : """ Toggle if to Write truth data """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__PerigeeParametersNtupleTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::PerigeeParametersNtupleTool'
  pass # class Trk__PerigeeParametersNtupleTool

class Trk__PositionMomentumWriter( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'TreeName' : 'PositionMomentumWriter', # str
    'TreeFolder' : '/val/', # str
    'TreeDescription' : 'Position Momentum Writer', # str
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__PositionMomentumWriter, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::PositionMomentumWriter'
  pass # class Trk__PositionMomentumWriter

class Trk__PrimaryTruthClassifier( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'maxRStartPrimary' : 25.0000, # float
    'maxZStartPrimary' : 200.000, # float
    'minREndPrimary' : 400.000, # float
    'minZEndPrimary' : 2300.00, # float
    'maxRStartSecondary' : 360.000, # float
    'maxZStartSecondary' : 2000.00, # float
    'minREndSecondary' : 1000.00, # float
    'minZEndSecondary' : 3200.00, # float
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__PrimaryTruthClassifier, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::PrimaryTruthClassifier'
  pass # class Trk__PrimaryTruthClassifier

class Trk__ResidualValidationNtupleHelper( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'IgnoreMissingTrackCovariance' : False, # bool
    'ResidualPullCalculatorTool' : PublicToolHandle('Trk::ResidualPullCalculator/ResidualPullCalculator'), # GaudiHandle
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
    'ResidualPullCalculatorTool' : """ Tool to calculate residuals and pulls """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__ResidualValidationNtupleHelper, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::ResidualValidationNtupleHelper'
  pass # class Trk__ResidualValidationNtupleHelper

class Trk__TrackDiff( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'NtupleFileName' : '/NTUPLES', # str
    'NtupleDirectoryName' : 'TrackValidation', # str
    'NtupleTreeName' : 'TrackDiff', # str
    'CompareOutliers' : True, # bool
    'CompareAllTSoS' : False, # bool
    'WriteNtuple' : True, # bool
    'WriteCompetingROTdata' : False, # bool
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__TrackDiff, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::TrackDiff'
  pass # class Trk__TrackDiff

class Trk__TrackInformationNtupleTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__TrackInformationNtupleTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::TrackInformationNtupleTool'
  pass # class Trk__TrackInformationNtupleTool

class Trk__TrackPositionNtupleHelper( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__TrackPositionNtupleHelper, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::TrackPositionNtupleHelper'
  pass # class Trk__TrackPositionNtupleHelper

class Trk__TrackSummaryNtupleTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'AddParticleID' : True, # bool
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
    'AddParticleID' : """ Toggle if to also Write electron ID and dEdx data """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__TrackSummaryNtupleTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::TrackSummaryNtupleTool'
  pass # class Trk__TrackSummaryNtupleTool

class Trk__TrkObserverTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'ObservedTrackCollectionName' : 'ObservedTrackCollection', # str
    'ObservedTracksXaodName' : 'ObservedTracks', # str
    'TrackParticleCreator' : PublicToolHandle('Trk::TrackParticleCreatorTool/TrackParticleCreatorTool'), # GaudiHandle
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__TrkObserverTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::TrkObserverTool'
  pass # class Trk__TrkObserverTool

class Trk__TruthNtupleTool( ConfigurableAlgTool ) :
  __slots__ = { 
    'MonitorService' : 'MonitorSvc', # str
    'OutputLevel' : 7, # int
    'AuditTools' : False, # bool
    'AuditInitialize' : False, # bool
    'AuditStart' : False, # bool
    'AuditStop' : False, # bool
    'AuditFinalize' : False, # bool
    'EvtStore' : ServiceHandle('StoreGateSvc'), # GaudiHandle
    'DetStore' : ServiceHandle('StoreGateSvc/DetectorStore'), # GaudiHandle
    'UserStore' : ServiceHandle('UserDataSvc/UserDataSvc'), # GaudiHandle
    'NtupleFileName' : 'TRKVAL/Validation', # str
    'NtupleTreeName' : 'Truth', # str
  }
  _propertyDocDct = { 
    'DetStore' : """ Handle to a StoreGateSvc/DetectorStore instance: it will be used to retrieve data during the course of the job """,
    'UserStore' : """ Handle to a UserDataSvc/UserDataSvc instance: it will be used to retrieve user data during the course of the job """,
    'NtupleTreeName' : """ Name of the ntuple tree """,
    'EvtStore' : """ Handle to a StoreGateSvc instance: it will be used to retrieve data during the course of the job """,
    'NtupleFileName' : """ Ntuple file handle """,
  }
  def __init__(self, name = Configurable.DefaultName, **kwargs):
      super(Trk__TruthNtupleTool, self).__init__(name)
      for n,v in kwargs.items():
         setattr(self, n, v)
  def getDlls( self ):
      return 'TrkValTools'
  def getType( self ):
      return 'Trk::TruthNtupleTool'
  pass # class Trk__TruthNtupleTool
